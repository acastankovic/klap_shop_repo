# Slider

## Include

Javascript and CSS files need to be included into script

```html
<link rel="stylesheet" type="text/css" href="/plugins/slider/nc-slider.min.css" />
<script src="/plugins/slider/nc-slider.min.js"></script>
```

#### initialization

```javascript
$('#slicer').ncSlider();
```
