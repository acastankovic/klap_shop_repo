(function ($) {

  $.fn.ncSlider = function (params) {

    // IE doesn't recognize exists() fn from common.js
    if (typeof exists === 'undefined' || typeof exists !== 'function') {
      function exists(data) {
        if (typeof data == 'boolean') return true;
        return typeof data != 'undefined' && data != null && data != '';
      };
    }

    if (!exists(params)) params = {};

    if (!exists(params.slideElement)) params.slideElement = '.nc-slide';
    if (!exists(params.bulletElement)) params.bulletElement = '.nc-slider-bullet';
    if (!exists(params.btnPrevElement)) params.btnPrevElement = '.nc-slide-prev';
    if (!exists(params.btnNextElement)) params.btnNextElement = '.nc-slide-next';
    if (!exists(params.containerWidth)) params.containerWidth = 1280;
    if (!exists(params.changeSpeed)) params.changeSpeed = 3000;
    if (!exists(params.showBullets)) params.showBullets = true;
    if (!exists(params.showArrowsNavigation)) params.showArrowsNavigation = true;

    var $slider = $(this[0]);
    var $slide = $slider.find(params.slideElement);
    var $bullet = $slider.find(params.bulletElement);
    var $slidePrev = $slider.find(params.btnPrevElement);
    var $slideNext = $slider.find(params.btnNextElement);


    var totalSlides = $slide.length;
    var counter = 1;

    var CHANGE_TYPE = {
      NEXT: 1,
      PREV: 2,
      ABSOLUTE: 3
    };


    var intervalSlider;

    function startSlider() {
      intervalSlider = setInterval( function () {
        changeImage(CHANGE_TYPE.NEXT);
      }, params.changeSpeed);
    };


    function stopSlider() {
      clearInterval(intervalSlider);
    };


    function addSlideElementCounterClass() {

      if (totalSlides > 1) {

        $slide.each(function (index, elem) {

          var counter = parseInt(index) + 1;

          $(elem).addClass('nc-slide-' + counter);
        });
      }
    };


    function addBulletElementCounterAttribute() {

      if (totalSlides > 1) {

        $bullet.each( function (index, elem) {
          var counter = parseInt(index) + 1;

          $(elem).attr('data-counter', counter);
        });
      }
    };


    function hideSlide(counter) {
      $slider.find('.nc-slide-' + counter).hide();
    };


    function showSlide(counter) {
      $slider.find('.nc-slide-' + counter).show();
    };


    function changeImage(type, counterAbsolute) {

      hideSlide(counter);

      if (type == CHANGE_TYPE.NEXT) setIncreasedCounter();
      else if (type == CHANGE_TYPE.PREV) setDecreasedCounter();
      else if (typeof counterAbsolute != 'undefined' && type == CHANGE_TYPE.ABSOLUTE) setAbsoluteCounter(counterAbsolute);

      showSlide(counter);
      setActiveBullet();
    };


    function setIncreasedCounter() {
      counter++;
      if (counter > totalSlides) counter = 1;
    };


    function setDecreasedCounter() {
      counter--;
      if (counter < 1) counter = totalSlides;
    };


    function setAbsoluteCounter(counterAbsolute) {
      counter = counterAbsolute;
    };


    function setActiveBullet() {

      var bulletIndex = counter - 1;

      $bullet.removeClass('active');
      $bullet.eq(bulletIndex).addClass('active');
    };


    function handleBulletsInitialization() {

      if (params.showBullets && $bullet.length != 0) {
        addBulletElementCounterAttribute();
        setActiveBullet();
      } else {
        $bullet.remove();
      }
    };


    function handleArrowsNavigationInitialization() {

      if (!params.showArrowsNavigation && $slidePrev.length != 0) {
        $slidePrev.remove();
      }

      if (!params.showArrowsNavigation && $slideNext.length != 0) {
        $slideNext.remove();
      }
    };


    function initSlider() {
      addSlideElementCounterClass();
      handleBulletsInitialization();
      handleArrowsNavigationInitialization();
    };


    $bullet.click(function (event) {
      eventPreventDefault(event);

      stopSlider();
      var counterAbsolute = $(this).attr('data-counter');
      changeImage(CHANGE_TYPE.ABSOLUTE, counterAbsolute);
      startSlider();
    });


    $slideNext.on('click', function (event) {
      eventPreventDefault(event);

      stopSlider();
      changeImage(CHANGE_TYPE.NEXT);
      startSlider();
    });


    $slidePrev.on('click', function (event) {
      eventPreventDefault(event);

      stopSlider();
      changeImage(CHANGE_TYPE.PREV);
      startSlider();
    });


    initSlider();
    if (totalSlides > 1) {
      startSlider();
    }
    else {
      $slider.append('<p>Nema dovoljno sljadova</p>');
    }
  };

}(jQuery));