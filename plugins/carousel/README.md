# Carousel

## Include

Javascript and CSS files need to be included into script

```html
<link rel="stylesheet" type="text/css" href="/plugins/carousel/nc-carousel.min.css" />
<script src="/plugins/carousel/nc-carousel.min.js"></script>
```

## HTML

For HTML unordered list should be given id or class which is used for carousel initialization  
LI items should contain content needed for display


#### example:

```html
<ul id="carousel">
    <li><div>Item 1</div></li>
    <li><div>Item 2</div></li>
    <li><div>Item 3</div></li>
</ul>
```


## JQUERY

ALl params passed to carousel plugin are optional


#### initialization

```javascript
$('#carousel').ncCarousel(
    {
        carouselSpeed: '',         // optional, int (default 4000)
        stopOnHover: '',           // optional, boolean (default false)
        responsiveBreakPoint: '',  // optional, int (default 992) - if stop on hover is "on" it will not work under break point resolutions 
        showNavigation: '',        // optional, boolean (default true)
        showBullets: '',           // optional, boolean (default true)
        spinAutomatically: ''      // optional, boolean (default true)
    }
);
```