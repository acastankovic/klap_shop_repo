(function ($) {

  $.fn.ncCarousel = function (params) {

    // IE doesn't recognize exists() fn from common.js
    if (typeof exists === 'undefined' || typeof exists !== 'function') {
      function exists(data) {
        if (typeof data == 'boolean') return true;
        return typeof data != 'undefined' && data != null && data != '';
      };
    }

    if (!exists(params)) params = {};

    if (!exists(params.carouselSpeed)) params.carouselSpeed = 4000;
    if (!exists(params.stopOnHover)) params.stopOnHover = false;
    if (!exists(params.responsiveBreakPoint)) params.responsiveBreakPoint = 992;
    if (!exists(params.showNavigation)) params.showNavigation = true;
    if (!exists(params.showBullets)) params.showBullets = true;
    if (!exists(params.spinAutomatically)) params.spinAutomatically = true;

    var carouselsTotal = this.length;

    if (carouselsTotal < 1) {
      return;
    }

    // element classes
    var carouselElemClass = 'nc-carousel';
    var wrapperElemClass = 'nc-carousel-wrapper';
    var innerWrapperElemClass = 'nc-carousel-inner-wrapper';
    var itemElemClass = 'nc-carousel-item';
    var navigationElemClass = 'nc-carousel-navigation';
    var btnPrevElemClass = 'nc-carousel-item-prev';
    var btnNextElemClass = 'nc-carousel-item-next';
    var bulletsElemClass = 'nc-carousel-bullets';
    var bulletElemClass = 'nc-carousel-bullet';


    var carouselItemWidth;
    var carouselWidth;


    var animating = false;
    var carouselSpeed = params.carouselSpeed;
    var position = 0;
    var stoppedOnHover = params.stopOnHover;
    var itemsTotal;
    var spinAutomatically = params.spinAutomatically;

    // DOM elements
    var $carouselWrapper;
    var $innerWrapper;
    var $carousel;
    var $item;

    if (carouselsTotal === 1) {

      $carousel = $(this[0]);

      wrapCarousel();

      $carouselWrapper = $carousel.parents('.' + wrapperElemClass);
      $innerWrapper = $carousel.parents('.' + innerWrapperElemClass);
      $item = $carousel.find('li');
      itemsTotal = $item.length;

      initCarousel();
    } else if (carouselsTotal > 1) {

      for (var i = 0; i < carouselsTotal; i++) {

        $carousel = $(this[i]);

        wrapCarousel();

        $carouselWrapper = $carousel.parents('.' + wrapperElemClass);
        $innerWrapper = $carousel.parents('.' + innerWrapperElemClass);
        $item = $carousel.find('li');
        itemsTotal = $item.length;

        initCarousel();
      }
    }

    var $carouselPrev = $carouselWrapper.find('.' + btnPrevElemClass);
    var $carouselNext = $carouselWrapper.find('.' + btnNextElemClass);
    var $carouselBullet = $carouselWrapper.find('.' + bulletElemClass);

    if (params.showBullets) {
      setBulletPositions();
      setBulletActiveClass();
    }

    function initCarousel() {
      setElementClasses();

      if (params.showNavigation && itemsTotal > 1) {
        renderNavigation();
      }

      if (params.showBullets && itemsTotal > 1) {
        renderBullets();
      }

      setWidth();

      if (spinAutomatically) {
        startCarousel();
      }
    };


    function wrapCarousel() {
      $carousel.wrap('<div class="' + wrapperElemClass + '"><div class="' + innerWrapperElemClass + '"></div></div>');
    };


    function setElementClasses() {

      if (!$carouselWrapper.hasClass(wrapperElemClass)) {
        $carouselWrapper.addClass(wrapperElemClass);
      }

      if (!$carousel.hasClass(carouselElemClass)) {
        $carousel.addClass(carouselElemClass);
      }

      if (!$item.hasClass(itemElemClass)) {
        $item.addClass(itemElemClass);
      }
    };


    function renderNavigation() {

      $carouselWrapper.append('<button class="' + navigationElemClass + ' ' + btnPrevElemClass + '"></button><button class="' + navigationElemClass + ' ' + btnNextElemClass + '"></button>');
    };


    function renderBullets() {

      var html = '<div class="' + bulletsElemClass + '">';

      for (var i = 0; i < itemsTotal; i++) {

        html += '<button class="' + bulletElemClass + '"></button>';
      }

      html += '</div>';

      $carouselWrapper.append(html);
    };


    function setCarouselItemWidth() {
      var innerWidth = parseFloat($innerWrapper.width());
      $carousel.find('li').width(innerWidth);
    };


    function setCarouselWidth() {

      carouselItemWidth = $item.width();

      carouselWidth = Number.parseFloat(carouselItemWidth) * Number.parseInt(itemsTotal);

      $carousel.width(Math.ceil(carouselWidth));
    };


    function setWidth() {
      setCarouselItemWidth();
      setCarouselWidth();
    };


    function setBulletPositions() {

      if (exists($carouselBullet)) {

        if ($carouselBullet.length != 0) {

          $carouselBullet.each(function (index, element) {
            $(element).attr('data-position', index);
          });
        }
      }
    };


    function setBulletActiveClass() {

      if (exists($carouselBullet)) {

        if ($carouselBullet.length != 0) {
          $carouselBullet.removeClass('active');
          $carouselBullet.eq(position).addClass('active');
        }
      }
    };


    var carouselInterval;

    function startCarousel() {
      carouselInterval = setInterval(function () {

        if (animating) return;

        position++;

        changeSlide();
      }, carouselSpeed);
    };


    function stopCarousel() {
      clearInterval(carouselInterval);
    };


    function changeSlide() {

      if (spinAutomatically && !stoppedOnHover) {
        stopCarousel();
      }

      if (position < 0) {
        position = itemsTotal - 1;
      }

      if (position == itemsTotal) {
        position = 0;
      }

      animating = true;

      var animate = position * carouselItemWidth;

      setBulletActiveClass();

      $carousel.animate({marginLeft: -animate}, function () {

        animating = false;

        if (spinAutomatically && !stoppedOnHover) {
          startCarousel();
        }
      });
    };


    $carouselPrev.on('click', function () {

      if (animating) return;

      position--;

      changeSlide();
    });


    $carouselNext.on('click', function () {

      if (animating) return;

      position++;

      changeSlide();
    });


    $carouselBullet.on('click', function () {

      if (animating) return;

      position = $(this).attr('data-position');

      changeSlide();
    });


    if (spinAutomatically && params.stopOnHover) {

      $innerWrapper.on('mouseenter', function () {

        var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

        if (windowWidth > params.responsiveBreakPoint) {
          stopCarousel();
          stoppedOnHover = true;
        }
      })
      .on('mouseleave', function () {

          var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

          if (windowWidth > params.responsiveBreakPoint) {
            startCarousel();
            stoppedOnHover = false;
          }
        });
    }


    $(window).on('resize', function () {

      var animate = position * carouselItemWidth;

      $carousel.css({marginLeft: -animate});

      setWidth();
    });
  };

}(jQuery));