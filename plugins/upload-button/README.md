# Custom Upload Button

## Include

Javascript and CSS files need to be included into script

```html
<link rel="stylesheet" type="text/css" href="/plugins/upload-button/upload-button.min.css" />
<script src="/plugins/upload-button/upload-button.min.css"></script>
```

## HTML

For HTML input file tag should be given id or class which is used for plugin initialization

#### example:

```html
<input type="file" id="uploadButton" />
```

## JQUERY

ALl params passed to upload button plugin are optional


#### initialization

```javascript
$('#uploadButton').ncUploadButton({
  buttonTitle: ''  // optional, string (default "Browse")
});
```