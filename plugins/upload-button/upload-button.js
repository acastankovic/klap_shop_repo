(function ($) {

  $.fn.ncUploadButton = function (params) {

    // IE doesn't recognize exists() fn from common.js
    if (typeof exists === 'undefined' || typeof exists !== 'function') {
      function exists(data) {
        if (typeof data == 'boolean') return true;
        return typeof data != 'undefined' && data != null && data != '';
      };
    }

    if (!exists(params)) params = {};
    if (!exists(params.buttonTitle)) params.buttonTitle = 'Browse';

    var buttonsTotal = this.length;

    if (buttonsTotal < 1) {
      return;
    }

    var wrapperElemClass = 'nc-upload-button-wrapper';
    var buttonElemClass = 'nc-upload-file-button';
    var buttonTitle = params.buttonTitle;

    var $btn;
    if (buttonsTotal === 1) {

      $btn = $(this[0]);

      initUploadButton();
    }
    else {

      for (var i = 0; i < buttonsTotal; i++) {

        $btn = $(this[i]);

        initUploadButton();
      }
    }


    function createUploadButton() {

      $btn.wrap('<div class="' + wrapperElemClass + '"></div>');
      $btn.after('<button class="' + buttonElemClass + '" data-title="' + buttonTitle + '">' + buttonTitle + '</button>');
    };


    function initUploadButton() {

      createUploadButton();
    };


    function getFileNameFormUploadedFile(value) {

      var fileName = '';
      if (value) {
        var startIndex = (value.indexOf('\\') >= 0 ? value.lastIndexOf('\\') : value.lastIndexOf('/'));
        fileName = value.substring(startIndex);
        if (fileName.indexOf('\\') === 0 || fileName.indexOf('/') === 0) {
          fileName = fileName.substring(1);
        }
      }

      return fileName;
    };


    $(document).on('click', '.' + buttonElemClass, function (event) {
      eventPreventDefault(event);

      $(this).parent('.' + wrapperElemClass).find('input[type="file"]').trigger('click');
    });


    $btn.on('change', function (event) {

      var value = event.target.value;

      if (value != '') {
        var fileName = getFileNameFormUploadedFile(value);
        $(this).next('.' + buttonElemClass).addClass('uploaded');
        $(this).next('.' + buttonElemClass).text('uploaded: ' + fileName);
      }
      else {
        $(this).next('.' + buttonElemClass).removeClass('uploaded');
        $(this).next('.' + buttonElemClass).text(buttonTitle);
      }
    });
  };

}(jQuery));