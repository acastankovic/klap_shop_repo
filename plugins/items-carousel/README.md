# Items Carousel


## Include

Javascript and CSS files need to be included into script  

```html
<link rel="stylesheet" type="text/css" href="/plugins/items-carousel/nc-items-carousel.min.css" />
<script src="/plugins/items-carousel/nc-items-carousel.min.js"></script>
```

## HTML

For HTML unordered list should be given id or class which is used for carousel initialization  
LI items should contain content needed for display


#### example

```html
<ul id="itemCarousel">
    <li><div>Item 1</div></li>
    <li><div>Item 2</div></li>
    <li><div>Item 3</div></li>
</ul>
```


## JQUERY

ALl params passed to carousel plugin are optional


#### initialization

```javascript
$('#itemCarousel').ncItemsCarousel(
    {
        carouselSpeed: '',      // optional, int (default 5000 ms)
        itemsChangingSpeed: '', // optional, int (default 380 ms)
        showNavigation: '',     // optional, boolean (default true)
        itemsPerRow: '',        // optional, int (default 5)
        spinAutomatically: ''   // optional, boolean (default false)
    }
);
```