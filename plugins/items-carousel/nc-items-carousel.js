(function ($) {

  $.fn.ncItemsCarousel = function (params) {

    // IE doesn't recognize exists() fn from common.js
    if (typeof exists === 'undefined' || typeof exists !== 'function') {
      function exists(data) {
        if (typeof data == 'boolean') return true;
        return typeof data != 'undefined' && data != null && data != '';
      };
    }

    if (!exists(params)) params = {};

    if (!exists(params.carouselSpeed)) params.carouselSpeed = 5000;
    if (!exists(params.itemsChangingSpeed)) params.itemsChangingSpeed = 380;
    if (!exists(params.showNavigation)) params.showNavigation = true;
    if (!exists(params.itemsPerRow)) params.itemsPerRow = 5;
    if (!exists(params.spinAutomatically)) params.spinAutomatically = false;
    if (!exists(params.stopOnHover)) params.stopOnHover = true;

    var carouselsTotal = this.length;

    if (carouselsTotal < 1) {
      return;
    }

    // element classes
    var wrapperElemClass = 'nc-carousel-wrapper';
    var innerWrapperElemClass = 'nc-carousel-inner-wrapper';
    var carouselElemClass = 'nc-carousel';
    var itemElemClass = 'nc-carousel-item';
    var itemInnerWrapperElemClass = 'nc-item-inner-wrapper';
    var navigationElemClass = 'nc-carousel-navigation';
    var btnPrevElemClass = 'nc-carousel-item-prev';
    var btnNextElemClass = 'nc-carousel-item-next';


    var itemWidth = 0;
    var animating = false;
    var carouselSpeed = params.carouselSpeed;
    var itemsChangingSpeed = params.itemsChangingSpeed;
    var itemsTotal;
    var spinAutomatically = params.spinAutomatically;
    var stoppedOnHover = params.stopOnHover;

    // DOM elements
    var $carouselWrapper;
    var $innerWrapper;
    var $carousel;
    var $item;

    if (carouselsTotal === 1) {

      $carousel = $(this[0]);

      wrapCarousel();

      $carouselWrapper = $carousel.parents('.' + wrapperElemClass);
      $innerWrapper = $carousel.parents('.' + innerWrapperElemClass);
      $item = $carousel.find('li');
      itemsTotal = $item.length;

      initCarousel();
    } else if (carouselsTotal > 1) {

      for (var i = 0; i < carouselsTotal; i++) {

        $carousel = $(this[i]);

        wrapCarousel();

        $carouselWrapper = $carousel.parents('.' + wrapperElemClass);
        $innerWrapper = $carousel.parents('.' + innerWrapperElemClass);
        $item = $carousel.find('li');
        itemsTotal = $item.length;

        initCarousel();
      }
    }

    var $carouselPrev = $carouselWrapper.find('.' + btnPrevElemClass);
    var $carouselNext = $carouselWrapper.find('.' + btnNextElemClass);

    function initCarousel() {
      setElementClasses();
      wrapItemInnerContent();
      if (itemsTotal > 1) {
        renderNavigation();
      }
      setItem();
      setItemWidth();
      setCarouselWidth();

      if (spinAutomatically) {
        startItemsCarousel();
      }
    };


    function setElementClasses() {

      if (!$carouselWrapper.hasClass(wrapperElemClass)) {
        $carouselWrapper.addClass(wrapperElemClass);
      }

      if (!$carousel.hasClass(carouselElemClass)) {
        $carousel.addClass(carouselElemClass);
      }

      if (!$item.hasClass(itemElemClass)) {
        $item.addClass(itemElemClass);
      }
    };


    function wrapItemInnerContent() {
      $item.wrapInner('<div class="' + itemInnerWrapperElemClass + '"></div>');
    }


    function renderNavigation() {
      $carouselWrapper.append('<button class="' + navigationElemClass + ' ' + btnPrevElemClass + '"></button><button class="' + navigationElemClass + ' ' + btnNextElemClass + '"></button>');
    };


    function wrapCarousel() {
      $carousel.wrap('<div class="' + wrapperElemClass + '"><div class="' + innerWrapperElemClass + '"></div></div>');
    };


    function setItem() {
      $item = $carouselWrapper.find("." + itemElemClass);
    };


    function setItemsAmountPerRow() {

      var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

      if(windowWidth >= 768) {
        return params.itemsPerRow;
      } else if(windowWidth >= 480) {
        return 3;
      } else if(windowWidth < 480 && windowWidth >= 380) {
        return 2;
      } else if(windowWidth < 380) {
        return 1;
      }
    };


    function setItemWidth() {

      var itemsPerRow = setItemsAmountPerRow();

      var innerWidth = parseFloat($innerWrapper.width());

      itemWidth = innerWidth / itemsPerRow;

      $item.css({width: itemWidth});

      var marginLeft = parseFloat($item.css('margin-left'));
      var marginRight = parseFloat($item.css('margin-right'));

      if (typeof (marginLeft) === 'number' && isNaN(marginLeft)) marginLeft = 0;
      if (typeof (marginRight) === 'number' && isNaN(marginRight)) marginRight = 0;

      itemWidth = itemWidth + marginLeft + marginRight;
    };


    function setCarouselWidth() {
      var width = itemWidth * (itemsTotal + 1);
      $carousel.width(width);
    };


    function duplicateFirstItemToLastPosition(btnElement) {

      var $item = $carouselWrapper.find('.' + itemElemClass);
      if (exists(btnElement)) {
        $item = $(btnElement).parents('.' + wrapperElemClass).find('.' + itemElemClass);
      }
      var $firstItemCopy = $item.first().clone();
      var $lastItem = $item.last();
      $firstItemCopy.insertAfter($lastItem);
    };


    function duplicateLastItemToFirstPosition(btnElement) {

      var $item = $carouselWrapper.find('.' + itemElemClass);
      if (exists(btnElement)) {
        $item = $(btnElement).parents('.' + wrapperElemClass).find('.' + itemElemClass);
      }
      var $lastItemCopy = $item.last().clone();
      var $firstItem = $item.first();
      $lastItemCopy.insertBefore($firstItem);
    };


    function removeFirstItem(btnElement) {
      if (exists(btnElement)) {
        var $item = $(btnElement).parents('.' + wrapperElemClass).find('.' + itemElemClass);
      }
      $item.first().remove();
      setItem();
    };


    function removeLastItem(btnElement) {
      if (exists(btnElement)) {
        var $item = $(btnElement).parents('.' + wrapperElemClass).find('.' + itemElemClass);
      }
      $item.last().remove();
      setItem();
    };


    function displayNextItem(elem = null) {

      if (!animating) {

        animating = true;

        if (spinAutomatically && exists(elem) && !stoppedOnHover) {
          stopItemsCarousel();
          $carousel = $(elem).parents('.' + wrapperElemClass).find('.' + carouselElemClass);
        }

        duplicateFirstItemToLastPosition(elem);

        $carousel.animate({left: -itemWidth}, itemsChangingSpeed, function () {

          $(this).css({left: 0});
          removeFirstItem(this);
          animating = false;

          if (spinAutomatically && exists(elem) && !stoppedOnHover) {
            startItemsCarousel();
          }
        });
      }
    };

    function displayPrevItem(elem = null) {

      if (!animating) {

        animating = true;

        if (spinAutomatically && exists(elem) && !stoppedOnHover) {
          stopItemsCarousel();
        }

        var $carousel = $(elem).parents('.' + wrapperElemClass).find('.' + carouselElemClass);

        $carousel.css({left: -itemWidth});

        duplicateLastItemToFirstPosition(elem);

        $carousel.animate({left: 0}, itemsChangingSpeed, function () {

          removeLastItem(this);
          animating = false;

          if (spinAutomatically && exists(elem) && !stoppedOnHover) {
            startItemsCarousel();
          }
        });
      }
    };


    var itemsCarouselInterval;

    function startItemsCarousel() {
      itemsCarouselInterval = setInterval( function () {
        displayNextItem();
      }, carouselSpeed);
    };


    function stopItemsCarousel() {
      clearInterval(itemsCarouselInterval);
    };

    $carouselPrev.on('click', function (event) {
      eventPreventDefault(event);

      displayPrevItem(this);
    });


    $carouselNext.on('click', function (event) {
      eventPreventDefault(event);

      displayNextItem(this);
    });


    if (spinAutomatically && params.stopOnHover) {

      $innerWrapper.on('mouseenter', function () {

        var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

        if (windowWidth > 992) {
          stopItemsCarousel();
          stoppedOnHover = true;
        }
      })
      .on('mouseleave', function () {

        var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

        if (windowWidth > 992) {
          startItemsCarousel();
          stoppedOnHover = false;
        }
      });
    }


    $(window).resize(function () {
      setItemWidth();
      setCarouselWidth();
    });
  };

}(jQuery));