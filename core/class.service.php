<?php

class Service {

  protected static $instances;

  public static function Instance() {

    $class = get_called_class();

    if (!isset(self::$instances[$class])) {
      self::$instances[$class] = new $class;
    }
    return self::$instances[$class];
  }

  protected function setAlias($alias, $aliases = null, $id = null) {

    // if aliases do not exist in item's parent category - save alias
    if (!exists($aliases)) {
      return $alias;
    }

    $aliasExists = false;
    foreach ($aliases as $item) {

      if ((string)$alias === (string)$item->alias && (int)$id !== (int)$item->id) {

        $aliasExists = true;
      }
    }

    // if alias do not exist in parent category - save alias
    if (!$aliasExists) {
      return $alias;
    }

    // if alias exist in parent category - create new alias
    $aliasesArray = array();
    foreach ($aliases as $item) {
      array_push($aliasesArray, $item->alias);
    }

    $newAlias = $this->findAvailableAlias($aliasesArray, $alias);
    return $newAlias;
  }

  protected function findAvailableAlias($aliases, $alias) {

    if (in_array($alias, $aliases)) {
      $counter = 1;

      while (in_array(($alias . '-' . ++$counter), $aliases)) ;

      $alias .= '-' . $counter;
    }

    return $alias;
  }

  protected function setGallery($data) {

    $galleryArray = array();

    if (@exists($data->gallery_json)) {

      $gallery = json_decode($data->gallery_json);

      if (@exists($gallery)) {

        foreach ($gallery as $g) {

          $item = new stdClass();
          $item->type = $g->type;

          if ((string)$g->type === 'media') {

            if (@exists($g->id)) {
              $item->id = $g->id;
            }

            if (@exists($g->description)) {
              $item->description = $g->description;
            }

            if (@exists($g->file_name)) {
              $item->file_name = $g->file_name;
              $item->url = Conf::get('media_url') . '/' . $g->file_name;
              $item->thumb_url = Conf::get('media_thumbs_url') . '/' . $g->file_name;
            }
          } else if ((string)$g->type === 'youtube_code') {

            if (@exists($g->description)) {
              $item->description = $g->description;
            }

            if (@exists($g->code)) {
              $item->code = $g->code;
              $item->embed_url = 'https://www.youtube.com/embed/' . $g->code;
              $item->watch_url = 'https://www.youtube.com/watch?v=' . $g->code;
              $item->image_url = 'http://img.youtube.com/vi/' . $g->code . '/0.jpg';
            }
          }

          array_push($galleryArray, $item);
        }
      }
    }

    return $galleryArray;
  }

  protected function setItemByLanguageId($data = null) {

    $languages = Languages::getActive();

    $result = array();

    // create default items array
    foreach ($languages as $lang) {

      $langId = $lang->id;

      $result[$langId] = new stdClass();
      $result[$langId]->id = 0;
      $result[$langId]->lang_id = $lang->id;
    }


    // populate array with existing items from base
    if (@exists($data)) {

      foreach ($data as $item) {

        $langId = $item->lang_id;

        $result[$langId] = $item;
      }
    }

    return $result;
  }

  protected function setLanguageGroupId($item) {
    return @exists($item->lang_group_id) ? $item->lang_group_id : $item->id;
  }

  protected function setLanguageGroupIdParams($langGroupId = null, $params = null) {

    $data = array();
    $data['lang_group_id'] = $langGroupId;
    if (@exists($params['fetchWithUnpublished'])) $data['fetchWithUnpublished'] = $params['fetchWithUnpublished'];

    return $data;
  }

  protected function setItemWithLanguageGroupsResponse($results = null, $langGroupId = null) {

    $response = new stdClass();
    $response->items = $this->setItemByLanguageId($results);
    $response->langGroupId = $langGroupId;

    return $response;
  }

  protected function setLangGroupIdFromParentId($data) {

    if (@exists($data['parent_id'])) {
      $data['lang_group_id'] = $data['parent_id'];
      unset($data['parent_id']);
    }

    return $data;
  }

  protected function setLangGroupIdFromId($data) {

    if (@exists($data['id'])) {
      $data['lang_group_id'] = $data['id'];
      unset($data['id']);
    }

    return $data;
  }

  protected function adminLoggedIn() {
    $activeSession = Dispatcher::instance()->dispatch('users', 'authentication', 'getActiveSession', null);
    return @exists($activeSession) && (bool)$activeSession !== false;
  }

  protected function getLoggedInUserId() {
    $activeSession = Dispatcher::instance()->dispatch('users', 'authentication', 'getActiveSession', null);
    if (@exists($activeSession) && (bool)$activeSession !== false) {
      return $activeSession->user_id;
    }
    return null;
  }

  protected function setItemComments($item, $commentTypeId) {

    $item->comments = null;
    if (@exists($item->allow_comments) && (int)$item->allow_comments === 1) {
      $item->comments = Dispatcher::instance()->dispatch('content', 'comments', 'fetchByTypeIdAndTargetId', array('type_id' => $commentTypeId, 'target_id' => $item->id, 'fetchWithUnpublished' => true));
    }
  }
}

?>