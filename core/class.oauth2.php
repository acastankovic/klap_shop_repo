<?php

class NormacorePdo extends OAuth2\Storage\Pdo {
  public function __construct($connection, $config = array()) {
    parent::__construct($connection, $config);
    $this->config['user_table'] = 'users';
  }

  // use a secure hashing algorithm when storing passwords. Override this for your application
  protected function hashPassword($password) {
    $enc_hash = Conf::get('enc_hash');
    $enc_type = Conf::get('enc_type');
    return hash($enc_type, $password . $enc_hash);
  }
}

class OAuth2 {

  public $server;
  public $response;

  public function __construct() {
    $this->init();
  }

  public function init() {
    // $dsn is the Data Source Name for your database, for exmaple "mysql:dbname=my_oauth2_db;host=localhost"
    $dsn = 'mysql:dbname=' . Conf::get('db_name') . ';host=' . Conf::get('db_hostname');
    $username = Conf::get('db_username');
    $password = Conf::get('db_password');

    $storage = new NormacorePdo(array('dsn' => $dsn, 'username' => $username, 'password' => $password));

    // Pass a storage object or array of storage objects to the OAuth2 server class
    $config = array(
      'access_lifetime' => Conf::get('Oauth2_access_lifetime'),
      'always_issue_new_refresh_token' => true,
      'refresh_token_lifetime' => Conf::get('Oauth2_refresh_token_lifetime')
    );
    $this->server = new OAuth2\Server($storage, $config);

    // Add the "Client Credentials" grant type (it is the simplest of the grant types)
    // TODO: paremetrizovati sa Conf koji grant tipovi su uključeni
    $this->server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage, $config));
    $this->server->addGrantType(new OAuth2\GrantType\UserCredentials($storage, $config));
    $this->server->addGrantType(new OAuth2\GrantType\RefreshToken($storage, $config));

    // Add the "Authorization Code" grant type (this is where the oauth magic happens)
    $this->server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));
  }

  public function token() {
    $this->server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
  }

  public function protected() {
    if (!$this->server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
      $this->response = $this->server->getResponse();
      return false;
    }
    return true;
  }

}