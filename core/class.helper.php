<?php

class Helper {

  protected static $instances;

  public static function Instance() {

    $class = get_called_class();

    if (!@exists(self::$instances[$class])) {
      self::$instances[$class] = new $class;
    }
    return self::$instances[$class];
  }

  public static function validateRequiredParams($params, $requiredFields) {

    if(!is_array($params)) $params = (array) $params;

    $sameNumberOfParams = true;
    foreach($requiredFields as $key) {
      if(!array_key_exists($key, $params)) {
        $sameNumberOfParams = false;
      }
    }

    if(!$sameNumberOfParams) return false;

    $validated = true;
    foreach ($params as $key => $value) {

      foreach ($requiredFields as $field) {

        if($field == $key) {

          if($value == '') {

            $validated = false;
          }
        }
      }
    }

    return $validated;
  }
}
?>