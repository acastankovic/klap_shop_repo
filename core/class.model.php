<?php

class Model {

  protected static $instances;

  protected $dbh;
  protected $table;

  protected $tableDescription = array();
  protected $tableConstraints = array();

  protected $error;

  protected $useCache = false;
  protected $cachedData = array();

  public function __construct(PDO $dbh = null) {
    if ($dbh != null) $this->dbh = $dbh;
    else $this->dbh = DB::Connect();
  }

  public static function Instance() {

    $class = get_called_class();

    if (!isset(self::$instances[$class])) {
      self::$instances[$class] = new $class;
    }
    return self::$instances[$class];
  }

  public function setTable($table) {
    $this->table = $table;
  }

  public function getTable() {
    return $this->table;
  }

  public function getDBH() {
    return $this->dbh;
  }

  public function setError($error) {
    $this->error = $error;
  }

  public function getError() {
    return $this->error;
  }

  public function useCache($ind) {
    $this->useCache = $ind;
  }

  public function lastInsertId() {
    return $this->dbh->lastInsertId();
  }

  public function required($data, $requirements) {

    $ok = true;
    foreach ($requirements as $requirement) {
      $found = false;
      foreach ($data as $key => $item) {
        if ($key == $requirement) $found = true;
      }
      if (!$found) {
        $ok = false;
        $this->setError($requirement . ' is required');
        break;
      }
    }
    return $ok;
  }

  //load table or a row
  public function load($id = null) {

    if (isset($id)) {
      $stm = $this->execute('SELECT * FROM ' . $this->table . ' WHERE `id` = :id;', array('id' => $id));
      $this->data = $this->fetch($stm, PDO::FETCH_OBJ);
      return $this->data;
    } else {
      $stm = $this->execute('SELECT * FROM ' . $this->table . ';');
      $this->data = $this->fetchAll($stm, PDO::FETCH_OBJ);
      return $this->data;
    }
  }

  //load last insert row
  public function loadLastInsert() {
    return self::load($this->lastInsertId());
  }

  //load table using array filter and order
  public function loadFilter($filter, $sort = null) {

    $query = 'SELECT * FROM ' . $this->table . ' WHERE ';
    foreach ($filter as $key => $value) {
      $query .= ' ' . $key . ' = :' . $key . ' AND';
    }
    $query = rtrim($query, 'AND');

    if (isset($sort)) $query = $query . ' ORDER BY ' . $sort;

    return $this->exafeAll($query, $filter, PDO::FETCH_OBJ);
  }

  //load one row using array filter and order
  public function loadOneFilter($filter) {

    $query = 'SELECT * FROM ' . $this->table . ' WHERE ';
    foreach ($filter as $key => $value) {
      $query .= ' ' . $key . ' = :' . $key . ' AND';
    }
    $query = rtrim($query, 'AND');

    return $this->exafe($query, $filter, PDO::FETCH_OBJ);
  }

  //load table filter
  public function existFilter($filter) {

    try {
      $query = 'SELECT COUNT(*) AS `num` FROM ' . $this->table . ' WHERE ';
      foreach ($filter as $key => $value) {
        $query .= ' ' . $key . ' = :' . $key . ' AND';
      }
      $query = rtrim($query, 'AND');

      $stm = $this->dbh->prepare($query);
      $stm->execute($filter);
      $this->data = $stm->fetch();
      if ($this->data['num'] > 0) return true;
      else return false;
    } catch (PDOException $e) {//echo $e->getMessage();
      $this->HandleDBError($e);
      return false;
    }
  }

  public function deleteFilter($filter) {

    try {
      $query = 'DELETE FROM ' . $this->table . ' WHERE ';
      foreach ($filter as $key => $value) {
        $query .= ' ' . $key . ' = :' . $key . ' AND';
      }
      $query = rtrim($query, 'AND');

      $stm = $this->dbh->prepare($query);
      $stm->execute($filter);
      return true;
    } catch (PDOException $e) {
      echo $e->getMessage();
      $this->HandleDBError($e);
      return false;
    }
  }

  //init table description
  public function initTable($table = null) {

    if (isset($table)) $this->table = $table;

    try {

      $stm = $this->dbh->prepare('DESC `' . $this->table . '`');
      $stm->execute();
    } catch (Exception $e) {
      $this->HandleDBError($e);
    }

    $ii = 0;
    while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
      $this->tableDescription[$row['Field']]['type'] = $this->GetType($row['Type']);
      $this->tableDescription[$row['Field']]['name'] = $row['Field'];
      $this->tableDescription[$row['Field']]['null'] = $row['Null'];
      $this->tableDescription[$row['Field']]['key'] = $row['Key'];
      $this->tableDescription[$row['Field']]['def'] = $row['Default'];
      $this->tableDescription[$row['Field']]['extra'] = $row['Extra'];
      $this->tableDescription[$row['Field']]['password'] = false;
      $this->tableDescription[$row['Field']]['size'] = $this->GetSize($row['Type']);
      $ii++;
    }

    $this->LoadForeignKeyConstrains();
  }

  private function getType($type) {

    if (!strpos($type, '(')) {
      return $type;
    } else {
      return substr($type, 0, strpos($type, '('));
    }
  }

  private function getSize($type) {

    if (!strpos($type, '(')) {
      return null;
    } else {
      return substr($type, strpos($type, '(') + 1, strpos($type, ')') - strpos($type, '(') - 1);
    }
  }

  public function getTableDescription() {
    return $this->tableDescription;
  }

  //load foreign key dependencies
  public function loadForeignKeyConstrains() {

    try {
      $stm = $this->dbh->prepare('SELECT k.COLUMN_NAME, i.TABLE_NAME, i.CONSTRAINT_TYPE, i.CONSTRAINT_NAME, k.REFERENCED_TABLE_NAME, k.REFERENCED_COLUMN_NAME 
                                        FROM information_schema.TABLE_CONSTRAINTS i 
                                        LEFT JOIN information_schema.KEY_COLUMN_USAGE k ON i.CONSTRAINT_NAME = k.CONSTRAINT_NAME 
                                        WHERE i.CONSTRAINT_TYPE = "FOREIGN KEY" 
                                        AND i.TABLE_SCHEMA = DATABASE()
                                        AND i.TABLE_NAME = "' . $this->table . '"');
      $stm->execute();
    } catch (PDOException $e) {
      $this->HandleDBError($e);
    }


    $ii = 0;
    while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
      $this->tableConstraints[$row['CONSTRAINT_NAME']]['type'] = $row['CONSTRAINT_TYPE'];
      $this->tableConstraints[$row['CONSTRAINT_NAME']]['column'] = $row['COLUMN_NAME'];
      $this->tableConstraints[$row['CONSTRAINT_NAME']]['refernce_column'] = $row['REFERENCED_COLUMN_NAME'];
      $this->tableConstraints[$row['CONSTRAINT_NAME']]['refernce_table'] = $row['REFERENCED_TABLE_NAME'];
      $this->tableDescription[$row['COLUMN_NAME']]['fk_name'] = $row['CONSTRAINT_NAME'];
      $ii++;
    }
  }

  public function loadForeignKeyTables() {

    foreach ($this->tableConstraints as $key => $value) {

      try {
        $stm = $this->dbh->prepare('SELECT * FROM ' . $this->tableConstraints[$key]['refernce_table'] . ';');
        $stm->execute();
      } catch (PDOException $e) {
        $this->HandleDBError($e);
      }

      $this->tableConstraints[$key]['data'] = $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    return $this->tableConstraints;
  }

  //build insert query
  public function buildInsertQuery($data) {

    $fieldsString = '(';
    $valuesString = '(';

    foreach ($this->tableDescription as $key => $field) {

      if ($field['name'] == 'id') {
        //do nothing for id
      } else if ($field['name'] == 'cdate') {
        $fieldsString .= '`cdate`, ';
        $valuesString .= 'NOW(), ';
      } else if ($field['name'] == 'udate') {
        $fieldsString .= '`udate`, ';
        $valuesString .= 'NOW(), ';
      } else {
        //data exist for this field
        if (isset($data[$field['name']])) {
          $fieldsString .= '`' . $field['name'] . '`' . ', ';
          $valuesString .= ':' . $field['name'] . ', ';
        }
      }
    }

    $fieldsString = rtrim($fieldsString, ', ');
    $fieldsString .= ')';
    $valuesString = rtrim($valuesString, ', ');
    $valuesString .= ')';

    $insertQuery = 'INSERT INTO ' . $this->table . ' ' . $fieldsString . ' VALUES ' . $valuesString . ';';

    return $insertQuery;
  }

  //build update query
  public function buildUpdateQuery($data, $filter = null) {

    $fieldsString = '';
    if ($filter == null) $whereString = ' WHERE `id` = :id';
    else {
      $whereString = ' WHERE ';
      $first = true;
      foreach ($filter as $key) {
        if (!$first) $whereString .= ' AND ';
        $first = false;
        $whereString .= $key . ' =:' . $key;
      }
    }

    foreach ($this->tableDescription as $key => $field) {

      $skip = false;

      if (isset($filter)) {
        foreach ($filter as $filterKey) {
          if ($key == $filterKey) $skip = true;
        }
      }

      if ($skip) {
      } else if ($field['name'] == 'id') {
        //$whereString = 'WHERE `id` = :id';
      } else if ($field['name'] == 'cdate') {
        //$fieldsString .= '`cdate`, ';
        //$valuesString .= 'NOW(), ';
      } else if ($field['name'] == 'udate') {
        $fieldsString .= '`udate` = NOW(), ';
      } else {
        //data exist for this field
        if (isset($data[$field['name']])) {
          $fieldsString .= '`' . $field['name'] . '`' . '=:' . $field['name'] . ', ';
        }
      }
    }

    $fieldsString = rtrim($fieldsString, ', ');

    $updateQuery = 'UPDATE ' . $this->table . ' SET ' . $fieldsString . ' ' . $whereString . ';';

    return $updateQuery;
  }

  //build delete query
  public function buildDeleteQuery() {

    $whereString = ' WHERE `id` = :id';

    $deleteQuery = 'DELETE FROM ' . $this->table . $whereString . ';';

    return $deleteQuery;
  }

  //unset data not used
  public function unsetData($data) {

    foreach ($data as $key => $value) {

      if (!isset($this->tableDescription[$key])) {
        unset($data[$key]);
      } else if ($data[$key] === null) {
        unset($data[$key]);
      }
    }

    return $data;
  }

  //insert data
  public function insert($data) {

    if (is_object($data)) $data = (array)$data;

    if (count($this->tableDescription) == 0) $this->InitTable();

    $data = $this->unsetData($data);
    unset($data['id']);
    unset($data['cdate']);
    unset($data['udate']);
    $query = $this->buildInsertQuery($data);

    return $this->execute($query, $data);
  }

  //update data
  public function update($data, $filter = null) {

    if (is_object($data)) $data = (array)$data;

    if (count($this->tableDescription) == 0) $this->InitTable();

    $data = $this->unsetData($data);
    $query = $this->buildUpdateQuery($data, $filter);

    return $this->execute($query, $data);
  }

  //delete data by id
  public function delete($data) {

    $query = $this->buildDeleteQuery();

    if (gettype($data) !== 'array') {
      $id = $data;
      $data = array();
      $data['id'] = $id;
    }

    return $this->execute($query, array('id' => $data['id']));
  }

  public function LoadFull($id = null, $filter = null) {

    $joinCounter = 1;

    $selectFields = '';
    $leftJoins = '';
    foreach ($this->tableDescription as $key => $field) {

      $selectFields .= 'j.' . $key . ',';
      if (!isset($field['fk_name'])) {
        //$selectFields .= 'j.' . $key . ',';
      } else {
        foreach ($this->tableConstraints as $name => $data) {

          if ($name == $field['fk_name']) {
            $referenceTable = ' j' . $joinCounter;
            $leftJoins .= ' LEFT JOIN ' . $data['refernce_table'] . $referenceTable . ' ON j.' . $data['column'] . ' = ' . $referenceTable . '.' . $data['refernce_column'];
            $selectFields .= $referenceTable . '.name AS ' . $data['refernce_table'] . '_name' . ',';
          }

          $joinCounter++;
        }
      }
    }

    $selectFields = rtrim($selectFields, ",");
    $selectQuery = 'SELECT ' . $selectFields . ' from ' . $this->table . ' j ' . $leftJoins;

    //echo $selectQuery;
    if (isset($id)) {
      $stm = $this->execute($selectQuery, array('id' => $id));
      $this->data = $this->fetch($stm, PDO::FETCH_OBJ);
      return $this->data;
    } else {
      $stm = $this->execute($selectQuery);
      $this->data = $this->fetchAll($stm, PDO::FETCH_OBJ);
      return $this->data;
    }
  }

  //handle error
  protected function handleDBError(PDOException $e) {

    Logger::putError($e);
  }

  public function getData() {

    return $this->data;
  }

  public function getDataArray() {
    if (gettype($this->data) === 'object') return $this->data->fetchAll(PDO::FETCH_ASSOC);
  }

  public function setOrder($ids) {

    $rang = 1;
    foreach ($ids as $id) {

      try {
        $stm = $this->dbh->prepare('UPDATE ' . $this->table . ' SET `rang` = :rang WHERE `id` = :id');
        $stm->execute(array('rang' => $rang, 'id' => $id));
        $this->data = $stm;
      } catch (PDOException $e) {
        //echo $e->getMessage();
        $this->handleDBError($e);
        return false;
      }

      $rang++;
    }

    return true;
  }

  public function execute($sql, $data = null) {
    try {

      if (Conf::get('debug')) Logger::putTrace($sql);
      $stm = $this->dbh->prepare($sql);
      $stm->execute($data);
      return $stm;
    } catch (PDOException $e) {
      $this->HandleDBError($e);
      return false;
    }
  }

  public function fetchAll($stm, $type = null) {

    try {

      if (!isset($type)) $type = PDO::FETCH_OBJ;

      if (is_object($stm)) return $stm->fetchAll($type);
      else Logger::putError('Invalid query result');
    } catch (Exception $e) {
      $this->HandleDBError($e);
      return false;
    }
  }

  public function fetch($stm, $type = null) {

    try {

      if (!isset($type)) $type = PDO::FETCH_OBJ;

      if (is_object($stm)) return $stm->fetch($type);
      else Logger::putError('Invalid query result');
    } catch (Exception $e) {
      $this->HandleDBError($e);
      return false;
    }
  }

  //execute and fetch
  public function exafe($sql, $data = null, $type = null) {
    if (!isset($type)) $type = PDO::FETCH_OBJ;
    $stm = $this->execute($sql, $data);
    return $this->fetch($stm, $type);
  }

  //execute and fetch all
  public function exafeAll($sql, $data = null, $type = null) {
    if (!isset($type)) $type = PDO::FETCH_OBJ;
    $stm = $this->execute($sql, $data);
    return $this->fetchAll($stm, $type);
  }

  protected function setLangId($data = null) {

    return @exists($data) && @exists($data['lang_id']) ? $data['lang_id'] : Trans::getLanguageId();
  }

  protected function fetchWithUnpublished($data) {

    return @exists($data) && @exists($data['fetchWithUnpublished']) && $data['fetchWithUnpublished'];
  }

  protected function setOrderByString($string, $alias = null) {

    $orderByArray = json_decode($string);

    if (@exists($orderByArray)) {

      $orderByString = 'ORDER BY';

      if (@exists($alias)) {
        $alias = trim($alias, '`');
        $alias = '`' . $alias . '`.';
      } else {
        $alias = '';
      }

      foreach ($orderByArray as $field => $direction) {

        $orderByString .= ' ' . $alias . '`' . $field . '`';

        if (@exists($direction)) {
          $orderByString .= ' ' . $direction;
        }

        $orderByString .= ', ';
      }
    }

    return ' ' . trim($orderByString, ', ');
  }

  protected function getLimitString($data) {

    if(@exists($data) && @exists($data['limit'])) {
      return ' LIMIT ' . $data['limit'];
    }
    return '';
  }

}

?>