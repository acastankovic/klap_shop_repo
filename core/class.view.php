<?php


class View {

  protected static $instances;

  public static $weekDays = array('1' => 'Monday', '2' => 'Thusday', '3' => 'Wendsday', '4' => 'Thursday', '5' => 'Friday', '6' => 'Saturday', '7' => 'Sunday');

  protected $tableDescription = array();
  protected $table;
  protected $controller;
  protected $inputForm = array();

  protected $content = '';

  protected $respondType;
  protected $moduleName = null;
  protected $isRootModule = false;

  function __construct() {
  }

  public static function Instance() {

    $class = get_called_class();

    if (!isset(self::$instances[$class])) {
      self::$instances[$class] = new $class;
    }
    return self::$instances[$class];
  }

  public function setRespondType($type) {
    $this->respondType = $type;
  }

  public function setController($controller) {
    $this->controller = $controller;
  }

  public function initController($controller) {
    $this->setController($controller);
    $this->setRespondType($controller->getRequest()->getRespondType());
    $this->setModuleName($controller->getModuleName());
    $this->setIsRootModule($controller->isRootModule());
  }

  public function respond($data, $template = null, $respondType = null) {

    if (isset($respondType)) $this->respondType = $respondType;

    if ($this->respondType == Request::JSON_REQUEST) {

      header('Content-Type: application/json');

      if (@exists(Conf::get('access_control_allowed_origins')) && Conf::get('access_control_allowed_origins') != false) {

        $headers = getallheaders();

        if (@exists($headers['Origin'])) {

          if (in_array($headers['Origin'], Conf::get('access_control_allowed_origins'))) {

            header('Access-Control-Allow-Origin: ' . $headers['Origin']);
          }
        }
      }

      $responseData = new stdClass();
      $responseData->success = true;
      $responseData->data = $data;
      echo json_encode($responseData);
    } else if ($this->respondType == Request::HTML_REQUEST) {
      $this->loadTemplate($template, $data);
      $this->displayContent();
    }
  }

  public function respondError($error, $template = null, $data = null) {

    if ($this->respondType == Request::JSON_REQUEST) {
      header('Content-Type: application/json');
      $responseData = new stdClass();
      $responseData->success = false;
      $responseData->error = $error;
      echo json_encode($responseData);
    } else if ($this->respondType == Request::HTML_REQUEST) {
      $this->loadTemplate($template, $data);
      $this->displayContent();
    }
  }

  //load and display template
  public function displayTemplate($template, $data = null) {

    $fileName = $this->getRootLocation() . '/templates/' . $template;
    include $fileName;
  }

  //loads template called by $controller into
  public function loadTemplate($template, $data = null) {

    $templateName = $this->getRootLocation() . '/templates/' . $template;

    try {
      ob_start();
      if (file_exists($templateName)) {
        require $templateName;
      } else {
        throw new Exception('Template does not exist: ' . $templateName);
      }
      $this->content = ob_get_clean();
    } catch (Exception $e) {
      Logger::putError($e);
    }
    return $this->content;
  }

  public function displayContent() {
    echo $this->content;
  }

  public function getContent() {
    return $this->content;
  }

  public function setModuleName($name) {
    $this->moduleName = $name;
  }

  public function setIsRootModule($value) {
    $this->isRootModule = $value;
  }

  protected function getRootLocation() {
    if (isset($this->moduleName)) {
      if ($this->isRootModule) return Conf::get('root') . '/app';
      else return Conf::get('root') . '/modules/' . $this->moduleName;
    }
    return Conf::get('root');
  }

  //simple combo, value=id, option=name
  public static function displayCombo($table, $data, $selected_id, $id, $name, $class = null, $option_class = null, $value_name = 'name') {

    echo '<select id="' . $id . '" name="' . $name . '"';
    if (isset($class)) echo " class='$class' ";
    echo '>';
    while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
      echo '<option value="' . $row['id'] . '"';
      if (isset($option_class)) echo ' class="' . $row[$option_class] . '"';
      if ($row['id'] == $selected_id) echo ' selected ';
      echo '>' . $row[$value_name] . '</option>';
    }
    echo '</select>';
  }

  //display combo numbers
  public static function displayComboNumbers($start, $end, $step, $selected_id, $id, $name, $class = null) {

    echo '<select id="' . $id . '" name="' . $name . '"';
    if (isset($class)) echo ' class="' . $class . '"';
    echo '>';
    for ($ii = $start; $ii <= $end; $ii += $step) {
      echo '<option value="' . $ii . '"';
      if ($ii == $selected_id) echo ' selected ';
      echo '>' . $ii . '</option>';
    }
    echo '</select>';
  }

  //display days in the week
  public static function displayComboWeekDays($selected_id, $id, $name, $class = null) {

    echo '<select id="' . $id . '" name="' . $name . '"';
    if (isset($class)) echo ' class="' . $class . '"';
    echo '>';
    for ($ii = 1; $ii <= 7; $ii++) {
      echo '<option value="' . $ii . '"';
      if ($ii == $selected_id) echo ' selected ';
      echo '>' . self::$weekDays[$ii] . '</option>';
    }
    echo '</select>';
  }

  //set description
  public function setTableDescription($description) {
    $this->tableDescription = $description;
  }

  //table name
  public function setTable($table) {
    $this->table = $table;
  }

  //insert form builder
  public function buildInsertForm() {

    foreach ($this->tableDescription as $key => $field) {

      if ($field['type'] == 'varchar') {

        $this->inputForm[$field['name']] = '<input type="text" name="' . $field['name'] . '" maxlength="' . $field['size'] . '"/>';

        return $this->inputForm[$field['name']];
      }
    }
  }
}

?>