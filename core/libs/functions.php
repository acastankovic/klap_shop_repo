<?php


if (!function_exists('getallheaders')) {

  function getallheaders() {

    if (!is_array($_SERVER)) {
      return array();
    }

    $headers = array();
    foreach ($_SERVER as $name => $value) {
      if (substr($name, 0, 5) == 'HTTP_') {
        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
      }
    }
    return $headers;
  }
}


//    function clientIP () {
//
//        $ip = '';
//
//        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
//            $ip = $_SERVER['HTTP_CLIENT_IP'];
//        }
//        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
//            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
//        }
//        else {
//            $ip = $_SERVER['REMOTE_ADDR'];
//        }
//
//        return $ip;
//    }


function clientIP() {
  $ipaddress = '';
  if (getenv('HTTP_CLIENT_IP'))
    $ipaddress = getenv('HTTP_CLIENT_IP');
  else if (getenv('HTTP_X_FORWARDED_FOR'))
    $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
  else if (getenv('HTTP_X_FORWARDED'))
    $ipaddress = getenv('HTTP_X_FORWARDED');
  else if (getenv('HTTP_FORWARDED_FOR'))
    $ipaddress = getenv('HTTP_FORWARDED_FOR');
  else if (getenv('HTTP_FORWARDED'))
    $ipaddress = getenv('HTTP_FORWARDED');
  else if (getenv('REMOTE_ADDR'))
    $ipaddress = getenv('REMOTE_ADDR');
  else
    $ipaddress = 'UNKNOWN';
  return $ipaddress;
}


function startsWith($haystack, $needle) {
  // search backwards starting from haystack length characters from the end
  return $needle === '' || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}


function endsWith($haystack, $needle) {
  // search forward starting from end minus needle length characters
  return $needle === '' || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}


function formTree($items, $rootId = 0) {

  if (!isset($rootId)) $rootId = 0;

  $hash = array();
  foreach ($items as $object) {

    if (is_object($object)) $object = (array)$object;

    if (isset($object['title'])) $object['name'] = $object['title'];

    $object['text'] = $object['name'];
    $object['icon'] = 'fa fa-folder c-primary';
    $object['state'] = false;

    $hash[$object['id']] = $object;
  }

  $tree = array();

  // build tree from hash
  foreach ($hash as $id => &$node) {

    if (isset($node['parent_id']) && $parent = $node['parent_id']) $hash[$parent]['children'][] =& $node;
    else $tree[] =& $node;
  }
  unset($node, $hash);

  $result = null;
  $rootItem['id'] = 0;
  $rootItem['children'] = $tree;

  if ($rootId == 0) $result = $tree;
  else {
    $result = findInTree($rootId, $rootItem);
    $result = array($result);
  }

  return $result;
}


function findInTree($id, $item) {

  if ((isset($item['id']) && (int)$item['id'] === (int)$id) || (isset($item['lang_group_id']) && (int)$item['lang_group_id'] === (int)$id)) return $item;
  else {
    if (isset($item['children'])) {
      foreach ($item['children'] as $child) {
        $res = findInTree($id, $child);
        if ($res) return $res;
      }
    }
  }
  return null;
}


function filterUrl($title) {

  $title = trim($title);
  $filter1 = '/[^\-\s\pN\pL]+/u'; // only letters, numbers, spaces, hypens
  $filter2 = '/[\-\s]+/';         // remove spaces and duplicate hypens
  $alias = preg_replace($filter1, '', mb_strtolower($title, 'UTF-8'));
  $alias = preg_replace($filter2, '-', $alias);
  $alias = trim($alias, '-');
  $find = array('ž', 'ć', 'č', 'š', 'đ', 'ä', 'ü');
  $replace = array('z', 'c', 'c', 's', 'd', 'a', 'u');
  $alias = str_replace($find, $replace, $alias);

  return $alias;
}


function truncateString($string, $max = 255) {
  if (mb_strlen($string, 'utf-8') >= $max) {
    $string = mb_substr(strip_tags($string), 0, $max - 5, 'utf-8') . '...';
  }
  return $string;
}


function exists($data) {

  if (!isset($data)) return false;
  if (is_string($data) && $data === '') return false;
  if (is_object($data)) $data = (array)$data;
  if (is_array($data) && empty($data)) return false;
  return true;
}


function formatPrice($price) {

  $alias = (string)Trans::getLanguageAlias();

  if ($alias === 'sr' || $alias === 'ср') {
    return number_format($price, 2, ',', '.');
  }
  else {
    return number_format($price, 2, '.', ',');
  }
}


function trimFields($data) {

  $keys = array_keys($data);

  for ($i = 0; $i < count($keys); $i++) {
    $data[$keys[$i]] = trim($data[$keys[$i]]);
  }

  return $data;
}


function unsetEmptyFields($data) {

  $keys = array_keys($data);

  for ($i = 0; $i < count($keys); $i++) {

    if ($data[$keys[$i]] == '') {
      unset($data[$keys[$i]]);
    }
  }

  return $data;
}


function dateFormat($date, $format = null) {
  if (!isset($format)) $format = 'd.m.Y.';
  return date_format(date_create($date), $format);
}


function sessionStart() {
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }
}


function validateEmail($email) {
  return filter_var($email, FILTER_VALIDATE_EMAIL);
}


function setStringValue($value) {
  return isset($value) ? htmlentities($value) : '';
}


function currentUrl() {

  $uri = ltrim(urldecode($_SERVER['REQUEST_URI']), '/');

  $base = Conf::get('base');

  if (isset($base) && (string)$base !== '') {
    if (strpos($uri, Conf::get('base')) !== false) {
      $uri = str_replace(Conf::get('base'), '', $uri);
    }
  }

  $url = Conf::get('url') . $uri;

  return $url;
}


function parseLink($link) {

  return (substr($link, 0, 7) === 'http://') || (substr($link, 0, 8) === 'https://') ? $link : Conf::get('url') . '/' . $link;
}


function exportFile($data, $heading = null, $preHeading = null) {

  if (@exists($preHeading)) {
    echo $preHeading . "\n";
  }

  if (@exists($heading)) {
    echo implode("\t", $heading) . "\n";
  }

  if (@exists($data)) {
    foreach ($data as $row) {
      echo implode("\t", array_values($row)) . "\n";
    }
  }
  else {
    echo 'No data available in table';
  }
  exit;
}

function IEBrowser() {
  return preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT']) || (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false);
}

?>