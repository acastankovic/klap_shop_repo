<?php


//all traffic should be redirected to index.php

class Dispatcher {

  protected $path;
  protected $modules = array();
  private static $instance;


  //singleton construct
  private function __construct() {

  }

  public static function instance() {

    if (!isset(self::$instance)) {
      $c = __CLASS__;
      self::$instance = new $c;
    }

    return self::$instance;
  }

  //force dispatch
  public function dispatch($moduleName, $controller, $action, $params, $respondType = null) {

    $module = $this->findModule($moduleName);
    $appName = $moduleName . 'Application';

    if (class_exists($appName)) {
      $app = new $appName;
      $app->setAppCall(true);
      $app->init($controller, $action, $params, null);
      $app->setModule($module);
      //force respond
      if (isset($respondType)) {
        $app->setRespondType($respondType);
      }
      $result = $app->runAction();
    } else {
      Logger::putError('Module not found: ' . $moduleName);
      die('Module "' . $moduleName . '" has not been defined.');
    }
    return $result;
  }

  public function dispatchUri() {

    $this->parseUri();
    $this->loadModules();
    $this->findAndRun();
  }

  protected function setPath($path) {
    $this->path = $path;
  }

  protected function parseUri() {

    $path = trim(parse_url(urldecode($_SERVER['REQUEST_URI']), PHP_URL_PATH), '/');

    if (Conf::get('base') !== '') {

      if (strpos($path, Conf::get('base')) === 0) {
        $path = substr($path, strlen(Conf::get('base')));
      }
    }
    $path = ltrim($path, '/');
    $path = rtrim($path, '/');

    $this->setPath($path);
  }

  protected function loadModules() {

    //root module, current application
    $this->modules = Conf::get('modules');
  }

  protected function findAndRun() {

    $actionFound = false;
    $app = null;
    $rootApp = null;
    //find action in modules
    foreach ($this->modules as $module) {

      $appName = $module->alias . 'Application';

      if (class_exists($appName)) {
        $app = new $appName;
        $path = $this->path;
        $app->setModule($module);

        if (!$module->root) {
          if (strpos($path, $module->alias) === 0) {
            $path = substr($path, strlen($module->alias));

            $wrongModule = false;
            if (strlen($path) > 0) {
              if ($path[0] == "/") $path = ltrim($path, "/");
              else $wrongModule = true;
            }
            if (!$wrongModule) $actionFound = $app->findAndInitAction($path);
          }
        } else {
          $rootApp = $app;
          $actionFound = $app->findAndInitAction($path);
        }

        if ($actionFound) break;
      } else {
        Logger::putError('Module not found: ' . $module->alias);
        die('Module "' . $module->alias . '" has not been defined.');
      }
    }

    if (!$actionFound) {

      $actionFound = $rootApp->findAndInitDefaultAction($path);
      if (!$actionFound) {
        Logger::putError('Invalid route: ' . $this->path);
        die('Invalid route: ' . $this->path);
      } else $rootApp->runAction();
    } else {
      //TODO: UAC check here
      //TODO: set request type
      $app->runAction();
    }
  }

  public function findModule($moduleName) {

    foreach ($this->modules as $module) {
      if ($module->alias == $moduleName) return $module;
    }

    Logger::putError('Module not found: ' . $moduleName);
    return null;
  }
}

?>