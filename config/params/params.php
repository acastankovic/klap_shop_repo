<?php


  Conf::set('base', ''); // project directory name (on production it should be empty string)


  //* Application parameters *//

  Conf::set('api', 'api/v1');
  Conf::set('site_name', 'Klapp shop');
  Conf::set('session_prefix', 'normacore_shop');


  // URL constants are stored in set_url_params.php
  if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
    require_once(str_replace('\config\params', '', dirname(__FILE__)) . '\core\libs\set_url_params.php'); // Windows
  } else {
    require_once(str_replace('/config/params', '', dirname(__FILE__)) . '/core/libs/set_url_params.php');
  }


  //* Database parameters *//

  Conf::set('db_hostname', 'webdev.normasoft.net');
  Conf::set('db_username', 'root');
  Conf::set('db_password', 'pr3ko7po8bg');
  Conf::set('db_name', 'klapp_shop');


  //* Meta parameters *//

  Conf::set('meta_tags', array(
    'title' => Conf::get('site_name'),
    'description' => '',
    'keywords' => '',
    'og' => array(
      'title' => Conf::get('site_name'),
      'image' => Conf::get('url') . '/css/img/logo.png',
      'width' => 1200,
      'height' => 630,
      'url' => Conf::get('url'),
      'site_name' => 'normacore-shop.com'
    )
  ));


  //* Email parameters *//

  Conf::set('mail_from_address', '');
  Conf::set('mail_from_name', 'Normacore');
  Conf::set('mail_to_address', '');    // recipient address
  Conf::set('mail_attachment', null);
  Conf::set('mail_uploaded_attachment', null);
  Conf::set('mail_cc', null);
  Conf::set('mail_bcc', null);
  Conf::set('mail_reply_to_address', '');
  Conf::set('mail_recipient_name', null);


  //* Pagination parameters *//

  Conf::set('pagination_exists', array(
    'articles' => false,
    'categories' => false,
    'products' => false
  ));

  Conf::set('items_per_page', array(
    'site_articles' => 8,
    'site_categories' => 10,
    'site_products' => 12,
    'admin_modal_images' => 30,
    'admin_media' => 10
  ));


  //* Categories parameters *//
  Conf::set('shop_category_id', 1);


  //* Articles parameters *//


  //* Menus parameters *//

  Conf::set('main_menu_id', 1);


  //* Slider parameters *//

  Conf::set('home_slider_id', 1);


  //* Permissions parameters *//

  Conf::set('multilingual_enabled', true);


  //* Default language parameters *//

  Conf::set('language_default', array(
    'id' => 1,
    'name' => 'serbian',
    'alias' => 'sr'
  ));

  //* Meta parameters *//

  Conf::set('general_meta_keywords', '');
  Conf::set('general_meta_description', '');


  //* Download parameters *//

  Conf::set('download_filename', array(
    'newsletter' => 'newsletter_list'
  ));
?>