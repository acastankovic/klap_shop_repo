<?php


class AuthenticationController extends Controller {

  protected $user;
  private $usersModel;
  private $helper;

  public function __construct() {
    parent::__construct();

    $helper = UsersHelper::Instance();
    if($helper instanceof UsersHelper) {
      $this->helper = $helper;
    }

    $model = Users::Instance();
    if($model instanceof Users) {
      $this->usersModel = $model;
    }

    sessionStart();
    Trans::initTranslations();
  }


  /*
   * Administration login
   *
   * @param   string username
   * @param   string password
   *
   * @return  object
  */
  public function adminLogin() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));


    $validatedFields = $this->helper->validateAdminLoginParams($params);

    // validate params
    if(!$validatedFields->success) {
      return $this->respond($validatedFields->success, $validatedFields->message);
    }

    $user = $this->usersModel->getByUsernameAndPassword($params['username'], $params['password']);

    if(!$this->helper->exists($user)) {
      return $this->respond(false, Trans::get('Incorrect username or password'));
    }

    if((int)$user->role_id === (int)Conf::get('user_role_id')['customer']) {
      return $this->respond(false, Trans::get('Invalid user role'));
    }

    $this->user = $user;

    $session = new Sessions();
    $session->open($user);

    return $this->respond(true, Trans::get('You are successfully logged in'), $user);
  }

  /*
   * Website login
   *
   * @param   string email
   * @param   string password
   *
   * @return  object
  */
  public function login() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $validatedParams = $this->helper->validateLoginParams($params);

    // validate params
    if(!$validatedParams->success) {
      return $this->respond($validatedParams->success, $validatedParams->message);
    }

    $user = $this->usersModel->getByEmailAndPassword($params['email'], $params['password']);

    if(!$this->helper->exists($user)) {
      return $this->respond(false, Trans::get('Incorrect email or password'));
    }

    if(!$this->helper->active($user)) {

      $this->sendRegistrationEmail($user->email, $user->activation_token);
      $this->usersModel->changeActivationTokenTime($user->id);

      return $this->respond(false, Trans::get('Your profile is not activated. The profile activation code will be emailed to you.'));
    }

    $this->user = $user;

    $session = new Sessions();
    $session->open($user);

    return $this->respond(true, Trans::get('You are successfully logged in'), $user);
  }

  public function logout() {

    $admin = trim(Security::Instance()->purifier()->purify($this->params('admin')));

    $session = new Sessions();
    $closed = $session->close($admin);

    if($closed) {
      return $this->respond(true, Trans::get('You are successfully logged out'));
    } else {
      return $this->respond(false, Trans::get('You are not logged in'));
    }
  }

  public function getActiveSession() {

    $admin = trim(Security::Instance()->purifier()->purify($this->params('admin')));

    $session = new Sessions();
    return $session->active($admin);
  }

  public function sendRegistrationEmail($email, $token) {

    $mailsView = new MailsView();
    $emailData = $mailsView->registrationTemplate($email, $token);

    $mailer = new Mailer();
    return $mailer->sendMail($emailData);
  }

//  public function active() {
//
//    $roleId = trim(Security::Instance()->purifier()->purify($this->params('role_id')));
//
//    $session = new Sessions();
//    $session->active($roleId);
//    $this->view->respond(null);
//  }
}

?>