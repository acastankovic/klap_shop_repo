<?php


class UsersController extends Controller {

  protected $user;
  private $usersModel;
  private $helper;

  public function __construct() {
    parent::__construct();

    $this->model->setTable('users');

    $helper = UsersHelper::Instance();
    if($helper instanceof UsersHelper) {
      $this->helper = $helper;
    }

    $model = Users::Instance();
    if($model instanceof Users) {
      $this->usersModel = $model;
    }

    Trans::initTranslations();
  }


  /************************************ FETCH ************************************/


  /*
   * @param   int  id
   *
   * @return  object
  */
  public function fetchOne() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    if(!@exists($params) || !@exists($params['id'])) {
      return $this->respond(false, Trans::get('Id is required'));
    }

    $data = $this->usersModel->getOne($params['id']);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   NULL
   *
   * @return  array of objects
  */
  public function fetchAll() {

    $data = $this->usersModel->getAll();

    $this->view->respond($data);
    return $data;
  }


  /************************************ ACTIONS ************************************/


  /****** ADMIN ******/

  /*
   * @param   string  username
   * @param   string  password
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
   * 3) int    id - last inserted
  */
  public function insertUser() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    if(!@exists($params) || !@exists($params['username']) || !@exists($params['password'])) {
      return $this->respond(false, Trans::get('Username and password are required'));
    }

    if ($this->usersModel->userExist($params['username'])) {
      return $this->respond(false, Trans::get('User with username') . ': "' . $params['username'] . '" ' . Trans::get('already exists'));
    }

    $data = new stdClass();
    $data->id = $this->usersModel->insertUser($params);
    $data->success = true;
    $data->message = Trans::get('User created');

    $this->view->respond($data);
    return $data;
  }

  /*
   * @param   array of form params
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
  */
  public function updateUser() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    if (!@exists($params['username'])) {
      return $this->respond(false, Trans::get('Username required'));
    }

    $this->usersModel->update($params);

    $data = new stdClass();
    $data->success = true;
    $data->message = Trans::get('User updated');

    $this->view->respond($data);
    return $data;
  }

  /*
   * @param   int  id
   *
   * @return  NULL
  */
  public function delete() {

    $id = trim(Security::Instance()->purifier()->purify($this->params('id')));

    $this->usersModel->delete($id);

    $this->view->respond(null);
    return null;
  }


  /****** WEBSITE ******/

  /*
   * Website register
   *
   * @params  defined in helper
   *
   * @return  object
  */
  public function register() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $validatedParams = $this->helper->validateRegistrationParams($params);

    // validate params
    if(!$validatedParams->success) {
      return $this->respond($validatedParams->success, $validatedParams->message);
    }

    $user = $this->usersModel->getByEmail($params['email']);

    // user with this email exists
    if($this->helper->exists($user)) {

      // if exists and not activated, send email with new token, and reset token in db
      if((int)$user->active !== 1) {

        $this->sendRegistrationEmail($user->email, $user->activation_token);
        $this->usersModel->changeActivationTokenTime($user->id);

        return $this->respond(false, Trans::get('User with this email address already exists, but the profile is not activated. The profile activation code will be emailed to you.'));
      }

      return $this->respond(false, Trans::get('User with this email address already exists'));
    }

    // insert user into db
    $params['activation_token'] = Encryption::generateStampWithString($params['email'], 8);
    $params['activation_token_time'] = date('Y-m-d H:i:s');
    $params['password'] = Encryption::encode($params['password']);
    $params['role_id'] = Conf::get('user_role_id')['customer'];
    $this->usersModel->insert($params);

    $this->sendRegistrationEmail($params['email'], $params['activation_token']);

    return $this->respond(true, Trans::get('The profile activation link is sent to your email address. Check your inbox or spam folder.'));
  }

  /*
   * Website activate
   *
   * @param   string activation token
   *
   * @return  object
  */
  public function activate() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $validatedParams = $this->helper->validateActivateParams($params);

    // validate params
    if(!$validatedParams->success) {
      return $this->respond($validatedParams->success, $validatedParams->message);
    }

    $user = $this->usersModel->getByActivationToken($params['token']);

    $validatedData = $this->helper->validateActivateData($user);

    // validate data
    if(!$validatedData->success) {
      return $this->respond($validatedData->success, $validatedData->message);
    }

    $this->usersModel->activate($user->id);

    return $this->respond(true, Trans::get('Your profile is activated'));
  }

  /*
   * @param   string  password_current
   * @param   string  password_new
   * @param   string  password_repeat_new
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
  */
  public function changePassword() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $validatedParams = $this->helper->validatePasswordChangeParams($params);

    // validate params
    if(!$validatedParams->success) {
      return $this->respond($validatedParams->success, $validatedParams->message);
    }

    $admin = @exists($params['role_id']) && (int)$params['role_id'] === Conf::get('user_role_id')['admin'] ? true : null;

    if(@exists($admin)) {

      $user = $this->usersModel->getOne($params['id']);

    } else {

      $user = Dispatcher::instance()->dispatch('users', 'authentication', 'getActiveSession', array('admin' => $admin), Request::JSON_REQUEST);
    }

    $validatedData = $this->helper->validatePasswordChangeData($params, $user, $admin);

    // validate data
    if(!$validatedData->success) {
      return $this->respond($validatedData->success, $validatedData->message);
    }

    $this->usersModel->changePassword($user->id, $params['password']);

    return $this->respond(true, Trans::get('Your password is changed'));
  }

  public function updateProfileData() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $validated = $this->helper->validateDataUpdate($params);

    // validate params
    if(!$validated->success) {
      return $this->respond($validated->success, $validated->message);
    }

    $authController = new AuthenticationController();
    $user = $authController->getActiveSession();

    if(!$this->helper->exists($user)) {
      return $this->respond(false, Trans::get('You must be logged in to change data'));
    }

    $this->usersModel->updateProfileData($user->id, $params['first_name'], $params['last_name'], $params['phone'], $params['address']);
    $changedUser = $this->usersModel->getOne($user->id);

    return $this->respond(true, Trans::get('Your data is changed'), $changedUser);
  }

  /*
   *
   * @param   string email
   *
   * @return  object
  */
  public function resetPasswordRequest() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $validatedParams = $this->helper->validateResetPasswordRequestParams($params);

    // validate params
    if(!$validatedParams->success) {
      return $this->respond($validatedParams->success, $validatedParams->message);
    }

    $user = $this->usersModel->getByEmail($params['email']);

    if(!$this->helper->exists($user)) {
      return $this->respond(false, 'User with this email address doesn\'t exist');
    }

    $this->usersModel->setResetPasswordToken($user->id, $user->email);
    $this->sendResetPasswordEmail($user->email, $user->reset_password_token);

    return $this->respond(true, 'Reset password link is sent to your email address. Check your inbox or spam folder.');
  }

  /*
   *
   * @param   string token
   * @param   string password
   * @param   string repeated_password
   *
   * @return  object
  */
  public function resetPassword() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $validatedParams = $this->helper->validateResetPasswordParams($params);

    // validate params
    if(!$validatedParams->success) {
      return $this->respond($validatedParams->success, $validatedParams->message);
    }

    $user = $this->usersModel->getByResetPasswordToken($params['token']);

    $validatedData = $this->helper->validateResetPasswordData($params, $user);

    // validate data
    if(!$validatedData->success) {
      return $this->respond($validatedData->success, $validatedData->message);
    }

    $this->usersModel->resetPassword($params['password'], $user->id);

    return $this->respond(true, 'Your password is changed. Try to login with the new password.');
  }

  public function sendRegistrationEmail($email, $token) {

    $mailsView = new MailsView();
    $emailData = $mailsView->registrationTemplate($email, $token);

    $mailer = new Mailer();
    return $mailer->sendMail($emailData);
  }

  public function sendResetPasswordEmail($email, $token) {

    $mailsView = new MailsView();
    $emailData = $mailsView->resetPasswordTemplate($email, $token);

    $mailer = new Mailer();
    return $mailer->sendMail($emailData);
  }
}

?>