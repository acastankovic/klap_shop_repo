<?php


class RolesController extends Controller {

  public function __construct() {
    parent::__construct();
    $this->model = new Model();
    $this->model->setTable('roles');
  }


  /************************************ FETCH ************************************/


  /*
   * @param   int  id
   *
   * @return  object
  */
  public function fetchOne() {

    $id = trim(Security::Instance()->purifier()->purify($this->params('id')));

    $data = $this->model->load($id);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   NULL
   *
   * @return  array of objects
  */
  public function fetchAll() {

    $data = $this->model->load();

    $this->view->respond($data);
    return $data;
  }
}

?>