<?php


class Users extends Model {

  private $selectQuery;

  public function __construct() {
    parent::__construct();
    $this->setTable('users');
    $this->setSelectQuery();
  }


  /************************************ FETCH ************************************/


  public function getOne($id) {

    $sql = $this->selectQuery;
    $sql .= ' WHERE `u`.`id` = :id';

    return $this->exafe($sql, array('id' => $id));
  }

  public function getAll() {

    $sql = $this->selectQuery;
    $results = $this->exafeAll($sql);

    foreach ($results as $result) {
      unset($result->password);
    }
    return $results;
  }

  public function getByEmail($email) {

    $sql = 'SELECT * FROM `users` WHERE `email` = :email;';
    return $this->exafe($sql, array('email' => $email));
  }

  public function getByUsernameAndPassword($username, $password) {

    $password = Encryption::encode($password);

    $sql = 'SELECT * FROM `users` WHERE `username` = :username AND `password` = :password';
    return $this->exafe($sql, array('username' => $username, 'password' => $password));
  }

  public function getByIdAndPassword($id, $password) {

    $password = Encryption::encode($password);

    $sql = 'SELECT * FROM `users` WHERE `id` = :id AND `password` = :password';
    return $this->exafe($sql, array('id' => $id, 'password' => $password));
  }

  public function getByEmailAndPassword($email, $password) {

    $encodedPassword = Encryption::encode($password);

    $sql = 'SELECT * FROM `users` WHERE `email` = :email AND `password` = :password;';
    return $this->exafe($sql, array('email' => $email, 'password' => $encodedPassword));
  }

  public function getByUsername($username) {

    $sql = 'SELECT * FROM `users` WHERE `username` = :username';
    return $this->exafe($sql, array('username' => $username));
  }

  public function getByActivationToken($token) {

    $sql = 'SELECT * FROM `users` WHERE `activation_token` = :activation_token;';
    return $this->exafe($sql, array('activation_token' => $token));
  }

  public function getByResetPasswordToken($token) {

    $sql = 'SELECT * FROM `users` WHERE `reset_password_token` = :reset_password_token;';
    return $this->exafe($sql, array('reset_password_token' => $token));
  }


  /************************************ ACTIONS ************************************/


  public function insertUser($params) {

    $params['password'] = Encryption::encode($params['password']);

    $this->insert($params);

    return $this->lastInsertId();
  }

  public function activate($id) {

    $sql = 'UPDATE `users` SET `active` = 1, `activation_token` = NULL, `activation_token_time` = NULL WHERE `id` = :id;';
    return $this->execute($sql, array('id' => $id));
  }

  public function changeActivationTokenTime($id) {

    $sql = 'UPDATE `users` SET `activation_token_time` = NOW() WHERE `id` = :id;';
    return $this->execute($sql, array('id' => $id));
  }

  public function changePassword($id, $password) {

    $password = Encryption::encode($password);

    $sql = 'UPDATE `users` SET `password` = :password WHERE `id` = :id';
    $this->execute($sql, array('id' => $id, 'password' => $password));
  }

  public function setResetPasswordToken($id, $email) {

    $token = Encryption::generateStampWithString($email, 8);

    $sql = 'UPDATE `users` SET `reset_password_token` = :reset_password_token, `reset_password_token_time` = NOW() WHERE `id` = :id;';
    return $this->execute($sql, array('reset_password_token' => $token, 'id' => $id));
  }

  public function resetPassword($password, $id) {

    $password = Encryption::encode($password);

    $sql = 'UPDATE `users` SET `password` = :password, `reset_password_token` = NULL, `reset_password_token_time` = NULL WHERE `id` = :id;';
    return $this->execute($sql, array('password' => $password, 'id' => $id));
  }

  public function updateProfileData($id, $firstName, $lastName, $phone, $address) {

    $sql = 'UPDATE `users` SET `first_name` = :first_name, `last_name` = :last_name, `phone` = :phone, `address` = :address WHERE `id` = :id;';
    return $this->execute($sql, array('id' => $id, 'first_name' => $firstName, 'last_name' => $lastName, 'phone' => $phone, 'address' => $address));
  }


  /************************************ OTHER ************************************/


  private function setSelectQuery() {
    $this->selectQuery = 'SELECT `u`.*, 
                          `r`.`name` AS `role`
                          FROM `users` AS `u` 
                          LEFT JOIN `roles` AS `r` ON `u`.`role_id` = `r`.`id`';
  }


  public function userExist($username) {

    $user = $this->getByUsername($username);

    return @exists($user) && (bool)$user !== false;
  }


  public function passwordExist($id, $password) {

    $user = $this->getByIdAndPassword($id, $password);

    return @exists($user) && (bool)$user !== false;
  }
}

?>