<?php


class Sessions extends Model {

  public function __construct() {
    parent::__construct();
    $this->setTable('sessions');
    sessionStart();
  }


  public function open($user) {

    $admin = (int)$user->role_id !== (int)Conf::get('user_role_id')['customer'] ? true : null;

    $alias = $this->setSessionAlias($admin);

    $session['ip'] = clientIP();
    $session['user_id'] = $user->id;
    $session['auth_token'] = Encryption::generateStamp();
    $session['status'] = 1;
    $this->insert($session);

    if (Conf::get('session_php')) {
      $_SESSION[$alias] = $session['auth_token'];
    }
  }

  public function close($admin = null) {

    $auth_token = null;
    $alias = $this->setSessionAlias($admin);

    if (Conf::get('session_php')) {
      $auth_token = $_SESSION[$alias];
    }

    $session['status'] = 0;
    $session['auth_token'] = $auth_token;
    $this->update($session, array('auth_token'));

    unset($_SESSION[Conf::get('session_alias')['language']]);
    unset($_SESSION[$alias]);
    return true;
  }

  public function active($admin = null) {

    $auth_token = null;
    $alias = $this->setSessionAlias($admin);

    if (Conf::get('session_php')) {
      if (!@exists($_SESSION[$alias])) return false;
      $auth_token = $_SESSION[$alias];
    }

    if(!@exists($auth_token)) {
      return false;
    }

    $sql = 'SELECT `s`.*, `u`.*
            FROM `sessions` AS `s`
            LEFT JOIN `users` AS `u` ON `s`.`user_id` = `u`.`id`
            WHERE `s`.`auth_token` = :auth_token';

    $active = $this->exafe($sql, array('auth_token' => $auth_token));

    if ($active) {
      if ($active->status) return $active;
    }

    return false;
  }

  private function setSessionAlias($admin = null) {

    if(@exists($admin)) {
      return Conf::get('session_alias')['user_token'];
    }
    return Conf::get('session_alias')['customer_token'];
  }

}

?>