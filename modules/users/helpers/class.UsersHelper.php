<?php

class UsersHelper extends Helper {

  private $registrationRequiredParams = array('first_name', 'last_name', 'email', 'phone', 'password', 'repeated_password');
  private $updateDataRequiredParams = array('first_name', 'last_name', 'phone', 'address');
  private $changePasswordRequiredParams = array('current_password', 'password', 'repeated_password');
  private $resetPasswordRequiredParams = array('token', 'password', 'repeated_password');

  public function validateRegistrationParams($params) {

    if (!@exists($params)) {
      return (object) array('success' => false, 'message' => Trans::get('Empty fields'));
    }

    if (!$this->validateRequiredParams($params, $this->registrationRequiredParams)) {
      return (object) array('success' => false, 'message' => Trans::get('You didn\'t fill out required fields'));
    }

    if (!validateEmail($params['email'])) {
      return (object) array('success' => false, 'message' => Trans::get('Invalid Email address'));
    }

    if (!$this->samePasswords($params['password'], $params['repeated_password'])) {
      return (object) array('success' => false, 'message' => Trans::get('Passwords do not match'));
    }

    if (strlen($params['password']) < 6) {
      return (object) array('success' => false, 'message' => Trans::get('Weak password: under 6 character'));
    }

    return (object) array('success' => true);
  }

  public function validateActivateParams($params) {
    if(!@exists($params) || !@exists($params['token'])) {
      return(object) array('success' => false, 'message' => Trans::get('Token param doesn\'t exist'));
    }

    return (object) array('success' => true);
  }

  public function validateActivateData($user) {

    if(!$this->activationTokenExists($user)) {
      return (object) array('success' => false, 'message' => Trans::get('Activation token doesn\'t exist'));
    }

    if($this->active($user)) {
      return (object) array('success' => false, 'message' => Trans::get('Profile is already activated'));
    }

    if($this->activationTokenExpired($user)) {
      return (object) array('success' => false, 'message' => Trans::get('Activation token expired'));
    }

    return (object) array('success' => true);
  }

  // login
  public function validateAdminLoginParams($params) {

    if(!@exists($params['username']) || !@exists($params['password'])) {
      return (object) array('success' => false, 'message' => Trans::get('Username and password are required'));
    }

    return (object) array('success' => true);
  }

  // login
  public function validateLoginParams($params) {

    if(!@exists($params['email']) || !@exists($params['password'])) {
      return (object) array('success' => false, 'message' => Trans::get('Email and password are required'));
    }

    if (!validateEmail($params['email'])) {
      return (object) array('success' => false, 'message' => Trans::get('Invalid Email address'));
    }

    return (object) array('success' => true);
  }

  public function validateDataUpdate($params) {

    if (!@exists($params)) {
      return (object) array('success' => false, 'message' => Trans::get('Empty fields'));
    }

    if (!$this->validateRequiredParams($params, $this->updateDataRequiredParams)) {
      return (object) array('success' => false, 'message' => Trans::get('You didn\'t fill out required fields'));
    }

    return (object) array('success' => true);
  }

  public function validatePasswordChangeParams($params) {

    if (!@exists($params)) {
      return (object) array('success' => false, 'message' => Trans::get('Empty fields'));
    }

    if (!$this->validateRequiredParams($params, $this->changePasswordRequiredParams)) {
      return (object) array('success' => false, 'message' => Trans::get('You didn\'t fill out required fields'));
    }

    if (!$this->samePasswords($params['password'], $params['repeated_password'])) {
      return (object) array('success' => false, 'message' => Trans::get('Passwords do not match'));
    }

    if (strlen($params['password']) < 6) {
      return (object) array('success' => false, 'message' => Trans::get('Weak password: under 6 character'));
    }

    return (object) array('success' => true);
  }

  public function validatePasswordChangeData($params, $user, $admin = null) {

    if(!$this->exists($user)) {
      return (object) array('success' => false, 'message' => Trans::get('You must be logged in to change password'));
    }

    if(!@exists($admin)) {

      $currentEnteredPassword = Encryption::encode($params['current_password']);
      if(!$this->samePasswords($currentEnteredPassword, $user->password)) {
        return (object) array('success' => false, 'message' => Trans::get('Current password is incorrect'));
      }
    }

    $newEnteredPassword = Encryption::encode($params['password']);

    if($this->samePasswords($newEnteredPassword, $user->password)) {
      return (object) array('success' => false, 'message' => Trans::get('New password can\'t be the same as old'));
    }

    return (object) array('success' => true);
  }

  // reset password request fields
  public function validateResetPasswordRequestParams($params) {

    if(!@exists($params) || !@exists($params['email'])) {
      return (object) array('success' => false, 'message' => Trans::get('Email is required'));
    }

    if (!validateEmail($params['email'])) {
      return (object) array('success' => false, 'message' => Trans::get('Invalid Email address'));
    }

    return (object) array('success' => true);
  }


  // reset password fields
  public function validateResetPasswordParams($params) {

    if (!@exists($params)) {
      return (object) array('success' => false, 'message' => Trans::get('Empty fields'));
    }

    if (!$this->validateRequiredParams($params, $this->resetPasswordRequiredParams)) {
      return (object) array('success' => false, 'message' => Trans::get('You didn\'t fill out required fields'));
    }

    if (!$this->samePasswords($params['password'], $params['repeated_password'])) {
      return (object) array('success' => false, 'message' => Trans::get('Passwords do not match'));
    }

    if (strlen($params['password']) < 6) {
      return (object) array('success' => false, 'message' => Trans::get('Weak password: under 6 character'));
    }

    return (object) array('success' => true);
  }

  // reset password data
  public function validateResetPasswordData($params, $user) {

    if (!@exists($user->id)) {
      return (object) array('success' => false, 'message' => Trans::get('Reset password token doesn\'t exist'));
    }

    if ($this->resetPasswordTokenExpired($user)) {
      return (object) array('success' => false, 'message' => Trans::get('Reset password token expired'));
    }

    $enteredPassword = Encryption::encode($params['password']);

    if ($this->samePasswords($user->password, $enteredPassword)) {
      return (object) array('success' => false, 'message' => Trans::get('New password can\'t be the same as old'));
    }

    return (object) array('success' => true);
  }

  public function activationTokenExists($user) {
    return @exists($user->id);
  }

  public function active($user) {
    return (int)$user->active === 1;
  }

  public function activationTokenExpired($user) {

    $tokenTime = $user->activation_token_time;
    $currentTime = date("Y-m-d H:i:s");

    $timeDiff = strtotime($currentTime) - strtotime($tokenTime);

    return $timeDiff > (int)Conf::get('activation_token_lifetime');
  }

  public function exists($data) {
    return @exists($data) && $data != false;
  }

  public function samePasswords($password1, $password2) {
    return (string)$password1 === (string)$password2;
  }

  public function resetPasswordTokenExpired($user) {

    $tokenTime = $user->reset_password_token_time;
    $currentTime = date("Y-m-d H:i:s");

    $timeDiff = strtotime($currentTime) - strtotime($tokenTime);

    return $timeDiff > (int)Conf::get('reset_password_token_lifetime');
  }
}

?>