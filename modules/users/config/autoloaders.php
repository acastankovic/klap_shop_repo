<?php


class UsersAutoloaders {

  public static function autoload_controllers($class_name) {

    $file = Conf::get('root') . '/modules/users/controllers/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }

  public static function autoload_models($class_name) {

    $file = Conf::get('root') . '/modules/users/models/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }

  public static function autoload_helpers($class_name) {

    $file = Conf::get('root') . '/modules/users/helpers/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }
}

spl_autoload_register('UsersAutoloaders::autoload_controllers');
spl_autoload_register('UsersAutoloaders::autoload_models');
spl_autoload_register('UsersAutoloaders::autoload_helpers');
require_once(Conf::get('root') . '/modules/users/app.php');

//register module
$item = new stdClass();
$item->title = 'Users';
$item->alias = 'users';
$item->root = false;
$modules = Conf::get('modules');
array_push($modules, $item);
Conf::set('modules', $modules);