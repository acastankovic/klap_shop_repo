<?php


class UsersApplication extends Application {

  protected $name = 'users';

  protected $resources = array('users', 'roles');

  protected $routes = array(

    /**** authentication ****/

    // admin login
    array('route'      => 'admin/login',
          'method'     => 'post',
          'controller' => 'authentication',
          'action'     => 'adminLogin'),

    // website login
    array('route'       => 'login',
          'method'      => 'post',
          'controller'  => 'authentication',
          'action'      => 'login'),

    array('route'       => 'logout',
          'method'      => 'post',
          'controller'  => 'authentication',
          'action'      => 'logout'),

    // active
    array('route'      => 'active',
          'method'     => 'get',
          'controller' => 'authentication',
          'action'     => 'active'),


    /**** users ****/

    //get user
    array('route'      => ':id',
          'method'     => 'get',
          'controller' => 'users',
          'action'     => 'fetchOne'),

    //get users
    array('route'      => '',
          'method'     => 'get',
          'controller' => 'users',
          'action'     => 'fetchAll'),

    //delete user
    array('route'      => ':id',
          'method'     => 'delete',
          'controller' => 'users',
          'action'     => 'delete'),

    //insert user
    array('route'      => 'insert',
          'method'     => 'post',
          'controller' => 'users',
          'action'     => 'insertUser'),

    //update user
    array('route'      => 'update',
          'method'     => 'put',
          'controller' => 'users',
          'action'     => 'updateUser'),

    array('route'       => 'register',
          'method'      => 'post',
          'controller'  => 'users',
          'action'      => 'register'),

//    array('route'       => 'activate/:token',
//          'method'      => 'get',
//          'controller'  => 'users',
//          'action'      => 'activate'),

    array('route'       => 'activate',
          'method'      => 'post',
          'controller'  => 'users',
          'action'      => 'activate'),

    array('route'       => ':id',
          'method'      => 'get',
          'controller'  => 'users',
          'action'      => 'fetchOne'),

    array('route'       => '',
          'method'      => 'get',
          'controller'  => 'users',
          'action'      => 'fetchAll'),

    array('route'       => 'update',
          'method'      => 'put',
          'controller'  => 'users',
          'action'      => 'updateProfileData'),

    // change password
    array('route'       => 'password',
          'method'      => 'put',
          'controller'  => 'users',
          'action'      => 'changePassword'),

    // reset password
    array('route'       => 'reset-password-request',
          'method'      => 'post',
          'controller'  => 'users',
          'action'      => 'resetPasswordRequest'),

    // reset password
    array('route'       => 'reset-password',
          'method'      => 'put',
          'controller'  => 'users',
          'action'      => 'resetPassword'),
  );
}

?>