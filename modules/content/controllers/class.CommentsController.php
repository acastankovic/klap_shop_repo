<?php


class CommentsController extends Controller {

  private $service;
  private $commentsModel;

  public function __construct() {
    parent::__construct();

    $this->model->setTable('comments');

    $service = CommentsServices::Instance();
    if ($service instanceof CommentsServices) {
      $this->service = $service;
    }

    $commentsModel = Comments::Instance();
    if ($commentsModel instanceof Comments) {
      $this->commentsModel = $commentsModel;
    }

    Trans::initTranslations();
  }


  /************************************ FETCH ************************************/


  /*
   * @param   int     target_id
   * @param   string  type
   *
   * @return  array of objects
  */
  public function fetchByTypeIdAndTargetId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $comments = $this->commentsModel->getByTypeIdAndTargetId($params);

    $data = formTree($comments, 0);

    $this->view->respond($data, null);
    return $data;
  }


  /************************************ ACTIONS ************************************/


  /*
   * @param   array of form params
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
   * 3) int    id - last inserted
  */
  public function insertComment() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $this->commentsModel->insert($params);
    $data = $this->commentsModel->lastInsertId();

    $this->view->respond($data);
    return $data;
  }


  public function deleteComment() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->delete($params);

    $this->view->respond($data);
    return $data;
  }


  public function publishComment() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->publish($params);

    $response = new stdClass();
    $response->published = $data->published;
    $response->message = (int)$data->published === 1 ? Trans::get('Comment is published') : Trans::get('Comment is unpublished');
    $response->buttonText = (int)$data->published === 1 ? Trans::get('Unpublish') : Trans::get('Publish');

    $this->view->respond($response);
    return $response;
  }
}

?>