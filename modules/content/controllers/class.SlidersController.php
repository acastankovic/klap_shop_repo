<?php


class SlidersController extends Controller {

  private $service;
  private $slidersModel;
  private $sliderItemsModel;

  public function __construct() {
    parent::__construct();

    $this->model->setTable('sliders');

    $service = SlidersServices::Instance();
    if ($service instanceof SlidersServices) {
      $this->service = $service;
    }

    $slidersModel = Sliders::Instance();
    if ($slidersModel instanceof Sliders) {
      $this->slidersModel = $slidersModel;
    }

    $sliderItemsModel = SliderItems::Instance();
    if ($sliderItemsModel instanceof SliderItems) {
      $this->sliderItemsModel = $sliderItemsModel;
    }

    Trans::initTranslations();
  }
    

  /************************************ FETCH ************************************/


  /*
   * @param   int  id
   *
   * @return  object
  */
  public function fetchOne() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->slidersModel->getOne($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   NULL
   *
   * @return  array of objects
  */
  public function fetchAll() {

    $data = $this->slidersModel->getAll();

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   int  id
   *
   * @return  object
  */
  public function fetchOneWithItems() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));
    $data = $this->service->loadOneWithItems($params);

    $this->view->respond($data);
    return $data;
  }

  /*** FETCH BY LANGUAGES ***/


  /*
   * @param   int  lang_id    (optional)
   *
   * @return  array of objects
  */
  public function fetchByLanguageId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->slidersModel->getByLanguageId($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   int  lang_group_id
   *
   * @return  array of objects
  */
  public function fetchByLanguageGroupId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->slidersModel->getByLanguageGroupId($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   int  lang_group_id
   * @param   int  lang_id        (optional)
   *
   * @return  object
  */
  public function fetchByLanguageGroupIdAndLanguageId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->slidersModel->getByLanguageGroupIdAndLanguageId($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * Fetch by parent's parent, language group and current language (groups items with same language group)
   *
   * @param   int  id
   * @param   bool fetchWithUnpublished     (optional)
   *
   * @return  object with two properties:
   * 1) array items - all items with same language group with lang_id as key
   * 2) int   langGroupId
  */
  public function fetchOneWithLanguageGroups() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadOneWithLanguageGroups($params);

    $this->view->respond($data);
    return $data;
  }


  /*************************************************************************
   *                              SLIDER ITEMS                              *
   *************************************************************************/


  /*
   * @param   int  id
   * @param   bool fetchWithUnpublished (optional)
   *
   * @return  object
  */
  public function fetchOneItem() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->sliderItemsModel->getOne($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   bool fetchWithUnpublished (optional)
   *
   * @return  array of objects
  */
  public function fetchAllItems() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->sliderItemsModel->getAll($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   int  parent_id
   * @param   bool fetchWithUnpublished (optional)
   *
   * @return  array of objects
  */
  public function fetchItemsByParentId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadItemsByParentId($params);

    return $data;
  }


  /*
   * @param   int  id
   *
   * @return  array
  */
  public function fetchTree() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $results = $this->sliderItemsModel->getByParentId($params);

    $data = formTree($results, 0);

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  /************************************ ACTIONS ************************************/


  /*
   * @param   array of form params
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
  */
  public function insertSlider() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = new stdClass();

    if (!@exists($params['name'])) {

      $data->success = false;
      $data->message = Trans::get('Name required');

    } else {

      $this->service->insert($params);
      $data->success = true;
      $data->message = Trans::get('Slider created');
    }

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  /*
   * @param   array of form params
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
  */
  public function updateSlider() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = new stdClass();

    if (!@exists($params['name'])) {

      $data->success = false;
      $data->message = Trans::get('Name required');

    } else {

      $this->service->update($params);

      $data->success = true;
      $data->message = Trans::get('Slider updated');
    }

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  /*************************************************************************
   *                              SLIDER ITEMS                              *
   *************************************************************************/


  /*
   * @param   array of form params
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
   * 3) int    id - last inserted
  */
  public function insertSliderItem() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = new stdClass();

    if (!@exists($params['image'])) {

      $data->success = false;
      $data->message = Trans::get('Image required');

    } else {

      $data->id = $this->service->insertItem($params);
      $data->success = true;
      $data->message = Trans::get('Slider item created');
    }

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  /*
   * @param   array of form params
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
  */
  public function updateSliderItem() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = new stdClass();

    if (!@exists($params['image'])) {

      $data->success = false;
      $data->message = Trans::get('Image required');

    } else {

      $this->service->updateItem($params);

      $data->success = true;
      $data->message = Trans::get('Slider item updated');
    }

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  /*
   * @param   int  id
   *
   * @return  NULL
  */
  public function deleteSliderItem() {

    $id = trim(Security::Instance()->purifier()->purify($this->params('id')));

    $this->sliderItemsModel->delete($id);

    $this->view->respond(null, null, Request::JSON_REQUEST);
    return null;
  }


  /*
   * @param   int  id
   * @param   int  position
   *
   * @return  object
  */
  public function updateItemsPosition() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $this->sliderItemsModel->updatePositions($params);

    $data = $this->sliderItemsModel->getOne($params);

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }
}

?>