<?php


class LanguagesController extends Controller {

  private $service;
  private $languagesModel;

  public function __construct() {
    parent::__construct();

    $this->model->setTable('languages');

    $service = LanguagesServices::Instance();
    if ($service instanceof LanguagesServices) {
      $this->service = $service;
    }

    $languagesModel = Languages::Instance();
    if ($languagesModel instanceof Languages) {
      $this->languagesModel = $languagesModel;
    }

    Trans::initTranslations();
  }


  /************************************ FETCH ************************************/


  /*
   * @param   int  id
   *
   * @return  object
  */
  public function fetchOne() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->languagesModel->getOne($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   NULL
   *
   * @return  array of objects
  */
  public function fetchAll() {

    $data = $this->languagesModel->getAll();

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   string  alias
   *
   * @return  object
  */
  public function fetchByAlias() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->languagesModel->getByAlias($params);

    $this->view->respond($data);
    return $data;
  }


  /************************************ ACTIONS ************************************/


  /*
   * @param   int  id
   *
   * @return  NULL
  */
  public function setLanguage() {

    $language = Dispatcher::instance()->dispatch('content', 'languages', 'fetchOne', array('id' => $this->params('id')));
    Trans::setLanguage($language);

    $this->view->respond(null, null, Request::JSON_REQUEST);
  }
}

?>