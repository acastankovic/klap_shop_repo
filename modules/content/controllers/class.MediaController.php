<?php


class MediaController extends Controller {

  private $service;
  private $mediaModel;

  public function __construct() {
    parent::__construct();

    $this->model->setTable('media');

    $service = MediaServices::Instance();
    if ($service instanceof MediaServices) {
      $this->service = $service;
    }

    $mediaModel = Media::Instance();
    if ($mediaModel instanceof Media) {
      $this->mediaModel = $mediaModel;
    }
  }


  /************************************ FETCH ************************************/


  /*
   * @param   int  id
   *
   * @return  object
  */
  public function fetchOne() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->mediaModel->getOne($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   NULL
   *
   * @return  array of objects
  */
  public function fetchAll() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->mediaModel->getAll($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   string  file_name
   *
   * @return  object
  */
  public function fetchByFileName() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->mediaModel->getByFileName($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   string  ids
   *
   * @return  array of objects
  */
  public function fetchByIds() {

    $ids = trim(Security::Instance()->purifier()->purify($this->params('ids')));

    $data = $this->mediaModel->getByIds($ids);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   string  search_term
   *
   * @return  array of objects
  */
  public function fetchBySearch() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadBySearch($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   string   mime_type
   * @param   int      page
   * @param   int      items_per_page
   *
   * @return  array of objects
  */
  public function fetchWithPagination() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadWithPagination($params);

    $this->view->respond($data);
    return $data;
  }


  /************************************ ACTIONS ************************************/


  /*
   * @param   array file      ($_FILES['file'])
   *
   * @return  object
  */
  public function uploadMedia() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->upload();

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }

  /*
   * @param   int id
   *
   * @return  NULL
  */
  public function delete() {

    $id = trim(Security::Instance()->purifier()->purify($this->params('id')));

    $this->service->delete($id);

    $this->view->respond(null, null, Request::JSON_REQUEST);
    return null;
  }


  /*
   * @param   NULL
   *
   * @return  NULL
  */
  public function createThumbs() {

    $data = $this->service->createAllThumbs();

    $this->view->respond($data);
    return $data;
  }
}

?>