<?php


class CategoriesController extends Controller {

  private $service;
  private $categoriesModel;

  public function __construct() {
    parent::__construct();

    $this->model->setTable('categories');

    $service = CategoriesServices::Instance();
    if ($service instanceof CategoriesServices) {
      $this->service = $service;
    }

    $categoriesModel = Categories::Instance();
    if ($categoriesModel instanceof Categories) {
      $this->categoriesModel = $categoriesModel;
    }

    Trans::initTranslations();
  }


  /************************************ FETCH ************************************/


  /*
   * @param   int  id
   * @param   bool fetchWithUnpublished (optional)
   * @param   int  lang_id    (optional)
   *
   * @return  object
  */
  public function fetchOne() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadOne($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   bool fetchWithUnpublished (optional)
   * @param   int  lang_id    (optional)
   *
   * @return  array of objects
  */
  public function fetchAll() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadAll($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   int    parent_id
   * @param   string alias
   *
   * @return  object
  */
  public function fetchByAlias() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadByAlias($params);

    return $data;
  }


  /*
   * @param   int  parent_id
   * @param   bool fetchWithUnpublished (optional)
   * @param   int  lang_id    (optional)
   *
   * @return  array of objects
  */
  public function fetchByParentId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadByParentId($params);

    return $data;
  }


  /*
   * @param   int  parent_id
   * @param   bool fetchWithUnpublished (optional)
   *
   * @return  array of objects
  */
  public function fetchChildren() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadChildren($params);

    return $data;
  }


  /*
   * @param   int  root_id
   * @param   int  lang_id
   *
   * @return  array of objects
  */
  public function fetchTree() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $rootId = $params['root_id'];
    $langId = $params['lang_id'];

    // $categories = Categories::getCategories($langId);
    $categories = $this->service->loadAll(array('lang_id' => $langId));

    $data = formTree($categories, $rootId);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   int  root_id
   * @param   bool    fetchWithUnpublished (optional)
   * @param   int     lang_id    (optional)
   *
   * @return  array of objects
  */
  public function fetchBySearch() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = null;
    if (@exists($params['search'])) {

      $data = $this->service->loadBySearch($params);
    }

    $this->view->respond($data, null);
    return $data;
  }


  /*
   * @param   string  ids
   * @param   bool    fetchWithUnpublished (optional)
   *
   * @return  array of objects
  */
  public function fetchByIds() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->categoriesModel->getByIds($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @return  array of objects
  */
  public function fetchForAdminTables() {

    $data = $this->categoriesModel->getForAdminTables();

    $this->view->respond($data);
    return $data;
  }

  /*** FETCH BY LANGUAGES ***/


  /*
   * @param   int  lang_id    (optional)
   * @param   bool fetchWithUnpublished (optional)
   *
   * @return  array of objects
  */
  public function fetchByLanguageId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->categoriesModel->getByLanguageId($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   int  lang_group_id
   * @param   bool fetchWithUnpublished     (optional)
   *
   * @return  array of objects
  */
  public function fetchByLanguageGroupId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->categoriesModel->getByLanguageGroupId($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   int  lang_group_id
   * @param   int  lang_id        (optional)
   * @param   bool fetchWithUnpublished     (optional)
   *
   * @return  object
  */
  public function fetchByLanguageGroupIdAndLanguageId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadByLanguageGroupIdAndLanguageId($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * Fetch by parent, language group and current language
   *
   * @param   int  lang_group_id
   * @param   int  lang_id        (optional)
   * @param   bool fetchWithUnpublished     (optional)
   *
   * @return  array of objects
  */
  public function fetchByParentIdAndLanguageGroupIdAndLanguageId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadByParentIdAndLanguageGroupIdAndLanguageId($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * Fetch by category parent, language group and current language (groups items with same language group)
   *
   * @param   int  id
   * @param   bool fetchWithUnpublished     (optional)
   *
   * @return  object with two properties:
   * 1) array items - all items with same language group with lang_id as key
   * 2) int   langGroupId
  */
  public function fetchOneWithLanguageGroups() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadOneWithLanguageGroups($params);

    $this->view->respond($data);
    return $data;
  }


  /************************************ ACTIONS ************************************/


  /*
   * @param   array of form params
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
   * 3) int    id - last inserted
  */
  public function insertCategory() {

    $params = $this->params();
    foreach ($params as $key => $item) {
      if ($key !== 'content') {
        $result[$key] = trimFields(Security::Instance()->purifier($params[$key]));
      }
    }

    $data = new stdClass();

    if (!@exists($params['name'])) {

      $data->success = false;
      $data->message = Trans::get('Name required');

    } else {

      $data->id = $this->service->insert($params);
      $data->success = true;
      $data->message = Trans::get('Category created');
    }

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  /*
   * @param   array of form params
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
  */
  public function updateCategory() {

    $params = $this->params();
    foreach ($params as $key => $item) {
      if ($key !== 'content') {
        $result[$key] = trimFields(Security::Instance()->purifier($params[$key]));
      }
    }

    $data = new stdClass();

    if (!@exists($params['name'])) {

      $data->success = false;
      $data->message = Trans::get('Name required');

    } else {

      $this->service->update($params);

      $data->success = true;
      $data->message = Trans::get('Category updated');
    }

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  /*
   * Changes item position (rang)
   *
   * @param   int  id
   * @param   int  parent_id
   * @param   int  position
   *
   * @return  object
  */
  public function updatePosition() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->updatePosition($params);

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }
}

?>