<?php


class MenusController extends Controller {

  private $service;
  private $menusModel;

  public function __construct() {
    parent::__construct();

    $this->model->setTable('menus');

    $service = MenusServices::Instance();
    if ($service instanceof MenusServices) {
      $this->service = $service;
    }

    $menusModel = Menus::Instance();
    if ($menusModel instanceof Menus) {
      $this->menusModel = $menusModel;
    }

    Trans::initTranslations();
  }
  

  /************************************ FETCH ************************************/


  /*
   * @param   int  id
   *
   * @return  object (menu properties, and menu items)
  */
  public function fetchOne() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadOne($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   NULL
   *
   * @return  array of objects
  */
  public function fetchAll() {

    $data = $this->menusModel->getAll();

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   int  id
   * @param   bool fetchWithUnpublished (optional)
   *
   * @return  object (menu properties, and menu items)
  */
  public function fetchOneWithItems() {

    $params = $this->params();

    $data = $this->service->loadOneWithItems($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   int  id
   *
   * @return  array
  */
  public function fetchTree() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $menu = $this->service->loadOne($params);

    $data = formTree($menu->items, 0);

    $this->view->respond($data);
    return $data;
  }


  /*** FETCH BY LANGUAGES ***/


  /*
   * @param   int  lang_id    (optional)
   *
   * @return  array of objects
  */
  public function fetchByLanguageId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->menusModel->getByLanguageId($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   int  lang_group_id
   *
   * @return  array of objects
  */
  public function fetchByLanguageGroupId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->menusModel->getByLanguageGroupId($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * @param   int  lang_group_id
   * @param   int  lang_id        (optional)
   *
   * @return  object
  */
  public function fetchByLanguageGroupIdAndLanguageId() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->menusModel->getByLanguageGroupIdAndLanguageId($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   * Fetch by parent's parent, language group and current language (groups items with same language group)
   *
   * @param   int  id
   * @param   bool fetchWithUnpublished     (optional)
   *
   * @return  object with two properties:
   * 1) array items - all items with same language group with lang_id as key
   * 2) int   langGroupId
  */
  public function fetchOneWithLanguageGroups() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadOneWithLanguageGroups($params);

    $this->view->respond($data);
    return $data;
  }


  /*************************************************************************
   *                            MENU ITEM TYPES                             *
   *************************************************************************/


  /*
   * @param   NULL    (optional)
   *
   * @return  array of objects
  */
  public function fetchTypes() {

    $data = $this->service->loadTypes();

    $this->view->respond($data);
    return $data;
  }

  /************************************ ACTIONS ************************************/


  /*
   * @param   array of form params
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
  */
  public function insertMenu() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = new stdClass();

    if (!@exists($params['name'])) {
      $data->success = false;
      $data->message = Trans::get('Name required');
    }
    else {

      $this->service->insert($params);

      $data->success = true;
      $data->message = Trans::get('Menu created');
    }

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  /*
   * @param   array of form params
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
  */
  public function updateMenu() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = new stdClass();

    if (!@exists($params['name'])) {
      $data->success = false;
      $data->message = Trans::get('Name required');
    }
    else {

      $this->service->update($params);

      $data->success = true;
      $data->message = Trans::get('Menu updated');
    }

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  /*************************************************************************
   *                                MENU ITEMS                              *
   *************************************************************************/


  /*
   * @param   array of form params
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
  */
  public function insertMenuItem() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = new stdClass();

    if (!@exists($params['name'])) {
      $data->success = false;
      $data->message = Trans::get('Title required');
    }
    else {

      $this->service->insertItem($params);

      $data->success = true;
      $data->message = Trans::get('Item created');
    }

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  /*
   * @param   array of form params
   *
   * @return  object:
   * 1) bool   success
   * 2) string message
  */
  public function updateMenuItem() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = new stdClass();

    if (!@exists($params['name'])) {
      $data->success = false;
      $data->message = Trans::get('Title required');
    }
    else {

      $this->service->updateItem($params);

      $data->success = true;
      $data->message = Trans::get('Item updated');
    }

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  /*
   * @param   int  id
   *
   * @return  NULL
  */
  public function deleteMenuItem() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $this->service->deleteItem($params);

    $this->view->respond(null, null, Request::JSON_REQUEST);
    return null;
  }


  /*
   * @param   int  id
   * @param   int  parent_id
   * @param   int  position
   *
   * @return  object
  */
  public function updateItemsPosition() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->updateItemsPosition($params);

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }
}

?>