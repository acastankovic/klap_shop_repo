<?php


class MediaServices extends Service {

  private $model;

  public function __construct() {

    $model = Media::Instance();

    if ($model instanceof Media) {
      $this->model = $model;
    }
  }


  /************************************ LOAD ************************************/

  public function loadBySearch($data) {

    if ((string)$data['search_term'] !== '') {
      return $this->model->getBySearch($data);
    }
    return null;
  }


  public function loadWithPagination($data) {

    $response = new stdClass();
    $response->total = $this->model->getTotal($data);
    $response->items = $this->model->getWithPagination($data);

    return $response;
  }


  /************************************ ACTIONS ************************************/


  public function upload() {

    $response = new stdClass();

    if (!empty($_FILES)) {

      $targetPath = Conf::get('media_root') . '/';

      $size = $_FILES['file']['size'];
      $name = $_FILES['file']['name'];
      $type = $_FILES['file']['type'];

      $mime = explode('/', $type);
      $mimeType = $mime[0];
      $mimeSubtype = $mime[1];
      $parsedName = $this->parseFileName($name);

      $targetFile = $targetPath . $parsedName;

      if (!$this->mediaExists($parsedName)) {

        if (!file_exists($targetFile)) {

          $tempFile = $_FILES['file']['tmp_name'];

          $upl = move_uploaded_file($tempFile, $targetFile);

          if ($upl && strtolower($mimeType) === 'image') {

            try {
              $image = new \claviska\SimpleImage();

              $image
                ->fromFile($targetFile)
                ->autoOrient()
                ->resize(100)
                ->toFile($targetPath . 'thumbs/' . $parsedName);
            }
            catch (Exception $e) {
              Logger::put('Error image: ' . $e->getMessage());
            }
          }
        }

        $data = array(
          'title' => $parsedName,
          'file_name' => $parsedName,
          'mime' => $type,
          'mime_type' => $mimeType,
          'mime_subtype' => $mimeSubtype,
          'size' => $size,
          'rang' => 1,
          'created_by' => $this->getLoggedInUserId()
        );

        $this->model->insert($data);

        $response->success = true;
        $response->message = Trans::get('File uploaded');
        $response->status = Conf::get('media_upload_status')['success'];
        $response->media = $this->model->loadLastInsert();

      } else {

        $response->success = false;
        $response->message = Trans::get('File already uploaded');
        $response->status = Conf::get('media_upload_status')['already_uploaded'];
        $response->media = $this->model->getByFileName(array('file_name' => $parsedName));
      }

    } else {
      $response->success = false;
      $response->message = Trans::get('File upload failed');
      $response->status = Conf::get('media_upload_status')['failed'];
      $response->media = null;
    }

    $response->file = $_FILES['file'];

    return $response;
  }


  public function delete($id) {

    $data = array('id' => $id);

    $media = $this->model->getOne($data);

    $targetFile = Conf::get('media_root') . '/' . $media->file_name;
    $targetThumbFile = Conf::get('media_thumbs_root') . '/' . $media->file_name;

    if (file_exists($targetFile)) unlink($targetFile);
    if (file_exists($targetThumbFile)) unlink($targetThumbFile);

    $this->model->delete($id);
  }


  /************************************ OTHER ************************************/


  private function parseFileName($uploadedFile) {

    $fileExtension = pathinfo($uploadedFile, PATHINFO_EXTENSION);
    $fileParts = explode('.', $uploadedFile);

    if (count($fileParts) == 2) {

      $fileName = $fileParts[0];

    } else {

      $newFileParts = array();
      foreach ($fileParts as $part) {

        if ($part != $fileExtension) {

          array_push($newFileParts, $part);
        }
      }

      $fileName = implode('', $newFileParts);
    }

    $file = filterUrl($fileName) . '.' . $fileExtension;

    return $file;
  }


  public function createAllThumbs() {

    $path = Conf::get('media_root') . '/';

    if ($handle = opendir($path)) {
      while (false !== ($file = readdir($handle))) {
        if ('.' === $file) continue;
        if ('..' === $file) continue;
        $fileName = $path . $file;

        if (is_file($fileName)) {
          try {
            $img = new abeautifulsite\SimpleImage($fileName);
            $img->thumbnail(100, 75);
            $img->save($path . 'thumbs/' . $file);
          }
          catch (Exception $e) {
            Logger::put('Error image: ' . $e->getMessage());
          }
        }

        // do something with the file
      }
      closedir($handle);
    }
  }


  private function mediaExists($fileName) {

    $data = array('file_name' => $fileName);

    $media = $this->model->getByFileName($data);
    return @exists($media) && (bool)$media !== false;
  }
}

?>