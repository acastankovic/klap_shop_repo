<?php


class NewsletterServices extends Service {

  private $model;

  public function __construct() {

    $model = Newsletter::Instance();

    if ($model instanceof Newsletter) {
      $this->model = $model;
    }
  }


  /************************************ ACTIONS ************************************/

  public function validateFields($params) {

    $response = new stdClass();
    $response->success = false;

    if (!@exists($params['email'])) {
      $response->message = Trans::get('Email required');
      return $response;
    }

    if (!validateEmail($params['email'])) {
      $response->message = Trans::get('Invalid E-mail address');
      return $response;
    }

    if ($this->exists($params)) {
      $response->message = Trans::get('User with email address') . ' "' . $params['email'] . '" ' . Trans::get('is already signed up');
      return $response;
    }

    $response->success = true;
    return $response;
  }


  private function exists($params) {

    $data = $this->model->getByEmail($params);

    return @exists($data) && (bool)$data !== false;
  }


  public function setDownloadData($data, $params = null) {

    $heading = array('Id', 'Email',);
    $body = array();

    if (@exists($data)) {

      foreach ($data as $item) {


        $itemArray = array(
          'id' => $item->id,
          'email' => $item->email
        );

        array_push($body, $itemArray);
      }
    }

    return (object)array('body' => $body, 'heading' => $heading);
  }
}

?>