<?php


class ArticlesServices extends Service {

  private $model;

  public function __construct() {

    $model = Articles::Instance();
    if ($model instanceof Articles) {
      $this->model = $model;
    }
  }


  /************************************ LOAD ************************************/


  public function loadOne($data) {

    if (Languages::enabled() && !@exists($data['fetchWithUnpublished'])) {

      $data = $this->setLangGroupIdFromId($data);

      $result = $this->model->getByLanguageGroupIdAndLanguageId($data);

    } else {

      $result = $this->model->getOne($data);
    }

    $this->setItemProperties($result);

    return $result;
  }


  public function loadAll($data = null) {

    if (Languages::enabled() && !@exists($data['fetchWithUnpublished'])) {

      $results = $this->model->getByLanguageId($data);

    } else {

      $results = $this->model->getAll($data);
    }

    foreach ($results as $result) {

      $this->setItemProperties($result);
    }

    return $results;
  }


  public function loadByAlias($data) {

    $adminLogged = $this->adminLoggedIn();

    $result = $this->model->getByAlias($data, $adminLogged);

    $this->setItemProperties($result, $data);

    return $result;
  }


  public function loadByParentId($data) {

    if (Languages::enabled() && !@exists($data['fetchWithUnpublished'])) {

      $data = $this->setLangGroupIdFromParentId($data);

      $results = $this->model->getByParentIdAndLanguageGroupIdAndLanguageId($data);

    } else {

      $results = $this->model->getByParentId($data);
    }

    foreach ($results as $result) {

      $this->setItemProperties($result);
    }

    return $results;
  }


  public function loadByCategoryParentId($data) {

    if (Languages::enabled() && !@exists($data['fetchWithUnpublished'])) {

      $data = $this->setLangGroupIdFromParentId($data);

      $results = $this->model->getByParentsParentIdAndLanguageGroupIdAndLanguageId($data);

    } else {

      $results = $this->model->getByCategoryParentId($data);
    }

    foreach ($results as $result) {

      $this->setItemProperties($result);
    }

    return $results;
  }


  public function loadBySearch($data) {

    if (Languages::enabled() && !@exists($data['fetchWithUnpublished'])) {

      $data['lang_id'] = Trans::getLanguageId();
    }

    $results = $this->model->getBySearch($data);

    foreach ($results as $result) {

      $this->setItemProperties($result);
    }

    return $results;
  }


  public function loadParentIds($data) {

    $results = $this->model->getParentIds($data);

    $idsString = '';
    foreach ($results as $result) {
      $idsString .= $result->category_id . ',';
    }

    return trim($idsString, ',');
  }

  /*** LOAD BY LANGUAGES ***/


  public function loadByLanguageGroupIdAndLanguageId($data) {

    $result = $this->model->getByLanguageGroupIdAndLanguageId($data);

    $this->setItemProperties($result);

    return $result;
  }


  public function loadByParentIdAndLanguageGroupIdAndLanguageId($data) {

    $results = $this->model->getByParentIdAndLanguageGroupIdAndLanguageId($data);

    foreach ($results as $result) {

      $this->setItemProperties($result);
    }

    return $results;
  }


  public function loadByParentsParentIdAndLanguageGroupIdAndLanguageId($data) {

    $results = $this->model->getByParentsParentIdAndLanguageGroupIdAndLanguageId($data);

    foreach ($results as $result) {

      $this->setItemProperties($result);
    }

    return $results;
  }


  public function loadOneWithLanguageGroups($data) {

    $results = null;
    $langGroupId = null;

    if (@exists($data['id']) && (int)$data['id'] !== 0) {

      $item = $this->model->getOne($data);

      if (@exists($item) && $item) {

        $langGroupId = $this->setLanguageGroupId($item);

        $langGroupIdParams = $this->setLanguageGroupIdParams($langGroupId, $data);

        $results = $this->model->getByLanguageGroupId($langGroupIdParams);

        foreach ($results as $result) {

          $this->setItemProperties($result);
          $this->setItemComments($result, Conf::get('comment_type_id')['article']);
        }
      }
    }

    return $this->setItemWithLanguageGroupsResponse($results, $langGroupId);
  }


  /************************************ ACTIONS ************************************/


  public function insert($data) {

    if ((string)$data['lang_group_id'] === '' || (int)$data['lang_group_id'] === 0) {
      unset($data['lang_group_id']);
    }

    if (@exists($data['event_date'])) {
      $data['event_date'] = date_format(date_create($data['event_date']), 'Y-m-d');
    }

    if (@exists($data['publish_date'])) {
      $data['publish_date'] = date_format(date_create($data['publish_date']), 'Y-m-d');
    }

    $data['created_by'] = $this->getLoggedInUserId();

    $alias = filterUrl($data['title']);
    $parent_id = $data['category_id'];

    $aliases = $this->model->getAliasesByParentId(array('parent_id' => $parent_id));

    $data['alias'] = $this->setAlias($alias, $aliases);

    $this->model->insert($data);

    return $this->model->lastInsertId();
  }


  public function update($data) {

    if ((string)$data['lang_group_id'] === '' || (int)$data['lang_group_id'] === 0) {
      unset($data['lang_group_id']);
    }

    if (@exists($data['event_date'])) {
      $data['event_date'] = date_format(date_create($data['event_date']), 'Y-m-d');
    }

    if (@exists($data['publish_date'])) {
      $data['publish_date'] = date_format(date_create($data['publish_date']), 'Y-m-d');
    }

    $data['updated_by'] = $this->getLoggedInUserId();

    $alias = filterUrl($data['title']);
    $parent_id = $data['category_id'];
    $id = $data['id'];

    $aliases = $this->model->getAliasesByParentId(array('parent_id' => $parent_id));

    $data['alias'] = $this->setAlias($alias, $aliases, $id);

    return $this->model->update($data);
  }


  public function updatePosition($data) {

    $this->model->updatePositions($data['id'], $data['position']);

    return $this->model->getOne(array('id' => $data['id'], 'fetchWithUnpublished' => true));
  }


  /************************************ OTHER ************************************/


  private function buildUrl($item) {

    $categories = Categories::getCategories();

    $url = '';
    if (@exists($item->alias)) $url = $item->alias;

    if (@exists($item->category_id)) {

      $categoryId = $item->category_id;
      foreach ($categories as $cat) {
        if ($cat->id == $categoryId) $category = $cat;
      }

      if (@exists($category)) {

        if ($category->alias) $url = $category->alias . '/' . $url;

        while ($category->parent_id != 0) {

          foreach ($categories as $cat) {

            if ($cat->id == $category->parent_id) {

              $url = $cat->alias . '/' . $url;
              $category = $cat;
            }
          }
        }
      }
    }

    return Conf::get('url') . '/' . $url;
  }


  private function setItemProperties($item, $data = null) {

    if (@exists($item) && $item != false) {

      if (!@exists($item->published)) {
        $item->published = 0;
      }

      $item->url = $this->buildUrl($item);

      $item->gallery = $this->setGallery($item);

      if (@exists($data) && @exists($data['shortcodes'])) {
        $gs = GalleriesServices::Instance();
        if ($gs instanceof GalleriesServices) $galleriesServices = $gs;
        $item->content = $galleriesServices->parseGalleryShortcodes($item->content);
      }
    }

    return $item;
  }
}

?>