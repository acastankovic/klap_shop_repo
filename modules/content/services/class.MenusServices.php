<?php


class MenusServices extends Service {

  private $menusModel;
  private $menuItemsModel;
  private $menuTypesModel;

  public function __construct() {

    $menusModel = Menus::Instance();

    if ($menusModel instanceof Menus) {
      $this->menusModel = $menusModel;
    }


    $menuItemsModel = MenuItems::Instance();

    if ($menuItemsModel instanceof MenuItems) {
      $this->menuItemsModel = $menuItemsModel;
    }


    $menuTypesModel = MenuTypes::Instance();

    if ($menuTypesModel instanceof MenuTypes) {
      $this->menuTypesModel = $menuTypesModel;
    }
  }


  /************************************ LOAD ************************************/


  public function loadOne($data) {

    $result = $this->menusModel->getOne($data);

    if (@exists($result) && $result != false) {
      $result->items = $this->menuItemsModel->getByMenuId(array('menu_id' => $result->id));
    }
    return $result;
  }


  public function loadOneWithItems($data) {

    $result = false;

    if (@exists($data) && @exists($data['id'])) {

      if (!Languages::enabled()) {

        $result = $this->loadOne($data);

      } else {

        $data = $this->setLangGroupIdFromId($data);

        $result = $this->menusModel->getByLanguageGroupIdAndLanguageId($data);

        if (@exists($result) && $result != false) {
          $result->items = $this->menuItemsModel->getByMenuId(array('menu_id' => $result->id));
        }
      }


      if (@exists($result) && $result != false) {

        if (@exists($result->items)) {

          $fetchWithUnpublished = @exists($data) && @exists($data['fetchWithUnpublished']) ? $data['fetchWithUnpublished'] : null;

          $categories = @exists($data['categories']) ? $data['categories'] : Categories::getCategories($result->lang_id);
          $articles = @exists($data['articles']) ? $data['articles'] : Dispatcher::instance()->dispatch('content', 'articles', 'fetchByLanguageId', array('lang_id' => $result->lang_id, 'fetchWithUnpublished' => $fetchWithUnpublished));

          foreach ($result->items as $item) {
            $item->target = $this->loadTarget($item, $categories, $articles);
          }
        }
      }
    }

    if ($result === false) $result = null;
    return $result;
  }


  public function loadItemsChildren($data) {


    $parentId = $data['id'];

    if (@exists($data['menu_id'])) {

      $results = $this->menuItemsModel->getByMenuId(array('menu_id' => $data['menu_id']));

    } else {

      $results = $this->menuItemsModel->getAll();
    }

//        $children = array();
//        foreach($results as $result) {
//
//            if(@exists($result)) {
//
//                if($this->isChildOf($results, $result, $parentId)) {
//                    array_push($children, $result);
//                }
//            }
//        }


    $nodeTree = formTree($results, $parentId);

    $childrenNodes = array();

    if (@exists($nodeTree[0]['children'])) {

      $children = $nodeTree[0]['children'];
      $childrenNodes = $this->findNodesChildren($children);
    }

    return $childrenNodes;
  }


  public function loadOneWithLanguageGroups($data) {

    $results = null;
    $langGroupId = null;

    if (@exists($data['id']) && (int)$data['id'] !== 0) {

      $item = $this->loadOne($data);

      if (@exists($item) && $item) {

        $langGroupId = $this->setLanguageGroupId($item);

        $langGroupIdParams = $this->setLanguageGroupIdParams($langGroupId, $data);

        $results = $this->menusModel->getByLanguageGroupId($langGroupIdParams);
      }
    }

    return $this->setItemWithLanguageGroupsResponse($results, $langGroupId);
  }


  public function loadTypes() {

    $currentAlias = Trans::getLanguageAlias();

    $results = $this->menuTypesModel->getAll();

    foreach ($results as $result) {

      $result->nameTranslated = $result->name;

      if (@exists($result->translations)) {

        $translations = json_decode($result->translations);

        if (!is_array($translations)) $translations = (array)$translations;

        $result->nameTranslated = $translations[$currentAlias];
      }
    }

    return $results;
  }

  /************************************ ACTIONS ************************************/


  public function insert($data) {

    if ((string)$data['lang_group_id'] === '' || (int)$data['lang_group_id'] === 0) {
      unset($data['lang_group_id']);
    }

    $data['created_by'] = $this->getLoggedInUserId();

    $this->menusModel->insert($data);
  }


  public function update($data) {

    if ((string)$data['lang_group_id'] === '' || (int)$data['lang_group_id'] === 0) {
      unset($data['lang_group_id']);
    }

    $data['updated_by'] = $this->getLoggedInUserId();

    return $this->menusModel->update($data);
  }


  /*************************************************************************
   *                                MENU ITEMS                              *
   *************************************************************************/


  public function insertItem($data) {

    $data['created_by'] = $this->getLoggedInUserId();

    $this->menuItemsModel->insert($data);
  }


  public function updateItem($data) {

    $data['updated_by'] = $this->getLoggedInUserId();

    $this->menuItemsModel->update($data);
  }


  public function deleteItem($data) {

    $children = $this->loadItemsChildren($data);

    if (!empty($children)) {

      foreach ($children as $child) {

        if (is_array($child)) $child = (object)$child;

        $this->menuItemsModel->delete($child->id);
      }
    }

    return $this->menuItemsModel->delete($data['id']);
  }


  public function updateItemsPosition($data) {

    $this->menuItemsModel->updateParentId($data);
    $this->menuItemsModel->updatePositions($data);

    return $this->menuItemsModel->getOne($data);
  }


  /************************************ OTHER ************************************/


  public function loadTarget($item, $categories, $articles) {

    switch ($item->type) {

      case Conf::get('menu_item_type_id')['article']:
        return $this->buildArticleUrl($categories, $articles, $item->target_id);

      case Conf::get('menu_item_type_id')['category']:
        return $this->buildCategoryUrl($categories, $item->target_id);

      case Conf::get('menu_item_type_id')['external_link']:
        return $item->url;

      default:
        return '-';
    }
  }


  public function buildArticleUrl($categories, $articles, $id) {

    $alias = '';
    $article = null;
    foreach ($articles as $art) {
      if ($art->id == $id) $article = $art;
    }

    if (@exists($article)) {

      $alias = $article->alias;
      $parentId = $article->category_id;

      $maxCount = 20;
      while ((int)$parentId !== 0 && $maxCount > 0) {
        $category = null;
        foreach ($categories as $cat) {
          if ((int)$cat->id === (int)$parentId) {
            $category = $cat;
          }
        }

        if (@exists($category)) {
          $parentId = $category->parent_id;
          $alias = $category->alias . '/' . $alias;
        }

        $maxCount--;
      }
    }

    return Conf::get('url') . '/' . $alias;
  }


  public function buildCategoryUrl($categories, $id) {

    $alias = '';
    $category = null;
    $maxCount = 20;
    while ((int)$id !== 0 && $maxCount > 0) {
      foreach ($categories as $cat) {
        if ($cat->id == $id) {
          $category = $cat;
          $id = $category->parent_id;
          $alias = $category->alias . '/' . $alias;
        }
      }
      $maxCount--;
    }

    if ($maxCount == 0) Logger::put('Cat url build error, cat id $id, alias $alias');
    return Conf::get('url') . '/' . $alias;
  }


  private function findNodesChildren($items, $nodes = null) {

    if (!@exists($nodes)) {
      $nodes = array();
    }

    foreach ($items as $item) {

      array_push($nodes, $item);

      if (@exists($item['children'])) {

        $nodes = $this->findNodesChildren($item['children'], $nodes);
      }
    }

    return $nodes;
  }


  private function isChildOf($items, $childItem, $parentId) {

    if ((int)$parentId === 0) return true;
    else if ((int)$parentId === (int)$childItem->id) return true;
    else if ((int)$childItem->id === 0) return false;
    else {
      $newChildItem = new stdClass();
      $newChildItem->id = 0;
      foreach ($items as $item) {
        if ((int)$item->id === (int)$childItem->parent_id) {
          $newChildItem = $item;
        }
      }
      return $this->isChildOf($items, $newChildItem, $parentId);
    }
  }
}

?>