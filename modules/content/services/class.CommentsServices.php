<?php


class CommentsServices extends Service {

  private $model;

  public function __construct() {

    $model = Comments::Instance();

    if ($model instanceof Comments) {
      $this->model = $model;
    }
  }


  /************************************ LOAD ************************************/


  public function loadOne($data) {

    return $this->model->getOne($data);
  }


  public function loadAll() {

    return $this->model->getAll();
  }


  public function loadItemsChildren($data) {

    $results = $this->loadAll();

    $nodeTree = formTree($results, $data['id']);

    $childrenNodes = array();

    if (@exists($nodeTree[0]['children'])) {

      $children = $nodeTree[0]['children'];
      $childrenNodes = $this->findNodesChildren($children);
    }

    return $childrenNodes;
  }

  /************************************ ACTIONS ************************************/


  public function delete($data) {

    $children = $this->loadItemsChildren($data);

    if (!empty($children)) {

      foreach ($children as $child) {

        if (is_array($child)) $child = (object)$child;

        $this->model->delete($child->id);
      }
    }

    return $this->model->delete($data['id']);
  }


  public function publish($data) {

    // change published value
    $data['published'] = (int)$data['published'] === 1 ? 0 : 1;

    $this->model->update($data);

    return $this->model->getOne($data);
  }


  /************************************ OTHER ************************************/


  private function findNodesChildren($items, $nodes = null) {

    if (!@exists($nodes)) {
      $nodes = array();
    }

    foreach ($items as $item) {

      array_push($nodes, $item);

      if (@exists($item['children'])) {

        $nodes = $this->findNodesChildren($item['children'], $nodes);
      }
    }

    return $nodes;
  }
}

?>