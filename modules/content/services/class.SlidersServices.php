<?php


class SlidersServices extends Service {

  private $slidersModel;
  private $sliderItemsModel;

  public function __construct() {

    $slidersModel = Sliders::Instance();
    if ($slidersModel instanceof Sliders) {
      $this->slidersModel = $slidersModel;
    }

    $sliderItemsModel = SliderItems::Instance();
    if ($sliderItemsModel instanceof SliderItems) {
      $this->sliderItemsModel = $sliderItemsModel;
    }
  }


  /************************************ LOAD ************************************/


  public function loadOneWithItems($data) {

    $result = $this->slidersModel->getOne($data);
    $result->items = $this->loadItemsByParentId(array('parent_id' => $result->id));

    return $result;
  }


  public function loadOneWithLanguageGroups($data) {

    $results = null;
    $langGroupId = null;

    if (@exists($data['id']) && (int)$data['id'] !== 0) {

      $item = $this->slidersModel->getOne($data);

      if (@exists($item) && $item) {

        $langGroupId = $this->setLanguageGroupId($item);

        $langGroupIdParams = $this->setLanguageGroupIdParams($langGroupId, $data);

        $results = $this->slidersModel->getByLanguageGroupId($langGroupIdParams);
      }
    }

    return $this->setItemWithLanguageGroupsResponse($results, $langGroupId);
  }


  /*************************************************************************
  *                              SLIDER ITEMS                              *
  *************************************************************************/

  public function loadItemsByParentId($data) {

    if (Languages::enabled() && !@exists($data['fetchWithUnpublished'])) {

      $data = $this->setLangGroupIdFromParentId($data);

      $results = $this->sliderItemsModel->getByParentLangGroupIdAndLanguageId($data);

    } else {

      $results = $this->sliderItemsModel->getByParentId($data);
    }

    return $results;
  }


  public function loadItemsByParentIdAndLanguageId($data) {

    return $this->sliderItemsModel->getByParentIdAndLanguageId($data);
  }


  /************************************ ACTIONS ************************************/


  public function insert($data) {

    if ((string)$data['lang_group_id'] === '' || (int)$data['lang_group_id'] === 0) {
      unset($data['lang_group_id']);
    }

    $data['created_by'] = $this->getLoggedInUserId();

    if (!@exists($data['show_bullets'])) $data['show_bullets'] = 0;
    if (!@exists($data['show_arrows'])) $data['show_arrows'] = 0;

    $this->slidersModel->insert($data);
  }


  public function update($data) {

    if ((string)$data['lang_group_id'] === '' || (int)$data['lang_group_id'] === 0) {
      unset($data['lang_group_id']);
    }

    $data['updated_by'] = $this->getLoggedInUserId();

    if (!@exists($data['show_bullets'])) $data['show_bullets'] = 0;
    if (!@exists($data['show_arrows'])) $data['show_arrows'] = 0;

    return $this->slidersModel->update($data);
  }


  /*** SLIDER ITEMS ***/


  public function insertItem($data) {

    $data['created_by'] = $this->getLoggedInUserId();

    $this->sliderItemsModel->insert($data);

    return $this->sliderItemsModel->lastInsertId();
  }


  public function updateItem($data) {

    $data['updated_by'] = $this->getLoggedInUserId();

    return $this->sliderItemsModel->update($data);
  }
}

?>