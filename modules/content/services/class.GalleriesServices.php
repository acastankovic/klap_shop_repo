<?php


class GalleriesServices extends Service {

  private $model;

  public function __construct() {

    $model = Galleries::Instance();

    if ($model instanceof Galleries) {
      $this->model = $model;
    }
  }


  /************************************ LOAD ************************************/


  public function loadOne($data) {

    $result = $this->model->getOne($data);

    $this->setItemProperties($result);

    return $result;
  }


  public function loadOneForAdmin($data) {

    $result = $this->model->getOneForAdmin($data);

    $this->setItemProperties($result);

    return $result;
  }


  public function loadAll($data = null) {

    $results = $this->model->getAll($data);

    foreach ($results as $result) {
      $this->setItemProperties($result);
    }

    return $results;
  }


  /************************************ ACTIONS ************************************/


  public function insert($data) {

    $data['created_by'] = $this->getLoggedInUserId();

    $this->model->insert($data);

    return $this->model->lastInsertId();
  }


  public function update($data) {

    $data['updated_by'] = $this->getLoggedInUserId();

    return $this->model->update($data);
  }


  /************************************ OTHER ************************************/


  private function setItemProperties($item) {

    $item->gallery = $this->setGallery($item);

    if (@exists($item->gallery)) {

      $firstItem = $item->gallery[0];

      if ((string)$firstItem->type === 'media') {
        $item->thumb = $firstItem->thumb_url;
      }
      else if ((string)$firstItem->type === 'youtube_code') {
        $item->thumb = $firstItem->image_url;
      }
    }
  }


  public function parseGalleryShortcodes($content) {

    $mv = MainView::Instance();
    if ($mv instanceof MainView) $mainView = $mv;

    //parse for gallery shortcode
    $subject = $content;

    $pattern = '/{' . Conf::get('nc_gallery_label') . '=(.*?)}/';

    preg_match_all($pattern, $subject, $matches, PREG_PATTERN_ORDER);

    foreach ($matches[1] as $match) {

      $item = $this->loadOne(array('id' => $match));

      $gallery = $mainView->renderGallery($item->gallery);

      $content = str_replace('{' . Conf::get('nc_gallery_label') . '=' . $match . '}', $gallery, $content);
    }

    return $content;
  }
}

?>