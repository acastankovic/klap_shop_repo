<?php


class ContentApplication extends Application {

  protected $name = 'content';

  protected $resources = array('categories', 'articles', 'media', 'menus', 'sliders', 'galleries', 'newsletter');

  protected $routes = array(


    /*************** CATEGORIES ***************/

    //insert article
    array('route'      => 'categories/insert',
          'method'     => 'post',
          'controller' => 'categories',
          'action'     => 'insertCategory'),

    //update article
    array('route'      => 'categories/update',
          'method'     => 'put',
          'controller' => 'categories',
          'action'     => 'updateCategory'),

    // get category tree
    array('route'      => 'categories/tree/:root_id/lang/:lang_id',
          'method'     => 'get',
          'controller' => 'categories',
          'action'     => 'fetchTree'),

    // update category rang
    array('route'      => 'categories/:id/position',
          'method'     => 'put',
          'controller' => 'categories',
          'action'     => 'updatePosition'),

    /*************** ARTICLES ***************/

    // insert article
    array('route'      => 'articles/insert',
          'method'     => 'post',
          'controller' => 'articles',
          'action'     => 'insertArticle'),

    // update article
    array('route'      => 'articles/update',
          'method'     => 'put',
          'controller' => 'articles',
          'action'     => 'updateArticle'),

    // articles tree
    array('route'      => 'articles/parent_id/:parent_id',
          'method'     => 'get',
          'controller' => 'articles',
          'action'     => 'fetchByParentId'),

    // update category rang
    array('route'      => 'articles/:id/position',
          'method'     => 'put',
          'controller' => 'articles',
          'action'     => 'updatePosition'),

    /**************** MEDIA ****************/

    // upload media
    array('route'      => 'upload',
          'method'     => 'post',
          'controller' => 'media',
          'action'     => 'uploadMedia'),

    // create thumbs
    array('route'      => 'media/thumbs/create',
          'method'     => 'get',
          'controller' => 'media',
          'action'     => 'createThumbs'),

    array('route'      => 'media/children',
          'method'     => 'post',
          'controller' => 'media',
          'action'     => 'fetchByParentId'),

    array('route'      => 'media/parent/:parent_id',
          'method'     => 'get',
          'controller' => 'media',
          'action'     => 'fetchByParentId'),

    // search
    array('route'      => 'media/search',
          'method'     => 'post',
          'controller' => 'media',
          'action'     => 'fetchBySearch'),

    array('route'      => 'media/pagination',
          'method'     => 'post',
          'controller' => 'media',
          'action'     => 'fetchWithPagination'),


    /**************** MENUS ****************/

    // menu
    array('route'      => 'menus/language_groups/:id',
          'method'     => 'get',
          'controller' => 'menus',
          'action'     => 'fetchOneWithLanguageGroups'),

    // insert menu
    array('route'      => 'menus/insert',
          'method'     => 'post',
          'controller' => 'menus',
          'action'     => 'insertMenu'),

    // update menu
    array('route'      => 'menus/update',
          'method'     => 'put',
          'controller' => 'menus',
          'action'     => 'updateMenu'),

    // menus tree
    array('route'      => 'menu_items/tree/:id',
          'method'     => 'get',
          'controller' => 'menus',
          'action'     => 'fetchTree'),

    array('route'      => 'menu_items/insert',
          'method'     => 'post',
          'controller' => 'menus',
          'action'     => 'insertMenuItem'),

    array('route'      => 'menu_items/update',
          'method'     => 'put',
          'controller' => 'menus',
          'action'     => 'updateMenuItem'),

    array('route'      => 'menu_items/:id',
          'method'     => 'delete',
          'controller' => 'menus',
          'action'     => 'deleteMenuItem'),

    // update category rang
    array('route'      => 'menu_items/:id/position',
          'method'     => 'put',
          'controller' => 'menus',
          'action'     => 'updateItemsPosition'),

    /**************** SLIDERS ****************/

    // slider
    array('route'      => 'sliders/language_groups/:id',
          'method'     => 'get',
          'controller' => 'sliders',
          'action'     => 'fetchOneWithLanguageGroups'),

    // insert slider
    array('route'      => 'sliders/insert',
          'method'     => 'post',
          'controller' => 'sliders',
          'action'     => 'insertSlider'),

    // update slider
    array('route'      => 'sliders/update',
          'method'     => 'put',
          'controller' => 'sliders',
          'action'     => 'updateSlider'),

    // insert slider item
    array('route'      => 'slider_items/insert',
          'method'     => 'post',
          'controller' => 'sliders',
          'action'     => 'insertSliderItem'),

    // update slider item
    array('route'      => 'slider_items/update',
          'method'     => 'put',
          'controller' => 'sliders',
          'action'     => 'updateSliderItem'),

    // delete slider item
    array('route'      => 'slider_items/:id',
          'method'     => 'delete',
          'controller' => 'sliders',
          'action'     => 'deleteSliderItem'),

    array('route'      => 'slider_items/tree/:parent_id',
          'method'     => 'get',
          'controller' => 'sliders',
          'action'     => 'fetchTree'),

    array('route'      => 'slider_items/:id/position',
          'method'     => 'put',
          'controller' => 'sliders',
          'action'     => 'updateItemsPosition'),


    /*************** GALLERIES ***************/

    array('route'      => 'galleries/insert',
          'method'     => 'post',
          'controller' => 'galleries',
          'action'     => 'insertGallery'),

    array('route'      => 'galleries/update',
          'method'     => 'put',
          'controller' => 'galleries',
          'action'     => 'updateGallery'),


    /************** LANGUAGES **************/

    array('route'      => 'languages/set',
          'method'     => 'post',
          'controller' => 'languages',
          'action'     => 'setLanguage'),

    /*************** COMMENTS ***************/

    // delete comment
    array('route'      => 'comments/:id',
          'method'     => 'delete',
          'controller' => 'comments',
          'action'     => 'deleteComment'),

    //publish category
    array('route'      => 'comments/publish',
          'method'     => 'put',
          'controller' => 'comments',
          'action'     => 'publishComment'),


    /*************** NEWSLETTER ***************/

    array('route'      => 'newsletter/download',
          'method'     => 'get',
          'controller' => 'newsletter',
          'action'     => 'download'),
  );
}

?>