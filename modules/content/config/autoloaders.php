<?php


class ContentAutoloaders {

  public static function autoload_controllers($class_name) {

    $file = Conf::get("root") . '/modules/content/controllers/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }

  public static function autoload_models($class_name) {

    $file = Conf::get("root") . '/modules/content/models/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }

  public static function autoload_services($class_name) {

    $file = Conf::get("root") . '/modules/content/services/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }
}

spl_autoload_register('ContentAutoloaders::autoload_controllers');
spl_autoload_register('ContentAutoloaders::autoload_models');
spl_autoload_register('ContentAutoloaders::autoload_services');
require_once(Conf::get("root") . '/modules/content/app.php');

//register module
$item = new stdClass();
$item->title = "Content";
$item->alias = "content";
$item->root = false;
$modules = Conf::get('modules');
array_push($modules, $item);
Conf::set('modules', $modules);