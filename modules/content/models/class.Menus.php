<?php


class Menus extends Model {

  private $selectQueryString;

  public function __construct() {
    parent::__construct();
    $this->setTable('menus');
    $this->setQueryStrings();
  }


  /************************************ FETCH ************************************/


  public function getOne($data) {

    if (!isset($data) || !isset($data['id'])) return null;

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `m`.`id` = :id';

    return $this->exafe($sql, array('id' => $data['id']));
  }
    

  public function getAll() {

    $sql = $this->selectQueryString;

    return $this->exafeAll($sql);
  }


  public function getByLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `m`.`lang_id` = :lang_id';

    return $this->exafeAll($sql, array('lang_id' => $langId));
  }


  public function getByLanguageGroupId($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `m`.`id` = :lang_group_id || `m`.`lang_group_id` = :lang_group_id';

    return $this->exafeAll($sql, array('lang_group_id' => $data['lang_group_id']));
  }


  public function getByLanguageGroupIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`m`.`id` = :lang_group_id || `m`.`lang_group_id` = :lang_group_id) AND `m`.`lang_id` = :lang_id';

    return $this->exafe($sql, array('lang_group_id' => $data['lang_group_id'], 'lang_id' => $langId));
  }


  /************************************ OTHER ************************************/


  private function setQueryStrings() {

    $this->selectQueryString = 'SELECT `m`.*, 
                                `l`.`name` AS `language_name`, `l`.`alias` AS `language_alias`, 
                                `uc`.`username` AS created_by_username, 
                                `uu`.`username` AS update_by_username 
                                FROM `menus` AS `m` 
                                LEFT JOIN  `languages` AS `l` ON `l`.`id` = `m`.`lang_id` 
                                LEFT JOIN `users` AS `uc` ON `uc`.`id` = `m`.`created_by`
                                LEFT JOIN `users` AS `uu` ON `uu`.`id` = `m`.`updated_by`';
  }
}

?>