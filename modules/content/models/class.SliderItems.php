<?php


class SliderItems extends Model {

  private $selectQueryString;

  public function __construct() {
    parent::__construct();
    $this->setTable('slider_items');
    $this->setQueryString();
  }


  /************************************ FETCH ************************************/


  public function getOne($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `si`.`id` = :id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `si`.`published` = 1';
    }

    return $this->exafe($sql, array('id' => $data['id']));
  }


  public function getAll($data) {

    $sql = $this->selectQueryString;

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' WHERE `si`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    return $this->exafeAll($sql);
  }


  public function getByParentId($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `si`.`slider_id` = :slider_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `si`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    return $this->exafeAll($sql, array('slider_id' => $data['parent_id']));
  }


  public function getByParentIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `si`.`slider_id` = :slider_id AND `s`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `si`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    return $this->exafeAll($sql, array('slider_id' => $data['parent_id'], 'lang_id' => $langId));
  }


  public function getByParentLangGroupIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`s`.`id` = :lang_group_id || `s`.`lang_group_id` = :lang_group_id) AND `s`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `si`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    return $this->exafeAll($sql, array('lang_group_id' => $data['lang_group_id'], 'lang_id' => $langId));
  }


  public function updatePositions($data) {

    $slider_item = $this->getOne($data);

    $sql = 'SELECT * FROM `slider_items` WHERE `slider_id` = :slider_id AND `id` != :id ORDER BY rang';

    $stm = $this->execute($sql, array('slider_id' => $slider_item->slider_id, 'id' => $data['id']));

    $items = $stm->fetchAll(PDO::FETCH_OBJ);

    $this->update(array('id' => $data['id'], 'rang' => $data['position']));

    $counter = 0;
    foreach ($items as $item) {

      if ((int)$counter === (int)$data['position']) {
        $counter++;
      }

      if ($counter != $item->rang) {
        //update
        $this->update(array('id' => $item->id, 'rang' => $counter));
      }

      $counter++;
    }
  }


  /************************************ OTHER ************************************/


  private function setQueryString() {

    $this->selectQueryString = 'SELECT `si`.*, 
                                `s`.`name` AS `slider_name`, `s`.`lang_id` AS `slider_lang_id`, `s`.`lang_group_id` AS `slider_lang_group_id`,
                                `uc`.`username` AS created_by_username, 
                                `uu`.`username` AS update_by_username   
                                FROM `slider_items` `si` 
                                LEFT JOIN `sliders` AS `s` ON `s`.`id` = `si`.`slider_id`
                                LEFT JOIN `users` AS `uc` ON `uc`.`id` = `si`.`created_by`
                                LEFT JOIN `users` AS `uu` ON `uu`.`id` = `si`.`updated_by`';
  }


  private function getOrderByString($data) {

    if (@exists($data['order_by'])) {
      return $this->setOrderByString($data['order_by'], 'si');
    }
    return ' ORDER BY `si`.`rang`, `si`.`id`';
  }
}

?>