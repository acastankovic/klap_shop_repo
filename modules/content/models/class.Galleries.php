<?php


class Galleries extends Model {

  public function __construct() {
    parent::__construct();
    $this->setTable('galleries');
  }


  /************************************ FETCH ************************************/


  public function getOne($data) {

    $sql = 'SELECT * FROM `galleries` WHERE id = :id';

    return $this->exafe($sql, array('id' => $data['id']));
  }

  public function getOneForAdmin($data) {

    $sql = 'SELECT `g`.*,
            `uc`.`username` AS `created_by_username`,
            `uu`.`username` AS `update_by_username`
            FROM `galleries` AS `g`
            LEFT JOIN `users` AS `uc` ON `uc`.`id` = `g`.`created_by`
            LEFT JOIN `users` AS `uu` ON `uu`.`id` = `g`.`updated_by`
            WHERE `g`.`id` = :id';

    return $this->exafe($sql, array('id' => $data['id']));
  }

  public function getAll($data = null) {

    $sql = 'SELECT * FROM `galleries`';

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, null);
  }
}

?>