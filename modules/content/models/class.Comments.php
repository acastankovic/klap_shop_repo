<?php


class Comments extends Model {

  public function __construct() {
    parent::__construct();
    $this->setTable('comments');
  }


  /************************************ FETCH ************************************/

  public function getOne($data) {

    $sql = 'SELECT * FROM `comments` WHERE `id` = :id';

    return $this->exafe($sql, array('id' => $data['id']));
  }


  public function getAll() {

    $sql = 'SELECT * FROM `comments`';

    return $this->exafeAll($sql);
  }


  public function getByTypeIdAndTargetId($data) {

    $sql = 'SELECT * FROM `comments` WHERE `type_id` = :type_id AND `target_id` = :target_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `published` = 1';
    }

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('type_id' => $data['type_id'], 'target_id' => $data['target_id']));
  }


  public function publish($data) {

    $sql = 'UPDATE `comments` SET `published` = :published WHERE `id` = :id';
    return $this->execute($sql, array('published' => $data['published'], 'id' => $data['id']));
  }
}

?>