<?php


class Newsletter extends Model {

  public function __construct() {
    parent::__construct();
    $this->setTable('newsletter');
  }


  /************************************ FETCH ************************************/


  public function getOne($data) {

    $sql = 'SELECT * FROM `newsletter` WHERE `email` = :id';

    return $this->exafe($sql, array('id' => $data['id']));
  }


  public function getAll($data = null) {

    $sql = 'SELECT * FROM `newsletter`';
    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql);
  }


  public function getByEmail($params) {

    $sql = 'SELECT * FROM `newsletter` WHERE `email` = :email';

    return $this->exafe($sql, array('email' => $params['email']));
  }
}

?>