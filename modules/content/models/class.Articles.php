<?php


class Articles extends Model {

  private $selectQueryString;
  private $orderByString;

  public function __construct() {
    parent::__construct();
    $this->setTable('articles');
    $this->setQueryString();
  }


  /************************************ FETCH ************************************/


  public function getOne($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `a`.`id` = :id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `a`.`published` = 1';
    }

    return $this->exafe($sql, array('id' => $data['id']));
  }


  public function getAll($data = null) {

    $sql = $this->selectQueryString;

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' WHERE `a`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql);
  }


  public function getByAlias($data, $adminLogged) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `a`.`category_id` = :category_id AND `a`.`alias` = :alias';
    if (!$adminLogged) $sql .= ' AND `a`.`published` = 1';

    return $this->exafe($sql, array('category_id' => $data['parent_id'], 'alias' => $data['alias']));
  }


  public function getByParentId($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `a`.`category_id` = :category_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `a`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('category_id' => $data['parent_id']));
  }


  public function getByCategoryParentId($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `c1`.`parent_id` = :category_parent_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `a`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('category_parent_id' => $data['parent_id']));
  }


  public function getBySearch($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`a`.`title` LIKE :search OR `a`.`subtitle` LIKE :search OR `a`.`content` LIKE :search)';

    if (@exists($data['parent_id'])) {
      $sql .= ' AND `a`.`category_id` = :parent_id';
    }

    if (@exists($data['lang_id'])) {
      $sql .= ' AND `a`.`lang_id` = :lang_id';
    }

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `a`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $stm = $this->dbh->prepare($sql);

    $stm->bindValue(':search', (string)'%' . $data['search'] . '%', PDO::PARAM_STR);

    if (@exists($data['parent_id'])) {
      $stm->bindValue(':parent_id', (int)$data['parent_id'], PDO::PARAM_INT);
    }

    if (@exists($data['lang_id'])) {
      $stm->bindValue(':lang_id', (int)$data['lang_id'], PDO::PARAM_INT);
    }

    $stm->execute();

    return $stm->fetchAll(PDO::FETCH_OBJ);
  }


  public function getAliasesByParentId($data) {

    $sql = 'SELECT `id`, `alias` FROM `articles` WHERE `category_id` = :category_id;';
    return $this->exafeAll($sql, array('category_id' => $data['parent_id']));
  }


  public function getByLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `a`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `a`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_id' => $langId));
  }


  public function getByLanguageGroupId($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `a`.`id` = :lang_group_id || `a`.`lang_group_id` = :lang_group_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `a`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_group_id' => $data['lang_group_id']));
  }


  public function getByLanguageGroupIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`a`.`id` = :lang_group_id || `a`.`lang_group_id` = :lang_group_id) AND `a`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `a`.`published` = 1';
    }

    return $this->exafe($sql, array('lang_group_id' => $data['lang_group_id'], 'lang_id' => $langId));
  }


  public function getByParentIdAndLanguageGroupIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`c1`.`id` = :lang_group_id || `c1`.`lang_group_id` = :lang_group_id) AND `c1`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `a`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_group_id' => $data['lang_group_id'], 'lang_id' => $langId));
  }


  public function getByParentsParentIdAndLanguageGroupIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`c2`.`id` = :lang_group_id || `c2`.`lang_group_id` = :lang_group_id) AND `c2`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `a`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_group_id' => $data['lang_group_id'], 'lang_id' => $langId));
  }


  public function getParentIds($data) {

    $langId = $this->setLangId($data);

    $sql = 'SELECT DISTINCT `category_id` FROM `articles` WHERE `category_id` != 0 && `category_id` IS NOT NULL';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `a`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_id' => $langId));
  }


  public function getForAdminTables() {

    $sql = 'SELECT `a`.`id`, `a`.`title`, `a`.`published`, `c`.`name` AS `parent_name`, `l`.`name` AS `language_name` FROM `articles` AS `a` LEFT JOIN `categories` AS `c` ON `c`.`id` = `a`.`category_id` LEFT JOIN `languages` AS `l` ON `l`.`id` = `a`.`lang_id`';
    return $this->exafeAll($sql);
  }


  public function updatePositions($id, $position) {

    $article = $this->getOne(array('id' => $id, 'fetchWithUnpublished' => true));

    $sql = 'SELECT * FROM `articles` WHERE `category_id` = :parent_id AND `id` <> :id ORDER BY `rang`';
    $articles = $this->exafeAll($sql, array('parent_id' => $article->category_id, 'id' => $id));

    $this->update(array('id' => $id, 'rang' => $position));

    $counter = 0;
    foreach ($articles as $article) {

      if ($counter == $position) {
        $counter++;
      }

      if ($counter != $article->rang) {

        $this->update(array('id' => $article->id, 'rang' => $counter));
      }

      $counter++;
    }

    return $this->getOne(array('id' => $id, 'fetchWithUnpublished' => true));
  }

  /************************************ OTHER ************************************/


  private function setQueryString() {

    $this->selectQueryString = 'SELECT `a`.*, 
                                `c1`.`id` AS `parent_id`, `c1`.`name` AS `parent_name`, `c1`.`lang_group_id` AS `parent_lang_group_id`, `c1`.`lang_id` AS `parent_lang_id`,
                                `c2`.`id` AS `categories_parent_id`, `c2`.`name` AS `categories_parent_name`, `c2`.`parent_id` AS `categories_parent_parent_id`, `c2`.`lang_group_id` AS `categories_parent_lang_group_id`, `c2`.`lang_id` AS `categories_parent_lang_id`,
                                `l`.`name` AS `language_name`, `l`.`alias` AS `language_alias`,
                                `uc`.`username` AS `created_by_username`,
                                `uu`.`username` AS `update_by_username`
                                FROM `articles` AS `a` 
                                LEFT JOIN `categories` AS `c1` ON `a`.`category_id` = `c1`.`id` 
                                LEFT JOIN `categories` AS `c2` ON `c1`.`parent_id` = `c2`.`id` 
                                LEFT JOIN `languages` AS `l` ON `l`.`id` = `a`.`lang_id`
                                LEFT JOIN `users` AS `uc` ON `uc`.`id` = `a`.`created_by`
                                LEFT JOIN `users` AS `uu` ON `uu`.`id` = `a`.`updated_by`';
  }


  private function getOrderByString($data) {

    if (@exists($data['order_by'])) {
      return $this->setOrderByString($data['order_by'], 'a');
    }
    return ' ORDER BY `a`.`rang`, `a`.`id` DESC';
  }

}

?>