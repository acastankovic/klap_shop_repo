<?php


class Sliders extends Model {

  private $selectQueryString;

  public function __construct() {
    parent::__construct();
    $this->setTable('sliders');
    $this->setQueryString();
  }


  /************************************ FETCH ************************************/


  public function getOne($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `s`.`id` = :id';

    return $this->exafe($sql, array('id' => $data['id']));
  }


  public function getAll() {

    $sql = $this->selectQueryString;

    return $this->exafeAll($sql);
  }


  public function getByLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `s`.`lang_id` = :lang_id';

    return $this->exafeAll($sql, array('lang_id' => $langId));
  }


  public function getByLanguageGroupId($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `s`.`id` = :lang_group_id || `s`.`lang_group_id` = :lang_group_id';

    return $this->exafeAll($sql, array('lang_group_id' => $data['lang_group_id']));
  }


  public function getByLanguageGroupIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`s`.`id` = :lang_group_id || `s`.`lang_group_id` = :lang_group_id) AND `s`.`lang_id` = :lang_id';

    return $this->exafe($sql, array('lang_group_id' => $data['lang_group_id'], 'lang_id' => $langId));
  }


  /************************************ OTHER ************************************/


  private function setQueryString() {

    $this->selectQueryString = 'SELECT `s`.*, 
                                `l`.`name` AS `language_name`, `l`.`alias` AS `language_alias` 
                                FROM `sliders` AS `s` 
                                LEFT JOIN `languages` AS `l` ON `s`.`lang_id` = `l`.`id`';
  }
}

?>