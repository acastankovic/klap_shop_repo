<?php


class Categories extends Model {

  static public $categories;
  static public $db;

  private $selectQueryString;
  private $orderByString;

  public function __construct() {
    parent::__construct();
    $this->setTable('categories');
    $this->setQueryString();
  }


  /************************************ FETCH ************************************/


  private static function setDBConn() {

    if (!isset(self::$db)) self::$db = DB::Connect();

    return self::$db;
  }


  public static function getCategories($langId = null) {

    $getCategories = false;

    if (isset($langId)) {
      $getCategories = true;
    }

    if (!isset(self::$categories)) {
      $getCategories = true;
    }

    if ($getCategories) {

      self::setDBConn();

      try {

        $sql = 'SELECT `c1`.*, 
                `c2`.`id` AS `parent_category_id`,  `c2`.`name` AS `parent_category_name`, `c2`.`alias` AS `parent_category_alias`, `c2`.`lang_group_id` AS `parent_lang_group_id`,
                `l`.`name` AS `language_name`, `l`.`alias` AS `language_alias`,
                `uc`.`username` AS created_by_username,
                `uu`.`username` AS update_by_username
                FROM `categories` `c1` 
                LEFT JOIN `categories` `c2` ON `c1`.`parent_id` = `c2`.`id` 
                LEFT JOIN `languages` AS `l` ON `l`.`id` = `c1`.`lang_id`
                LEFT JOIN `users` AS `uc` ON `uc`.`id` = `c1`.`created_by`
                LEFT JOIN `users` AS `uu` ON `uu`.`id` = `c1`.`updated_by`';

        if (isset($langId)) $sql .= ' WHERE `c1`.`lang_id` = :lang_id';

        $sql .= ' ORDER BY `c1`.`rang`';

        $stm = self::$db->prepare($sql);

        if (isset($langId)) $stm->bindValue(':lang_id', (int)$langId, PDO::PARAM_INT);

        $stm->execute();

        $categories = self::fetchAll($stm);

        foreach ($categories as $category) {
          $category->url = self::buildUrl($category, $categories);
        }

        self::$categories = $categories;

        return self::$categories;
      }
      catch (PDOException $e) {
        self::HandleDBError($e);
        return false;
      }
    }
    else return self::$categories;
  }


  public function getOne($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `c1`.`id` = :id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `c1`.`published` = 1';
    }

    return $this->exafe($sql, array('id' => $data['id']));
  }


  public function getAll($data = null) {

    $sql = $this->selectQueryString;

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' WHERE `c1`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql);
  }


  public function getByAlias($data, $adminLogged) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `c1`.`parent_id` = :parent_id AND `c1`.`alias` = :alias';
    if (!$adminLogged) $sql .= ' AND `c1`.`published` = 1';

    return $this->exafe($sql, array('parent_id' => $data['parent_id'], 'alias' => $data['alias']));
  }


  public function getByParentId($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `c1`.`parent_id` = :parent_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `c1`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('parent_id' => $data['parent_id']));
  }


  public function getBySearch($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`c1`.`name` like :search or `c1`.`subtitle` like :search or `c1`.`content` like :search)';

    if (@exists($data['lang_id'])) {
      $sql .= ' AND `c1`.`lang_id` = :lang_id';
    }

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `c1`.`published` = 1';
    }

    $stm = $this->dbh->prepare($sql);

    $stm->bindValue(':search', (string)'%' . $data['search'] . '%', PDO::PARAM_STR);

    if (@exists($data['lang_id'])) {
      $stm->bindValue(':lang_id', (int)$data['lang_id'], PDO::PARAM_INT);
    }

    $stm->execute();

    return $stm->fetchAll(PDO::FETCH_OBJ);
  }


  public function getByIds($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `c1`.`id` in (' . $data['ids'] . ')';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `c1`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql);
  }


  public function getForAdminTables() {

    $sql = 'SELECT `c1`.`id`, `c1`.`name`, `c1`.`published`, `c2`.`name` AS `parent_name`, `l`.`name` AS `language_name` FROM `categories` AS `c1`  LEFT JOIN `categories` AS `c2` ON `c2`.`id` = `c1`.`parent_id` LEFT JOIN `languages` AS `l` ON `l`.`id` = `c1`.`lang_id`';
    return $this->exafeAll($sql);
  }


  public function getAliasesByParentId($data) {

    $sql = 'SELECT `id`, `alias` FROM `categories` WHERE `parent_id` = :parent_id';
    return $this->exafeAll($sql, array('parent_id' => $data['parent_id']));
  }


  public function getByLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `c1`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `c1`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_id' => $langId));
  }


  public function getByLanguageGroupId($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `c1`.`id` = :lang_group_id || `c1`.`lang_group_id` = :lang_group_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `c1`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_group_id' => $data['lang_group_id']));
  }


  public function getByLanguageGroupIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`c1`.`id` = :lang_group_id || `c1`.`lang_group_id` = :lang_group_id) AND `c1`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `c1`.`published` = 1';
    }

    return $this->exafe($sql, array('lang_group_id' => $data['lang_group_id'], 'lang_id' => $langId));
  }


  public function getByParentIdAndLanguageGroupIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`c2`.`id` = :lang_group_id || `c2`.`lang_group_id` = :lang_group_id) AND `c2`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `c1`.`published` = 1';
    }
    else {
      $sql .= ' ORDER BY `c1`.`rang`';
    }

    return $this->exafeAll($sql, array('lang_group_id' => $data['lang_group_id'], 'lang_id' => $langId));
  }


  /************************************ ACTIONS ************************************/


  public function updateParentId($id, $parent_id) {

    return $this->update(array('id' => $id, 'parent_id' => $parent_id));
  }


  public function updatePositions($id, $position) {

    $category = $this->getOne(array('id' => $id, 'fetchWithUnpublished' => true));
    $sql = 'SELECT * FROM `categories` WHERE `parent_id` = :parent_id AND `id` <> :id ORDER BY `rang`';
    $categories = $this->exafeAll($sql, array('parent_id' => $category->parent_id, 'id' => $id));

    $this->update(array('id' => $id, 'rang' => $position));

    $counter = 0;
    foreach ($categories as $category) {

      if ($counter == $position) {
        $counter++;
      }

      if ($counter != $category->rang) {

        $this->update(array('id' => $category->id, 'rang' => $counter));
      }

      $counter++;
    }
  }


  /************************************ OTHER ************************************/


  private function setQueryString() {

    $this->selectQueryString = 'SELECT `c1`.*, 
                                `c2`.`id` AS `parent_category_id`,  `c2`.`name` AS `parent_category_name`, `c2`.`alias` AS `parent_category_alias`, `c2`.`lang_group_id` AS `parent_lang_group_id`,
                                `l`.`name` AS `language_name`, `l`.`alias` AS `language_alias`,
                                `uc`.`username` AS created_by_username,
                                `uu`.`username` AS update_by_username
                                FROM `categories` `c1` 
                                LEFT JOIN `categories` `c2` ON `c1`.`parent_id` = `c2`.`id` 
                                LEFT JOIN `languages` AS `l` ON `l`.`id` = `c1`.`lang_id`
                                LEFT JOIN `users` AS `uc` ON `uc`.`id` = `c1`.`created_by`
                                LEFT JOIN `users` AS `uu` ON `uu`.`id` = `c1`.`updated_by`';
  }


  public static function buildUrl($category, $categories) {

    $alias = $category->alias;
    $parentId = $category->parent_id;

    $maxCount = 20;
    while ($parentId != 0 && $maxCount > 0) {

      $category = null;
      foreach ($categories as $cat) {
        if ($cat->id == $parentId) {
          $category = $cat;
        }
      }

      if (isset($category)) {
        $parentId = $category->parent_id;
        $alias = $category->alias . '/' . $alias;
      }
      $maxCount--;
    }

    return Conf::get('url') . '/' . $alias;
  }


  private function getOrderByString($data) {

    if (@exists($data['order_by'])) {
      return $this->setOrderByString($data['order_by'], 'c1');
    }
    return ' ORDER BY `c1`.`rang`, `c1`.`name`';
  }
}

?>