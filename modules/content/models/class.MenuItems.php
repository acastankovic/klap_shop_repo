<?php


class MenuItems extends Model {

  private $selectQueryString;
  private $orderByString;

  public function __construct() {
    parent::__construct();
    $this->setTable('menu_items');
    $this->setQueryStrings();
  }


  /************************************ FETCH ************************************/


  public function getOne($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `mi`.`id` = :id';
    return $this->exafe($sql, array('id' => $data['id']));
  }


  public function getAll() {

    $sql = $this->selectQueryString;
    $sql .= $this->orderByString;
    return $this->exafeAll($sql);
  }


  public function getByMenuId($data) {

    $sql = 'SELECT `mi1`.*, `mi2`.`name` AS `parent_name` FROM `menu_items` `mi1` 
            LEFT JOIN `menu_items` `mi2` ON `mi1`.`parent_id` = `mi2`.`id` 
            WHERE `mi1`.`menu_id` = :menu_id 
            ORDER BY `mi1`.`rang`';

    return $this->exafeAll($sql, array('menu_id' => $data['menu_id']));
  }


  /************************************ ACTIONS ************************************/


  public function updateParentId($data) {

    return $this->update(array('id' => $data['id'], 'parent_id' => $data['parent_id']));
  }


  private function updatePositionByMenuId($data) {

    $sql = 'UPDATE `menu_items` SET `rang` = :rang WHERE `id` = :id AND `menu_id` = :menu_id';
    return $this->execute($sql, array('rang' => $data['rang'], 'id' => $data['id'], 'menu_id' => $data['menu_id']));
  }


  public function updatePositions($data) {

    $menu_item = $this->getOne($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `mi`.`parent_id` = :parent_id AND `mi`.`id` != :id AND `m`.`id` = :menu_id';
    $sql .= $this->orderByString;

    $menuItems = $this->exafeAll($sql, array('parent_id' => $menu_item->parent_id, 'id' => $data['id'], 'menu_id' => $menu_item->menu_id));

    $this->updatePositionByMenuId(array('id' => $data['id'], 'rang' => $data['position'], 'menu_id' => $menu_item->menu_id));

    $counter = 0;
    foreach ($menuItems as $item) {

      if ((int)$counter === (int)$data['position']) {
        $counter++;
      }

      if ((int)$counter !== (int)$item->rang) {

        $this->updatePositionByMenuId(array('id' => $item->id, 'rang' => $counter, 'menu_id' => $item->menu_id));
      }

      $counter++;
    }
  }

  /************************************ OTHER ************************************/


  private function setQueryStrings() {

    $this->selectQueryString = 'SELECT `mi`.*, 
                                `mi2`.`name` AS `parent_name`,
                                `m`.`name` AS `menu_name`, `m`.`lang_group_id` AS `menu_lang_group_id`, `m`.`lang_id` AS `menu_lang_id`, 
                                `uc`.`username` AS created_by_username, 
                                `uu`.`username` AS update_by_username   
                                FROM `menu_items` AS `mi`
                                LEFT JOIN `menus` AS `m` ON `m`.`id` = `mi`.`menu_id`
                                LEFT JOIN `menu_items` AS `mi2` ON `mi2`.`id` = `mi`.`parent_id`
                                LEFT JOIN `users` AS `uc` ON `uc`.`id` = `mi`.`created_by`
                                LEFT JOIN `users` AS `uu` ON `uu`.`id` = `mi`.`updated_by`';

    $this->orderByString = ' ORDER BY `mi`.`rang`, `mi`.`id` DESC';
  }
}

?>