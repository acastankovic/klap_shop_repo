<?php


class ShortCodes extends Model {

  public function __construct() {
    parent::__construct();
  }

  public function parse($text) {

    //parse for gallery shortcode
    $subject = $text;
    $pattern = '/{{nsgallery=(.*?)}}/';
    preg_match_all($pattern, $subject, $matches, PREG_PATTERN_ORDER);
    foreach ($matches[1] as $match) {
      $gallery = Dispatcher::instance()->dispatch("content", "galleries", "fetchOne", array("id" => $match));
      $content = $this->galleryContent($gallery);
      $text = str_replace("{{nsgallery=" . $match . "}}", $content, $text);
    }

    return $text;
  }

  public function galleryContent($gallery) {

    $images = $gallery->images;
    $content = "<div class='nsgallery'>";

      foreach ($images as $image) {

        if ($image->type == 'video') {

          $content .= "<figure>";
            $content .= "<a data-fancybox='gallery' href='https://www.youtube.com/embed/" . $image->url . ";autoplay=1&rel=0&controls=1&showinfo=0' class='fancybox'>";
              $content .= "<img src='" . Conf::get('url') . "/css/img/icon-youtube.png' class='icon-youtube' />";
              $content .= "<img src='http://img.youtube.com/vi/" . $image->url . "/0.jpg' />";
            $content .= "</a>";
          $content .= "</figure>";
        }
        else {
          $content .= "<figure>";
            $content .= "<a data-fancybox='gallery' href='" . $image->url . "' class='fancybox'>";
              $content .= "<img src='" . $image->url . "' />";
            $content .= "</a>";
          $content .= "</figure>";
        }
      }
    $content .= "</div>";

    return $content;
  }
}

?>