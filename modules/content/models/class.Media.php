<?php


class Media extends Model {

  private $selectQueryString;
  private $selectTotalQueryString;

  public function __construct() {
    parent::__construct();
    $this->setTable('media');
    $this->setQueryStrings();
  }


  /************************************ FETCH ************************************/


  public function getOne($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `id` = :id';
    return $this->exafe($sql, array('id' => $data['id']));
  }


  public function getAll($data = null) {

    $sql = $this->selectQueryString;
    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql);
  }


  public function getByFileName($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `file_name` = :file_name';
    return $this->exafe($sql, array('file_name' => $data['file_name']));
  }


  public function getByIds($ids) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `id` IN (' . $ids . ')';

    return $this->exafeAll($sql);
  }


  public function getBySearch($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`url` LIKE :search OR `name` LIKE :search) ORDER BY `cdate` DESC';
    return $this->exafeAll($sql, array('search' => '%' . $data['search_term'] . '%'));
  }


  public function getWithPagination($data) {

    $limit = $data['items_per_page'];
    $page = @exists($data['page']) ? $data['page'] : 1;
    $offset = @exists($page) ? ($page - 1) * $limit : 0;

    $sql = $this->selectQueryString;

    if (@exists($data)) {

      if (@exists($data['mime_type']) || @exists($data['search'])) {
        $sql .= ' WHERE';
      }

      if (@exists($data['mime_type'])) {
        $sql .= ' `mime_type` = :mime_type';
      }

      if (@exists($data['search'])) {
        $sql .= ' `title` LIKE :search OR `file_name` LIKE :search OR `mime` LIKE :search';
      }

      if (@exists($data['order_by'])) {
        $sql .= ' ORDER BY ' . $data['order_by'];
      }

      if (@exists($data['order_direction'])) {
        $sql .= ' ' . $data['order_direction'];
      }
    }

    $sql .= ' LIMIT :offset, :limit';

    $stm = $this->dbh->prepare($sql);

    if (@exists($data)) {

      if (@exists($data['mime_type'])) {
        $stm->bindValue(':mime_type', (string)$data['mime_type'], PDO::PARAM_STR);
      }

      if (@exists($data['search'])) {
        $stm->bindValue(':search', (string)'%' . $data['search'] . '%', PDO::PARAM_STR);
      }
    }

    $stm->bindValue(':limit', (int)$limit, PDO::PARAM_INT);
    $stm->bindValue(':offset', (int)$offset, PDO::PARAM_INT);

    $stm->execute();

    return $stm->fetchAll(PDO::FETCH_OBJ);
  }


  public function getTotal($data) {

    $sql = $this->selectTotalQueryString;

    if (@exists($data) && @exists($data['mime_type'])) {
      $sql .= ' WHERE `mime_type` = :mime_type';
    }

    $stm = $this->dbh->prepare($sql);

    if (@exists($data) && @exists($data['mime_type'])) {
      $stm->bindValue(':mime_type', (string)$data['mime_type'], PDO::PARAM_STR);
    }

    $stm->execute();

    $result = $stm->fetch(PDO::FETCH_OBJ);

    return $result->total;
  }


  /************************************ OTHER ************************************/


  private function setQueryStrings() {

    $this->selectQueryString = 'SELECT * FROM `media`';

    $this->selectTotalQueryString = 'SELECT COUNT(`id`) AS `total` FROM `media`';
  }
}

?>