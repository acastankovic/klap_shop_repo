<?php


class MenuTypes extends Model {

  public function __construct() {
    parent::__construct();
    $this->setTable('menu_types');
  }


  /************************************ FETCH ************************************/


  public function getOne($data) {

    $sql = 'SELECT * FROM `menu_types` WHERE `id` = :id';

    return $this->exafe($sql, array('id' => $data['id']));
  }


  public function getAll() {

    $sql = 'SELECT * FROM `menu_types`';

    return $this->exafeAll($sql);
  }
}

?>