<?php


Class AdminAutoloaders {

  public static function autoload_controllers($class_name) {

    $file = Conf::get('root') . '/modules/admin/controllers/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }

  public static function autoload_models($class_name) {

    $file = Conf::get('root') . '/modules/admin/models/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }

  public static function autoload_views($class_name) {

    $file = Conf::get('root') . '/modules/admin/views/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }

  public static function autoload_content_views($class_name) {

    $file = Conf::get('root') . '/modules/admin/views/content/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }

  public static function autoload_users_views($class_name) {

    $file = Conf::get('root') . '/modules/admin/views/users/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }

  public static function autoload_shop_views($class_name) {

    $file = Conf::get('root') . '/modules/admin/views/shop/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }

}

spl_autoload_register('AdminAutoloaders::autoload_controllers');
spl_autoload_register('AdminAutoloaders::autoload_models');
spl_autoload_register('AdminAutoloaders::autoload_views');
spl_autoload_register('AdminAutoloaders::autoload_content_views');
spl_autoload_register('AdminAutoloaders::autoload_users_views');
spl_autoload_register('AdminAutoloaders::autoload_shop_views');
require_once(Conf::get('root') . '/modules/admin/app.php');

//register module
$item = new stdClass();
$item->title = 'Admin';
$item->alias = 'admin';
$item->root = false;
$modules = Conf::get('modules');
array_push($modules, $item);
Conf::set('modules', $modules);