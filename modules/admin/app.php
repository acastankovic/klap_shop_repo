<?php


class AdminApplication extends Application {

  protected $name = 'admin';

  protected $resources = array('sys');

  protected $routes = array(

    // dashboard
    array('route'      => '',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'dashboardPage'),

    // login
    array('route'      => 'login',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'loginPage'),

    // logout (action)
    array('route'      => 'logout',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'logout'),


    /***************** MEDIA *****************/

    // media table
    array('route'      => 'media',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'mediaPage'),

    // media table
    array('route'      => 'media/:parent_id/insert',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'mediaInsertPage'),


    /***************** CATEGORIES *****************/

    // categories table
    array('route'      => 'categories',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'categoriesPage'),

    // category insert/update
    array('route'      => 'categories/:id/insert',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'categoryInsertPage'),


    /***************** ARTICLES *****************/

    // articles table
    array('route'      => 'articles',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'articlesPage'),

    // article insert/update
    array('route'      => 'articles/:id/insert',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'articleInsertPage'),

    // articles categories
    array('route'      => 'articles/categories/sort',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'articlesCategoriesPage'),

    // articles sorting
    array('route'      => 'articles/categories/:id/sort',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'articlesSortPage'),


    /***************** MENUS *****************/

    // menus table
    array('route'      => 'menus',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'menusPage'),

    // menus items table
    array('route'      => 'menus/:id/insert',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'menuItemsPage'),


    /***************** SLIDERS *****************/

    // sliders table
    array('route'      => 'sliders',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'slidersPage'),

    // slider items table
    array('route'      => 'sliders/:slider_id/insert',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'sliderItemsPage'),

    // slider items insert/update
    array('route'      => 'sliders/:slider_id/slider_items/:id/insert',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'sliderItemInsertPage'),


    /************** GALLERIES **************/

    // galleries table
    array('route'      => 'galleries',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'galleriesPage'),

    // gallery insert/update
    array('route'      => 'galleries/:id/insert',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'galleryInsertPage'),


    /***************** USERS *****************/

    // users table
    array('route'      => 'users',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'usersPage'),

    // user insert/update
    array('route'      => 'users/:id/insert',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'userInsertPage'),


    /************** NEWSLETTER **************/

    // newsletter table
    array('route'      => 'newsletter',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'newsletterPage'),


    /********************************************
     *                                           *
     *                    SHOP                   *
     *                                           *
     ********************************************/


    /*********** SHOP CATEGORIES ***********/

    // shop categories table
    array('route'      => 'categories/:type',
          'method'     => 'get',
          'controller' => 'ShopAdmin',
          'action'     => 'categoriesPage'),

    // shop category insert/update
    array('route'      => 'categories/:type/:id/insert',
          'method'     => 'get',
          'controller' => 'ShopAdmin',
          'action'     => 'categoryInsertPage'),


    /************** PRODUCTS **************/

    // products table
    array('route'      => 'products',
          'method'     => 'get',
          'controller' => 'ShopAdmin',
          'action'     => 'productsPage'),

    // product insert/update
    array('route'      => 'products/:id/insert',
          'method'     => 'get',
          'controller' => 'ShopAdmin',
          'action'     => 'productInsertPage'),


    /************** VENDORS **************/

    // vendors table
    array('route'      => 'vendors',
          'method'     => 'get',
          'controller' => 'ShopAdmin',
          'action'     => 'vendorsPage'),

    // vendor insert/update
    array('route'      => 'vendors/:id/insert',
          'method'     => 'get',
          'controller' => 'ShopAdmin',
          'action'     => 'vendorInsertPage'),

    /************** ORDERS **************/

    // orders table
    array('route'      => 'orders',
          'method'     => 'get',
          'controller' => 'ShopAdmin',
          'action'     => 'ordersPage'),

    // order insert/update
    array('route'      => 'orders/:id/insert',
          'method'     => 'get',
          'controller' => 'ShopAdmin',
          'action'     => 'orderInsertPage'),


    /***************** DATA *****************/

    // table data
    array('route'      => 'table/:name',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'tablePage'),

    // save data for table :name (action)
    array('route'      => 'tables/:name/data',
          'method'     => 'post',
          'controller' => 'admin',
          'action'     => 'saveData'),

    // delete data for table :name (action)
    array('route'      => 'tables/:name/data/:id',
          'method'     => 'delete',
          'controller' => 'admin',
          'action'     => 'deleteData'),


    /***************** TRANSLATIONS *****************/

    // load translations js (action)
    array('route'      => 'translations-js',
          'method'     => 'get',
          'controller' => 'admin',
          'action'     => 'translationsJs')
  );
}

?>