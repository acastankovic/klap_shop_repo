$(document).on('ready',
  function () {
    initDropZone();
  }
);

var myDropzone;

function initDropZone() {

  Dropzone.autoDiscover = false;

  myDropzone = new Dropzone('#mediaUpload',
    {
      url: BASE_URL + '/content/upload',
      maxFiles: 20, // Maximum Number of Files
      maxFilesize: 1200,
      dictDefaultMessage: NC_TRANSLATION[LANG.ALIAS].sUploadFiles
    }
  );

  myDropzone.on('error', function (file, response) {
    console.log(file);
    console.log(response);
  })
    .on('queuecomplete', function () {

      setTimeout(
        function () {
          location.reload();
        }, 800
      );
    });
};


/****************************** ACTIONS ******************************/

// save
$('#save').on('click',
  function (event) {
    event.preventDefault();

    ajaxCall(
      '/admin/tables/media/data',
      'POST',
      $('#data-form').serialize(),
      function (response) {

        if (ajaxSuccess(response)) {
          openMessagePopup(NC_TRANSLATION[LANG.ALIAS].sFileChanged,
            function () {
              location.reload();
            }
          );
        } else defaultErrorHandler(response);
      }
    );
  }
);


// edit
$('.edit').on('click',
  function () {
    var id = $(this).data('id');
    var parentId = $(this).parents('tr').data('parentid');
    $('#modal-entry *[name="parent_id"]').select2('val', parentId);
    $('tr[data-id="' + id + '"] td').each(
      function () {
        var name = $(this).data('name');
        var value = $(this).data('value');
        var type = $(this).data('type');
        if (type === 'text') $('#modal-entry *[name="' + name + '"]').text(value);
        else $('#modal-entry *[name="' + name + '"]').val(value);
      }
    );
  }
);


// delete
$(document).on('click', '.delete',
  function () {
    var message = NC_TRANSLATION[LANG.ALIAS].sDeleteMediaWarning;
    asyncDeleteAction(this, '/content/media/', message);
  }
);