$(document).on('ready',
  function () {
    init();
  }
);


function init() {
  initCkeditor();
  setResource();
  initMediaModalDropZone();
  initTooltips();
};


/****************************** ACTIONS ******************************/

// insert / update
$('.save-insert-form').on('click',
  function (event) {
    event.preventDefault();

    if (actionAllowed(this)) {

      disableActionButtons();

      var langSuffix = getLanguageSuffix();

      var id = $('#' + RESOURCE + '-id' + langSuffix).val();
      var name = $('#' + RESOURCE + '-name' + langSuffix).val();

      if (name.trim() === '') {
        openWarningPopup(
          NC_TRANSLATION[LANG.ALIAS].sNameRequired,
          function () {
            enableActionButtons();
          }
        );
        return;
      }


      if (id == 0) {

        //insert
        asyncInsertAction('/shop/vendors/insert/', '/admin/vendors/:id/insert', $('#insert-form' + langSuffix));
      } else {

        // update
        asyncUpdateAction('/shop/vendors/update/');
      }
    }
  }
);

/************************** / END OF ACTIONS **************************/