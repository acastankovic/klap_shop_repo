// delete
$(document).on('click', '.delete',
  function () {
    var route = '/shop/vendors/';
    var message = NC_TRANSLATION[LANG.ALIAS].sDeleteVendorWarning;
    asyncDeleteAction(this, route, message);
  }
);