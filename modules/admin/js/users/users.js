// delete
$(document).on('click', '.delete',
   function () {
      var message  = NC_TRANSLATION[LANG.ALIAS].sDeleteUserWarning;
      asyncDeleteAction(this, '/users/users/', message);
   }
);