$(document).on('ready',
   function () {
      init();
   }
);


function init() {
   initMediaModalDropZone();
};


/****************************** ACTIONS ******************************/


// insert / update
$('.save-insert-form').on('click',
   function(event) {
      event.preventDefault();

      if(actionAllowed(this)) {

      	disableActionButtons();

         var langSuffix = getLanguageSuffix();

         var id       = $('#' + RESOURCE + '-id' + langSuffix).val();
         var username = $('#' + RESOURCE + '-username' + langSuffix).val();

         if(username.trim() === '') {
            openWarningPopup(
         		NC_TRANSLATION[LANG.ALIAS].sUsernameRequired,
         		function() {
         			enableActionButtons();
         		}
         	);
         	return;
         }

      	// password required only for inserting new user
      	if(id == 0 && $('#' + RESOURCE + '-password' + langSuffix).length > 0) {

            var password = $('#' + RESOURCE + '-password' + langSuffix).val();

      		if(password.trim() === '') {

      			openWarningPopup(
      				NC_TRANSLATION[LANG.ALIAS].sPasswordRequired,
	         		function() {
	         			enableActionButtons();
	         		}
      			);
      			return;
      		}
      	}

         if(id == 0) {

            // insert
			asyncInsertAction('/users/insert/', '/admin/users/:id/insert', $('#insert-form' + langSuffix));
         }
         else {

            // update
            asyncUpdateAction('/users/update/');
         }
      }
	}
);


$('#openPasswordSection').on('ifChanged', 
	function() {

		if(this.checked) {
			$('.change-password-section').slideDown();
		}
		else{
			$('.change-password-section').slideUp();
		}
	}
);


$('#change-password-form').on('submit',
	function(event) {
		event.preventDefault();

        var newPassword 	   = $(this).find('input[name="password_new"]').val();
        var repeatNewPassword = $(this).find('input[name="password_repeat_new"]').val();

		var proceed = validateRequiredFields();

		if(newPassword != repeatNewPassword) {
			openWarningPopup(NC_TRANSLATION[LANG.ALIAS].sPasswordsDontMatch);
			proceed = false;
		}

		if(proceed) {

			ajaxCall(
				'/users/password',
				'PUT',
				$('#change-password-form').serialize(),
				function(response) {

					if(ajaxSuccess(response)) {

						var success = response.data.success; 
						var message = response.data.message; 

						if(!success) {
							openWarningPopup(message);
                     		return;
						}

						openMessagePopup(message, 
							function() {
								closeChangePasswordSection();
							}
						);
					}
					else defaultErrorHandler(response);
				}
			);			
		}
	}
);


function closeChangePasswordSection() {
	$('#change-password-form').find('input[type="password"]').val('');
	$('.change-password-section').slideUp();
	setTimeout(function() { $('#openPasswordSection').iCheck('uncheck'); }, 800);
};