$(document).ready(
  function () {
    initArticlesTreeGrid();
  }
);

//drag and drop handler
function handleDnDArticlesTree(e, data) {

  var nodeId = data.node.id;
  var newPosition = data.position;

  ajaxCall(
    '/content/articles/' + nodeId + '/position',
    'PUT',
    {position: newPosition, fetchWithUnpublished: true},
    function (response) {
      // console.log(response.data.rang);
      console.log(response.data);
      // if(ajaxSuccess(response)) {
      // }
      // else defaultErrorHandler(response);
    }
  );
};


var items = [];

function initArticlesTreeGrid() {

  var parentId = $('#parentId').val();

  ajaxCall(
    '/content/articles/parent_id/' + parentId,
    'GET',
    {fetchWithUnpublished: true, order_by: {rang: "", id: ""}},
    function (response) {

      if (ajaxSuccess(response)) {

        items = response.data;

        if (typeof items != 'undefined' && items != null) {

          for (var key in items) {

            var item = items[key];
            item.name = item.title;
            item.text = item.title;
            item.icon = "fa fa-folder c-primary";
            item.state = false;
            item.parent_id = item.category_id;
          }
        }

        $('#tree').on('move_node.jstree', function (e, data) {
          handleDnDArticlesTree(e, data);

        }).jstree({
          "core": {
            "check_callback": true,
            'data': items
          },
          "plugins": ["dnd"]
        });
        $('#tree').jstree(true).settings.core.data = items;
        $('#tree').jstree(true).refresh();
      } else defaultErrorHandler(response);
    }
  );
};