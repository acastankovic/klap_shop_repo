// delete
$(document).on('click', '.delete',
  function () {
    var message = NC_TRANSLATION[LANG.ALIAS].sDeleteArticleWarning;
    asyncDeleteAction(this, '/content/articles/', message);
  }
);