$(document).on('ready',
  function () {
    init();
  }
);


function init() {
  initTreeGrid();
  initCkeditor();
  initDateField();
  setResource();
  initMediaModalDropZone();
  initTooltips();
};


// datepicker for event/publish date
function initDateField() {

  if ($('.date').length > 0) {

    $('.date').datepicker({
      dateFormat: 'dd.mm.yy'
    });
  }
};


/****************************** ACTIONS ******************************/


// insert / update
$('.save-insert-form').on('click',
  function (event) {
    event.preventDefault();

    if (actionAllowed(this)) {

      disableActionButtons();

      var langSuffix = getLanguageSuffix();

      var id = $('#' + RESOURCE + '-id' + langSuffix).val();
      var title = $('#' + RESOURCE + '-title' + langSuffix).val();

      if (title.trim() === '') {
        openWarningPopup(
          NC_TRANSLATION[LANG.ALIAS].sTitleRequired,
          function () {
            enableActionButtons();
          }
        );
        return;
      }

      setFormParentId();
      setFormRang();
      setGallery();
      // setTags();

      if (id == 0) {

        // insert
        asyncInsertAction('/content/articles/insert/', '/admin/articles/:id/insert', $('#insert-form' + langSuffix));
      }
      else {

        // update
        asyncUpdateAction('/content/articles/update/');
      }
    }
  }
);


/************************** / END OF ACTIONS **************************/


/******************************** TAGS ********************************/


$('.open-tag-field').on('click',
  function (event) {
    event.preventDefault();

    if ($('.tag-field').length == 0) {
      renderTagField();
    }
  }
);


$(document).on('click', '.add-tag',
  function (event) {
    event.preventDefault();

    var tagValue = $(this).parents('.tag-wrapper').find('.tag-field').val();

    if (tagValue != '') {
      var tag = '<button type="button" class="btn btn-sm btn-primary tag remove-tag" data-toggle="tooltip" data-placement="top" title="Remove" data-value="' + tagValue + '">' + tagValue + '</button>';
      $(this).parents('.tag-wrapper').html(tag);

    }
  }
);


$(document).on('click', '.remove-tag',
  function (event) {
    event.preventDefault();
    $(this).parents('.tag-wrapper').remove();
  }
);


function renderTagField() {

  var html = '<div class="tag-wrapper">';
    html += '<input type="text" class="tag-field form-control" />';
    html += '<button type="button" class="btn btn-sm btn-success btn-transparent add-tag">Ok</button>';
  html += '</div>';

  $('#articleTags').append(html);
};


function getTagsData() {

  var tags = [];
  if ($('.tag').length > 0) {
    $('.tag').each(
      function () {
        var tag = $(this).attr('data-value');
        if (tag != '' && typeof tag != "undefined") tags.push(tag);
      }
    );
  }
  return tags;
};

function setTags() {
  var tags = getTagsData();
  $('#' + RESOURCE + '-tags').val(JSON.stringify(tags));
}

/**************************** / END OF TAGS ****************************/