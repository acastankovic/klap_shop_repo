$(document).on('ready',
  function () {
    init();
  }
);


function init() {
  initMediaModalDropZone();
};


/****************************** ACTIONS ******************************/


// insert / update
$('.save-insert-form').on('click',
  function (event) {
    event.preventDefault();

    if (actionAllowed(this)) {

      disableActionButtons();

      var langSuffix = getLanguageSuffix();

      var id = $('#' + RESOURCE + '-id' + langSuffix).val();
      var parentId = $('#' + RESOURCE + '-slider_id' + langSuffix).val();
      var image = $('#' + RESOURCE + '-image' + langSuffix).val();

      if (image.trim() == '') {
        openWarningPopup(
          NC_TRANSLATION[LANG.ALIAS].sImageRequired,
          function () {
            enableActionButtons();
          }
        );
        return;
      }

      if (id == 0) {
        // insert
        ajaxCall(
          '/content/slider_items/insert/',
          'POST',
          $('#insert-form' + langSuffix).serialize(),
          function (response) {

            if (ajaxSuccess(response)) {

              var success = response.data.success;
              var message = response.data.message;

              if (!success) {
                openWarningPopup(message,
                  function () {
                    enableActionButtons();
                  }
                );
                return;
              }

              var id = response.data.id;

              openMessagePopup(message,
                function () {
                  window.location = BASE_URL + '/admin/sliders/' + parentId + '/slider_items/' + id + '/insert';
                }
              );
            } else defaultErrorHandler(response);
          }
        );
      }
      else {
        // update
        asyncUpdateAction('/content/slider_items/update/');
      }
    }
  }
);