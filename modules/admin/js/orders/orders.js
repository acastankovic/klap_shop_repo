// delete
$(document).on('click', '.delete',
  function () {
    var route = '/shop/orders/delete/';
    var message = NC_TRANSLATION[LANG.ALIAS].sDeleteItemWarning;
    asyncDeleteAction(this, route, message);
  }
);