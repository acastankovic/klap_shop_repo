$(document).on('ready',
  function () {
    setResource();
  }
);

$(document).on('ifChanged', '.paid-checkbox',
  function () {

    var value = this.checked ? 1 : 0;

    var langSuffix = getLanguageSuffix();

    $('#' + RESOURCE + '-paid' + langSuffix).val(value);
  }
);


// insert / update
$('.save-insert-form').on('click',
  function (event) {
    event.preventDefault();

    if (actionAllowed(this)) {

      disableActionButtons();

      var langSuffix = getLanguageSuffix();
      var id = $('#' + RESOURCE + '-id' + langSuffix).val();


      if (id == 0) {
        return;
      } else {

        // update
        asyncUpdateAction('/shop/orders/change');
      }
    }
  }
);