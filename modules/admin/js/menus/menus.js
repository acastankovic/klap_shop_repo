$('.new-form-entry').on('click',
  function () {

    disableCallToActionButtons();

    var id = 0;

    ajaxCall(
      '/content/menus/language_groups/' + id,
      'GET',
      null,
      function (response) {

        if (ajaxSuccess(response)) {

          var items = response.data.items;
          var langGroupId = response.data.langGroupId;
          if (!exists(langGroupId)) {
            langGroupId = '';
          }

          setModalLanguageTabsActiveClass();
          renderFormModalContent(items, langGroupId);
        } else defaultErrorHandler(response);
      }
    );
  }
);


$('.edit').on('click',
  function () {

    disableCallToActionButtons();

    var id = $(this).attr('data-id');
    var langId = $(this).attr('data-lang_id');

    ajaxCall(
      '/content/menus/language_groups/' + id,
      'GET',
      null,
      function (response) {

        if (ajaxSuccess(response)) {

          var items = response.data.items;
          var langGroupId = response.data.langGroupId;

          setModalLanguageTabsActiveClass(langId);
          renderFormModalContent(items, langGroupId, langId);
        } else defaultErrorHandler(response);
      }
    );
  }
);


// on modal close
$('#form-modal').on('hide.bs.modal',
  function () {

    setTimeout(
      function () {
        enableCallToActionButtons();
      }, 700
    );
  }
);


$('.save-insert-form').on('click',
  function (event) {
    event.preventDefault();

    if (actionAllowed(this)) {

      disableActionButtons();

      var langSuffix = getLanguageSuffix();

      var $form = $('#insert-form' + langSuffix);

      var id = $form.find('input[name="id"]').val();
      var name = $form.find('input[name="name"]').val();

      if (name.trim() === '') {
        openWarningPopup(
          NC_TRANSLATION[LANG.ALIAS].sNameRequired,
          function () {
            enableActionButtons();
          }
        );
        return;
      }

      if (id == 0) {

        //insert
        asyncInsertAction('/content/menus/insert/', '', $form, true);
      }
      else {

        //update
        asyncUpdateAction('/content/menus/update/');
      }
    }
  }
);


// delete
$(document).on('click', '.delete',
  function () {
    var message = NC_TRANSLATION[LANG.ALIAS].sDeleteMenuWarning;
    asyncDeleteAction(this, '/content/menus/', message);
  }
);


function renderFormModalContent(items, langGroupId, langId) {

  var html = '';

  for (var key in items) {

    var item = items[key];

    var name = typeof item.name === 'undefined' ? '' : item.name;

    if (!exists(langId)) langId = getLanguageId();

    var activeClass = langId == key ? ' active' : '';

    var langSuffix = getLanguageSuffix(item.lang_id);

    html += '<div class="language-wrapper' + activeClass + '" id="languageWrapper' + langSuffix + '">';
      html += '<form id="insert-form' + langSuffix + '">';
        html += '<div class="col-md-12">';
          html += '<div class="form-group">';
            html += '<label>Name</label>';
            html += '<input type="hidden" name="id" value="' + item.id + '">';
            html += '<input type="hidden" name="lang_group_id" value="' + langGroupId + '">';
            html += '<input type="hidden" name="lang_id" value="' + item.lang_id + '">';
          html += '<input type="text" name="name" value="' + name + '" class="form-control">';
          html += '</div>';
        html += '</div>';
      html += '</form>';
    html += '</div>';
  }

  $('.form-modal-content').html(html);
};


function disableCallToActionButtons() {
  $('.edit').addClass('disabled');
  $('.new-form-entry').addClass('disabled');
};


function enableCallToActionButtons() {
  $('.edit').removeClass('disabled');
  $('.new-form-entry').removeClass('disabled');
};