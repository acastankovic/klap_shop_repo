$(document).on('ready',
  function () {
    initMenuTree();
    setResource();
  }
);


/********************** CATEGORIES TREE **********************/

function initMenuTree() {

  var menuId = $('#menuId').val();

  $.ajax({
    url: BASE_URL + '/content/menu_items/tree/' + menuId,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Accept', 'application/json');
    },
    success: function (response) {

      if (response.success) {

        $('#tree3').on('move_node.jstree', function (e, data) {
          handleDnDMenuTree(e, data);

        }).jstree({
          'core': {
            'check_callback': true,
            'data': response.data
          },
          'plugins': ['dnd']
        });
        $('#tree3').jstree(true).settings.core.data = response.data;
        $('#tree3').jstree(true).refresh();
      }
    },
    error: function () {
      alert('Server error');
    }
  });
};


//drag and drop handler
function handleDnDMenuTree(e, data) {

  var nodeId = data.node.id;
  var newParentId = data.parent;
  var newPosition = data.position;

  if (newParentId === '#') newParentId = 0;

  ajaxCall(
    '/content/menu_items/' + nodeId + '/position',
    'PUT',
    {parent_id: newParentId, position: newPosition},
    function (response) {

      if (ajaxSuccess(response)) {

        var $row = $('tr[data-id="' + response.data.id + '"]');
        $row.attr('data-parentid', response.data.parent_id);

        var parentName = '';
        if (exists(response.data.parent_name)) {
          parentName = response.data.parent_name;
        }

        $row.find('td[data-name="parent_name"]').text(parentName);
      } else defaultErrorHandler(response);
    }
  );
};


function setModalMenuId() {
  var menuId = $('#menuId').val();
  $('#modal-entry *[name="menu_id"]').val(menuId);
};

/********************** / END OF CATEGORIES TREE **********************/


/****************************** ACTIONS ******************************/

$('#save').on('click',
  function (event) {
    event.preventDefault();

    var id = $('#modal-entry *[name="id"]').val();
    var endpoint = '/content/menu_items/insert/';
    var method = 'POST';

    if (id != '') {

      endpoint = '/content/menu_items/update/';
      method = 'PUT';
    }

    ajaxCall(
      endpoint,
      method,
      $('#data-form').serialize(),
      function (response) {

        if (ajaxSuccess(response)) {

          var success = response.data.success;
          var message = response.data.message;

          if (!success) {
            openWarningPopup(message);
            return;
          }

          openMessagePopup(message,
            function () {
              location.reload();
            }
          );
        } else defaultErrorHandler(response);
      }
    );
  }
);


// delete
$(document).on('click', '.delete',
  function () {

    var id = $(this).data('id');
    var menuId = $('#menuId').val();
    var row = $(this).parents('tr');

    swal({
        title: NC_TRANSLATION[LANG.ALIAS].sAreYouSure,
        text: NC_TRANSLATION[LANG.ALIAS].sDeleteMenuItemWarning,
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn-danger',
        confirmButtonText: NC_TRANSLATION[LANG.ALIAS].sConfirmDelete,
        cancelButtonText: NC_TRANSLATION[LANG.ALIAS].sCancel,
        closeOnConfirm: true
      },
      function (isConfirm) {

        if (isConfirm) {

          ajaxCall(
            '/content/menu_items/' + id,
            'DELETE',
            'menu_id=' + menuId,
            function (response) {

              if (ajaxSuccess(response)) {

                $('.table').DataTable().row(row).remove().draw();
                location.reload();
              } else defaultErrorHandler(response);
            }
          );
        }
        else {
          swal(NC_TRANSLATION[LANG.ALIAS].sCanceled, NC_TRANSLATION[LANG.ALIAS].sCanceledMessage, 'info');
        }
      });
  }
);


/************************* / END OF ACTIONS **************************/


/******************************* MODAL *******************************/

$('.edit').on('click',
  function () {
    var id = $(this).attr('data-id');
    var targetId = $(this).parents('tr').attr('data-targetid');
    var parentId = $(this).parents('tr').attr('data-parentid');
    var type = $(this).parents('tr').attr('data-type');
    var url = $(this).parents('tr').attr('data-url');

    $('#modal-entry *[name="id"]').val(id);
    $('#modal-entry *[name="target_id"]').val(targetId);
    $('#modal-entry *[name="parent_id"]').select2('val', parentId);
    $('#modal-entry *[name="type"]').select2('val', type);
    $('#modal-entry *[name="url"]').val(url);

    setModalFieldsVisibility(type, targetId);

    $('tr[data-id="' + id + '"] td').each(
      function () {
        var name = $(this).data('name');
        var value = $(this).data('value');
        var type = $(this).data('type');
        if (type === 'text') $('#modal-entry *[name="' + name + '"]').text(value);
        else $('#modal-entry *[name="' + name + '"]').val(value);
      }
    );

  }
);


$('.new-entry').on('click',
  function (event) {
    event.preventDefault();
    setModalMenuId();
  }
);


$('#modal-entry').on('hide.bs.modal',
  function () {
    hideAllEditableFields();
    $('#modal-entry *[name="id"]').val('');
    $('#modal-entry *[name="target_id"]').val('');
    $('#modal-entry *[name="parent_id"]').val('');
    $('#modal-entry *[name="type"]').select2('val', 0);
    $('#modal-entry *[name="url"]').val('');
  }
);


$('#articlesSelect').on('change',
  function () {
    var value = $(this).val();
    $('#modal-entry *[name="target_id"]').val(value);
  }
);


$('#categoriesSelect').on('change',
  function () {
    var value = $(this).val();
    $('#modal-entry *[name="target_id"]').val(value);
  }
);


$('#menuItemTypesSelect').on('change',
  function () {
    var value = $(this).val();
    setModalFieldsVisibility(value);
  }
);


function setModalFieldsVisibility(type, targetId = null) {

  if (type == MENU_ITEM_TYPE_ID.ARTICLE) showArticlesSelect(targetId);
  else if (type == MENU_ITEM_TYPE_ID.CATEGORY) showCategoriesSelect(targetId);
  else if (type == MENU_ITEM_TYPE_ID.EXTERNAL_LINK) showUrlField();
  else hideAllEditableFields();
};


function showArticlesSelect(targetId = null) {
  var targetValue = exists(targetId) ? targetId : 0;
  $('#articlesSelect').select2('val', targetValue);
  $('.url-field-wrapper').hide();
  $('.categories-select-wrapper').hide();
  $('.articles-select-wrapper').fadeIn();
};


function showCategoriesSelect(targetId = null) {
  var targetValue = exists(targetId) ? targetId : 0;
  $('#categoriesSelect').select2('val', targetValue);
  $('.url-field-wrapper').hide();
  $('.articles-select-wrapper').hide();
  $('.categories-select-wrapper').fadeIn();
};


function showUrlField() {
  $('#modal-entry *[name="target_id"]').val('');
  $('.articles-select-wrapper').hide();
  $('.categories-select-wrapper').hide();
  $('.url-field-wrapper').fadeIn();
};


function hideAllEditableFields() {
  $('#modal-entry *[name="target_id"]').val('');
  $('.url-field-wrapper').hide();
  $('.articles-select-wrapper').hide();
  $('.categories-select-wrapper').hide();
};


/************************** / END OF MODAL ***************************/