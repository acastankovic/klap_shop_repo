$(document).on('ready',
  function () {
    init();
  }
);


function init() {
  initTreeGrid();
  initCkeditor();
  setResource();
  initMediaModalDropZone();
  initTooltips();
};


/****************************** ACTIONS ******************************/


// insert / update
$('.save-insert-form').on('click',
  function (event) {
    event.preventDefault();

    if (actionAllowed(this)) {

      disableActionButtons();

      var langSuffix = getLanguageSuffix();

      var id = $('#' + RESOURCE + '-id' + langSuffix).val();
      var name = $('#' + RESOURCE + '-name' + langSuffix).val();

      if (name.trim() === '') {
        openWarningPopup(
          NC_TRANSLATION[LANG.ALIAS].sNameRequired,
          function () {
            enableActionButtons();
          }
        );
        return;
      }

      setFormParentId();
      setGallery();

      if (id == 0) {

        var redirectUrl = setRedirectUrl(id);

        // insert
        asyncInsertAction('/content/categories/insert/', redirectUrl, $('#insert-form' + langSuffix));
      } else {

        // update
        asyncUpdateAction('/content/categories/update/');
      }
    }
  }
);


/************************** END OF ACTIONS ***************************/


function setRedirectUrl(id) {
  if (shopCategoryPage()) return '/admin/categories/shop/:id/insert';
  return '/admin/categories/:id/insert';
};


function shopCategoryPage() {
  var $pageType = $('#pageType');
  if (typeof SHOP_CATEGORY_ID == 'undefined' || SHOP_CATEGORY_ID == null || SHOP_CATEGORY_ID == '') return false;
  if ($pageType.length == 0) return false;
  if ($pageType.val() == '') return false;
  return $pageType.val() == SHOP_CATEGORY_ID;
};