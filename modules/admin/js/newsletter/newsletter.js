// delete
$(document).on('click', '.delete',
  function () {
    var route = '/content/newsletter/';
    var message = NC_TRANSLATION[LANG.ALIAS].sDeleteItemWarning;
    asyncDeleteAction(this, route, message);
  }
);