$(document).on('ready',
  function () {
    init();
  }
);


function init() {
  initMediaModalDropZone();
};


/****************************** ACTIONS ******************************/


// insert / update
$('.save-insert-form').on('click',
  function (event) {
    event.preventDefault();

    if (actionAllowed(this)) {

      disableActionButtons();

      var langSuffix = getLanguageSuffix();

      var id = $('#' + RESOURCE + '-id' + langSuffix).val();

      setGallery();

      if (id == 0) {

        // insert
        asyncInsertAction('/content/galleries/insert/', '/admin/galleries/:id/insert', $('#insert-form' + langSuffix));
      }
      else {

        // update
        asyncUpdateAction('/content/galleries/update/');
      }
    }
  }
);