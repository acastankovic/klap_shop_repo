// delete
$(document).on('click', '.delete',
  function () {
    var route = '/content/galleries/';
    var message = NC_TRANSLATION[LANG.ALIAS].sDeleteGalleryWarning;
    asyncDeleteAction(this, route, message);
  }
);