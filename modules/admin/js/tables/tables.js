$(document).on('click', '#save-changes',
  function () {
    var tableName = $('#table-name').val();
    $.ajax({
      url: BASE_URL + '/admin/tables/' + tableName + '/data',
      type: 'POST',
      data: $('#data-form').serialize(),
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Accept', 'application/json');
      },
      success: function (response) {
        if (response.success) {
          //TODO: instead of reload row should be added
          location.reload();
          //window.location.href = BASE_URL + '/admin/table/' + tableName;
        }
        //else alert('Auth error');
      },
      error: function () {
        alert('Server error');
      }
    });
  }
);


$(document).on('click', '.edit',
  function () {
    var id = $(this).data("id");
    $('tr[data-id="' + id + '"] td').each(
      function () {
        var name = $(this).data('name');
        var value = $(this).data('value');
        var type = $(this).data('type');
        if (type === 'text') $('#modal-entry *[name="' + name + '"]').text(value);
        else $('#modal-entry *[name="' + name + '"]').val(value);
      }
    );
  }
);


$(document).on('click', '.delete',
  function () {
    var id = $(this).data('id');
    var tableName = $('#table-name').val();
    var row = $(this).parents('tr');
    swal({
        title: NC_TRANSLATION[LANG.ALIAS].sAreYouSure,
        text: NC_TRANSLATION[LANG.ALIAS].sDeleteItemWarning,
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn-danger',
        confirmButtonText: NC_TRANSLATION[LANG.ALIAS].sConfirmDelete,
        cancelButtonText: NC_TRANSLATION[LANG.ALIAS].sCancel,
        closeOnConfirm: true
      },
      function (isConfirm) {
        if (isConfirm) {

          $.ajax({
            url: BASE_URL + '/admin/tables/' + tableName + '/data/' + id,
            type: 'DELETE',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Accept', 'application/json');
            },
            success: function (response) {

              if (response.success) {
                $('.table').DataTable().row(row)
                  .remove()
                  .draw();
              }
              else alert('Server error');
            },
            error: function () {
              alert('Server error');
            }
          });


        }
        else {
          swal(NC_TRANSLATION[LANG.ALIAS].sCanceled, NC_TRANSLATION[LANG.ALIAS].sCanceledMessage, 'info');
        }
      });
  }
);
