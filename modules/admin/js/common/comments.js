$('.comments-wrapper-toggler').on('click',
  function (event) {
    event.preventDefault();

    var $wrapper = $(this).parent('.toggler-wrapper');
    var $comments = $(this).parents('.comments-wrapper').find('.comments');

    $wrapper.toggleClass('active');

    if ($wrapper.hasClass('active')) {
      $wrapper.find('i').removeClass('fa-chevron-down');
      $wrapper.find('i').addClass('fa-chevron-up');
      $comments.slideDown();
    } else {
      $wrapper.find('i').removeClass('fa-chevron-up');
      $wrapper.find('i').addClass('fa-chevron-down');
      $comments.slideUp();
    }
  }
);


// publish
$(document).on('click', '.comment-publish',
  function (event) {
    event.preventDefault();

    var id = $(this).data('id');
    var published = $(this).attr('data-published');

    var self = this;
    ajaxCall(
      '/content/comments/publish/',
      'PUT',
      'id=' + id + '&published=' + published,
      function (response) {

        if (ajaxSuccess(response)) {

          var published = response.data.published;
          var message = response.data.message;
          var buttonText = response.data.buttonText;

          openMessagePopup(message);
          $(self).attr('data-published', published);
          $(self).text(buttonText);
        } else defaultErrorHandler(response);
      }
    );
  }
);


// delete
$('.comment .delete').on('click',
  function (event) {
    event.preventDefault();

    var id = $(this).data('id');
    var parentId = $(this).data('parent_id');
    var $comment = $(this).parent('.comment-header').parent('.comment');

    var warningMessage = $(this).hasClass('hasChildren') ? NC_TRANSLATION[LANG.ALIAS].sDeleteCommentWithChildrenWarning : NC_TRANSLATION[LANG.ALIAS].sDeleteCommentWarning;

    swal({
        title: NC_TRANSLATION[LANG.ALIAS].sAreYouSure,
        text: warningMessage,
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn-danger',
        confirmButtonText: NC_TRANSLATION[LANG.ALIAS].sConfirmDelete,
        cancelButtonText: NC_TRANSLATION[LANG.ALIAS].sCancel,
        closeOnConfirm: true
      },
      function (isConfirm) {

        if (isConfirm) {

          ajaxCall(
            '/content/comments/' + id,
            'DELETE',
            'parent_id=' + parentId,
            function (response) {

              if (ajaxSuccess(response)) {

                $comment.remove();
              } else defaultErrorHandler(response);
            }
          );
        } else {
          swal(NC_TRANSLATION[LANG.ALIAS].sCanceled, NC_TRANSLATION[LANG.ALIAS].sCanceledMessage, 'info');
        }
      }
    );
  }
);