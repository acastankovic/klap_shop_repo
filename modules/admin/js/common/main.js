$(document).ready(
  function () {
    setElements();
  }
);

function NC_TRANSLATE(key, group) {

  //var result = typeof group != 'undefined' ? NC_TRANSLATIONS['group'][key] : NC_TRANSLATIONS['default'][key];
  //return result ? result : key;

  var result = key;
  if(typeof group != 'undefined') {
    result = NC_TRANSLATIONS['group'][key];
  }
  else if(typeof NC_TRANSLATIONS['default'] != 'undefined') {
    result = NC_TRANSLATIONS['group'][key];
  }

  return result;
}

$('.new-entry').on('click',
  function (event) {
    event.preventDefault();
    $('#modal-entry input').val('');
  }
);


$('.set-language').on('click',
  function (event) {
    event.preventDefault();

    var id = $(this).attr('data-id');

    ajaxCall(
      '/content/languages/set/',
      'POST',
      'id=' + id,
      function (response) {

        if (ajaxSuccess(response)) {
          location.reload();
        } else defaultErrorHandler(response);
      }
    );
  }
);


$('#form-modal').on('hide.bs.modal',
  function (event) {
    setTimeout(
      function () {
        removeLanguageTabsActiveClass();
        $('.form-modal-content').html('');
      }, 800
    );
  }
);


$('.gallery-wrapper-toggler').on('click',
  function (event) {
    event.preventDefault();

    var $wrapper = $(this).parent('.toggler-wrapper');
    var $gallery = $wrapper.next('.gallery-wrapper');

    $wrapper.toggleClass('active');

    if ($wrapper.hasClass('active')) {
      $wrapper.find('i').removeClass('fa-chevron-down');
      $wrapper.find('i').addClass('fa-chevron-up');
      $gallery.slideDown();
    } else {
      $wrapper.find('i').removeClass('fa-chevron-up');
      $wrapper.find('i').addClass('fa-chevron-down');
      $gallery.slideUp();
    }
  }
);


$(document).on('ifChanged', '.publish-checkbox',
  function () {

    var value = this.checked ? 1 : 0;
    var langId = $(this).data('lang_id');

    //console.log('langId: ' + langId + ', value: ' + value);

    var langSuffix = getLanguageSuffix(langId);

    $('#insert-form' + langSuffix).find('.published').val(value);
  }
);


$(document).on('ifChanged', '.allow-comments-checkbox',
  function () {

    var value = this.checked ? 1 : 0;
    var langId = $(this).data('lang_id');

    //console.log('langId: ' + langId + ', value: ' + value);

    var langSuffix = getLanguageSuffix(langId);

    $('#insert-form' + langSuffix).find('.allow-comments').val(value);
  }
);


$('.remove-date').on('click',
  function (event) {
    event.preventDefault();

    $(this).parents('.date-field-wrapper').find('.date').val('');
  }
);