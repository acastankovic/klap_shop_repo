function ajaxCall(url, type, data, successHandler, errorHandler) {

  if (typeof errorHandler === 'undefined') errorHandler = defaultErrorHandler;

  $.ajax({
    url: BASE_URL + url,
    type: type,
    data: data,
    cache: false,
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Accept', 'application/json');
    },
    success: successHandler,
    error: errorHandler
  });
};


function defaultErrorHandler(error) {
  alert('Server error');
  console.log(error);
  if ($('.action-buttons').length > 0) {
    enableActionButtons();
  }
};


function asyncInsertAction(endpoint, redirectUrl, $form, reload = null) {

  endpoint = stripBeginningAndEndingSlashes(endpoint);
  endpoint = '/' + endpoint;
  redirectUrl = stripBeginningAndEndingSlashes(redirectUrl);

  ajaxCall(
    endpoint,
    'POST',
    $form.serialize(),
    function (response) {

      if (ajaxSuccess(response)) {

        var success = response.data.success;
        var message = response.data.message;

        if (!success) {
          openWarningPopup(message,
            function () {
              enableActionButtons();
            }
          );
          return;
        }

        if (typeof response.data.id != 'undefined' && response.data.id != null) {

          var id = response.data.id;

          redirectUrl = redirectUrl.replace(':id', id);
        }

        openMessagePopup(message,
          function () {

            if (typeof reload != 'undefined' && reload != null && reload == true) {
              location.reload();
            } else {
              window.location = BASE_URL + '/' + redirectUrl;
            }
          }
        );
      } else defaultErrorHandler(response);
    }
  );
};


function asyncUpdateAction(url, langId = null) {

  if (typeof langId == 'undefined' || langId == null) langId = getLanguageId();

  var langSuffix = getLanguageSuffix();

  ajaxCall(
    url,
    'PUT',
    $('#insert-form' + langSuffix).serialize(),
    function (response) {

      if (ajaxSuccess(response)) {

        var success = response.data.success;
        var message = response.data.message;

        if (typeof success != 'undefined' && !success) {
          openWarningPopup(message,
            function () {
              enableActionButtons();
            }
          );
          return;
        }

        openMessagePopup(message,
          function () {
            location.reload();
          }
        );
      } else defaultErrorHandler(response);
    }
  );
};


function asyncDeleteAction(element, route, warningMessage) {

  var id = $(element).data('id');
  var row = $(element).parents('tr');
  var route = stripBeginningAndEndingSlashes(route);
  var url = '/' + route + '/' + id;

  swal({
      title: NC_TRANSLATION[LANG.ALIAS].sAreYouSure,
      text: warningMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: NC_TRANSLATION[LANG.ALIAS].sConfirmDelete,
      cancelButtonText: NC_TRANSLATION[LANG.ALIAS].sCancel,
      closeOnConfirm: true
    },
    function (isConfirm) {

      if (isConfirm) {

        ajaxCall(
          url,
          'DELETE',
          null,
          function (response) {

            if (ajaxSuccess(response)) {

              $('.table').DataTable().row(row).remove().draw();
            } else defaultErrorHandler(response);
          }
        );
      } else {
        swal(NC_TRANSLATION[LANG.ALIAS].sCanceled, NC_TRANSLATION[LANG.ALIAS].sCanceledMessage, 'info');
      }
    });
};


function asyncPublishAction(element, url) {

  if (actionAllowed(element)) {

    disableActionButtons();

    var id = $(element).attr('data-id');
    var action = $(element).attr('data-action');

    renderPublishChanges(element, action);

    var published = 0;
    if (action == 'publish') {
      published = 1;
    }

    ajaxCall(
      url,
      'PUT',
      'id=' + id + '&published=' + published,
      function (response) {

        if (ajaxSuccess(response)) {

          openMessagePopup(response.data);
          $('.published').val(published);
          enableActionButtons();
        } else defaultErrorHandler(response);
      }
    );
  }
};