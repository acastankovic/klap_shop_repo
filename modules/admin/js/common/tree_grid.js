// click on categories tree root category leaf
$(document).on('click', '.tree-grid-root-category',
  function () {

    var langId = $(this).attr('data-lang_id');
    var tree = treeElem(langId);

    $(this).addClass('root-selected');
    $(tree).jstree('deselect_all');
  }
);


// click on categories tree leaf
$('.tree-grid').on('select_node.jstree',
  function (event, data) {

    var langId = $(this).attr('data-lang_id');
    var langSuffix = getLanguageSuffix(langId);
    var treeGridRootCategory = treeGridRootCategoryElem(langId);
    var tree = treeElem(langId);

    $(treeGridRootCategory).removeClass('root-selected');

    if ($('#category-id' + langSuffix).length > 0) {

      var categoryId = $('#category-id' + langSuffix).val();
      var selectedNodeId = $(tree).jstree().get_selected(true)[0].id;

      if (categoryId == selectedNodeId) {
        openWarningPopup(NC_TRANSLATION[LANG.ALIAS].sCategoryOwnParent);
        $(tree).jstree(true).deselect_node(data.node);
      }
    }
  }
);


// init categories tree
function initTreeGrid() {

  var $treeGrid = $('.tree-grid');

  if ($treeGrid.length != 0) {

    $treeGrid.each(
      function () {

        var langId = $(this).attr('data-lang_id');
        var rootCategoryId = $(this).attr('data-root_id');
        var treeGridRootCategory = treeGridRootCategoryElem(langId);
        var parentIdFormField = parentIdFormFieldElem(langId);

        var self = this;

        ajaxCall(
          '/content/categories/tree/' + rootCategoryId + '/lang/' + langId,
          'GET',
          null,
          function (response) {

            if (ajaxSuccess(response)) {

              $(self).on('move_node.jstree', function (e, data) {
                handleDnD(e, data);
              }).jstree({
                'core': {
                  'check_callback': true,
                  'data': response.data
                },
                'plugins': ['dnd']
              }).bind('loaded.jstree', function (e, data) {

                if ($(parentIdFormField).length > 0) {

                  // invoked after jstree has loaded
                  var parentId = $(parentIdFormField).val();
                  if (parentId && parentId !== '0') {
                    setTimeout(function () {
                      $(self).jstree(true).select_node(parentId);
                    }, 2200);
                  }
                  else {
                    $(treeGridRootCategory).addClass('root-selected');
                  }
                }
              });

              $(self).jstree(true).settings.core.data = response.data;
              $(self).jstree(true).refresh();
            }
            else defaultErrorHandler(response);
          }
        );
      }
    );
  }
};