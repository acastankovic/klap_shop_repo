$(document).on('ready',
  function () {
    init();
  }
);


function init() {
  initTreeGrid();
  initCkeditor();
  setResource();
  initMediaModalDropZone();
  initTooltips();
};

/****************************** ACTIONS ******************************/


// insert / update
$('.save-insert-form').on('click',
  function (event) {
    event.preventDefault();

    if (actionAllowed(this)) {

      disableActionButtons();

      var langSuffix = getLanguageSuffix();

      var id = $('#' + RESOURCE + '-id' + langSuffix).val();
      var title = $('#' + RESOURCE + '-title' + langSuffix).val();

      if (title.trim() === '') {
        openWarningPopup(
          NC_TRANSLATION[LANG.ALIAS].sTitleRequired,
          function () {
            enableActionButtons();
          }
        );
        return;
      }

      setFormParentId();
      setFormRang();
      setGallery();
      setYouTubeVideoCode(RESOURCE, langSuffix);

      if (id == 0) {

        //insert
        asyncInsertAction('/shop/products/insert/', '/admin/products/:id/insert', $('#insert-form' + langSuffix));
      } else {

        //update
        asyncUpdateAction('/shop/products/update/');
      }

    }
  }
);

/************************** / END OF ACTIONS **************************/