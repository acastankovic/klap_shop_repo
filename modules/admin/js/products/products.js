// delete
$(document).on('click', '.delete',
  function () {
    var route = '/shop/products/';
    var message = NC_TRANSLATION[LANG.ALIAS].sDeleteProductWarning;
    asyncDeleteAction(this, route, message);
  }
);