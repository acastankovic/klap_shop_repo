<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo Trans::get('Gallery'); ?> | <?php echo Trans::get('Administration dashboard'); ?></title>
    <?php $this->displayTemplate('common/meta.php', null) ?>
    <?php $this->displayTemplate('common/css.php', null) ?>
    <link rel="stylesheet" href="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/dropzone/dropzone.min.css"/>
  </head>
  <body class="sidebar-light fixed-topbar theme-sltl bg-light-dark color-default" id="galleryInsertPage">

    <?php $this->displayTemplate('layout/ienotice.php', null); ?>

    <section>

      <?php $this->displayTemplate('layout/leftbar.php', null); ?>

      <div class="main-content">

        <?php $this->displayTemplate('layout/topbar.php', null); ?>

        <div class="page-content">

          <div class="panel">

            <?php $this->displayPanelHeader(array('title' => Trans::get('Gallery content'))); ?>

            <div class="panel-content">

              <?php $this->displayInsertPageContent(); ?>

            </div><!-- /end of panel-content -->

          </div><!-- /end of panel -->

        </div><!-- /end of page-content -->

        <?php $this->displayTemplate('layout/footer.php', null); ?>

      </div><!-- /end of main-content -->

    </section>

    <?php $this->displayModals(); ?>
    <!-- /end of modals -->

    <?php $this->displayTemplate('common/scripts.php', null); ?>
    <?php $this->displayTemplate('common/constants.php', null); ?>
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/dropzone/dropzone.min.js"></script>
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/js/galleries/gallery_insert.js"></script>
  </body>
</html>