<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo Trans::get('Menu'); ?> | <?php echo Trans::get('Administration dashboard'); ?></title>
    <?php $this->displayTemplate('common/meta.php', null) ?>
    <?php $this->displayTemplate('common/css.php', null) ?>
  </head>
  <body class="sidebar-light fixed-topbar theme-sltl bg-light-dark color-default" id="menuItemsPage">

    <?php $this->displayTemplate('layout/ienotice.php', null); ?>

    <?php $this->displayHiddenFields(); ?>

    <section>

      <?php $this->displayTemplate('layout/leftbar.php', null); ?>

      <div class="main-content">

        <?php $this->displayTemplate('layout/topbar.php', null); ?>

        <div class="page-content">

          <div class="panel">

            <?php $this->displayPanelHeader(array('title' => Trans::get('Menu items'))); ?>

            <div class="panel-content">

              <div class="row">

                <?php $this->renderBackButton(array('url' => Conf::get('url') . '/admin/menus')); ?>

                <?php $this->displayTablePageTree(array('title' => Trans::get('Menu tree'))); ?>

                <div class="col-md-8 main-wrapper">

                  <?php $this->displayMenuItemsTable(); ?>

                </div><!-- /end of main-wrapper -->

              </div><!-- /end of row -->

            </div><!-- /end of panel-content -->

          </div><!-- /end of panel -->

        </div><!-- /end of page-content -->

        <?php $this->displayTemplate('layout/footer.php', null); ?>

      </div><!-- /end of main-content -->

    </section>

    <?php $this->displayMenuItemsFormModal(); ?>
    <!-- /end of form-modal -->

    <?php $this->displayTemplate('common/scripts.php', null); ?>
    <?php $this->displayTemplate('common/constants.php', null); ?>
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/jstree/jstree.js"></script>
    <!-- JSTree -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/js/menus/menu_items.js"></script>
  </body>
</html>