<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo Trans::get('Slider item'); ?> | <?php echo Trans::get('Administration dashboard'); ?></title>
    <?php $this->displayTemplate('common/meta.php', null) ?>
    <?php $this->displayTemplate('common/css.php', null) ?>
    <link href="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet">
  </head>
  <body class="sidebar-light fixed-topbar theme-sltl bg-light-dark color-default" id="sliderItemInsertPage">

    <?php $this->displayTemplate('layout/ienotice.php', null); ?>

    <section>

      <?php $this->displayTemplate('layout/leftbar.php', null); ?>

      <div class="main-content">

        <?php $this->displayTemplate('layout/topbar.php', null); ?>

        <div class="page-content">

          <div class="panel">

            <?php $this->displayPanelHeader(array('title' => Trans::get('Slider item content'))); ?>

            <div class="panel-content">

              <?php $this->renderBackButton(array('url' => Conf::get('url') . '/admin/sliders/' . $this->parentId . '/insert')); ?>

              <?php $this->displayInsertPageContent(); ?>

            </div><!-- /end of panel-content -->

          </div><!-- /end of panel -->

        </div><!-- /end of page-content -->

        <?php $this->displayTemplate('layout/footer.php', null); ?>

      </div><!-- /end of main-content -->

    </section>

    <?php $this->displayMediaModal(); ?>
    <!-- /end of media-modal -->

    <?php $this->displayTemplate('common/scripts.php', null); ?>
    <?php $this->displayTemplate('common/constants.php', null); ?>
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/dropzone/dropzone.min.js"></script>
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/js/sliders/slider_item_insert.js"></script>
  </body>
</html>