<!DOCTYPE html>
<html lang="en">
  <head>
    <title>test</title>
    <?php $this->displayTemplate('layout/head.php', null) ?>
  </head>
  <!-- LAYOUT: Apply "submenu-hover" class to body element to have sidebar submenu show on mouse hover -->
  <!-- LAYOUT: Apply "sidebar-collapsed" class to body element to have collapsed sidebar -->
  <!-- LAYOUT: Apply "sidebar-top" class to body element to have sidebar on top of the page -->
  <!-- LAYOUT: Apply "sidebar-hover" class to body element to show sidebar only when your mouse is on left / right corner -->
  <!-- LAYOUT: Apply "submenu-hover" class to body element to show sidebar submenu on mouse hover -->
  <!-- LAYOUT: Apply "fixed-sidebar" class to body to have fixed sidebar -->
  <!-- LAYOUT: Apply "fixed-topbar" class to body to have fixed topbar -->
  <!-- LAYOUT: Apply "rtl" class to body to put the sidebar on the right side -->
  <!-- LAYOUT: Apply "boxed" class to body to have your page with 1200px max width -->

  <!-- THEME STYLE: Apply "theme-sdtl" for Sidebar Dark / Topbar Light -->
  <!-- THEME STYLE: Apply  "theme sdtd" for Sidebar Dark / Topbar Dark -->
  <!-- THEME STYLE: Apply "theme sltd" for Sidebar Light / Topbar Dark -->
  <!-- THEME STYLE: Apply "theme sltl" for Sidebar Light / Topbar Light -->

  <!-- THEME COLOR: Apply "color-default" for dark color: #2B2E33 -->
  <!-- THEME COLOR: Apply "color-primary" for primary color: #319DB5 -->
  <!-- THEME COLOR: Apply "color-red" for red color: #C9625F -->
  <!-- THEME COLOR: Apply "color-green" for green color: #18A689 -->
  <!-- THEME COLOR: Apply "color-orange" for orange color: #B66D39 -->
  <!-- THEME COLOR: Apply "color-purple" for purple color: #6E62B5 -->
  <!-- THEME COLOR: Apply "color-blue" for blue color: #4A89DC -->
  <!-- BEGIN BODY -->
  <body class="sidebar-light fixed-topbar theme-sltl bg-light-dark color-default dashboard">

    <?php $this->displayTemplate('layout/ienotice.php', null); ?>

    <section>
      <?php $this->displayTemplate('layout/leftbar.php', null); ?>
      <div class="main-content">
        <?php $this->displayTemplate('layout/topbar.php', null); ?>
        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content page-thin">
          <div class="row">
            <div class="col-md-4 col-sm-6 portlets">
              <div class="panel">
                <div class="panel-header panel-controls">
                  <h3><i class="icon-list"></i> <strong>Todo</strong> List</h3>
                </div>
                <div class="panel-content">
                  <ul class="todo-list ui-sortable">
                    <li class="high">
                      <span class="span-check">
                        <input id="task-1" type="checkbox" data-checkbox="icheckbox_square-blue"/>
                        <label for="task-1"></label>
                      </span>
                      <span class="todo-task">Send email to Bob Linch</span>
                      <div class="todo-date clearfix">
                        <div class="completed-date"></div>
                        <div class="due-date">Due on <span class="due-date-span">15 December 2014</span></div>
                      </div>
                      <span class="todo-options pull-right">
                        <a href="javascript:;" class="todo-delete"><i class="icons-office-52"></i></a>
                      </span>
                      <div class="todo-tags pull-right">
                        <div class="label label-success">Work</div>
                      </div>
                    </li>
                    <li>
                      <span class="span-check">
                        <input id="task-2" type="checkbox" data-checkbox="icheckbox_square-blue"/>
                        <label for="task-2"></label>
                      </span>
                      <span class="todo-task">Call datacenter for servers</span>
                      <div class="todo-date clearfix">
                        <div class="completed-date"></div>
                        <div class="due-date">Due on <span class="due-date-span">7 January</span></div>
                      </div>
                      <span class="todo-options pull-right">
                        <a href="javascript:;" class="todo-delete"><i class="icons-office-52"></i></a>
                      </span>
                    </li>
                    <li class="low">
                      <span class="span-check">
                        <input id="task-3" type="checkbox" data-checkbox="icheckbox_square-blue"/>
                        <label for="task-3"></label>
                      </span>
                      <span class="todo-task">Remove all unused icons</span>
                      <div class="todo-date clearfix">
                        <div class="completed-date"></div>
                        <div class="due-date">Due on <span class="due-date-span">5 January</span></div>
                      </div>
                      <span class="todo-options pull-right">
                        <a href="javascript:;" class="todo-delete"><i class="icons-office-52"></i></a>
                      </span>
                    </li>
                    <li class="medium">
                      <span class="span-check">
                        <input id="task-4" type="checkbox" data-checkbox="icheckbox_square-blue"/>
                        <label for="task-4"></label>
                      </span>
                      <span class="todo-task">Read my todo list</span>
                      <div class="todo-date clearfix">
                        <div class="completed-date"></div>
                        <div class="due-date">Due on <span class="due-date-span">4 January</span></div>
                      </div>
                      <span class="todo-options pull-right">
                        <a href="javascript:;" class="todo-delete"><i class="icons-office-52"></i></a>
                      </span>
                      <div class="todo-tags pull-right">
                        <div class="label label-info">Tuesday</div>
                      </div>
                    </li>
                    <li>
                      <span class="span-check">
                        <input id="task-6" type="checkbox" data-checkbox="icheckbox_square-blue"/>
                        <label for="task-6"></label>
                      </span>
                      <span class="todo-task">Have a breakfeast before 12</span>
                      <div class="todo-date clearfix">
                        <div class="completed-date"></div>
                        <div class="due-date">Due on <span class="due-date-span">1 January</span></div>
                      </div>
                      <span class="todo-options pull-right">
                        <a href="javascript:;" class="todo-delete"><i class="icons-office-52"></i></a>
                      </span>
                    </li>
                  </ul>
                  <div class="clearfix m-t-10">
                    <div class="pull-left">
                      <button type="button" class="btn btn-sm btn-default check-all-tasks">Check All Done</button>
                    </div>
                    <div class="pull-right">
                      <button type="button" class="btn btn-sm btn-dark add-task">Add Task</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!-- /end of page-content -->
        <?php $this->displayTemplate('layout/footer.php', null); ?>
      </div><!-- /end of main-content -->
    </section>


    <?php $this->displayTemplate('common/scripts.php', null); ?>
    <?php $this->displayTemplate('common/constants.php', null); ?>
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/noty/jquery.noty.packaged.min.js"></script>
    <!-- Notifications -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script>
    <!-- Inline Edition X-editable -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script>
    <!-- Context Menu -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/multidatepicker/multidatespicker.min.js"></script>
    <!-- Multi dates Picker -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/js/widgets/todo_list.js"></script>
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/metrojs/metrojs.min.js"></script>
    <!-- Flipping Panel -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/charts-chartjs/Chart.min.js"></script>
    <!-- ChartJS Chart -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/charts-highstock/js/highstock.min.js"></script>
    <!-- financial Charts -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/charts-highstock/js/modules/exporting.min.js"></script>
    <!-- Financial Charts Export Tool -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/maps-amcharts/ammap/ammap.min.js"></script>
    <!-- Vector Map -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/maps-amcharts/ammap/maps/js/worldLow.min.js"></script>
    <!-- Vector World Map  -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/maps-amcharts/ammap/themes/black.min.js"></script>
    <!-- Vector Map Black Theme -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/skycons/skycons.min.js"></script>
    <!-- Animated Weather Icons -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/simple-weather/jquery.simpleWeather.js"></script>
    <!-- Weather Plugin -->
    <script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/js/widgets/widget_weather.js"></script>
  </body>
</html>