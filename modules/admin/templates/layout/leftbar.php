<div class="sidebar">
  <div class="logopanel">
    <h1><a href="<?php echo Conf::get('url') . "/admin"; ?>"></a></h1>
  </div>
  <div class="sidebar-inner">
    <ul class="nav nav-sidebar">
      <!-- media -->
      <li class="nav-parent <?php if (isset($this) && isset($this->activeNavigation) && $this->activeNavigation === 'media') echo 'active'; ?>">
        <a href=""><i class="icon icon-folder-alt"></i><span><?php echo Trans::get('Media'); ?> </span><span class="fa arrow"></span></a>
        <ul class="children collapse">
          <li><a href="<?php echo Conf::get('url') . '/admin/media/'; ?>"><?php echo Trans::get('List'); ?></a></li>
        </ul>
      </li>
      <!-- categories -->
      <li class="nav-parent <?php if (isset($this) && isset($this->activeNavigation) && $this->activeNavigation === 'categories') echo 'active'; ?>">
        <a href=""><i class="icon icon-layers"></i><span><?php echo Trans::get('Categories'); ?> </span><span class="fa arrow"></span></a>
        <ul class="children collapse">
          <li><a href="<?php echo Conf::get('url') . '/admin/categories'; ?>"><?php echo Trans::get('List'); ?></a></li>
          <li><a href="<?php echo Conf::get('url') . '/admin/categories/0/insert'; ?>"><?php echo Trans::get('Add new'); ?></a>
          </li>
        </ul>
      </li>
      <!-- articles -->
      <li class="nav-parent <?php if (isset($this) && isset($this->activeNavigation) && $this->activeNavigation === 'articles') echo 'active'; ?>">
        <a href=""><i class="icon icon-docs"></i><span><?php echo Trans::get("Articles"); ?> </span><span class="fa arrow"></span></a>
        <ul class="children collapse">
          <li><a href="<?php echo Conf::get('url') . '/admin/articles/'; ?>"><?php echo Trans::get('List'); ?></a></li>
          <li><a href="<?php echo Conf::get('url') . '/admin/articles/0/insert'; ?>"><?php echo Trans::get('Add new'); ?></a></li>
          <li><a href="<?php echo Conf::get('url') . '/admin/articles/categories/sort'; ?>"><?php echo Trans::get('Sort'); ?></a></li>
        </ul>
      </li>
      <!-- menus -->
      <li class="nav-parent <?php if (isset($this) && isset($this->activeNavigation) && $this->activeNavigation === 'menus') echo 'active'; ?>">
        <a href=""><i class="icon icon-grid"></i><span><?php echo Trans::get('Menus'); ?> </span><span class="fa arrow"></span></a>
        <ul class="children collapse">
          <li><a href="<?php echo Conf::get('url') . '/admin/menus/'; ?>"><?php echo Trans::get('List'); ?></a></li>
        </ul>
      </li>
      <!-- sliders -->
      <li class="nav-parent <?php if (isset($this) && isset($this->activeNavigation) && $this->activeNavigation === 'sliders') echo 'active'; ?>">
        <a href=""><i class="icon icon-loop"></i><span><?php echo Trans::get('Sliders'); ?> </span><span class="fa arrow"></span></a>
        <ul class="children collapse">
          <li><a href="<?php echo Conf::get('url') . '/admin/sliders/'; ?>"><?php echo Trans::get('List'); ?></a></li>
        </ul>
      </li>
      <!-- galleries -->
      <li class="nav-parent <?php if (isset($this) && isset($this->activeNavigation) && $this->activeNavigation === 'galleries') echo 'active'; ?>">
        <a href=""><i class="icon icon-picture"></i><span><?php echo Trans::get('Galleries'); ?> </span><span class="fa arrow"></span></a>
        <ul class="children collapse">
          <li><a href="<?php echo Conf::get('url') . '/admin/galleries/'; ?>"><?php echo Trans::get('List'); ?></a></li>
          <li><a href="<?php echo Conf::get('url') . '/admin/galleries/0/insert'; ?>"><?php echo Trans::get('Add new'); ?></a>
          </li>
        </ul>
      </li>
      <!-- products -->
      <li class="nav-parent <?php if (isset($this) && isset($this->activeNavigation) && $this->activeNavigation === 'products') echo 'active'; ?>">
        <a href=""><i class="icon icon-basket"></i><span><?php echo Trans::get('Products'); ?> </span><span class="fa arrow"></span></a>
        <ul class="children collapse">
          <li><a href="<?php echo Conf::get('url') . '/admin/products/'; ?>"><?php echo Trans::get('List'); ?></a></li>
          <li><a href="<?php echo Conf::get('url') . '/admin/products/0/insert'; ?>"><?php echo Trans::get('Add new'); ?></a>
          </li>
        </ul>
      </li>
      <!-- shop categories -->
      <li class="nav-parent <?php if (isset($this) && isset($this->activeNavigation) && $this->activeNavigation === 'shop_categories') echo 'active'; ?>">
        <a href=""><i class="icon icon-basket"></i><span><?php echo Trans::get('Shop Categories'); ?> </span><span class="fa arrow"></span></a>
        <ul class="children collapse">
          <!--             <li><a href="--><?php //echo Conf::get('url') . '/admin/categories/shop'; ?><!--">-->
          <?php //echo Trans::get('List'); ?><!--</a></li>-->
          <li><a
              href="<?php echo Conf::get('url') . '/admin/categories/shop/0/insert'; ?>"><?php echo Trans::get('Add new'); ?></a>
          </li>
        </ul>
      </li>
      <!-- vendors -->
      <li class="nav-parent <?php if (isset($this) && isset($this->activeNavigation) && $this->activeNavigation === 'vendors') echo 'active'; ?>">
        <a href=""><i class="icon icon-basket"></i><span><?php echo Trans::get('Vendors'); ?> </span><span class="fa arrow"></span></a>
        <ul class="children collapse">
          <li><a href="<?php echo Conf::get('url') . "/admin/vendors/"; ?>"><?php echo Trans::get('List'); ?></a></li>
          <li><a href="<?php echo Conf::get('url') . "/admin/vendors/0/insert"; ?>"><?php echo Trans::get('Add new'); ?></a>
          </li>
        </ul>
      </li>
      <!-- orders -->
      <li class="nav-parent <?php if (isset($this) && isset($this->activeNavigation) && $this->activeNavigation === 'orders') echo 'active'; ?>">
        <a href=""><i class="icon icon-basket"></i><span><?php echo Trans::get('Orders'); ?> </span><span class="fa arrow"></span></a>
        <ul class="children collapse">
          <li><a href="<?php echo Conf::get('url') . '/admin/orders/'; ?>"><?php echo Trans::get('List'); ?></a></li>
        </ul>
      </li>
      <!-- newsletter -->
      <li class="nav-parent <?php if (isset($this) && isset($this->activeNavigation) && $this->activeNavigation === 'newsletter') echo 'active'; ?>">
        <a href=""><i class="icon icon-envelope"></i><span><?php echo Trans::get('Newsletter'); ?> </span><span class="fa arrow"></span></a>
        <ul class="children collapse">
          <li><a href="<?php echo Conf::get('url') . '/admin/newsletter/'; ?>"><?php echo Trans::get('List'); ?></a> </li>
          <!--          <li><a href="--><?php //echo Conf::get('url') . '/admin/newsletter/0/insert'; ?><!--">-->
          <?php //echo Trans::get('Add new'); ?><!--</a></li>-->
        </ul>
      </li>
      <?php if ((int)$this->user->role_id === (int)Conf::get('user_role_id')['admin']) { ?>
        <!-- users -->
        <li class="nav-parent <?php if (isset($this) && isset($this->activeNavigation) && $this->activeNavigation === 'users') echo 'active'; ?>">
          <a href=""><i class="icon-users"></i><span><?php echo Trans::get('Users'); ?> </span><span class="fa arrow"></span></a>
          <ul class="children collapse">
            <li><a href="<?php echo Conf::get('url') . '/admin/users/'; ?>"><?php echo Trans::get('List'); ?></a></li>
            <li><a href="<?php echo Conf::get('url') . '/admin/users/0/insert'; ?>"><?php echo Trans::get('Add new'); ?></a></li>
          </ul>
        </li>
      <?php } ?>
    </ul>
  </div>
</div>
<!-- /end of leftbar -->