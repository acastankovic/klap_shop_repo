<div class="topbar">
  <div class="header-left">
    <div class="topnav">
      <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>
    </div>
  </div>

  <div class="header-right">
    <ul class="header-menu nav navbar-nav">
      <?php
      if (Languages::enabled()) {

        $languages = Languages::getActive();

        if (count($languages) > 1) {

          echo '<li class="dropdown" id="language-header">';
            echo '<a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">';
              echo '<span class="username">' . Trans::get('Language') . ': ' . Trans::getLanguageAlias() . '</span>';
            echo '</a>';
            echo '<ul class="dropdown-menu">';
            foreach ($languages as $language) {
              echo '<li><a href="#" class="set-language" data-id="' . $language->id . '"><span>' . $language->nameTranslated . '</span></a></li>';
            }
            echo '</ul>';
          echo '</li>';
        }
      }
      ?>
      <li class="dropdown" id="user-header">
        <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
          <?php if (@exists($this->user->image) && file_exists(Conf::get('media_root') . '/' . $this->user->image)) { ?>
            <img src="<?php echo Conf::get('media_url') . '/' . $this->user->image; ?>" alt="<?php echo $this->user->image; ?>">
          <?php } else { ?>
            <img src="<?php echo Conf::get('url'); ?>/modules/admin/css/img/user.png" alt="user image">
          <?php } ?>
          <span class="username"><?php echo $this->user->username; ?></span>
        </a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo Conf::get('url') . '/admin/users/' . $this->user->id . '/insert'; ?>"><i class="icon-user"></i><span><?php echo Trans::get('My Profile'); ?></span></a></li>
          <li><a href="<?php echo Conf::get('url') . '/admin/logout'; ?>"><i class="icon-logout"></i><span><?php echo Trans::get('Logout'); ?></span></a></li>
        </ul>
      </li>
      <!-- /end of user dropdown -->
    </ul>
  </div>
  <!-- /end of header-right -->
</div>
<!-- /end of topbar -->