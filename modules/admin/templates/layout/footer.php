<div class="loader-overlay">
  <div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
  </div>
</div>
<!-- /end of preloader -->

<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>

<div class="footer">
  <div class="copyright">
    <p class="pull-left sm-pull-reset">
      <span>Copyright <span class="copyright">©</span> <?php echo date('Y'); ?> </span>
      <span>Normasoft</span>.
      <span>All rights reserved. </span>
    </p>
  </div>
</div>
<!-- /end of footer -->