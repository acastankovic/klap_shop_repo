<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/gsap/main-gsap.min.js"></script>
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/jquery-cookies/jquery.cookies.min.js"></script> <!-- Jquery Cookies, for theme -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script> <!-- simulate synchronous behavior when using AJAX -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script> <!-- Custom Scrollbar sidebar -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script> <!-- Show Dropdown on Mouseover -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/retina/retina.min.js"></script> <!-- Retina Display -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/select2/select2.min.js"></script> <!-- Select Inputs -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/js/builder.js"></script> <!-- Theme Builder -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/custom/js/application.js"></script> <!-- Main Application Script -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/js/plugins.js"></script> <!-- Main Plugin Initialization Script -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/js/widgets/notes.js"></script> <!-- Notes Widget -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/js/quickview.js"></script> <!-- Chat Script -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/js/pages/search.js"></script> <!-- Search Script -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script> <!-- Select Inputs -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/sweetalert/sweet-alert.min.js"></script><!-- message popus -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/typed/typed.min.js"></script>
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/admin/layout3/js/layout.js"></script>

<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/assets/custom/js/pages/table_dynamic.js"></script>

<!-- common scripts -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/js/common/translations.js"></script> <!--  -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/js/common/util.js"></script> <!--  -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/js/common/tree_grid.js"></script> <!--  -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/js/common/main.js"></script> <!--  -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/js/common/async.js"></script> <!-- async functions -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/js/common/media_modals.js"></script> <!-- Media Modals Script -->
<script src="<?php echo Conf::get('url'); ?>/modules/admin/js/common/languages.js"></script> <!-- languages -->

<!-- <script src="<?php //echo Conf::get('url'); ?>/modules/admin/js/common/table_pagination.js"></script> --> <!-- pagination -->