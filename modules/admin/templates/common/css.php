<link rel="stylesheet" href="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/css/style.css" />
<link rel="stylesheet" href="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/css/theme.css" />
<link rel="stylesheet" href="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/css/ui.css" />
<link rel="stylesheet" href="<?php echo Conf::get('url'); ?>/modules/admin/assets/admin/layout3/css/layout.css" />
<link rel="stylesheet" href="<?php echo Conf::get('url'); ?>/modules/admin/css/main.css" />
<link rel="stylesheet" href="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/metrojs/metrojs.min.css" />
<link rel="stylesheet" href="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/maps-amcharts/ammap/ammap.min.css" />
<link rel="stylesheet" href="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/sweetalert/sweet-alert.css" />
<link rel="stylesheet" href="<?php echo Conf::get('url'); ?>/modules/admin/assets/global/plugins/jstree/src/themes/default/style.min.css" />
<!-- favicon -->
<link rel="icon" href="<?php echo Conf::get('url') ?>/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="<?php echo Conf::get('url') ?>/favicon.ico" type="image/x-icon" />