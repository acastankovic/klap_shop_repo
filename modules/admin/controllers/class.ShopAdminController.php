<?php


class ShopAdminController extends AdminController {

  public function __construct() {

    parent::__construct();

    Trans::initTranslations();
  }


  /*************************************************************************
   *                            CATEGORIES                                  *
   *************************************************************************/


  public function categoriesPage() {

    $pageType = $this->params('type');

    if (isset($pageType) && (string)$pageType === 'shop') {

      $data = array('items' => Dispatcher::instance()->dispatch('content', 'categories', 'fetchForAdminTables', null));
      $data['shopPage'] = true;

      $tableView = new CategoriesAdminView($this, $data);
      $tableView->respond(null, 'pages/categories/categories.php');
    } else $this->dashboardPage();
  }


  public function categoryInsertPage() {

    $pageType = $this->params('type');

    if (isset($pageType) && (string)$pageType === 'shop') {

      $id = (int)$this->params('id');

      $fetchResults = Dispatcher::instance()->dispatch('content', 'categories', 'fetchOneWithLanguageGroups', array('id' => $id, 'fetchWithUnpublished' => true));

      $data = array(
        'items' => $fetchResults->items,
        'langGroupId' => $fetchResults->langGroupId,
        'languages' => Languages::getActive(),
        'shopPage' => true
      );

      $tableView = new CategoriesAdminView($this, $data);
      $tableView->respond(null, 'pages/categories/category_insert.php');
    } else $this->dashboardPage();
  }


//    private function getShopRootCategoryIds() {
//
//        $shopRootCategories = Dispatcher::instance()->dispatch('content', 'categories', 'fetchOneWithLanguageGroups', array('id' => Conf::get('shop_category_id'), 'fetchWithUnpublished' => true));
//
//        $items = $shopRootCategories->items;
//
//        $ids = array();
//        foreach ($items as $key => $item) {
//
//            $newItem = new stdClass();
//            $newItem->id = $item->id;
//            $newItem->lang_id = $item->lang_id;
//
//            array_push($ids, $newItem);
//        }
//
//        return $ids;
//    }

  /*************************************************************************
   *                              PRODUCTS                                  *
   *************************************************************************/


  public function productsPage() {

    $data = array('items' => Dispatcher::instance()->dispatch('shop', 'products', 'fetchForAdminTables', null));

    $tableView = new ProductsAdminView($this, $data);
    $tableView->respond(null, 'pages/products/products.php');
  }


  public function productInsertPage() {

    $id = (int)$this->params('id');

    $fetchResults = Dispatcher::instance()->dispatch('content', 'products', 'fetchOneWithLanguageGroups', array('id' => $id, 'fetchWithUnpublished' => true));

    $data = array(
      'items' => $fetchResults->items,
      'langGroupId' => $fetchResults->langGroupId,
      'vendors' => Dispatcher::instance()->dispatch('shop', 'vendors', 'fetchAll', array('fetchWithUnpublished' => true)),
      'languages' => Languages::getActive()
    );

    $tableView = new ProductsAdminView($this, $data);
    $tableView->respond(null, 'pages/products/product_insert.php');
  }


  /*************************************************************************
   *                               VENDORS                                  *
   *************************************************************************/


  public function vendorsPage() {

    $data = array('items' => Dispatcher::instance()->dispatch('shop', 'vendors', 'fetchAll', array('fetchWithUnpublished' => true)));

    $tableView = new VendorsAdminView($this, $data);
    $tableView->respond(null, 'pages/vendors/vendors.php');
  }


  public function vendorInsertPage() {

    $id = (int)$this->params('id');

    $fetchResults = Dispatcher::instance()->dispatch('shop', 'vendors', 'fetchOneWithLanguageGroups', array('id' => $id, 'fetchWithUnpublished' => true));

    $data = array(
      'langGroupId' => $fetchResults->langGroupId,
      'items' => $fetchResults->items,
      'languages' => Languages::getActive()
    );

    $tableView = new VendorsAdminView($this, $data);
    $tableView->respond($data, 'pages/vendors/vendor_insert.php');
  }


  /*************************************************************************
   *                                ORDERS                                  *
   *************************************************************************/


  public function ordersPage() {

    $data = array('items' => Dispatcher::instance()->dispatch('shop', 'orders', 'fetchAll', array('fetchWithUnpublished' => true)));

    $tableView = new OrdersAdminView($this, $data);
    $tableView->respond(null, 'pages/orders/orders.php');
  }


  public function orderInsertPage() {

    $id = (int)$this->params('id');

    $data = array('item' => Dispatcher::instance()->dispatch('shop', 'orders', 'fetchOne', array('id' => $id, 'fetchWithUnpublished' => true)));

    $tableView = new OrdersAdminView($this, $data);
    $tableView->respond(null, 'pages/orders/order_insert.php');
  }

}

?>