<?php


class UsersAdminView extends AdminView {

  public $activeNavigation;
  private $resource;
  private $items;
  private $item;
  private $roles;
  private $loggedInUser;

  public function __construct($controller, $data) {
    parent::__construct($controller);

    $this->activeNavigation = 'users';
    $this->resource = 'user';

    if (@exists($data)) {

      if (!is_array($data)) $data = (array)$data;

      if (@exists($data['items'])) $this->items = $data['items'];
      if (@exists($data['item'])) $this->item = $data['item'];
      if (@exists($data['roles'])) $this->roles = $data['roles'];
      if (@exists($data['loggedInUser'])) $this->loggedInUser = $data['loggedInUser'];
    }
  }


  /********************************** TABLE PAGE **********************************/

  public function displayTable() {

    $this->renderAddNewItemLinkButton(array('url' => Conf::get('url') . '/admin/users/0/insert'));
    $this->renderTable();
  }


  public function renderTable() {

    echo '<div class="table-wrapper">';

      echo '<table class="table table-dynamic table-tools filter-select">';

        echo '<thead>';
          echo '<tr>';
            echo '<th>' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('First Name') . '</th>';
            echo '<th>' . Trans::get('Last Name') . '</th>';
            echo '<th>' . Trans::get('Username') . '</th>';
            echo '<th>' . Trans::get('Email') . '</th>';
            echo '<th>' . Trans::get('Role') . '</th>';
            echo '<th class="text-right">' . Trans::get('Actions') . '</th>';
          echo '</tr>';
        echo '</thead>';

        echo '<tbody>';

        if (@exists($this->items)) {

          foreach ($this->items as $item) {
            echo '<tr data-id="' . $item->id . '">';
              $this->displayCellRaw(array('name' => 'id', 'type' => 'int'), $item);
              $this->displayCellRaw(array('name' => 'first_name', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'last_name', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'username', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'email', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'role', 'type' => 'varchar'), $item);
              echo '<td class="text-right">';
                echo '<a class="btn btn-sm btn-default" href="' . Conf::get('url') . '/admin/users/' . $item->id . '/insert"><i class="icon-note"></i></a>';
                $this->renderUserDeleteButton($item);
              echo '</td>';
            echo '</tr>';
          }
        }
        echo '</tbody>';

        echo '<tfoot>';
          echo '<tr>';
            echo '<th style="visibility: hidden;">' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('First Name') . '</th>';
            echo '<th>' . Trans::get('Last Name') . '</th>';
            echo '<th>' . Trans::get('Username') . '</th>';
            echo '<th>' . Trans::get('Email') . '</th>';
            echo '<th>' . Trans::get('Role') . '</th>';
            echo '<th style="visibility: hidden;"></th>';
          echo '</tr>';
        echo '</tfoot>';

      echo '</table>';

    echo '</div>';
  }


  // logged in user can't delete itself
  public function renderUserDeleteButton($user) {

    if ((int)$user->id !== (int)$this->user->id) {
      echo '<a class="btn btn-sm btn-danger delete" data-id="' . $user->id . '"><i class="icons-office-52"></i></a>';
    }
  }


  /********************************* INSERT PAGE *********************************/


  public function displayInsertPage() {

    echo '<div class="row">';

      echo '<input type="hidden" id="field-resource" value="' . $this->resource . '" />';

      echo '<div class="col-md-9">';
        $this->renderForm();
      echo '</div>';

      echo '<div class="col-md-3 side-panels">';
        $this->displayInsertPageActions(array('item' => $this->item));
        $this->renderImagePanel(array('item' => $this->item));
      echo '</div>';

    echo '</div>';
  }


  private function renderForm() {

    $item = $this->item;

    $id = @exists($item->id) ? $item->id : 0;
    $firstName = @exists($item->first_name) ? htmlentities($item->first_name) : '';
    $lastName = @exists($item->last_name) ? htmlentities($item->last_name) : '';
    $username = @exists($item->username) ? htmlentities($item->username) : '';
    $email = @exists($item->email) ? htmlentities($item->email) : '';
    $image = @exists($item->image) ? $item->image : '';

    $langId = Trans::getLanguageId();

    $langSuffix = $this->getLanguageSuffix();

    echo '<form id="insert-form' . $langSuffix . '">';

      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'id', 'value' => $id));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'image', 'value' => $image, 'className' => 'image-field' . $langSuffix));
      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'username', 'value' => $username, 'label' => Trans::get('Username')));

      if ((int)$id === 0) {
        $this->displayFormField(array('type' => 'password', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'password', 'value' => $username, 'label' => Trans::get('Password')));
        $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'created_by', 'value' => $this->loggedInUser->user_id));
      }
      else {
        $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'updated_by', 'value' => $this->loggedInUser->user_id));
      }

      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'first_name', 'value' => $firstName, 'label' => Trans::get('First Name')));
      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'last_name', 'value' => $lastName, 'label' => Trans::get('Last Name')));
      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'email', 'value' => $email, 'label' => Trans::get('Email')));

      if ($this->adminLoggedIn()) {
        $this->renderRolesSelect();
      }

      //$this->renderFormFeaturedImage($item, $langId);

    echo '</form>';

    if (!$this->newUserInsert()) {
      $this->displayChangePasswordSection();
    }
  }


  public function renderRolesSelect() {

    $roleId = isset($this->item->role_id) ? $this->item->role_id : Conf::get('user_role_id')['editor'];

    echo '<div class="form-group">';
      echo '<label>' . Trans::get('Roles') . '</label>';
      echo '<select name="role_id" id="user-role_id" class="form-control">';
        foreach ($this->roles as $role) {
          $selected = (int)$roleId === (int)$role->id ? 'selected="selected"' : '';
          echo '<option value="' . $role->id . '" ' . $selected . '>' . $role->name . '</option>';
        }
      echo '</select>';
    echo '</div>';
  }


  public function displayChangePasswordSection() {

    echo '<div class="form-group" style="margin-top:20px;">';

      echo '<input type="checkbox" id="openPasswordSection" />';

      echo '<label>' . Trans::get('Change Password') . '</label>';

      echo '<div class="change-password-section">';

        echo '<form id="change-password-form">';

          echo '<input type="hidden" name="id" value="' . $this->item->id . '">';

          $langId = Trans::getLanguageId();

          $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'role_id', 'required' => true, 'value' => Conf::get('user_role_id')['admin']));
          $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'current_password',  'value' => $this->item->password, 'required' => true));
          $this->displayFormField(array('type' => 'password', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'password', 'required' => true, 'label' => Trans::get('New password')));
          $this->displayFormField(array('type' => 'password', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'repeated_password', 'required' => true, 'label' => Trans::get('Repeat new password')));

          echo '<button type="submit" class="btn btn-success">' . Trans::get('Change') . '</button>';

        echo '</form>';

      echo '</div>';

    echo '</div>';
  }


  /************************************ OTHER ************************************/


  public function adminLoggedIn() {
    return (int)$this->user->role_id === (int)Conf::get('user_role_id')['admin'];
  }


  public function newUserInsert() {
    return (int)$this->item->id === 0;
  }
}

?>