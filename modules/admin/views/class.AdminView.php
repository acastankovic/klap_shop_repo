<?php


class AdminView extends View {

  protected $tableName;
  protected $user;

  public function __construct($controller) {

    parent::__construct($controller);
    $this->setController($controller);
    $this->setRespondType ($controller->getRequest()->getRespondType());
    $this->setModuleName($controller->getModuleName());
    $this->user = $controller->getUser();
  }


  public function setTableName($tableName) {
    $this->tableName = $tableName;
  }


  public function cleanString($string) {
    return htmlentities($string);
  }


  public function createSelectOptions($tableName, $value = null) {

    $data = Cache::get($tableName . 'Data');
    if (!$data) {
      $model = new Model();
      $model->setTable($tableName);
      $data = $model->load();
      Cache::set($tableName . 'Data', $data);
    }

    $html = '';
    foreach($data as $row) {
      $html .= '<option value="' . $row->id . '">' . $row->name . '</option>';
    }

    return $html;
  }


  public function displayCell($cell, $row) {

    $cellName = $cell['name'];
    $cellType = $cell['type'];
    if (endsWith($cellName, '_id')) echo "<td data-name='".$cellName."' data-type='".$cellType."' data-value='".$row->{$cellName}."'>" . $row->{str_replace("_id", "s_name", $cellName)} ."</td>";
    else echo "<td data-name='".$cellName."' data-type='".$cellType."' data-value='".$row->{$cellName}."'>".$row->{$cellName}."</td>";
  }


  public function displayCellRaw($cell, $row, $maxLength = null) {

    $cellName = $cell['name'];
    $cellType = $cell['type'];

    $value = $row->{$cellName};
    if (isset($maxLength)) {
      $value = truncateString($row->{$cellName}, $maxLength);
    }

    // echo "<td data-name='".$cellName."' data-type='".$cellType."' data-value='".$row->{$cellName}."'>".$row->{$cellName}."</td>";
    ?><td data-name="<?php echo $cellName; ?>" data-type="<?php echo $cellType; ?>" data-value="<?php echo htmlentities($value); ?>"><?php echo $value; ?></td><?php
  }


  public function displayInput($cell) {

    $cellName = $cell['name'];
    if ($cell['type'] === 'varchar') {
      echo '<div class="col-md-12"><div class="form-group"><label>' . $cellName . '</label>';
        echo '<div>';
        echo '<input class="form-control" name="' . $cellName . '" value="">';
      echo '</div></div></div>';
    }
    else if ($cell['type'] === 'int') {
      if ($cellName === 'id') {
        echo '<input type="hidden" name="' . $cellName . '" value="">';
      }
      else if (endsWith($cellName, '_id')) {
        $tableName = $this->getForeignTable ($cellName);
        $this->displaySelect ($tableName, $cellName, $tableName);
      }
      else {
        echo '<div class="col-md-12"><div class="form-group"><label>' . $cellName . '</label>';
          echo '<div>';
          echo '<input class="form-control" name="' . $cellName . '" value="">';
        echo '</div></div></div>';
      }
    }
  }


  public function displayTextInput($label, $name) {

    echo '<div class="col-md-12">';
    echo '<div class="form-group">';
    echo '<label>' . $label . '</label>';
    echo '<div><input class="form-control" name="' . $name . '" value="" /></div>';
    echo '</div>';
    echo '</div>';
  }


  public function displayTextCheckbox($label, $name) {

    echo '<div class="col-md-12">';
      echo '<div class="form-group">';
        echo '<label>' . $label . '</label>';
        echo '<div><input type="checkbox" class="form-control" name="' . $name . '" value="" /></div>';
      echo '</div>';
    echo '</div>';
  }


  public function displayTextArea($label, $name) {

    echo '<div class="col-md-12">';
      echo '<div class="form-group">';
        echo '<label>' . $label . '</label>';
        echo '<div><textarea class="form-control" name="' . $name . '" style="height:80px;"></textarea></div>';
      echo '</div>';
    echo '</div>';
  }


  public function displayPasswordInput($label, $name) {

    echo '<div class="col-md-12">';
      echo '<div class="form-group">';
        echo '<label>' . $label . '</label>';
        echo '<div><input type="password" class="form-control" name="' . $name . '" value=""></div>';
      echo '</div>';
    echo '</div>';
  }


  public function displayFormField($data) {

    if(is_object($data)) $data = (array) $data;

    if(!$this->requiredFormFieldAttributesDefined($data)) return;

    $elemId = $this->setFormFieldElementId($data);

    $value = isset($data['value']) ? $data['value'] : '';

    if((string)$data['type'] === 'hidden') {

      $class = isset($data['className']) ? 'class="' . $data['className'] . '"' : '';

      echo '<input type="' . $data['type'] . '" name="' . $data['name'] . '" id="' . $elemId . '" value="' . $value . '" ' . $class . ' />';
    }
    else{

      $class       = isset($data['className']) ? ' ' . $data['className'] : '';
      $placeholder = isset($data['label']) && $data['label'] ? 'placeholder="' . $data['label'] . '"' : '';
      $disabled    = isset($data['disabled']) ? ' disabled' : '';
      $required    = isset($data['required']) ? ' required' : '';

      echo '<div class="form-group">';
        if(@exists($data['label'])) echo '<label>' . $data['label'] . '</label>';
        if(@exists($data['info']))  echo '<button type="button" class="btn btn-transparent btn-primary info-tooltip" data-toggle="tooltip" data-placement="top" title="' . $data['info'] . '"><i class="fa fa-info"></i></button>';
        echo "<input type='" . $data['type'] . "' 
                     name='" . $data['name'] . "' 
                     id='" . $elemId . "' 
                     value='" . $value . "' 
                     class='form-control" . $class . $disabled . $required . "'  
                     " . $placeholder . $disabled . $required . " />";
      echo '</div>';
    }
  }


  public function displayFormTextarea($data) {

    if(is_object($data)) $data = (array) $data;

    if(!$this->requiredFormFieldAttributesDefined($data, true)) return;

    $elemId = $this->setFormFieldElementId($data);

    $rows = isset($data['rows']) ? $data['rows'] : 4;
    $cols = isset($data['cols']) ? $data['cols'] : 2;

    $placeholder = isset($data['label']) && $data['label'] ? 'placeholder="' . $data['label'] . '"' : '';
    $class       = isset($data['className']) ? ' ' . $data['className'] : '';
    $disabled    = isset($data['disabled']) ? ' disabled' : '';
    $required    = isset($data['required']) ? ' required' : '';
    $value       = isset($data['value']) ? $data['value'] : '';

    echo '<div class="form-group">';
      if(@exists($data['label'])) echo '<label>' . $data['label'] . '</label>';
      if(@exists($data['info']))  echo '<button type="button" class="btn btn-transparent btn-primary info-tooltip" data-toggle="tooltip" data-placement="top" title="' . $data['info'] . '"><i class="fa fa-info"></i></button>';
      echo '<textarea name="' . $data['name'] . '" 
                      id="' . $elemId . '" 
                      class="form-control' . $class . $disabled . $required . '" 
                      rows=' . $rows . ' 
                      cols=' . $cols . ' 
                      ' . $placeholder . $disabled . $required . '>' . $value . '</textarea>';
    echo '</div>';
  }


  public function displayFormTextEditor($data) {

    if(!$this->requiredFormFieldAttributesDefined($data, true)) return;

    $elemId = $this->setFormFieldElementId($data);

    $cols = isset($data['cols']) ? $data['cols'] : 80;
    $rows = isset($data['rows']) ? $data['rows'] : 30;

    echo '<div class="form-group">';
      if(@exists($data['label'])) echo '<label>' . $data['label'] . '</label>';
      if(@exists($data['info']))  echo '<button type="button" class="btn btn-transparent btn-primary info-tooltip" data-toggle="tooltip" data-placement="top" title="' . $data['info'] . '"><i class="fa fa-info"></i></button>';
      echo '<textarea name="' . $data['name'] . '" id="' . $elemId . '" class="cke-editor" cols="' . $cols . '" rows="' . $rows . '">' . $data['value'] . '</textarea>';
    echo '</div>';
  }


  public function displayFormDateField($data) {

    if(!$this->requiredFormFieldAttributesDefined($data)) return;

    $elemId = $this->setFormFieldElementId($data);

    echo '<div class="form-group">';
      if(@exists($data['label'])) echo '<label>' . $data['label'] . '</label>';
      if(@exists($data['info']))  echo '<button type="button" class="btn btn-transparent btn-primary info-tooltip" data-toggle="tooltip" data-placement="top" title="' . $data['info'] . '"><i class="fa fa-info"></i></button>';
      echo '<div class="input-group date-field-wrapper">';
        echo '<span class="input-group-addon" style="border:1px solid #ccc!important;"><i class="fa fa-calendar-o"></i></span>';
        echo '<input type="' . $data['type'] . '" name="' . $data['name'] . '" id="' . $elemId . '" class="form-control ' . $data['name'] . ' date" value="' . $data['value'] . '" placeholder="' . $data['label'] . '" />';
        echo '<button type="button" class="remove-date"><i class="fa fa-times"></i></button>';
      echo '</div>';
    echo '</div>';
  }


  public function displayFormCheckbox($data) {

    if(is_object($data)) $data = (array) $data;

    if(!$this->requiredFormFieldAttributesDefined($data)) return;

    $elemId = $this->setFormFieldElementId($data);

    $value = isset($data['value']) ? $data['value'] : 0;

    $checked = $value == 1 ? ' checked="checked"' : '';

    $className = @exists($data['className']) ? ' class="' . $data['className'] . '"' : '';

    echo '<div class="form-group">';
      echo '<input type="' . $data['type'] . '" name="' . $data['name'] . '" id="' . $elemId . '" value="' . $value . '"' . $checked . '' . $className . ' />';
      if(@exists($data['label'])) echo '<label>' . $data['label'] . '</label>';
    echo '</div>';
  }


  public function requiredFormFieldAttributesDefined($data, $typeNotRequired = null) {

    if(!isset($typeNotRequired)) {
      if(!isset($data['type'])) {echo '<div class="text-danger">' . Trans::get('field attribute "type" required') . ' ! </div>'; return false;}
    }
    if(!isset($data['resource'])) {echo '<div class="text-danger">' . Trans::get('field attribute "resource" required') . ' ! </div>'; return false;}
    if(!isset($data['langId'])) {echo '<div class="text-danger">' . Trans::get('field attribute "langId" required') . ' ! </div>'; return false;}
    if(!isset($data['name'])) {echo '<div class="text-danger">' . Trans::get('field attribute "name" required') . ' ! </div>'; return false;}
    return true;
  }


  public function setFormFieldElementId($data) {
    return $data['resource'] . '-' . $data['name'] . '-langId-' . $data['langId'];
  }


  public function displaySelect($label, $name, $tableName, $defaultSelection = null) {

    echo '<div class="col-md-12">';
      echo '<div class="form-group">';
        echo '<label>' . $label . '</label>';
        echo '<div>';
          echo '<select class="form-control" data-search="true" name="' . $name . '">';
          if (isset($defaultSelection)) echo '<option value="0">' . $defaultSelection . '</option>';
          echo $this->createSelectOptions($tableName);
          echo '</select>';
        echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  public function displayCategoriesSelect($label, $name, $categories, $defaultSelection=null) {

    echo '<div class="col-md-12">';
      echo '<div class="form-group">';
        echo '<label>' . $label . '</label>';
        echo '<div>';
          echo '<select class="form-control" data-search="true" name="' . $name . '">';
          if (isset($defaultSelection)) echo '<option value="0">' . $defaultSelection . '</option>';
          foreach($categories as $row) {
            echo '<option value="' . $row->id . '">' . $row->name . '</option>';
          }
          echo '</select>';
        echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  public function displayFeaturedImageFromGalleryJson($item) {

    if(isset($item) && isset($item->gallery_json)) {
      $gallery = json_decode($item->gallery_json);

      if(@exists($gallery)) {

        $item = reset($gallery);

        $imageUrl = '';

        if((string)$item->type === 'media') {

          $imageUrl = $this->setMediaImageUrl(array('image' => $item->file_name, 'thumb' => true));
        }
        else if((string)$item->type === 'youtube_code') {

          $imageUrl = $this->setYoutubeImageUrl(array('code' => $item->code));
        }

        echo '<td><img src="' . $imageUrl . '" alt="' . $imageUrl . '" height="40px" /></td>';
      }
    }
  }


  public function displayTree($items, $child = false) {

    if ($child) echo '<ul role="group" class="jstree-children">';
    else echo "<ul class='jstree-container-ul'>";

    foreach ($items as $item) {

      if (isset($item['children'])) $class = 'jstree-open';
      else $class = 'jstree-leaf';

      echo '<li role="treeitem" id="j3_1" class="jstree-node ' . $class . '" aria-selected="false">';
        echo '<i class="jstree-icon jstree-ocl"></i>';
        echo '<a class="jstree-anchor" href="#">';
          echo '<i class="jstree-icon jstree-themeicon fa fa-folder-o c-primary jstree-themeicon-custom"></i>';
          echo $item['name'];
        echo '</a>';
        if (isset($item['children'])) $this->displayTree($item['children'], true);
      echo '</li>';
    }

    echo '</ul>';
  }


  protected function getForeignTable($cellName) {
    if ($cellName === 'parent_id') return $this->tableName;
    return str_replace('_id', 's', $cellName);
  }


  public function setMediaTableThumb($media) {

    $image = Conf::get('url') . '/modules/admin/css/img/icon-document.png';

    if((string)$media->mime_type === 'image') {

      if(file_exists(Conf::get('media_root') . '/' . $media->file_name)) {
        $image = Conf::get('media_thumbs_url'). '/' . $media->file_name;
      }
      else{
        $image = Conf::get('url') . '/modules/admin/css/img/no-image.png';
      }
    }
    return $image;
  }


  public function renderMediaTableThumb($media) {

    $thumbImage = $this->setMediaTableThumb($media);
    echo '<td><img src="' . $thumbImage . '" alt="' . $thumbImage . '" height="40px"/></td>';
  }


  public function renderTableThumb($image) {
    $thumb = $this->setMediaImageUrl(array('image' => $image, 'thumb' => true));
    echo '<td><img src="' . $thumb . '" alt="' . $thumb . '" height="40px"/></td>';
  }

  public function displayPanelHeader($data) {

    $title = isset($data) && isset($data['title']) ? $data['title'] : '';

    echo '<div class="panel-header">';
     echo '<h3><i class="icon-note"></i>' . $title . '</h3>';
    echo '</div>';
  }


  public function displayInsertPageActions($data) {

    $item = $data['item'];

    echo '<div class="panel actions-panel">';
      echo '<div class="panel-header bg-primary">';
        echo '<h3>' . Trans::get('Actions') . '</h3>';
      echo '</div>';
      echo '<div class="panel-content">';

        if(Languages::enabled() && isset($data['displayLanguageNotice'])) {
          echo '<p>* ' . Trans::get('Save separately for each language') . '.</p>';
        }

        if(@exists($data['displayPublishCheckbox'])) {

          $this->renderPublishCheckbox($item);
        }

        if(@exists($data['displayAllowCommentsCheckbox'])) {
          $this->renderAllowCommentsCheckbox($item);
        }

        echo '<div class="clearfix">';
          $this->renderSaveButton();

          if(isset($data['displayPreviewButton'])){
            $this->renderPreviewButton($item);
          }
        echo '</div>';

        echo '<div class="spinner-overlay action-buttons-overlay">';
          $this->renderSpinner();
        echo '</div>';

      echo '</div>';
    echo '</div>';
  }


  public function displayStatusPanel($data) {

    $item = $data['item'];

    if((int)$item->id !== 0) {
      echo '<div class="panel">';
        echo '<div class="panel-header">';
          echo '<h3>' . Trans::get('Status') . '</h3>';
        echo '</div>';
        echo '<div class="panel-content">';
          $this->renderStatus($item);
        echo '</div>';
      echo '</div>';
    }
  }

  /************************************* STATUS *************************************/


  public function renderStatus($data) {

    echo '<ul class="panel-status fa-ul">';
      $this->renderPublishStatus($data);
      $this->renderCreatedStatus($data);
      $this->renderUpdatedStatus($data);
    echo '</ul>';
  }


  public function renderPublishStatus($data) {

    if(@exists($data->published)) {

      $status = (int)$data->published === 1 ? Trans::get('Published') : Trans::get('Unpublished');
      echo '<li><i class="fa-li fa fa-eye"></i><label>' . Trans::get('Status') . '</label>: <span>' . $status . '</span></li>';
    }
  }


  public function renderPublishStatusForTable($data) {

    $status = (int)$data->published === 1 ? Trans::get('Published') : Trans::get('Unpublished');
    echo '<td data-name="published" data-type="int" data-value="' . $data->published . '">' . $status . '</td>';
  }


  public function renderCreatedStatus($data) {

    if(@exists($data->cdate)) {

      echo '<li>';
        echo '<i class="fa-li fa fa-calendar"></i><label>' . Trans::get('Created at') . '</label>: <span>' . dateFormat($data->cdate, 'd.m.Y. | h:i:s') . '</span>';
        if(@exists($data->created_by_username)) {
          echo '<br /><label>' . Trans::get('By') . '</label>: <span>' . $data->created_by_username . '</span>';
        }
      echo '</li>';
    }
  }


  public function renderUpdatedStatus($data) {

    if(@exists($data->udate)) {

      echo '<li>';
        echo '<i class="fa-li fa fa-calendar"></i><label>' . Trans::get('Last updated at') . '</label>: <span>' . dateFormat($data->udate, 'd.m.Y. | h:i:s') . '</span>';
        if(@exists($data->update_by_username)) {
          echo '<br /><label>' . Trans::get('By') . '</label>: <span>' . $data->update_by_username . '</span>';
        }
      echo '</li>';
    }
  }


  /************************************** BUTTONS **************************************/


  // add new item (table page)
  public function renderAddNewItemLinkButton($data) {

    $url   = isset($data) && isset($data['url']) ? $data['url'] : '#';
    $title = isset($data['title']) ? $data['title'] : Trans::get('Add new');

    echo '<div class="panel-header panel-header-gray">';
      echo '<a href="' . $url . '" class="btn btn-primary m-0"><i class="icon-plus"></i>' . $title . '</a>';
    echo '</div>';
  }


  public function renderAddNewItemModalButton($data = null) {

    $modalElemId  = isset($data['modalElementId']) ? $data['modalElementId'] : 'modal-entry';
    $btnElemClass = isset($data['buttonElementClass']) ? $data['buttonElementClass'] : 'new-entry';

    echo '<div class="panel-header panel-header-gray">';
      echo '<button type="button" class="btn btn-primary ' . $btnElemClass . ' m-0" data-toggle="modal" data-target="#' . $modalElemId . '"><i class="icon-plus"></i>' . Trans::get('Add new') . '</button>';
    echo '</div>';
  }


  // save (insert page)
  public function renderSaveButton() {
    echo '<button type="button" class="btn btn-warning btn-transparent text-left save-insert-form">';
      echo '<i class="icons-office-54"></i>' . Trans::get('Save');
    echo '</button>';
  }


  // publish/unpublish action (insert page)
//  public function renderPublishButton($data) {
//
//    $publishVisible   = (int)$data->published === 1 ? ' hidden' : '';
//    $unPublishVisible = (int)$data->published === 0 ? ' hidden' : '';
//
//    echo '<button type="button" class="btn btn-primary btn-transparent text-left publish' . $publishVisible . '" data-action="publish" data-id="' . $data->id . '">';
//      echo '<i class="icons-office-51" style="color:#319DB5;"></i>' . Trans::get("Publish");
//    echo '</button>';
//
//    echo '<button type="button" class="btn btn-danger btn-transparent text-left publish' . $unPublishVisible . '" data-action="unpublish" data-id="' . $data->id . '">';
//      echo '<i class="icons-office-52" style="font-size:10px;color:#d9534f;"></i>' . Trans::get('Unpublish');
//    echo '</button>';
//  }


  // link to page (insert page)
  public function renderPreviewButton($data) {

    if(isset($data->id) && $data->id != 0) {
      if(@exists($data->url)) {
        echo '<a class="btn btn-primary text-left btn-transparent" href="' . $data->url . '" target="_blank">';
          echo '<i class="fa fa-eye"></i>' . Trans::get('Preview');
        echo '</a>';
      }
    }
  }


  public function renderBackButton($data) {

    echo '<div class="back-btn-wrapper"><a href="' . $data['url'] . '" class="btn btn-warning"><i class="icon-arrow-left"></i>' . Trans::get('Back') . '</a></div>';
  }


  public function renderOpenMediaModalButton($type) {

    $property = $this->setOpenMediaModalButtonProperties($type);

    // echo '<button type="button" class="btn btn-transparent ' . $property->class . '" style="display:inline-block;" data-type="' . $type . '">';
    echo '<button type="button" class="btn ' . $property->class . '" style="display:inline-block;" data-type="' . $type . '">';
      echo $property->icon . $property->title;
    echo '</button>';
  }


  private function setOpenMediaModalButtonProperties($type) {

    $property = new stdClass();

    switch ($type) {
      case Conf::get('modal_type')['document']:
        $property->title = Trans::get('Add document');
        $property->icon  = '<i class="icon-docs"></i>';
        $property->class = 'btn-success open-media-modal';
        return $property;
      case Conf::get('modal_type')['youtube_video']:
        $property->title = Trans::get('Add Youtube video');
        $property->icon  = '<i class="icon-social-youtube"></i>';
        $property->class = 'btn-danger open-youtube-video-modal';
        return $property;
      default:
        $property->title = Trans::get('Add image');
        //$property->icon  = '<i class="icon-picture"></i>';
        $property->icon  = '';
        $property->class = 'btn-primary open-media-modal';
        return $property;
    }
  }


  public function renderPublishCheckbox($data) {

    $langId = @exists($data->lang_id) ? $data->lang_id : Trans::getLanguageId();

    $checked = @exists($data->published) && (int)$data->published === 1 ? 'checked="checked"' : '';

    echo '<div class="checkbox-wrapper">';
      echo '<input type="checkbox" class="publish-checkbox" data-lang_id="' . $langId . '" ' . $checked . ' />';
      echo '<label>' . Trans::get('Publish') . '</label>';
    echo '</div>';
  }


  public function renderAllowCommentsCheckbox($data) {

    $checked = @exists($data->allow_comments) && (int)$data->allow_comments === 1 ? 'checked="checked"' : '';

    echo '<div class="checkbox-wrapper">';
      echo '<input type="checkbox" class="allow-comments-checkbox" data-lang_id="' . $data->lang_id . '" ' . $checked . ' />';
      echo '<label>' . Trans::get('Allow comments') . '</label>';
    echo '</div>';
  }


  /*********************************** CATEGORY TREE ***********************************/


  // table page
  public function displayTablePageTree($data = null) {

    $title = @exists($data) && @exists($data['title']) ? $data['title'] : Trans::get('Category tree');

    echo '<h4>' . $title . '</h4>';

    echo '<div class="col-md-4 tree-wrapper">';
      echo '<div id="tree3"></div>';
    echo '</div>';
  }


  // insert page
  public function displayInsertPageTree($data) {

    $langId    = $data['langId'];
    $title     = @exists($data['title']) ? $data['title'] : Trans::get('Category');
    $rootTitle = @exists($data['rootTitle']) ? $data['rootTitle'] : Trans::get('No category - root');
    $rootId    = @exists($data['rootId']) ? $data['rootId'] : 0;

    if(isset($data['rootCategories'])) {

      foreach ($data['rootCategories'] as $category) {

        if((int)$category->lang_id === (int)$langId) {
          $rootId = $category->id;
        }
      }
    }

    $langSuffix = $this->getLanguageSuffix($data);

    echo '<div class="panel">';
      echo '<div class="panel-header"><h3>' . $title . '</h3></div>';
      echo '<div class="panel-content">';
        echo '<div class="tree-grid-wrapper">';
        echo '<div class="tree-grid-root-category" id="root-category-selector' . $langSuffix . '" data-lang_id="' . $langId . '">' . $rootTitle . '</div>';
        echo '<div class="tree-grid" id="tree3' . $langSuffix . '" data-lang_id="' . $langId . '" data-root_id="' . $rootId . '"></div>';
      echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  /*************************************** MODALS ***************************************/


  public function displayModals($data = null) {
    $this->displayMediaModal();
    $this->displayYoutubeVideoModal();
    $this->displayGalleryItemDescriptionModal();
    $this->displayGalleriesModal($data);
  }


  // media modal
  public function displayMediaModal() {

    echo '<div class="modal fade" id="media-modal" tabindex="-1" role="dialog" aria-hidden="true">';
      echo '<div class="modal-dialog" style="width: 90%;">';
        echo '<div class="modal-content">';

          echo '<div class="modal-header">';
            echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>';
            echo '<h4 class="modal-title"><strong>' . Trans::get('Choose') . '</strong></h4>';
          echo '</div>';

          echo '<div class="modal-body media-modal-body">';
            echo '<div class="row">';
              echo '<div class="col-md-12">';

                echo '<h3>' . Trans::get("Upload media") . '</h3>';

                echo '<h4 class="media-modal-message image">' . Trans::get('Only images allowed') . '!</h4>';
                echo '<h4 class="media-modal-message document">' . Trans::get('Only documents allowed') . '!</h4>';

                echo '<div class="media-modal-upload-wrapper">';
                  echo '<form id="mediaModalUpload" method="post" class="dropzone dz-clickable"></form>';
                echo '</div>';

                echo '<div class="row">';
                  echo '<div class="media-modal-items-wrapper"></div>';
                echo '</div>';

              echo '</div>';
            echo '</div>';

            echo '<div class="spinner-overlay">';
              $this->renderSpinner();
            echo '</div>';

          echo '</div>';

          echo '<div class="modal-footer bg-gray-light">';
            echo '<button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">' . Trans::get('Close') . '</button>';
          echo '</div>';

        echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  // youtube video modal
  public function displayYoutubeVideoModal() {

    echo '<div class="modal fade" id="youtube-video-modal" tabindex="-1" role="dialog" aria-hidden="true">';
      echo '<div class="modal-dialog">';
        echo '<div class="modal-content">';

          echo '<div class="modal-header">';
            echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>';
            echo '<h4 class="modal-title"><strong>' . Trans::get('Enter Youtube link') . '</strong></h4>';
          echo '</div>';

          echo '<div class="modal-body">';
            echo '<div class="row">';
              echo '<div class="col-md-12">';
              echo '<input type="text" class="form-control" id="youtubeVideoUrl" placeholder="' . Trans::get('Link') . '" />';
              echo '</div>';
            echo '</div>';
            echo '<div class="spinner-overlay">';
            $this->renderSpinner();
            echo '</div>';
          echo '</div>';

          echo '<div class="modal-footer bg-gray-light">';
            echo '<button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">' . Trans::get('Close') . '</button>';
            echo '<button type="button" class="btn btn-primary btn-embossed" id="saveYoutubeVideo">' . Trans::get('Save') . '</button>';
          echo '</div>';

        echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  // gallery item description modal
  public function displayGalleryItemDescriptionModal() {

    echo '<div class="modal fade" id="gallery-item-desc-modal" tabindex="-1" role="dialog" aria-hidden="true">';
      echo '<div class="modal-dialog">';
        echo '<div class="modal-content">';

        echo '<div class="modal-header">';
          echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>';
          echo '<h4 class="modal-title"><strong>' . Trans::get('Enter description') . '</strong></h4>';
        echo '</div>';

        echo '<div class="modal-body">';
          echo '<div class="row">';
            echo '<div class="col-md-12">';
              echo '<textarea class="form-control" id="galleryItemDesc" placeholder="' . Trans::get('Description') . '" rows="10"></textarea>';
            echo '</div>';
          echo '</div>';
          echo '<div class="spinner-overlay">';
          $this->renderSpinner();
          echo '</div>';
        echo '</div>';

        echo '<div class="modal-footer bg-gray-light">';
          echo '<button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">' . Trans::get('Close') . '</button>';
          echo '<button type="button" class="btn btn-primary btn-embossed" id="saveGalleryItemDesc">' . Trans::get('Save') . '</button>';
        echo '</div>';

        echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  function displayGalleriesModal($data = null) {

    echo '<div class="modal fade" id="galleries-modal" tabindex="-1" role="dialog" aria-hidden="true">';
      echo '<div class="modal-dialog" style="width: 90%;">';
        echo '<div class="modal-content">';

        echo '<div class="modal-header">';
          echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>';
          echo '<h4 class="modal-title"><strong>' . Trans::get('Choose') . '</strong></h4>';
        echo '</div>';

        echo '<div class="modal-body media-modal-body">';
          echo '<div class="row">';
            echo '<div class="col-md-12">';

              echo '<div class="row">';
                echo '<div class="gallery-modal-items-wrapper">';
                if(@exists($data) && @exists($data->galleries)) {
                  foreach ($data->galleries as $gallery) {
                    echo '<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">';
                      echo '<figure>';
                        echo '<a href="#" data-id="' . $gallery->id . '" class="add-gallery">';
                          echo '<img src="' . $gallery->thumb . '" alt="cat">';
                          echo '<figcaption>' . $gallery->name . '</figcaption>';
                        echo '</a>';
                      echo '</figure>';
                    echo '</div>';
                  }
                } else {
                  echo 'no galleries';
                }
                echo '</div>';
              echo '</div>';

            echo '</div>';
          echo '</div>';

          echo '<div class="spinner-overlay">';
          $this->renderSpinner();
          echo '</div>';

        echo '</div>';

        echo '<div class="modal-footer bg-gray-light">';
          echo '<button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">' . Trans::get('Close') . '</button>';
        echo '</div>';

        echo '</div>';
      echo '</div>';
    echo '</div>';
  }

  /****************************** IMAGES (feature, other) ******************************/


  protected function renderImagePanel($data) {

    $langId = @exists($data['langId']) ? $data['langId'] : Trans::getLanguageId();
    $type = @exists($data['type']) ? $data['type'] : Conf::get('modal_type')['image'];
    $item = $data['item'];
    $image = null;

    $langSuffix = $this->getLanguageSuffix(array('langId' => $langId));

    if($type == Conf::get('modal_type')['intro_image']) {
      $title =  Trans::get('Intro image');
      $elementId = 'introImageContainer';

      if(@exists($item->intro_image)) {
        $image = $item->intro_image;
      }
    }
    else if($type == Conf::get('modal_type')['image']) {
      $title =  Trans::get('Image');
      $elementId = 'imageContainer';

      if(@exists($item->image)) {
        $image = $item->image;
      }
    }

    echo '<div class="panel image-panel">';

      echo '<div class="panel-header"><h3>' . $title . '</h3></div>';

      echo '<div class="panel-content">';

        echo '<div id="' . $elementId . $langSuffix . '">';

        if(@exists($image)) {

          $mediaImage = $this->setMediaImageUrl(array('image' => $image));

          echo '<div class="panel-image-wrapper">';
            echo '<img src="' . $mediaImage . '" alt="' . $image . '">';
            echo '<button class="btn btn-sm btn-danger remove-image" data-type="' . $type . '"><i class="icons-office-52"></i></button>';
          echo '</div>';
        }
        echo '</div>';

        $this->renderOpenMediaModalButton($type);

      echo '</div>';

    echo '</div>';
  }


  public function renderFormGallery($data, $langId) {

    $typeImage = Conf::get('modal_type')['gallery_image'];
    $typeYoutubeVideo = Conf::get('modal_type')['youtube_video'];
    $langSuffix = $this->getLanguageSuffix(array('langId' => $langId));

    echo '<div class="form-media-warpper">';

      echo '<section class="toggler-wrapper"><a href="#" class="gallery-wrapper-toggler"><i class="fa fa-chevron-down left"></i><h3>' . Trans::get('Gallery') . '</h3></a></section>';

      echo '<div class="gallery-wrapper">';

        echo '<div id="galleryContainer' . $langSuffix . '" class="gallery-container clearfix">';

        if(@exists($data->gallery_json)) {

          $gallery = json_decode($data->gallery_json);

          if(@exists($gallery)) {

            foreach ($gallery as $item) {

              $itemJson = htmlentities(json_encode($item));

              $descValue = '';
              $descLabel = Trans::get('No description');
              if(@exists($item->description)) {
                $descLabel = truncateString($item->description, 50);
                $descValue = htmlentities($item->description);
              }

              if((string)$item->type === 'media') {

                $image = $this->setMediaImageUrl(array('image' => $item->file_name));

                echo '<div class="gallery-image-wrapper item-image-wrapper" data-desc="' . $descValue . '">';
                  echo '<button class="btn btn-sm btn-danger btn-transparent remove-image" data-type="' . $typeImage . '"><i class="icons-office-52"></i></button>';
                  echo '<figure><img src="' . $image . '" alt="' . $item->file_name . '" /></figure>';
                  echo '<button type="button" class="btn btn-warning btn-transparent text-left open-gallery-item-desc-modal"><i class="icon-pencil"></i>' . Trans::get('Add description') . '</button>';
                  echo '<div class="item-image-desc">';
                    echo '<p class="item-desc">' . $descLabel . '</p>';
                  echo '</div>';
                  echo '<input type="hidden" class="gallery-item" value="' . $itemJson . '" />';
                echo '</div>';
              }
              else if((string)$item->type === 'youtube_code') {

                echo '<div class="gallery-image-wrapper item-image-wrapper" data-desc="' . $descValue . '">';
                  echo '<button class="btn btn-sm btn-danger btn-transparent remove-image" data-code="' . $item->code . '" data-type="' . $typeYoutubeVideo . '"><i class="icons-office-52"></i></button>';
                  echo '<figure>';
                  echo '<img class="gallery-youtube-icon" src="' . Conf::get('url') . '/modules/admin/css/img/icon-youtube.png" alt="icon-youtube" />';
                  $this->displayYoutubeImage(array("code" => $item->code));
                  echo '</figure>';
                  echo '<button type="button" class="btn btn-warning btn-transparent text-left open-gallery-item-desc-modal"><i class="icon-pencil"></i>' . Trans::get('Add description') . '</button>';
                  echo '<div class="item-image-desc">';
                    echo '<p class="item-desc">' . $descLabel . '</p>';
                  echo '</div>';
                  echo '<input type="hidden" class="gallery-item" value="' . $itemJson . '" />';
                echo '</div>';
              }
            }
          }
        }

        echo '</div>';

        $this->renderOpenMediaModalButton($typeImage);
        $this->renderOpenMediaModalButton($typeYoutubeVideo);
      echo '</div>';

    echo '</div>';
  }


  /************************************* DOCUMENTS *************************************/


  public function renderFormDocument($data, $langId) {

    $type = Conf::get('modal_type')['document'];
    $langSuffix = $this->getLanguageSuffix(array('langId' => $langId));

    echo '<div class="form-media-warpper">';

      echo '<label>' . Trans::get('Document') . '</label>';

      echo '<div id="documentContainer' . $langSuffix . '">';

      if(isset($data->featured_document)) {

        echo '<div class="media-document-wrapper">';
          echo '<figure>';
            echo '<img src="' . Conf::get('url') . '/modules/admin/css/img/icon-document.png" alt="icon-document" />';
            echo '<figcaption>' . $data->featured_document . '</figcaption>';
          echo '</figure>';
          echo '<button class="btn btn-sm btn-danger btn-transparent remove-document"><i class="icons-office-52"></i></button>';
        echo '</div>';
      }

      echo '</div>';

    $this->renderOpenMediaModalButton($type);

    echo '</div>';
  }


  /*********************************** LANGUAGES ***********************************/

  public function displayLanguageTabs($languages) {

    if(Conf::get('multilingual_enabled')) {

      if(@exists($languages)) {

        echo '<div>';

          echo '<input type="hidden" value="' . Trans::getLanguageId() . '" id="langId" />';

          echo '<ul class="nav nav-tabs language-tabs" id="languageTabs">';

          foreach ($languages as $key => $lang) {

            $activeClass = $key == 0 ? ' active' : '';

            echo '<li class="nav-item' . $activeClass . '">';
              echo '<a href="#" class="nav-link' . $activeClass . '" data-id="' . $lang->id . '">' . $lang->nameTranslated . '</a>';
            echo '</li>';
          }
          echo '</ul>';
        echo '</div>';
      }
    }
  }


  public function getLanguageSuffix($data = null) {

    $langId = isset($data) && isset($data['langId']) ? $data['langId'] : Trans::getLanguageId();

    return '-langId-' . $langId;
  }


  /********************************* YOUTUBE VIDEO *********************************/


  public function setYoutubeImageUrl($data) {

    if(!isset($data) || !isset($data['code'])) return '';
    return 'http://img.youtube.com/vi/' . $data['code'] . '/0.jpg';
  }


  public function displayYoutubeImage($data) {
    if(!isset($data) || !isset($data['code'])) return '';
    $image = $this->setYoutubeImageUrl($data);
    echo '<img src="' . $image . '" alt="youtube-video" />';
  }


  public function renderFormYoutubeVideoField($data) {

    $langSuffix = $this->getLanguageSuffix(array('langId' => $data['langId']));

    $value = isset($data['value']) && (string)$data['value'] !== '' ? 'https://www.youtube.com/embed/' . $data['value'] : '';

    echo '<div class="form-group">';
      echo '<label>' . Trans::get('Youtube video link') . '</label>';
      echo '<input type="text" name="youtube_video_code" id="' . $data['resource'] . '-youtube_video_code" value="' . $value . '" class="form-control youtube_video_code'.$langSuffix.'" placeholder="' . Trans::get('Youtube video link') . '" />';
    echo '</div>';
  }


  /************************************* OTHER *************************************/

  public function renderSpinner() {

    echo '<div class="spinner">';
      echo '<div class="bounce1"></div>';
      echo '<div class="bounce2"></div>';
      echo '<div class="bounce3"></div>';
    echo '</div>';
  }


  public function setMediaImageUrl($data) {

    $noImage = Conf::get('url') . '/modules/admin/css/img/no-image.png';

    if(!isset($data) || !isset($data['image']) || (string)$data['image'] === '') return $noImage;

    $image = $data['image'];
    $url   = Conf::get('media_url');
    $root  = Conf::get('media_root');

    if(isset($data) && isset($data['thumb'])) {

      $url  = Conf::get('media_thumbs_url');
      $root = Conf::get('media_thumbs_root');
    }

    $mediaImage = $url . '/' . $image;
    $mediaRoot  = $root . '/' . $image;

    if(!file_exists($mediaRoot)) return $noImage;
    return $mediaImage;
  }


  /********************************** PAGINATION **********************************/


  public function renderTablePagination($data) {

    $total = $data['total'];
    $itemsPerPage = $data['itemsPerPage'];

    if($total <= $itemsPerPage) return;

    $page = isset($data['page']) ? $data['page'] : 1;

    $totalPagination = ceil((int)$total / (int)$itemsPerPage);

    $from = 1;
    if((int)$page !== 0 || (int)$page !== 1) {
      $from = ((int)$page * (int)$itemsPerPage - (int)$itemsPerPage) + 1;
    }

    $to = (int)$page * (int)$itemsPerPage;
    if($to > $total) $to = $total;

    $prevPage = 1;
    $prevPageDisabledClass = ' disabled';
    if($page > 1) {
      $prevPage = (int)$page - 1;
      $prevPageDisabledClass = '';
    }

    $nextPage = $totalPagination;
    $nextPageDisabledClass = ' disabled';
    if($page < $totalPagination) {
      $nextPage = (int)$page + 1;
      $nextPageDisabledClass = '';
    }

    $minButtonsForDisplay = 6;
    $url = $this->buildPaginationUrl($data);

    echo '<div class="row">';
      echo '<div class="col-md-6">';
        echo '<div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">' . Trans::get('Show') . ' ' . $from . ' ' . Trans::get('to') . ' ' . $to . ' ' . Trans::get('from') . ' ' . Trans::get('total') . ' ' . $total .' ' . Trans::get('elements') . '</div>';
      echo '</div>';
      echo '<div class="col-md-12">';
        echo '<div class="dataTables_paginate paging_simple_numbers">';
          echo '<ul class="pagination">';
            echo '<li class="paginate_button previous ' . $prevPageDisabledClass . '"><a href="' . $url . $prevPage . '">' . Trans::get('Previous') . '</a></li>';

            if($totalPagination <= $minButtonsForDisplay) {

              for ($i = 1; $i <= $totalPagination; $i++) {

                $activeClass = (int)$i === (int)$page ? ' active' : '';
                echo '<li class="paginate_button' . $activeClass . '"><a href="' . $url . $i . '">' . $i . '</a></li>';
              }
            }
            else{

              if($page < $minButtonsForDisplay) {

                for ($i = 1; $i <= $minButtonsForDisplay; $i++) {

                  $activeClass = (int)$i === (int)$page ? ' active' : '';
                  echo '<li class="paginate_button' . $activeClass . '"><a href="' . $url . $i . '">' . $i . '</a></li>';
                }
                echo '<li class="paginate_button"><a href="#">...</a></li>';
                echo '<li class="paginate_button"><a href="' . $url . $totalPagination . '">' . $totalPagination . '</a></li>';
              }
              else {

                if((int)$page === (int)$totalPagination) {
                  echo '<li class="paginate_button"><a href="' . $url . '1">1</a></li>';
                  echo '<li class="paginate_button"><a href="#">...</a></li>';

                  for ($i = ((int)$totalPagination - 2); $i <= $totalPagination; $i++) {

                    $activeClass = (int)$i === (int)$page ? ' active' : '';
                    echo '<li class="paginate_button' . $activeClass . '"><a href="' . $url . $i . '">' . $i . '</a></li>';
                  }
                }
                else if((int)$page === ((int)$totalPagination - 1)) {
                  echo '<li class="paginate_button"><a href="' . $url . '1">1</a></li>';
                  echo '<li class="paginate_button"><a href="#">...</a></li>';
                  echo '<li class="paginate_button"><a href="' . $url . $prevPage . '">' . $prevPage . '</a></li>';
                  echo '<li class="paginate_button active"><a href="' . $url . $page . '">' . $page . '</a></li>';
                  echo '<li class="paginate_button"><a href="' . $url . $nextPage . '">' . $nextPage . '</a></li>';
                }
                else {
                  echo '<li class="paginate_button"><a href="' . $url . '1">1</a></li>';
                  echo '<li class="paginate_button"><a href="#">...</a></li>';
                  echo '<li class="paginate_button"><a href="' . $url . $prevPage . '">' . $prevPage . '</a></li>';
                  echo '<li class="paginate_button active"><a href="' . $url . $page . '">' . $page . '</a></li>';
                  echo '<li class="paginate_button"><a href="' . $url . $nextPage . '">' . $nextPage . '</a></li>';
                  echo '<li class="paginate_button"><a href="#">...</a></li>';
                  echo '<li class="paginate_button"><a href="' . $url . $totalPagination . '">' . $totalPagination . '</a></li>';
                }
              }
            }

            echo '<li class="paginate_button next' . $nextPageDisabledClass . '"><a href="' . $url . $nextPage . '">' . Trans::get('Next') . '</a></li>';
          echo '</ul>';
        echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  public function renderTableFilters($params = null) {

    echo '<div class="table-filters-wrapper">';
      $this->renderTableFilterShowItemsSelect($params);
      $this->renderTableSearch($params);
    echo '</div>';
  }


  public function renderTableFilterShowItemsSelect($params = null) {

    $values = [10, 25, 50, 100];

    $selectedValue = isset($params) && isset($params['itemsPerPage']) ? $params['itemsPerPage'] : $values[0];

    echo '<div class="col-xs-6 col-md-6 table-items-select-wrapper">';
      echo '<label>' . ucfirst(Trans::get('show')) . '</label><br />';
      echo '<select id="tableShowItems">';
      foreach ($values as $value) {
        $selected = $value == $selectedValue ? 'selected="selected"' : '';
        echo '<option value="' . $value . '"' . $selected . '>' . $value . '</option>';
      }
      echo '</select>';
      echo '<br />';
      echo '<label>' . Trans::get('elements') . '</label>';
    echo '</div>';
  }


  public function renderTableSearch($params = null) {

    echo '<div class="col-xs-6 col-md-6 table-search-wrapper text-right">';

    if(isset($params) && isset($params['search'])) {
      echo '<label class="tag-label">' . Trans::get('Search term') . ': </label>';
      echo '<button type="button" class="table-search-tag" id="tableSerchTag" data-toggle="tooltip" data-placement="top" title="' . Trans::get('Clear search') . '">' . $params['search'] . '<i class="icon icon-close"></i></button>';
    }
    else{
      echo '<form id="tableSearchForm">';
        echo '<label>' . Trans::get('Search') . ': </label>';
        echo '<button type="submit"><i class="icon icon-magnifier"></i></button>';
        echo '<input type="search" class="form-control" id="tableSearch" placeholder="' . Trans::get('Search') . '..." />';
      echo '</form>';
    }
    echo '</div>';
  }

  private function buildPaginationUrl($data) {

    $route = str_replace(Conf::get('url'), '', $data['route']);
    $route = str_replace('admin', '', $route);
    $route = trim($route, '/');

    if(isset($data['route'])) unset($data['route']);

    $url = Conf::get('url') . '/admin/' . $route;

    $url .= '?items_per_page=' . $data['itemsPerPage'];

    foreach ($data as $key => $value) {

      if((string)$key !== 'page' && (string)$key !== 'total' && (string)$key !== 'itemsPerPage' && (string)$key !== 'url') {

        $url .= '&' . $key . '=' . $value;
      }
    }

    $url .= '&page=';

    return $url;
  }


  public function displaySortingCell($orderBy, $title, $params) {

    $elementClass = 'sorting';
    $direction = '';
    $clearSortingBtn = '';
    if(isset($params) && $params['order_by'] == $orderBy && isset($params['order_direction'])) {
      $elementClass = 'sorting_' . $params['order_direction'];
      $direction = 'data-order_direction="' . $params['order_direction'] . '"';
      $clearSortingBtn = '<button type="button" class="btn btn-danger clear-table-sorting" data-toggle="tooltip" data-placement="top" title="' . Trans::get('Clear sorting') . '"><i class="icon icon-close"></i></button>';
    }

    echo '<th class="sort-cell ' . $elementClass . '" data-order_by="' . $orderBy . '" ' . $direction . '>' . $title . $clearSortingBtn . '</th>';
  }


  /********************************** COMMENTS **********************************/

  protected function renderCommentsSection($comments = null) {

    echo '<div class="comments-wrapper">';

      echo '<section class="toggler-wrapper">';
        echo '<a href="#" class="comments-wrapper-toggler">';
          echo '<i class="fa fa-chevron-down left"></i>';
          echo '<h3>' . Trans::get('Comments') . '</h3>';
        echo '</a>';
      echo '</section>';

      echo '<div class="comments clearfix">';

        if(@exists($comments)) {
          $this->renderComments($comments);
        }
        else{
          echo Trans::get('There are no comments for this page');
        }

      echo '</div>';

    echo '</div>';
  }


  protected function renderComments($comments) {

    if(@exists($comments)) {

      foreach ($comments as $comment) {

        $publishString = (int)$comment['published'] === 1 ? Trans::get('Unpublish') : Trans::get('Publish');
        $hasChildrenClass = @exists($comment['children']) && count($comment['children']) > 0 ? ' hasChildren' : '';

        echo '<div class="col-sm-12 comment">';

          echo '<div class="row comment-header text-right">';
            echo '<button type="button" class="comment-publish' . $hasChildrenClass . '" data-id="' . $comment['id'] . '" data-parent_id="' . $comment['parent_id'] . '" data-published="' . $comment['published'] . '">' . $publishString . '</button>';
            echo '<button type="button" class="delete' . $hasChildrenClass . '" data-id="' . $comment['id'] . '" data-parent_id="' . $comment['parent_id'] . '">' . Trans::get('Delete') . '</button>';
          echo '</div>';

          echo '<div class="row comment-body">';
            echo '<div class="col-md-1">';
              echo '<img src="' . Conf::get('url') . '/modules/admin/css/img/user.png" />';
            echo '</div>';
            echo '<div class="col-md-11">';
              echo '<span>' . $comment['name'] . '</span>';
              echo '<span>' . $comment['email'] . '</span>';
              echo '<span class="c-date">' . dateFormat($comment['cdate'], 'd.m.Y. (h:i:s)') . '</span>';
              echo '<div class="message">' . $comment['message'] . '</div>';
            echo '</div>';
          echo '</div>';

          if(@exists($comment['children']) && count($comment['children']) > 0) {
            $this->renderComments($comment['children']);
          }

        echo '</div>';
      }
    }
  }
}
?>