<?php


class MenusAdminView extends AdminView {

  public $activeNavigation;
  private $resource;
  private $languages;
  private $items;
  private $categories;
  private $articles;
  private $menuTypes;
  private $menu;


  public function __construct($controller, $data) {

    parent::__construct($controller);

    $this->activeNavigation = 'menus';
    $this->resource = 'menu_item';

    if (@exists($data)) {

      if (!is_array($data)) $data = (array)$data;

      if (@exists($data['menu'])) $this->menu = $data['menu'];
      if (@exists($data['menu'])) $this->items = $data['menu']->items;
      if (@exists($data['items'])) $this->items = $data['items'];
      if (@exists($data['languages'])) $this->languages = $data['languages'];
      if (@exists($data['categories'])) $this->categories = $data['categories'];
      if (@exists($data['articles'])) $this->articles = $data['articles'];
      if (@exists($data['menuTypes'])) $this->menuTypes = $data['menuTypes'];
    }
  }

  /********************************** MENUS PAGE **********************************/

  public function displayTable() {

    $this->renderAddNewItemModalButton(array('modalElementId' => 'form-modal', 'buttonElementClass' => 'new-form-entry'));
    $this->renderTable();
  }


  private function renderTable() {

    echo '<div class="table-wrapper">';

      echo '<table class="table table-dynamic table-tools filter-select">';

        echo '<thead>';
          echo '<tr>';
            echo '<th>' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Title') . '</th>';
            echo '<th>' . Trans::get('Language') . '</th>';
            echo '<th class="text-right">' . Trans::get('Actions') . '</th>';
          echo '</tr>';
        echo '</thead>';

        echo '<tbody>';

        if (@exists($this->items)) {

          foreach ($this->items as $item) {

            echo '<tr data-id="' . $item->id . '">';
              $this->displayCellRaw(array('name' => 'id', 'type' => 'int'), $item);
              $this->displayCellRaw(array('name' => 'name', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'language_name', 'type' => 'varchar'), $item);
              echo '<td class="text-right">';
                echo '<a class="btn btn-sm btn-default m-0" href="' . Conf::get('url') . '/admin/menus/' . $item->id . '/insert"><i class="fa fa-sitemap"></i></a>';
                echo '<button type="button" class="btn btn-sm btn-default edit" data-id="' . $item->id . '" data-lang_id="' . $item->lang_id . '" data-toggle="modal" data-target="#form-modal" style="margin:0 10px;"><i class="icon-note"></i></button>';
                echo '<button type="button" class="btn btn-sm btn-danger m-0 delete" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
              echo '</td>';
            echo '</tr>';
          }
        }
        echo '</tbody>';

        echo '<tfoot>';
          echo '<tr>';
            echo '<th style="visibility: hidden;">' . Trans::get('Id') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Title') . '</th>';
            echo '<th>' . Trans::get('Language') . '</th>';
            echo '<th style="visibility: hidden;"></th>';
          echo '</tr>';
        echo '</tfoot>';

      echo '</table>';

    echo '</div>';
  }


  public function displayFormModal() {

    echo '<div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-hidden="true">';
      echo '<div class="modal-dialog">';
        echo '<div class="modal-content">';

          echo '<div class="modal-header">';
            echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>';
            echo '<h4 class="modal-title">' . Trans::get('Menu data') . '</h4>';
          echo '</div>';

          echo '<div class="modal-body">';
            $this->displayLanguageTabs($this->languages);
            echo '<div class="row">';
              echo '<div class="form-modal-content"></div>';
              if (Languages::enabled()) {
                echo '<div class="col-md-12">* ' . Trans::get('Save separately for each language') . '.</div>';
              }
            echo '</div>';
          echo '</div>';

          echo '<div class="modal-footer bg-gray-light action-buttons" style="position:relative;">';
            echo '<div class="spinner-overlay action-buttons-overlay">';
            $this->renderSpinner();
            echo '</div>'; // spinner-overlay
            echo '<button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">' . Trans::get('Close') . '</button>';
            echo '<button type="button" class="btn btn-primary btn-embossed save-insert-form">' . Trans::get('Save') . '</button>';
          echo '</div>';

        echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  /******************************** MENU ITEMS PAGE ********************************/


  public function displayHiddenFields() {
    echo '<input type="hidden" id="menuId" value="' . $this->menu->id . '" />';
  }


  public function displayMenuItemsTable() {
    $this->renderAddNewItemModalButton();
    $this->renderMenuItemsTable();
  }


  private function renderMenuItemsTable() {

    echo '<div class="table-wrapper">';

      echo '<table class="table table-dynamic table-tools filter-select">';

        echo '<thead>';
          echo '<tr>';
            echo '<th>' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Title') . '</th>';
            echo '<th>' . Trans::get('Parent') . '</th>';
            echo '<th>' . Trans::get('Target') . '</th> ';
            echo '<th class="text-right">' . Trans::get('Actions') . '</th>';
          echo '</tr>';
        echo '</thead>';

        echo '<tbody>';

        if (@exists($this->items)) {

          foreach ($this->items as $item) {

            echo '<tr data-id="' . $item->id . '" data-parentid="' . $item->parent_id . '" data-targetid="' . $item->target_id . '" data-type="' . $item->type . '" data-url="' . $item->url . '">';
              $this->displayCellRaw(array('name' => 'id', 'type' => 'int'), $item);
              $this->displayCellRaw(array('name' => 'name', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'parent_name', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'target', 'type' => 'varchar'), $item);
              echo '<td class="text-right">';
                echo '<button type="button" class="btn btn-sm btn-default edit" data-id="' . $item->id . '" data-toggle="modal" data-target="#modal-entry"><i class="icon-note"></i></button>';
                echo '<button type="button" class="delete btn btn-sm btn-danger" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
              echo '</td>';
            echo '</tr>';
          }
        }
        echo '</tbody>';

        echo '<tfoot>';
          echo '<tr>';
            echo '<th style="visibility: hidden;">' . Trans::get('Id') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Title') . '</th>';
            echo '<th>' . Trans::get('Parent') . '</th>';
            echo '<th>' . Trans::get('Target') . '</th>';
            echo '<th style="visibility: hidden;"></th>';
          echo '</tr>';
        echo '</tfoot>';

      echo '</table>';

    echo '</div>';
  }


  public function displayMenuItemsFormModal() {

    echo '<div class="modal fade gallery" id="modal-entry" tabindex="-1" role="dialog" aria-hidden="true">';
      echo '<div class="modal-dialog">';
        echo '<div class="modal-content">';

          echo '<div class="modal-header">';
            echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>';
            echo '<h4 class="modal-title">' . Trans::get('Menu item data') . '</h4>';
          echo '</div>';

          echo '<div class="modal-body">';
            echo '<form id="data-form" role="form">';
              echo '<div class="row">';
                echo '<input type="hidden" name="id" value="">';
                echo '<input type="hidden" name="menu_id" value="' . $this->menu->id . '">';
                echo '<input type="hidden" name="target_id" value="">';
                $this->displayTextInput(Trans::get('Title'), 'name');
                $this->renderMenuItemParentSelect();
                $this->renderMenuItemTypesSelect();
                $this->renderCategoriesSelect();
                $this->renderArticlesSelect();
                echo '<div class="url-field-wrapper">';
                  $this->displayTextInput(Trans::get('Url'), 'url');
                echo '</div>';
              echo '</div>';
            echo '</form>';
          echo '</div>';

          echo '<div class="modal-footer bg-gray-light">';
            echo '<button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">' . Trans::get('Close') . '</button>';
            echo '<button type="button" class="btn btn-primary btn-embossed" data-dismiss="modal" id="save">' . Trans::get('Save') . '</button>';
          echo '</div>';

        echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  private function renderArticlesSelect() {

    echo '<div class="articles-select-wrapper">';
      echo '<div class="col-md-12">';
        echo '<div class="form-group">';
          echo '<label>' . Trans::get('Articles') . '</label>';
          echo '<div>';
            echo '<select class="form-control" data-search="true" id="articlesSelect">';
              echo '<option value="0">' . Trans::get('Select article') . '</option>';
              if (@exists($this->articles)) {
                foreach ($this->articles as $article) {
                  echo '<option value="' . $article->id . '">' . $article->title . '</option>';
                }
              }
            echo '</select>';
          echo '</div>';
        echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  private function renderCategoriesSelect() {

    echo '<div class="categories-select-wrapper">';
      echo '<div class="col-md-12">';
        echo '<div class="form-group">';
          echo '<label>' . Trans::get('Categories') . '</label>';
          echo '<div>';
            echo '<select class="form-control" data-search="true" id="categoriesSelect">';
              echo '<option value="0">' . Trans::get('Select categories') . '</option>';
              if (@exists($this->categories)) {
                foreach ($this->categories as $category) {
                  echo '<option value="' . $category->id . '">' . $category->name . '</option>';
                }
              }
            echo '</select>';
          echo '</div>';
        echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  private function renderMenuItemTypesSelect() {

    echo '<div class="col-md-12">';
      echo '<div class="form-group">';
        echo '<label>' . Trans::get('Type') . '</label>';
        echo '<div>';
          echo '<select class="form-control" data-search="true" name="type" id="menuItemTypesSelect">';
            echo '<option value="0">' . Trans::get('Select type') . '</option>';
            if (@exists($this->menuTypes)) {
              foreach ($this->menuTypes as $type) {
                echo '<option value="' . $type->id . '">' . $type->nameTranslated . '</option>';
              }
            }
          echo '</select>';
        echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  private function renderMenuItemParentSelect() {

    echo '<div class="col-md-12">';
      echo '<div class="form-group">';
        echo '<label>' . Trans::get('Parent') . '</label>';
        echo '<div>';
          echo '<select class="form-control" data-search="true" name="parent_id" id="menuItemParentSelect">';
          echo '<option value="0">' . Trans::get('Select parent') . '</option>';
          if (@exists($this->items)) {
            foreach ($this->items as $item) {
              echo '<option value="' . $item->id . '">' . $item->name . '</option>';
            }
          }
          echo '</select>';
        echo '</div>';
      echo '</div>';
    echo '</div>';
  }
}

?>