<?php


class MediaAdminView extends AdminView {

  public $activeNavigation;
  private $items;
  private $paginationParams;

  public function __construct($controller, $data) {
    parent::__construct($controller);

    $this->activeNavigation = 'media';

    if (@exists($data)) {

      if (!is_array($data)) $data = (array)$data;

      if (@exists($data['items'])) $this->items = $data['items'];
      if (@exists($data['paginationParams'])) $this->paginationParams = $data['paginationParams'];
    }
  }

  /********************************** TABLE PAGE **********************************/

  public function displayTable() {
    $this->renderTable();
  }


  public function renderTable() {

    // $this->renderTableFilters($this->paginationParams);

    echo '<div class="table-wrapper">';

      // echo '<table class="table table-dynamic table-tools filter-select dataTable">';
      echo '<table class="table table-dynamic table-tools filter-select">';

        echo '<thead>';
          echo '<tr>';
            echo '<th>' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Preview') . '</th>';
            echo '<th>' . Trans::get('Title') . '</th>';
            echo '<th>' . Trans::get('File') . '</th>';
            echo '<th>' . Trans::get('Type') . '</th>';
            // $this->displaySortingCell('id', Trans::get('Id'), $this->paginationParams);
            // echo '<th>' . Trans::get('Preview') . '</th>';
            // $this->displaySortingCell('title', Trans::get('Title'), $this->paginationParams);
            // $this->displaySortingCell('file_name', Trans::get('File name'), $this->paginationParams);
            // $this->displaySortingCell('mime', Trans::get('Type'), $this->paginationParams);
            echo '<th class="text-right">' . Trans::get('Actions') . '</th>';
          echo '</tr>';
        echo '</thead>';

        echo '<tbody>';

        if (@exists($this->items)) {

          foreach ($this->items as $item) {

            echo '<tr data-id="' . $item->id . '">';
              $this->displayCellRaw(array('name' => 'id', 'type' => 'int'), $item);
              $this->renderMediaTableThumb($item);
              $this->displayCellRaw(array('name' => 'title', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'file_name', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'mime', 'type' => 'varchar'), $item);
              echo '<td class="text-right">';
                echo '<button type="button" class="btn btn-sm btn-default edit" data-id="' . $item->id . '" data-toggle="modal" data-target="#modal-entry"><i class="icon-note"></i></button>';
                echo '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
              echo '</td>';
            echo '</tr>';
          }
        }

        echo '</tbody>';

        echo '<tfoot>';
          echo '<tr>';
            echo '<th style="visibility: hidden">' . Trans::get('Id') . '</th>';
            echo '<th style="visibility: hidden">' . Trans::get('Preview') . '</th>';
            echo '<th style="visibility: hidden">' . Trans::get('Title') . '</th>';
            echo '<th style="visibility: hidden">' . Trans::get('File name') . '</th>';
            echo '<th>' . Trans::get('Type') . '</th>';
            echo '<th style="visibility: hidden"></th>';
          echo '</tr>';
        echo '</tfoot>';

      echo '</table>';

      // $this->renderTablePagination($this->paginationParams);

    echo '</div>';
  }


  public function displayFormModal() {

    echo '<div class="modal fade" id="modal-entry" tabindex="-1" role="dialog" aria-hidden="true">';
      echo '<div class="modal-dialog">';
        echo '<div class="modal-content">';

          echo '<div class="modal-header">';
            echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>';
            echo '<h4 class="modal-title">' . Trans::get('Media data') . '</h4>';
          echo '</div>';

          echo '<div class="modal-body">';
            echo '<div class="row">';
              echo '<div class="col-md-12">';
                echo '<form id="data-form" role="form">';
                  echo '<div class="row">';
                    echo '<input type="hidden" name="id" value="">';
                    $this->displayTextInput(Trans::get('Name'), 'title');
                  echo '</div>';
                echo '</form>';
              echo '</div>';
            echo '</div>';
          echo '</div>';

          echo '<div class="modal-footer bg-gray-light">';
            echo '<button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">' . Trans::get('Close') . '</button>';
            echo '<button type="button" class="btn btn-primary btn-embossed" data-dismiss="modal" id="save">' . Trans::get('Save') . '</button>';
          echo '</div>';

        echo '</div>';
      echo '</div>';
    echo '</div>';
  }
}

?>