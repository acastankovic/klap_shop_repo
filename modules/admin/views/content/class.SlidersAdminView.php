<?php


class SlidersAdminView extends AdminView {

  public $activeNavigation;
  private $resource;
  private $languages;
  private $items;
  private $item;
  public $parentId;

  public function __construct($controller, $data) {

    parent::__construct($controller);

    $this->activeNavigation = 'sliders';
    $this->resource = 'slide';

    if (@exists($data)) {

      if (!is_array($data)) $data = (array)$data;

      if (@exists($data['items'])) $this->items = $data['items'];
      if (@exists($data['item'])) $this->item = $data['item'];
      if (@exists($data['languages'])) $this->languages = $data['languages'];
      if (@exists($data['parentId'])) $this->parentId = $data['parentId'];
    }
  }


  /********************************** TABLE PAGE **********************************/

  public function displayTable() {

    $this->renderAddNewItemModalButton(array('modalElementId' => 'form-modal', 'buttonElementClass' => 'new-form-entry'));
    $this->renderTable();
  }


  private function renderTable() {

    echo '<div class="table-wrapper">';

      echo '<table class="table table-dynamic table-tools filter-select">';

        echo '<thead>';
          echo '<tr>';
            echo '<th>' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Name') . '</th>';
            echo '<th>' . Trans::get('Language') . '</th>';
            echo '<th class="text-right">' . Trans::get('Actions') . '</th>';
          echo '</tr>';
        echo '</thead>';

        echo '<tbody>';

        if (@exists($this->items)) {

          foreach ($this->items as $item) {
            echo '<tr data-id="' . $item->id . '">';
              $this->displayCellRaw(array('name' => 'id', 'type' => 'int'), $item);
              $this->displayCellRaw(array('name' => 'name', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'language_name', 'type' => 'varchar'), $item);
              echo '<td class="text-right">';
                echo '<a class="btn btn-sm btn-default m-0" data-id="' . $item->id . '" href="' . Conf::get('url') . '/admin/sliders/' . $item->id . '/insert"><i class="fa fa-sitemap"></i></a>';
                echo '<button type="button" class="btn btn-sm btn-default edit" data-id="' . $item->id . '" data-lang_id="' . $item->lang_id . '" data-toggle="modal" data-target="#form-modal" style="margin:0 10px;"><i class="icon-note"></i></button>';
                echo '<button type="button" class="btn btn-sm btn-danger m-0 delete" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
              echo '</td>';
            echo '</tr>';
          }
        }
        echo '</tbody>';

        echo '<tfoot>';
          echo '<tr>';
            echo '<th style="visibility: hidden;">' . Trans::get('Id') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Name') . '</th>';
            echo '<th>' . Trans::get('Language') . '</th>';
            echo '<th style="visibility: hidden;"></th>';
          echo '</tr>';
        echo '</tfoot>';

      echo '</table>';

    echo '</div>';
  }


  public function displayFormModal() {

    echo '<div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-hidden="true">';
      echo '<div class="modal-dialog">';
        echo '<div class="modal-content">';

        echo '<div class="modal-header">';
          echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>';
          echo '<h4 class="modal-title">' . Trans::get('Menu data') . '</h4>';
        echo '</div>';

        echo '<div class="modal-body">';
          $this->displayLanguageTabs($this->languages);
          echo '<div class="row">';
            echo '<div class="form-modal-content"></div>';
            if (Languages::enabled()) {
              echo '<div class="col-md-12">* ' . Trans::get('Save separately for each language') . '.</div>';
            }
          echo '</div>';
        echo '</div>';

        echo '<div class="modal-footer bg-gray-light action-buttons" style="position:relative;">';
          echo '<div class="spinner-overlay action-buttons-overlay">';
            $this->renderSpinner();
          echo '</div>';
          echo '<button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">' . Trans::get('Close') . '</button>';
          echo '<button type="button" class="btn btn-primary btn-embossed save-insert-form">' . Trans::get('Save') . '</button>';
        echo '</div>';

        echo '</div>';
      echo '</div>';
    echo '</div>';
  }


  /******************************* SLIDER ITEMS PAGE *******************************/


  public function displayHiddenFields() {
    echo '<input type="hidden" id="sliderId" value="' . $this->parentId . '" />';
  }


  public function displaySliderItemsTable() {

    $url = Conf::get('url') . '/admin/sliders/' . $this->parentId . '/slider_items/0/insert';

    $this->renderAddNewItemLinkButton(array('url' => $url));

    $this->renderSliderItemsTable();
  }


  public function renderSliderItemsTable() {

    echo '<div class="table-wrapper">';

      echo '<table class="table table-dynamic table-tools filter-select">';

        echo '<thead>';
          echo '<tr>';
            echo '<th>' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Name') . '</th>';
            echo '<th>' . Trans::get('Image') . '</th>';
            echo '<th>' . Trans::get('Caption') . '</th>';
            echo '<th>' . Trans::get('Link') . '</th>';
            echo '<th>' . Trans::get('Visibility') . '</th>';
            echo '<th class="text-right">' . Trans::get('Actions') . '</th>';
          echo '</tr>';
        echo '</thead>';

        echo '<tbody>';

        if (@exists($this->items)) {

          foreach ($this->items as $item) {
            echo '<tr data-id="' . $item->id . '">';
              $this->displayCellRaw(array('name' => 'id', 'type' => 'int'), $item);
              $this->displayCellRaw(array('name' => 'name', 'type' => 'varchar'), $item);
              $this->renderTableThumb($item->image);
              $this->displayCellRaw(array('name' => 'caption', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'url', 'type' => 'varchar'), $item);
              $this->renderPublishStatusForTable($item);
              echo '<td class="text-right">';
                echo '<a class="btn btn-sm btn-default edit" data-id="' . $item->id . '" href="' . Conf::get('url') . '/admin/sliders/' . $this->parentId . '/slider_items/' . $item->id . '/insert"><i class="icon-note"></i></a>';
                echo '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
              echo '</td>';
            echo '</tr>';
          }
        }
        echo '</tbody>';

        echo '<tfoot>';
          echo '<tr>';
            echo '<th style="visibility: hidden;">' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Name') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Image') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Caption') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Link') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Visibility') . '</th>';
            echo '<th style="visibility: hidden;"></th>';
          echo '</tr>';
        echo '</tfoot>';

      echo '</table>';

    echo '</div>';
  }


  /*************************** SLIDER ITEM INSERT PAGE ***************************/


  public function displayInsertPageContent() {

    echo '<div class="row">';

      echo '<div class="col-md-9">';
        $this->renderForm();
      echo '</div>';

      echo '<div class="col-md-3 side-panels">';
        $this->displayInsertPageActions(array('item' => $this->item, 'displayPublishCheckbox' => true));
        $this->renderImagePanel(array('item' => $this->item));
      echo '</div>';

    echo '</div>';
  }


  private function renderForm() {

    $item = $this->item;

    $id = @exists($item->id) ? $item->id : 0;
    $parentId = @exists($item->slider_id) ? $item->slider_id : '';
    $published = @exists($item->published) ? $item->published : 0;
    $image = @exists($item->image) ? $item->image : '';
    $name = @exists($item->name) ? htmlentities($item->name) : '';
    $caption = @exists($item->caption) ? htmlentities($item->caption) : '';
    $url = @exists($item->url) ? htmlentities($item->url) : '';

    $langId = Trans::getLanguageId();

    echo '<input type="hidden" id="field-resource" value="' . $this->resource . '" />';

    $langSuffix = $this->getLanguageSuffix();

    echo '<form id="insert-form' . $langSuffix . '">';

      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'id', 'value' => $id));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'slider_id', 'value' => $parentId, 'className' => 'parentId-field' . $langSuffix));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'image', 'value' => $image, 'className' => 'image-field' . $langSuffix));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'published', 'value' => $published, 'className' => 'published'));
      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'name', 'value' => $name, 'label' => Trans::get('Name')));
      $this->displayFormTextarea(array('name' => 'caption', 'resource' => $this->resource, 'langId' => $langId, 'value' => $caption, 'label' => Trans::get('Caption'), 'rows' => 4));
      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'url', 'value' => $url, 'label' => Trans::get('Link')));

    echo '</form>';
  }
}

?>