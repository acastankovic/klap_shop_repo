<?php


class ArticlesAdminView extends AdminView {

  public $activeNavigation;
  private $resource;
  private $items;
  private $langGroupId;
  private $languages;
  private $parentId;
  public $pageTitle;

  public function __construct($controller, $data) {
    parent::__construct($controller);

    $this->activeNavigation = 'articles';
    $this->resource = 'article';

    if (@exists($data)) {

      if (!is_array($data)) $data = (array)$data;

      if (@exists($data['items'])) $this->items = $data['items'];
      if (@exists($data['langGroupId'])) $this->langGroupId = $data['langGroupId'];
      if (@exists($data['languages'])) $this->languages = $data['languages'];
      if (@exists($data['parentId'])) $this->parentId = $data['parentId'];
      if (@exists($data['pageTitle'])) $this->pageTitle = $data['pageTitle'];
    }
  }


  /********************************** TABLE PAGE **********************************/

  public function displayTable() {

    $this->renderAddNewItemLinkButton(array('url' => Conf::get('url') . '/admin/articles/0/insert'));
    $this->renderTable();
  }


  private function renderTable() {

    echo '<div class="table-wrapper">';

      echo '<table class="table table-dynamic table-tools filter-select">';

        echo '<thead>';
          echo '<tr>';
            echo '<th>' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Title') . '</th>';
            echo '<th>' . Trans::get('Parent category') . '</th>';
            echo '<th>' . Trans::get('Language') . '</th>';
            echo '<th>' . Trans::get('Visibility') . '</th>';
            echo '<th class="text-right">' . Trans::get('Actions') . '</th>';
          echo '</tr>';
        echo '</thead>';

        echo '<tbody>';

        if (@exists($this->items)) {

          foreach ($this->items as $item) {
            echo '<tr data-id="' . $item->id . '">';
              $this->displayCellRaw(array('name' => 'id', 'type' => 'int'), $item);
              $this->displayCellRaw(array('name' => 'title', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'parent_name', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'language_name', 'type' => 'varchar'), $item);
              $this->renderPublishStatusForTable($item);
              echo '<td class="text-right">';
                echo '<a class="btn btn-sm btn-default edit" data-id="' . $item->id . '" href="' . Conf::get('url') . '/admin/articles/' . $item->id . '/insert"><i class="icon-note"></i></a>';
                echo '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
              echo '</td>';
            echo '</tr>';
          }
        }
        echo '</tbody>';

        echo '<tfoot>';
          echo '<tr>';
            echo '<th style="visibility: hidden;">' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Title') . '</th>';
            echo '<th>' . Trans::get('Parent category') . '</th>';
            echo '<th>' . Trans::get('Language') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Visibility') . '</th>';
            echo '<th style="visibility: hidden;"></th>';
          echo '</tr>';
        echo '</tfoot>';

      echo '</table>';

    echo '</div>';
  }


  /********************************* INSERT PAGE *********************************/


  public function displayInsertPageContent() {

    $this->displayLanguageTabs($this->languages);

    echo '<div class="row">';

      echo '<input type="hidden" id="field-resource" value="' . $this->resource . '" />';

      foreach ($this->items as $langId => $item) {

        $activeClass = (int)$langId === (int)Trans::getLanguageId() ? ' active' : '';

        $langSuffix = $this->getLanguageSuffix(array('langId' => $langId));

        echo '<div class="language-wrapper' . $activeClass . '" id="languageWrapper' . $langSuffix . '">';

          echo '<div class="col-md-9">';
            $this->renderForm($item);
            if (@exists($item->comments)) {
              $this->renderCommentsSection($item->comments);
            }
          echo '</div>';

          echo '<div class="col-md-3 side-panels">';
            $this->displayInsertPageActions(array('item' => $item, 'displayPublishCheckbox' => true, 'displayPreviewButton' => true, 'displayLanguageNotice' => true, 'displayAllowCommentsCheckbox' => true));
            $this->displayStatusPanel(array('item' => $item));
            $this->renderImagePanel(array('item' => $item, 'langId' => $langId, 'type' => Conf::get('modal_type')['intro_image']));
            $this->renderImagePanel(array('item' => $item, 'langId' => $langId, 'type' => Conf::get('modal_type')['image']));
            $this->displayInsertPageTree(array('langId' => $langId));
          echo '</div>';

        echo '</div>';
      }

    echo '</div>';
  }


  private function renderForm($item) {

    $id = @exists($item->id) ? $item->id : 0;
    $categoryId = @exists($item->category_id) ? $item->category_id : 0;
    $published = @exists($item->published) ? $item->published : 0;
    $allowComments = @exists($item->allow_comments) ? $item->allow_comments : 0;
    $rang = @exists($item->rang) ? $item->rang : 9999;
    $content = @exists($item->content) ? $item->content : '';
    $introText = @exists($item->intro_text) ? $item->intro_text : '';
    $introImage = @exists($item->intro_image) ? $item->intro_image : '';
    $image = @exists($item->image) ? $item->image : '';
    $galleryJson = @exists($item->gallery_json) ? htmlentities($item->gallery_json) : '';
    $title = @exists($item->title) ? htmlentities($item->title) : '';
    $subtitle = @exists($item->subtitle) ? htmlentities($item->subtitle) : '';
    $alias = @exists($item->alias) ? htmlentities($item->alias) : '';
    $metaTitle = @exists($item->meta_title) ? htmlentities($item->meta_title) : '';
    $metaDescription = @exists($item->meta_description) ? htmlentities($item->meta_description) : '';
    $metaKeywords = @exists($item->meta_keywords) ? htmlentities($item->meta_keywords) : '';
    $eventDate = @exists($item->event_date) && (string)$item->event_date != '0000-00-00 00:00:00' ? dateFormat($item->event_date) : '';
    $publishDate = @exists($item->publish_date) && (string)$item->publish_date != '0000-00-00 00:00:00' ? dateFormat($item->publish_date) : '';
    $langId = $item->lang_id;

    $langSuffix = $this->getLanguageSuffix(array('langId' => $langId));

    echo '<form id="insert-form' . $langSuffix . '">';

      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'id', 'value' => $id));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'category_id', 'value' => $categoryId, 'className' => 'parentId-field' . $langSuffix));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'published', 'value' => $published, 'className' => 'published'));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'allow_comments', 'value' => $allowComments, 'className' => 'allow-comments'));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'rang', 'value' => $rang, 'className' => 'rang'));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'intro_image', 'value' => $introImage, 'className' => 'intro-image-field' . $langSuffix));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'image', 'value' => $image, 'className' => 'image-field' . $langSuffix));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'lang_id', 'value' => $langId));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'lang_group_id', 'value' => $this->langGroupId));
      echo '<input type="hidden" name="gallery_json" id="' . $this->resource . '-gallery_json' . $langSuffix . '" value="' . $galleryJson . '" class="gallery-json-field' . $langSuffix . '" />';
      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'title', 'value' => $title, 'label' => Trans::get('Title')));
      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'subtitle', 'value' => $subtitle, 'label' => Trans::get('Subtitle')));
      $this->displayFormTextarea(array('name' => 'intro_text', 'resource' => $this->resource, 'langId' => $langId, 'value' => $introText, 'label' => Trans::get('Intro text'), 'rows' => 4, 'info' => Trans::get('Short description when articles are listed, not on the article page itself')));
      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'alias', 'value' => $alias, 'label' => Trans::get('Alias'), 'disabled' => true));
      $this->displayFormTextEditor(array('name' => 'content', 'resource' => $this->resource, 'langId' => $langId, 'value' => $content, 'label' => Trans::get('Content')));
      echo '<div class="row">';
        echo '<div class="col-md-6">';
          $this->displayFormDateField(array('type' => 'text', 'name' => 'event_date', 'resource' => $this->resource, 'langId' => $langId, 'value' => $eventDate, 'label' => Trans::get('Event date'), 'info' => Trans::get('Date when event will held place')));
        echo '</div>';
        echo '<div class="col-md-6">';
          $this->displayFormDateField(array('type' => 'text', 'name' => 'publish_date', 'resource' => $this->resource, 'langId' => $langId, 'value' => $publishDate, 'label' => Trans::get('Publish date'), 'info' => Trans::get('Page will not be published before this date')));
        echo '</div>';
      echo '</div>';
        $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'meta_title', 'value' => $metaTitle, 'label' => Trans::get('Meta Title'), 'info' => Trans::get('For better browser ranking (not visible on page)')));
      echo '<div class="row">';
        echo '<div class="col-md-6">';
          $this->displayFormTextarea(array('name' => 'meta_description', 'resource' => $this->resource, 'langId' => $langId, 'value' => $metaDescription, 'label' => Trans::get('Meta Description'), 'rows' => 4, 'info' => Trans::get('For better browser ranking (not visible on page)')));
        echo '</div>';
        echo '<div class="col-md-6">';
          $this->displayFormTextarea(array('name' => 'meta_keywords', 'resource' => $this->resource, 'langId' => $langId, 'value' => $metaKeywords, 'label' => Trans::get('Meta Keywords'), 'rows' => 4, 'info' => Trans::get('For better browser ranking (not visible on page)')));
        echo '</div>';
      echo '</div>';

      $this->renderFormGallery($item, $langId);

    echo '</form>';
  }


  /******************************* CATEGORIES PAGE *******************************/


  public function displayCategoriesPage() {

    echo '<div class="table-wrapper">';

      echo '<table class="table table-dynamic table-tools filter-select">';

        echo '<thead>';
          echo '<tr>';
            echo '<th>' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Title') . '</th>';
            echo '<th>' . Trans::get('Language') . '</th>';
            echo '<th>' . Trans::get('Visibility') . '</th>';
            echo '<th class="text-right">' . Trans::get('Actions') . '</th>';
          echo '</tr>';
        echo '</thead>';

        echo '<tbody>';

        if (@exists($this->items)) {

          foreach ($this->items as $item) {
            echo '<tr data-id="' . $item->id . '">';
              $this->displayCellRaw(array('name' => 'id', 'type' => 'int'), $item);
              $this->displayCellRaw(array('name' => 'name', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'language_name', 'type' => 'varchar'), $item);
              $this->renderPublishStatusForTable($item);
              echo '<td class="text-right">';
                echo '<a class="btn btn-sm btn-default edit" data-id="' . $item->id . '" href="' . Conf::get('url') . '/admin/articles/categories/' . $item->id . '/sort"><i class="icon-note"></i></a>';
              echo '</td>';
            echo '</tr>';
          }
        }
        echo '</tbody>';

        echo '<tfoot>';
          echo '<tr>';
            echo '<th style="visibility: hidden;">' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Title') . '</th>';
            echo '<th>' . Trans::get('Language') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Visibility') . '</th>';
            echo '<th style="visibility: hidden;"></th>';
          echo '</tr>';
        echo '</tfoot>';

      echo '</table>';

    echo '</div>';
  }


  /********************************** SORT PAGE **********************************/


  public function displaySortPageHiddenFields() {

    echo '<input type="hidden" id="parentId" value="' . $this->parentId . '"/>';
  }
}

?>