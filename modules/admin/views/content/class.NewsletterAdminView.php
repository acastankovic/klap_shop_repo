<?php

class NewsletterAdminView extends AdminView {

  public $activeNavigation;
  private $resource;
  private $items;

  public function __construct($controller, $data) {
    parent::__construct($controller);

    $this->activeNavigation = 'newsletter';
    $this->resource = 'newsletter';

    if (@exists($data)) {

      if (!is_array($data)) $data = (array)$data;

      if (@exists($data['items'])) $this->items = $data['items'];
    }
  }


  /********************************** TABLE PAGE **********************************/


  public function displayTable() {

    $this->renderAddNewItemLinkButton(array('url' => Conf::get('url') . '/content/newsletter/download', 'title' => 'Download'));
    $this->renderTable();
  }


  private function renderTable() {

    echo '<div class="table-wrapper">';

      echo '<table class="table table-dynamic table-tools filter-select">';

        echo '<thead>';
          echo '<tr>';
            echo '<th>' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Email') . '</th>';
            echo '<th class="text-right">' . Trans::get('Actions') . '</th>';
          echo '</tr>';
        echo '</thead>';

        echo '<tbody>';

          if (@exists($this->items)) {

            foreach ($this->items as $item) {
              echo '<tr data-id="' . $item->id . '">';
                $this->displayCellRaw(array('name' => 'id', 'type' => 'int'), $item);
                $this->displayCellRaw(array('name' => 'email', 'type' => 'varchar'), $item);
                echo '<td class="text-right">';
                  //echo '<a class="btn btn-sm btn-default edit" data-id="' . $item->id . '" href="' . Conf::get('url') . '/admin/newsletter/' . $item->id . '/insert"><i class="icon-note"></i></a>';
                  echo '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
                echo '</td>';
              echo '</tr>';
            }
          }
        echo '</tbody>';

        echo '<tfoot>';
          echo '<tr>';
            echo '<th style="visibility: hidden;">' . Trans::get('Id') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Email') . '</th>';
            echo '<th style="visibility: hidden;"></th>';
          echo '</tr>';
        echo '</tfoot>';

      echo '</table>';

    echo '</div>';
  }
}

?>