<?php


class OrdersAdminView extends AdminView {

  public $activeNavigation;
  public $pageTitle;
  private $resource;
  private $items;
  private $item;
  private $statuses;

  public function __construct($controller, $data) {
    parent::__construct($controller);

    $this->activeNavigation = 'orders';
    $this->resource = 'order';

    if (@exists($data['items'])) $this->items = $data['items'];
    if (@exists($data['item'])) $this->item = $data['item'];
    if (@exists($data['pageTitle'])) $this->pageTitle = $data['pageTitle'];

    $this->statuses = array(
      OrderStatus::NOT_ORDERED => Trans::get('Not ordered'),
      OrderStatus::PENDING => Trans::get('Pending'),
      OrderStatus::IN_DELIVERY => Trans::get('In delivery'),
      OrderStatus::DELIVERED => Trans::get('Delivered')
    );
  }


  /********************************** TABLE PAGE **********************************/

  public function renderCustomerItem($customer, $showMessage = null) {
    echo '<div>' . Trans::get('Name') . ': ' . $customer->name . '</div>';
    echo '<div>' . Trans::get('Email') . ': ' . $customer->email . '</div>';
    echo '<div>' . Trans::get('Phone') . ': ' . $customer->phone . '</div>';
    echo '<div>' . Trans::get('Address') . ': ' . $customer->address . '</div>';
    if (@exists($showMessage) && $showMessage) {
      echo '<div>' . Trans::get('Message') . ': ' . $customer->message . '</div>';
    }
  }


  private function renderOrderItem($item) {
    echo '<div>' . Trans::get('Product ID') . ': ' . $item->id . '</div>';
    echo '<div>' . Trans::get('Code') . ': ' . $item->code . '</div>';
    echo '<div>' . Trans::get('Title') . ': ' . $item->title . '</div>';
    echo '<div>' . Trans::get('Original price') . ': ' . $item->original_price . '</div>';
    echo '<div>' . Trans::get('Discount') . ': ' . $item->discount_price . '</div>';
    echo '<div>' . Trans::get('Price with discount') . ': ' . $item->total_price . '</div>';
    echo '<div>' . Trans::get('Quantity') . ': ' . $item->quantity . '</div>';
    echo '<div>' . Trans::get('Total price') . ': ' . $item->quantity_total_price . '</div>';
  }


  public function displayTable() {
    $this->renderTable();
  }


  public function renderTable() {

    echo '<div class="table-wrapper">';
      echo '<table class="table table-dynamic table-tools filter-select">';
        echo '<thead>';
          echo '<tr>';
            echo '<th>' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Products') . '</th>';
            echo '<th>' . Trans::get('Quantity') . '</th>';
            echo '<th>' . Trans::get('Vat') . '</th>';
            echo '<th>' . Trans::get('Discount') . '</th>';
            echo '<th>' . Trans::get('Price') . '</th>';
            echo '<th>' . Trans::get('Customer info') . '</th>';
            echo '<th>' . Trans::get('Status') . '</th>';
            echo '<th>' . Trans::get('Paid') . '</th>';
            echo '<th class="text-right">' . Trans::get('Actions') . '</th>';
          echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        if (@exists($this->items)) {

          foreach ($this->items as $item) {

            echo '<tr data-id="' . $item->id . '">';
              $this->displayCellRaw(array('name' => 'id', 'type' => 'int'), $item);
              $this->renderTableOrderItems($item);
              $this->displayCellRaw(array('name' => 'quantity', 'type' => 'int'), $item);
              $this->displayCellRaw(array('name' => 'order_vat', 'type' => 'int'), $item);
              $this->displayCellRaw(array('name' => 'order_discount', 'type' => 'int'), $item);
              $this->displayCellRaw(array('name' => 'order_price', 'type' => 'int'), $item);
              $this->renderTableCustomerInfo($item);
              $this->renderStatusString($item);
              $this->renderPaidStatus($item);
              echo '<td class="text-right">';
                echo '<a class="btn btn-sm btn-default edit" data-id="' . $item->id . '" href="' . Conf::get('url') . '/admin/orders/' . $item->id . '/insert"><i class="icon-note"></i></a>';
                echo '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
              echo '</td>';
            echo '</tr>';
          }
        }
        echo '</tbody>';
        echo '<tfoot>';
          echo '<tr>';
            echo '<th style="visibility: hidden;">' . Trans::get('Id') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Products') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Quantity') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Vat') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Discount') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Price') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Customer info') . '</th>';
            echo '<th>' . Trans::get('Status') . '</th>';
            echo '<th>' . Trans::get('Paid') . '</th>';
            echo '<th style="visibility: hidden;"></th>';
          echo '</tr>';
        echo '</tfoot>';
      echo '</table>';
    echo '</div>';
  }


  public function renderTableOrderItems($item) {

    $oItems = json_decode($item->items);

    echo '<td data-name="items" data-type="text" data-value="">';

    if (@exists($oItems)) {
      foreach ($oItems as $oItem) {
        echo '<div class="order-item">';
        $this->renderOrderItem($oItem);
        echo '</div>';
      }
    } else {
      echo '-';
    }

    echo '</td>';
  }


  public function renderTableCustomerInfo($item) {

    $customer = json_decode($item->customer_json);

    echo '<td data-name="items" data-type="text" data-value="">';

    if (@exists($customer)) {
      echo '<div class="customer-item">';
      $this->renderCustomerItem($customer);
      echo '</div>';
    } else {
      echo '-';
    }

    echo '</td>';
  }


  public function renderPaidStatus($item) {

    $paidString = $item->paid ? Trans::get('Yes') : Trans::get('No');

    echo '<td data-name="paid" data-type="int" data-value="' . $item->paid . '">' . $paidString . '</td>';
  }

  public function renderStatusString($item) {

    echo '<td data-name="status" data-type="int" data-value="' . $item->status . '">' . $this->statuses[$item->status] . '</td>';
  }


  /********************************* INSERT PAGE *********************************/

  public function displayInsertPageContent() {

    echo '<div class="row">';

      echo '<div class="col-md-8">';
        $this->renderForm();
      echo '</div>';

      echo '<div class="col-md-4">';
        $this->displayInsertPageActions(array('item' => $this->item));
        $this->displayStatusPanel(array('item' => $this->item));
      echo '</div>';

    echo '</div>';
  }


  private function renderForm() {

    $item = $this->item;

    $id = @exists($item->id) ? $item->id : 0;
    $session = @exists($item->session) ? htmlentities($item->session) : '';
    $quantity = @exists($item->quantity) ? $item->quantity : 0;
    $orderVat = @exists($item->order_vat) ? $item->order_vat : 0;
    $orderDiscount = @exists($item->order_discount) ? $item->order_discount : 0;
    $orderPrice = @exists($item->order_price) ? $item->order_price : 0;
    $status = @exists($item->status) ? $item->status : 0;
    $paid = @exists($item->paid) ? $item->paid : 0;
    $itemsJson = @exists($item->items) ? $item->items : '';
    $customerJson = @exists($item->customer_json) ? $item->customer_json : '';

    $langId = Trans::getLanguageId();

    echo '<input type="hidden" id="field-resource" value="' . $this->resource . '" />';

    $langSuffix = $this->getLanguageSuffix();

    echo '<form id="insert-form' . $langSuffix . '">';

    $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'id', 'value' => $id));
    $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'session', 'value' => $session, 'label' => Trans::get('Session'), 'disabled' => true));

    echo '<div class="row">';
      echo '<div class="col-sm-6 col-md-3">';
        $this->displayFormField(array('type' => 'text', 'name' => 'quantity', 'resource' => $this->resource, 'langId' => $langId, 'value' => $quantity, 'label' => Trans::get('Quantity'), 'disabled' => true));
      echo '</div>';
      echo '<div class="col-sm-6 col-md-3">';
        $this->displayFormField(array('type' => 'text', 'name' => 'order_vat', 'resource' => $this->resource, 'langId' => $langId, 'value' => $orderVat, 'label' => Trans::get('Vat'), 'disabled' => true));
      echo '</div>';
      echo '<div class="col-sm-6 col-md-3">';
        $this->displayFormField(array('type' => 'text', 'name' => 'order_discount', 'resource' => $this->resource, 'langId' => $langId, 'value' => $orderDiscount, 'label' => Trans::get('Discount'), 'disabled' => true));
      echo '</div>';
      echo '<div class="col-sm-6 col-md-3">';
        $this->displayFormField(array('type' => 'text', 'name' => 'order_price', 'resource' => $this->resource, 'langId' => $langId, 'value' => $orderPrice, 'label' => Trans::get('Price'), 'disabled' => true));
      echo '</div>';
    echo '</div>';

    echo '<div class="row">';
      echo '<div class="col-md-6">';
        $this->renderStatusSelectBox($status, $langSuffix);
      echo '</div>';
      echo '<div class="col-md-6">';
        $this->displayFormCheckbox(array('type' => 'checkbox', 'name' => 'paid', 'resource' => $this->resource, 'langId' => $langId, 'value' => $paid, 'label' => Trans::get('Paid'), 'className' => 'paid-checkbox'));
      echo '</div>';
    echo '</div>';

    $this->renderCustomerInfo($customerJson);
    $this->renderOrderItems($itemsJson);

    echo '</form>';
  }


  private function renderStatusSelectBox($status, $langSuffix) {

    echo '<div class="form-group">';
      echo '<label>' . Trans::get('Discount Type') . '</label>';
      echo '<select class="form-control" id="' . $this->resource . '-status' . $langSuffix . '" name="status">';
        echo '<option value="">' . Trans::get('Choose') . '</option>';

        foreach ($this->statuses as $key => $value) {

          $selected = $key == $status ? ' selected="selected"' : '';

          echo '<option value="' . $key . '"' . $selected . '>' . $value . '</option>';
        }
      echo '</select>';
    echo '</div>';
  }


  private function renderCustomerInfo($customerJson) {

    $customer = json_decode($customerJson);

    if (@exists($customer)) {

      echo '<div class="customer-item">';
        echo '<div class="row">';
          echo '<div class="col-md-12">';
            echo '<h3>' . Trans::get('Customer info') . '</h3>';
            $this->renderCustomerItem($customer, true);
          echo '</div>';
        echo '</div>';
      echo '</div>';
    }
  }


  private function renderOrderItems($itemsJson) {

    $oItems = json_decode($itemsJson);

    if (@exists($oItems)) {
      echo '<div class="order-items">';
        echo '<div class="row">';
          echo '<div class="col-md-12">';
          echo '<h3>' . Trans::get('Order items') . '</h3>';
          echo '<div class="row">';
            foreach ($oItems as $oItem) {
              echo '<div class="col-md-6 order-item">';
                $this->renderOrderItem($oItem);
              echo '</div>';
            }
          echo '</div>';
          echo '</div>';
        echo '</div>';
      echo '</div>';
    }
  }

}

?>