<?php


class ProductsAdminView extends AdminView {

  public $activeNavigation;
  public $pageTitle;
  private $parentId;
  private $resource;
  private $mediaCategoryId;
  private $items;
  private $langGroupId;
  private $languages;
  private $vendors;

  private $shopRootCategories;

  public function __construct($controller, $data) {
    parent::__construct($controller);

    $this->activeNavigation = 'products';
    $this->resource = 'product';
    $this->mediaCategoryId = Conf::get('media_category_id')['products'];

    if (@exists($data)) {

      if (!is_array($data)) $data = (array)$data;

      if (@exists($data['items'])) $this->items = $data['items'];
      if (@exists($data['langGroupId'])) $this->langGroupId = $data['langGroupId'];
      if (@exists($data['languages'])) $this->languages = $data['languages'];
      if (@exists($data['parentId'])) $this->parentId = $data['parentId'];
      if (@exists($data['pageTitle'])) $this->pageTitle = $data['pageTitle'];
      if (@exists($data['vendors'])) $this->vendors = $data['vendors'];

      if (@exists($data['shopRootCategories'])) $this->shopRootCategories = $data['shopRootCategories'];
    }

  }


  /********************************** TABLE PAGE **********************************/


  public function displayTable() {

    $this->renderAddNewItemLinkButton(array('url' => Conf::get('url') . '/admin/products/0/insert'));
    $this->renderTable();
  }


  public function renderTable() {

    echo '<div class="table-wrapper">';
      echo '<table class="table table-dynamic table-tools filter-select">';
        echo '<thead>';
          echo '<tr>';
            echo '<th>' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Title') . '</th>';
            echo '<th>' . Trans::get('Parent category') . '</th>';
            echo '<th>' . Trans::get('Measure') . '</th>';
            echo '<th>' . Trans::get('Price') . '</th>';
            echo '<th>' . Trans::get('Discount') . '</th>';
            echo '<th>' . Trans::get('Language') . '</th>';
            echo '<th>' . Trans::get('Visibility') . '</th>';
            echo '<th class="text-right">' . Trans::get('Actions') . '</th>';
          echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        if (@exists($this->items)) {

          foreach ($this->items as $item) {

            echo '<tr data-id="' . $item->id . '">';
              $this->displayCellRaw(array('name' => 'id', 'type' => 'int'), $item);
              $this->displayCellRaw(array('name' => 'title', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'parent_name', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'code', 'type' => 'varchar'), $item);
              $this->displayCellRaw(array('name' => 'price', 'type' => 'int'), $item);
              $this->renderDiscountValueForTable($item);
              $this->displayCellRaw(array('name' => 'language_name', 'type' => 'varchar'), $item);
              $this->renderPublishStatusForTable($item);
              echo '<td class="text-right">';
                echo '<a class="btn btn-sm btn-default edit" data-id="' . $item->id . '" href="' . Conf::get('url') . '/admin/products/' . $item->id . '/insert"><i class="icon-note"></i></a>';
                echo '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $item->id . '"><i class="icons-office-52"></i></button>';
              echo '</td>';
            echo '</tr>';
          }
        }
        echo '</tbody>';
        echo '<tfoot>';
          echo '<tr>';
            echo '<th style="visibility: hidden;">' . Trans::get('Id') . '</th>';
            echo '<th>' . Trans::get('Title') . '</th>';
            echo '<th>' . Trans::get('Parent category') . '</th>';
            echo '<th>' . Trans::get('Measure') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Price') . '</th>';
            echo '<th>' . Trans::get('Discount') . '</th>';
            echo '<th>' . Trans::get('Language') . '</th>';
            echo '<th style="visibility: hidden;">' . Trans::get('Visibility') . '</th>';
            echo '<th style="visibility: hidden;"></th>';
          echo '</tr>';
        echo '</tfoot>';
      echo '</table>';
    echo '</div>';
  }


  public function renderDiscountValueForTable($item) {

    $value = $item->discount_value;
    if ((int)$item->discount_type === (int)Conf::get('discount_type')['percent']) {
      $value .= '%';
    }

    echo '<td data-name="discount_value" data-type="varchar" data-value="' . $value . '">' . $value . '</td>';
  }

  /********************************* INSERT PAGE *********************************/


  public function displayInsertPageContent() {

    $this->displayLanguageTabs($this->languages);

    echo '<div class="row">';

      echo '<input type="hidden" id="field-resource" value="' . $this->resource . '" />';

      foreach ($this->items as $langId => $item) {

        $activeClass = (int)$langId === (int)Trans::getLanguageId() ? ' active' : '';

        $langSuffix = $this->getLanguageSuffix(array('langId' => $langId));

        echo '<div class="language-wrapper' . $activeClass . '" id="languageWrapper' . $langSuffix . '">';

          echo '<div class="col-md-9">';
          $this->renderForm($item);
          if (@exists($item->comments)) {
            $this->renderCommentsSection($item->comments);
          }
          echo '</div>';

          echo '<div class="col-md-3 side-panels">';
            $this->displayInsertPageActions(array('item' => $item, 'displayPublishCheckbox' => true, 'displayPreviewButton' => true, 'displayLanguageNotice' => true, 'displayAllowCommentsCheckbox' => true));
            $this->displayStatusPanel(array('item' => $item));
            $this->renderImagePanel(array('item' => $item, 'langId' => $langId, 'type' => Conf::get('modal_type')['intro_image']));
            $this->renderImagePanel(array('item' => $item, 'langId' => $langId, 'type' => Conf::get('modal_type')['image']));
            $this->displayInsertPageTree(array('langId' => $langId, 'rootId' => Conf::get('shop_category_id')));
          echo '</div>';

        echo '</div>';
      }

    echo '</div>';
  }


  public function renderForm($item) {

    $id = @exists($item->id) ? $item->id : 0;
    $categoryId = @exists($item->category_id) ? $item->category_id : 0;
    $published = @exists($item->published) ? $item->published : 0;
    $allowComments = @exists($item->allow_comments) ? $item->allow_comments : 0;
    $rang = @exists($item->rang) ? $item->rang : 9999;
    $content = @exists($item->content) ? $item->content : '';
    $introText = @exists($item->intro_text) ? $item->intro_text : '';
    $introImage = @exists($item->intro_image) ? $item->intro_image : '';
    $image = @exists($item->image) ? $item->image : '';
    $galleryJson = @exists($item->gallery_json) ? htmlentities($item->gallery_json) : '';
    $price = @exists($item->price) ? $item->price : 0;
    $discountValue = @exists($item->discount_value) ? $item->discount_value : 0;
    $title = @exists($item->title) ? htmlentities($item->title) : '';
    $subtitle = @exists($item->subtitle) ? htmlentities($item->subtitle) : '';
    $alias = @exists($item->alias) ? htmlentities($item->alias) : '';
    $metaTitle = @exists($item->meta_title) ? htmlentities($item->meta_title) : '';
    $metaDescription = @exists($item->meta_description) ? htmlentities($item->meta_description) : '';
    $metaKeywords = @exists($item->meta_keywords) ? htmlentities($item->meta_keywords) : '';
    $code = @exists($item->code) ? htmlentities($item->code) : '';
    $youtubeVideoLink = @exists($item->youtube_video_code) ? 'https://www.youtube.com/embed/' . $item->youtube_video_code : '';
    $langId = $item->lang_id;

    $langSuffix = $this->getLanguageSuffix(array('langId' => $langId));

    echo '<form id="insert-form' . $langSuffix . '">';

      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'id', 'value' => $id));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'category_id', 'value' => $categoryId, 'className' => 'parentId-field' . $langSuffix));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'published', 'value' => $published, 'className' => 'published'));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'allow_comments', 'value' => $allowComments, 'className' => 'allow-comments'));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'rang', 'value' => $rang, 'className' => 'rang'));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'intro_image', 'value' => $introImage, 'className' => 'intro-image-field' . $langSuffix));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'image', 'value' => $image, 'className' => 'image-field' . $langSuffix));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'lang_id', 'value' => $langId));
      $this->displayFormField(array('type' => 'hidden', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'lang_group_id', 'value' => $this->langGroupId));
      echo '<input type="hidden" name="gallery_json" id="' . $this->resource . '-gallery_json' . $langSuffix . '" value="' . $galleryJson . '" class="gallery-json-field' . $langSuffix . '" />';
      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'title', 'value' => $title, 'label' => Trans::get('Title')));
      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'subtitle', 'value' => $subtitle, 'label' => Trans::get('Subtitle')));
      $this->displayFormTextarea(array('name' => 'intro_text', 'resource' => $this->resource, 'langId' => $langId, 'value' => $introText, 'label' => Trans::get('Intro text'), 'rows' => 4, 'info' => Trans::get('Short description when products are listed, not on the products page itself')));
      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'alias', 'value' => $alias, 'label' => Trans::get('Alias'), 'disabled' => true));
      $this->displayFormTextEditor(array('name' => 'content', 'resource' => $this->resource, 'langId' => $langId, 'value' => $content, 'label' => Trans::get('Content')));
      echo '<div class="row">';
        echo '<div class="col-md-6">';
          $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'code', 'value' => $code, 'label' => Trans::get('Measure')));
        echo '</div>';
        echo '<div class="col-md-6">';
          $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'price', 'value' => $price, 'label' => Trans::get('Price')));
        echo '</div>';
      echo '</div>';
      echo '<div class="row">';
        echo '<div class="col-md-6">';
          $this->renderDiscountTypeSelectBox($item, $langSuffix);
        echo '</div>';
        echo '<div class="col-md-6">';
          $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'discount_value', 'value' => $discountValue, 'label' => Trans::get('Discount Value')));
        echo '</div>';
      echo '</div>';
      echo '<div class="row">';
        echo '<div class="col-md-6">';
          $this->renderVendorsSelectBox($item, $langSuffix);
        echo '</div>';
        echo '<div class="col-md-6">';
          $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'youtube_video_code', 'value' => $youtubeVideoLink, 'label' => Trans::get('Youtube video link'), 'info' => Trans::get('Copy Youtube video link and paste it in the field')));
        echo '</div>';
      echo '</div>';
      $this->displayFormField(array('type' => 'text', 'resource' => $this->resource, 'langId' => $langId, 'name' => 'meta_title', 'value' => $metaTitle, 'label' => Trans::get('Meta Title'), 'info' => Trans::get('For better browser ranking (not visible on page)')));
      echo '<div class="row">';
        echo '<div class="col-md-6">';
          $this->displayFormTextarea(array('name' => 'meta_description', 'resource' => $this->resource, 'langId' => $langId, 'value' => $metaDescription, 'label' => Trans::get('Meta Description'), 'rows' => 4, 'info' => Trans::get('For better browser ranking (not visible on page)')));
        echo '</div>';
        echo '<div class="col-md-6">';
          $this->displayFormTextarea(array('name' => 'meta_keywords', 'resource' => $this->resource, 'langId' => $langId, 'value' => $metaKeywords, 'label' => Trans::get('Meta Keywords'), 'rows' => 4, 'info' => Trans::get('For better browser ranking (not visible on page)')));
        echo '</div>';
      echo '</div>';

      $this->renderFormGallery($item, $langId);

    echo '</form>';
  }


  public function renderDiscountTypeSelectBox($item, $langSuffix) {

    $percentSelected = @exists($item->discount_type) && (int)$item->discount_type === (int)Conf::get('discount_type')['percent'] ? ' selected="selected"' : '';
    $absoluteSelected = @exists($item->discount_type) && (int)$item->discount_type === (int)Conf::get('discount_type')['absolute'] ? ' selected="selected"' : '';

    echo '<div class="form-group">';
      echo '<label>' . Trans::get('Discount Type') . '</label>';
      echo '<select class="form-control" id="' . $this->resource . '-discount_type' . $langSuffix . '" name="discount_type">';
        echo '<option value="">' . Trans::get('Choose') . '</option>';
        echo '<option value="' . Conf::get('discount_type')['percent'] . '"' . $percentSelected . '>' . Trans::get('percent') . '</option>';
        echo '<option value="' . Conf::get('discount_type')['absolute'] . '"' . $absoluteSelected . '>' . Trans::get('absolute') . '</option>';
      echo '</select>';
    echo '</div>';
  }


  public function renderVendorsSelectBox($item, $langSuffix) {

    if (@exists($this->vendors)) {

      echo '<div class="form-group">';
        echo '<label>' . Trans::get('Vendors') . '</label>';
        echo '<select class="form-control" id="' . $this->resource . '-vendor_id' . $langSuffix . '" name="vendor_id">';
          echo '<option value="">' . Trans::get('Choose') . '</option>';
          foreach ($this->vendors as $vendor) {

            if ((int)$vendor->lang_id === (int)$item->lang_id) {

              $selected = '';
              if (@exists($item->vendor_id)) {
                $selected = (int)$vendor->id === (int)$item->vendor_id ? ' selected="selected"' : '';
              }

              echo '<option value="' . $vendor->id . '"' . $selected . '>' . $vendor->name . '</option>';
            }
          }
        echo '</select>';
      echo '</div>';
    }
  }


  /********************************** SORT PAGE **********************************/


  public function displaySortPageHiddenFields() {

    echo '<input type="hidden" id="parentId" value="' . $this->parentId . '"/>';
  }
}

?>