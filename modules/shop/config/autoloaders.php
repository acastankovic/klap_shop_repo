<?php


Class ShopAutoloaders {

  public static function autoload_controllers($class_name) {

    $file = Conf::get('root') . '/modules/shop/controllers/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }

  public static function autoload_models($class_name) {
    $file = Conf::get('root') . '/modules/shop/models/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }

  public static function autoload_services($class_name) {

    $file = Conf::get('root') . '/modules/shop/services/class.' . $class_name . '.php';
    if (file_exists($file)) {
      require_once($file);
    }
  }
}

spl_autoload_register('ShopAutoloaders::autoload_controllers');
spl_autoload_register('ShopAutoloaders::autoload_models');
spl_autoload_register('ShopAutoloaders::autoload_services');
require_once(Conf::get('root') . '/modules/shop/app.php');

//register module
$item = new stdClass();
$item->title = 'Shop';
$item->alias = 'shop';
$item->root = false;
$modules = Conf::get('modules');
array_push($modules, $item);
Conf::set('modules', $modules);