<?php

class VendorsServices extends Service {

  private $model;

  public function __construct() {

    $model = Vendors::Instance();
    if ($model instanceof Vendors) {
      $this->model = $model;
    }
  }


  /************************************ LOAD ************************************/


  public function loadOne($data) {

    if (Languages::enabled() && !@exists($data['fetchWithUnpublished'])) {

      $data = $this->setLangGroupIdFromId($data);

      $result = $this->model->getByLanguageGroupIdAndLanguageId($data);

    } else {

      $result = $this->model->getOne($data);
    }

    $this->setItemProperties($result);

    return $result;
  }


  public function loadAll($data = null) {

    if (Languages::enabled() && !@exists($data['fetchWithUnpublished'])) {

      $results = $this->model->getByLanguageId($data);

    } else {

      $results = $this->model->getAll($data);
    }

    foreach ($results as $result) {

      $this->setItemProperties($result);
    }

    return $results;
  }


  public function loadByAlias($data, $adminLogged) {

    $result = $this->model->getByAlias($data, $adminLogged);

    $this->setItemProperties($result);

    return $result;
  }


  /*** LOAD BY LANGUAGES ***/


  public function loadByLanguageId($data) {

    return $this->model->getByLanguageId($data);
  }


  public function loadByLanguageGroupId($data) {

    return $this->model->getByLanguageGroupId($data);
  }


  public function loadByLanguageGroupIdAndLanguageId($data) {

    $result = $this->model->getByLanguageGroupIdAndLanguageId($data);

    $this->setItemProperties($result);

    return $result;
  }


  public function loadOneWithLanguageGroups($data) {

    $results = null;
    $langGroupId = null;

    if (@exists($data['id']) && (int)$data['id'] !== 0) {

      $item = $this->model->getOne($data);

      if (@exists($item) && $item) {

        $langGroupId = $this->setLanguageGroupId($item);

        $langGroupIdParams = $this->setLanguageGroupIdParams($langGroupId, $data);

        $results = $this->model->getByLanguageGroupId($langGroupIdParams);

        foreach ($results as $result) {

          $this->setItemProperties($result);
        }
      }
    }

    return $this->setItemWithLanguageGroupsResponse($results, $langGroupId);
  }


  /************************************ ACTIONS ************************************/


  public function insert($data) {

    if ((string)$data['lang_group_id'] === '' || (int)$data['lang_group_id'] === 0) {
      unset($data['lang_group_id']);
    }

    $alias = filterUrl($data['name']);

    $aliases = $this->model->getAliases();

    $data['alias'] = $this->setAlias($alias, $aliases);

    $data['created_by'] = $this->getLoggedInUserId();

    $this->model->insert($data);

    return $this->model->lastInsertId();
  }


  public function update($data) {

    if ((string)$data['lang_group_id'] === '' || (int)$data['lang_group_id'] === 0) {
      unset($data['lang_group_id']);
    }

    $alias = filterUrl($data['name']);
    $id = $data['id'];

    $aliases = $this->model->getAliases();

    $data['alias'] = $this->setAlias($alias, $aliases, $id);

    $data['updated_by'] = $this->getLoggedInUserId();

    return $this->model->update($data);
  }


  public function publish($data) {

    return $this->model->update($data);
  }


  /************************************ OTHER ************************************/


  private function setItemProperties($item) {

    if (@exists($item) && $item != false) {

      if (!@exists($item->published)) $item->published = 1;
    }

    return $item;
  }
}

?>