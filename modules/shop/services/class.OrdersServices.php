<?php

class OrdersServices extends Service {

  private $productsServices;
  private $ordersModel;
  private $orderSession;
  public $items;

  public function __construct() {

    $productsServices = ProductsServices::Instance();
    if ($productsServices instanceof ProductsServices) {
      $this->productsServices = $productsServices;
    }

    $ordersModel = Orders::Instance();
    if ($ordersModel instanceof Orders) {
      $this->ordersModel = $ordersModel;
    }

    $this->getOrderSession();
    $this->items = $this->loadItems();
  }


  /************************************ LOAD ************************************/

  public function loadOne($data) {

    return $this->ordersModel->getOne($data);
  }


  public function loadAll($data = null) {

    return $this->ordersModel->getAll($data);
  }


  public function loadOrder() {
    return $this->ordersModel->getBySession($this->orderSession);
  }


  public function loadItems() {

    $order = $this->loadOrder();

    if (!$order) return false;
    else {

      $items = json_decode($order->items);
      if (!@exists($items)) $items = array();

      return $items;
    }
  }


  /************************************ ACTIONS ************************************/


  public function addItem($data) {

    $productId = $data['product_id'];
    $quantity = 1;
    $quantityAddedManually = false;

    if (@exists($data['quantity']) && is_numeric($data['quantity'])) {
      $quantityAddedManually = true;
      $quantity = ceil($data['quantity']);
    }

    $product = $this->productsServices->loadOne(array('id' => $productId));

    if ($this->orderExists()) {

      if ($this->itemInCart($product->id)) {

        if (!$quantityAddedManually) {
          $this->increaseItemQty($product->id);
        } else {
          $this->setItemAbsoluteQty($product->id, $quantity);
        }

        return $this->setOrderResponse($product->id);
      } else {

        $item = $this->setOrderItem($product);

        array_push($this->items, $item);

        $this->updateOrder($this->items);

        return $this->setOrderResponse($product->id);
      }
    } else {

      $item = $this->setOrderItem($product);

      $newItems = array();
      array_push($newItems, $item);

      $newOrder = new stdClass();
      $newOrder->session = $this->setOrderSession();
      $newOrder->items = json_encode($newItems);
      $newOrder->quantity = $quantity;
      $newOrder->order_discount = PricesCalculationService::getDiscountPrice($product);
      $newOrder->order_vat = PricesCalculationService::getVatPrice($product);
      $newOrder->order_price = PricesCalculationService::getProductPrice($product);

      $this->ordersModel->insert($newOrder);

      return $this->setOrderResponse($product->id, $newOrder);
    }
  }


  // increase item quantity by one ( +1 )
  public function increaseItemQty($productId) {

    foreach ($this->items as $item) {

      if ((int)$item->id === (int)$productId) {

        $item->quantity = (int)$item->quantity + 1;
        $item->quantity_total_price = (int)$item->total_price * (int)$item->quantity;
      }
    }

    $this->updateOrder($this->items);

    return $this->setOrderResponse($productId);
  }


  // decrease item quantity by one ( -1 )
  public function decreaseItemQty($productId) {

    foreach ($this->items as $item) {

      if ((int)$item->id === (int)$productId) {

        if ((int)$item->quantity > 0) {

          $item->quantity = (int)$item->quantity - 1;
          $item->quantity_total_price = (int)$item->total_price * (int)$item->quantity;
        }
      }
    }

    $this->updateOrder($this->items);

    return $this->setOrderResponse($productId);
  }


  public function setItemAbsoluteQty($productId, $absoluteValue = null) {

    $value = (int)$absoluteValue;

    foreach ($this->items as $item) {

      if ((int)$item->id === (int)$productId) {

        $item->quantity =  $value <= 0 ? 1 : $value;
        $item->quantity_total_price = $item->total_price * (int)$item->quantity;
      }
    }

    $this->updateOrder($this->items);

    return $this->setOrderResponse($productId);
  }


  // remove product from cart
  public function removeItem($productId) {

    $itemsCount = count($this->items);

    for ($i = 0; $i < $itemsCount; $i++) {

      if ((int)$this->items[$i]->id === (int)$productId) {

        unset($this->items[$i]);
      }
    }

    $this->items = array_values($this->items);

    $this->updateOrder($this->items);

    return $this->setOrderResponse($productId);
  }


  // remove all products from cart
  public function emptyOrder() {

    $this->items = array();

    $this->updateOrder($this->items);

    return $this->setOrderResponse();
  }


  public function updateOrder($items) {

    $itemsQuantity = 0;
    $orderPrice = 0;
    $orderDiscount = 0;
    $orderVat = 0;

    foreach ($items as $item) {

      $itemsQuantity += $item->quantity;
      $orderPrice += (int)$item->quantity * $item->total_price;

      $orderDiscount += $item->discount_price != 0 ? (int)$item->quantity * $item->discount_price : 0;
      $orderVat += $item->vat_price != 0 ? (int)$item->quantity * $item->vat_price : 0;
    }

    $data = new stdClass();
    $data->items = json_encode($items);
    $data->quantity = $itemsQuantity;
    $data->discount = $orderDiscount;
    $data->vat = $orderVat;
    $data->price = $orderPrice;
    $data->session = $this->orderSession;

    $this->ordersModel->updateBySession($data);
  }


  public function insertCustomerInfo($data) {
    $this->ordersModel->insertCustomerInfo($this->orderSession, $data);
  }


  /********************************* ADMIN ACTIONS *********************************/

  public function deleteOrder($id) {
    return $this->ordersModel->delete($id);
  }


  public function changeOrder($data) {

    if (!@exists($data['paid'])) {
      $data['paid'] = 0;
    }

    if (!@exists($data['status'])) {
      $data['status'] = 0;
    }

    return $this->ordersModel->update($data);
  }

  /************************************* OTHER *************************************/


  public function setOrderResponse($productId = null, $newOrder = null) {

    $order = $this->loadOrder();

    $response = new stdClass();
    $response->product_id = @exists($productId) ? $productId : 0;

    if (@exists($order) && $order != false) {

      $response->items = json_decode($order->items);
      $response->quantity = $order->quantity;
      $response->order_discount = $order->order_discount;
      $response->order_vat = $order->order_vat;
      $response->order_price = $order->order_price;

    } else {

      $response->items = json_decode($newOrder->items);
      $response->quantity = $newOrder->quantity;
      $response->order_discount = $newOrder->order_discount;
      $response->order_vat = $newOrder->order_vat;
      $response->order_price = $newOrder->order_price;
    }

    return $response;
  }


  public function setOrderItem($product) {

    $item = new stdClass();

    $item->id = trim($product->id);
    $item->code = trim($product->code);
    $item->title = trim($product->title);
    // $item->subtitle               = trim($product->subtitle);
    // $item->content                = trim($product->content);
    $item->category_id = trim($product->category_id);
    $item->image = trim($product->image);
    $item->url = trim($product->url);
    $item->quantity = 1;

    // vat
    $item->vat_value = trim($product->vat);
    $item->vat_price = trim(PricesCalculationService::getVatPrice($product));

    // discount
    $item->discount_type = trim($product->discount_type);
    $item->discount_value = trim($product->discount_value);
    $item->discount_price = trim(PricesCalculationService::getDiscountPrice($product));

    // prices
    $item->original_price = trim($product->price);
    $item->price_without_discount = trim(PricesCalculationService::getPriceWithoutDiscount($product));
    $item->total_price = trim(PricesCalculationService::getProductPrice($product));
    $item->quantity_total_price = trim(PricesCalculationService::getProductPrice($product));

    return $item;
  }


  public function itemInCart($productId) {

    $inCart = false;

    if (!empty($this->items)) {

      foreach ($this->items as $item) {

        if ((int)$item->id === (int)$productId) $inCart = true;
      }
    }

    return $inCart;
  }


  public function getOrderSession() {
    $this->orderSession = @exists($_SESSION[Conf::get('session_alias')['shop_token']]) ? $_SESSION[Conf::get('session_alias')['shop_token']] : false;
  }


  public function setOrderSession() {

    $_SESSION[Conf::get('session_alias')['shop_token']] = Encryption::generateStamp();
    return $_SESSION[Conf::get('session_alias')['shop_token']];
  }


  public function unsetOrderSession() {
    unset($_SESSION[Conf::get('session_alias')['shop_token']]);
    return true;
  }


  public function orderExists() {

    if (!@exists($this->orderSession) || $this->orderSession == false) return false;
    $order = $this->loadOrder();
    return @exists($order);
  }

  public function changeOrderStatus($status, $id) {
    $this->ordersModel->updateOrderStatus($status, $id);
  }
}

?>