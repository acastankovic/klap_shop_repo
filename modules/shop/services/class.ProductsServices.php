<?php

class ProductsServices extends Service {

  private $model;

  public function __construct() {

    $model = Products::Instance();
    if ($model instanceof Products) {
      $this->model = $model;
    }
  }


  /************************************ LOAD ************************************/


  public function loadOne($data) {

    if (Languages::enabled() && !@exists($data['fetchWithUnpublished'])) {

      $data = $this->setLangGroupIdFromId($data);

      $result = $this->model->getByLanguageGroupIdAndLanguageId($data);

    } else {

      $result = $this->model->getOne($data);
    }

    $this->setItemProperties($result);

    return $result;
  }


  public function loadAll($data = null) {

    if (Languages::enabled() && !@exists($data['fetchWithUnpublished'])) {

      $results = $this->model->getByLanguageId($data);

    } else {

      $results = $this->model->getAll($data);
    }

    foreach ($results as $result) {

      $this->setItemProperties($result);
    }

    return $results;
  }


  public function loadByAlias($data) {

    $adminLogged = $this->adminLoggedIn();

    $result = $this->model->getByAlias($data, $adminLogged);

    $this->setItemProperties($result);

    return $result;
  }


  public function loadByParentId($data) {

    if (Languages::enabled() && !@exists($data['fetchWithUnpublished'])) {

      $data = $this->setLangGroupIdFromParentId($data);

      $results = $this->model->getByParentIdAndLanguageGroupIdAndLanguageId($data);

    } else {

      $results = $this->model->getByParentId($data);
    }

    foreach ($results as $result) {

      $this->setItemProperties($result);
    }

    return $results;
  }


  public function loadByCategoryParentId($data) {

    if (Languages::enabled() && !@exists($data['fetchWithUnpublished'])) {

      $data = $this->setLangGroupIdFromParentId($data);

      $results = $this->model->getByParentsParentIdAndLanguageGroupIdAndLanguageId($data);

    } else {

      $results = $this->model->getByCategoryParentId($data);
    }

    foreach ($results as $result) {

      $this->setItemProperties($result);
    }

    return $results;
  }


  public function loadForAdminTables() {

    return $this->model->getForAdminTables();
  }


  public function loadWithFilters($data) {

    $results = new stdClass();
    $results->total = $this->model->getTotal($data);
    $results->items = $this->model->getWithFilters($data);

    foreach ($results->items as $item) {

      $this->setItemProperties($item);
    }

    return $results;
  }


  /*** LOAD BY LANGUAGES ***/


  public function loadByLanguageId($data) {

    return $this->model->getByLanguageId($data);
  }


  public function loadByLanguageGroupId($data) {

    return $this->model->getByLanguageGroupId($data);
  }


  public function loadByLanguageGroupIdAndLanguageId($data) {

    $result = $this->model->getByLanguageGroupIdAndLanguageId($data);

    $this->setItemProperties($result);

    return $result;
  }


  public function loadByParentIdAndLanguageGroupIdAndLanguageId($data) {

    $results = $this->model->getByParentIdAndLanguageGroupIdAndLanguageId($data);

    foreach ($results as $result) {

      $this->setItemProperties($result);
    }

    return $results;
  }


  public function loadByParentsParentIdAndLanguageGroupIdAndLanguageId($data) {

    $results = $this->model->getByParentsParentIdAndLanguageGroupIdAndLanguageId($data);

    foreach ($results as $result) {

      $this->setItemProperties($result);
    }

    return $results;
  }


  public function loadOneWithLanguageGroups($data) {

    $results = null;
    $langGroupId = null;

    if (@exists($data['id']) && (int)$data['id'] !== 0) {

      $item = $this->model->getOne($data);

      if (@exists($item) && $item) {

        $langGroupId = $this->setLanguageGroupId($item);

        $langGroupIdParams = $this->setLanguageGroupIdParams($langGroupId, $data);

        $results = $this->model->getByLanguageGroupId($langGroupIdParams);

        foreach ($results as $result) {

          $this->setItemProperties($result);
          $this->setItemComments($result, Conf::get('comment_type_id')['product']);
        }
      }
    }

    return $this->setItemWithLanguageGroupsResponse($results, $langGroupId);
  }


  /************************************ ACTIONS ************************************/


  public function insert($data) {

    if ((string)$data['lang_group_id'] === '' || (int)$data['lang_group_id'] === 0) {
      unset($data['lang_group_id']);
    }

    $data['created_by'] = $this->getLoggedInUserId();

    $alias = filterUrl($data['title']);
    $parent_id = $data['category_id'];

    $aliases = $this->model->getAliasesByParentId(array('parent_id' => $parent_id));

    $data['alias'] = $this->setAlias($alias, $aliases);

    $this->model->insert($data);

    return $this->model->lastInsertId();
  }


  public function update($data) {

    if ((string)$data['lang_group_id'] === '' || (int)$data['lang_group_id'] === 0) {
      unset($data['lang_group_id']);
    }

    $data['updated_by'] = $this->getLoggedInUserId();

    $alias = filterUrl($data['title']);
    $parent_id = $data['category_id'];
    $id = $data['id'];

    $aliases = $this->model->getAliasesByParentId(array('parent_id' => $parent_id));

    $data['alias'] = $this->setAlias($alias, $aliases, $id);

    return $this->model->update($data);
  }


  public function publish($data) {

    return $this->model->update($data);
  }


  /************************************ OTHER ************************************/


  private function buildUrl($item) {

    $categories = Categories::getCategories();

    $url = '';
    if (@exists($item->alias)) $url = $item->alias;

    if (@exists($item->category_id)) {

      $categoryId = $item->category_id;
      foreach ($categories as $cat) {
        if ($cat->id == $categoryId) $category = $cat;
      }

      if (@exists($category)) {

        if ($category->alias) $url = $category->alias . '/' . $url;

        $deadLoopCounter = 100;

        while ((int)$category->id !== (int)Conf::get('shop_category_id') && $deadLoopCounter > 0) {

          $deadLoopCounter--;

          foreach ($categories as $cat) {

            if ((int)$cat->id === (int)$category->parent_id) {

              $url = $cat->alias . '/' . $url;
              $category = $cat;
            }
          }
        }

        if ($deadLoopCounter == 0) Logger::put('Error, no parent category for ' . $category->id);
      }

    }

    return Conf::get('url') . '/' . $url;
  }


  private function setItemProperties($item) {

    if (@exists($item) && $item != false) {

      if (!@exists($item->published)) $item->published = 1;

      $item->url = $this->buildUrl($item);

      $item->gallery = $this->setGallery($item);
    }

    return $item;
  }


  public function findFirstLevelParent($categories, $categoryId) {

    foreach ($categories as $cat) {
      if ($cat->id == $categoryId) $category = $cat;
    }

    if (@exists($category)) {

      while ($category->id != Conf::get('shop_category_id')) {

        foreach ($categories as $cat) {

          if ($cat->id == $category->parent_id) {

            $result = $category;

            $category = $cat;
          }
        }
      }
    }

    return $result;
  }
}

?>