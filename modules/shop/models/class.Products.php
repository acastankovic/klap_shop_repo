<?php


class Products extends Model {

  private $selectQueryString;

  public function __construct() {
    parent::__construct();
    $this->setTable('products');
    $this->setQueryString();
  }


  /************************************ FETCH ************************************/


  public function getOne($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `p`.`id` = :id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `p`.`published` = 1';
    }

    return $this->exafe($sql, array('id' => $data['id']));
  }


  public function getAll($data = null) {

    $sql = $this->selectQueryString;

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' WHERE `p`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql);
  }


  public function getByAlias($data, $adminLogged) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `p`.`category_id` = :category_id and `p`.`alias` = :alias';
    if (!$adminLogged) $sql .= ' AND `p`.`published` = 1';

    return $this->exafe($sql, array('category_id' => $data['parent_id'], 'alias' => $data['alias']));
  }


  public function getByParentId($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `p`.`category_id` = :category_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `p`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('category_id' => $data['parent_id']));
  }


  public function getAliasesByParentId($data) {

    $sql = 'SELECT `id`, `alias` FROM `products` WHERE `category_id` = :category_id;';
    return $this->exafeAll($sql, array('category_id' => $data['parent_id']));
  }


  public function getByLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `p`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `p`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_id' => $langId));
  }


  public function getByLanguageGroupId($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `p`.`id` = :lang_group_id || `p`.`lang_group_id` = :lang_group_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `p`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_group_id' => $data['lang_group_id']));
  }


  public function getByLanguageGroupIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`p`.`id` = :lang_group_id || `p`.`lang_group_id` = :lang_group_id) AND `p`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `p`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafe($sql, array('lang_group_id' => $data['lang_group_id'], 'lang_id' => $langId));
  }


  public function getByParentIdAndLanguageGroupIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`c1`.`id` = :lang_group_id || `c1`.`lang_group_id` = :lang_group_id) AND `p`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `p`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_group_id' => $data['lang_group_id'], 'lang_id' => $langId));
  }


  public function getByParentsParentIdAndLanguageGroupIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`c2`.`id` = :lang_group_id || `c2`.`lang_group_id` = :lang_group_id) AND `c2`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `p`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_group_id' => $data['lang_group_id'], 'lang_id' => $langId));
  }


  public function getForAdminTables() {

    $sql = 'SELECT `p`.`id`, `p`.`title`, `p`.`published`, `p`.`code`, `p`.`price`, `p`.`discount_type`, `p`.`discount_value`, `p`.`vendor_id`, `c`.`name` AS `parent_name`, `v`.`name` AS `vendor_name`, `l`.`name` AS `language_name` FROM `products` AS `p` LEFT JOIN `categories` AS `c` ON `c`.`id` = `p`.`category_id` LEFT JOIN `languages` AS `l` ON `l`.`id` = `p`.`lang_id` LEFT JOIN `vendors` AS `v` ON `v`.`id` = `p`.`vendor_id`';
    return $this->exafeAll($sql);
  }


  public function getTotal($data) {

    $whereString = ' WHERE `p`.`lang_id` = :lang_id';

    if (@exists($data)) {

      if (@exists($data['search'])) {
        $whereString .= ' AND (`p`.`title` like :search or `p`.`subtitle` like :search or `p`.`content` like :search or `p`.`code` like :search)';
      }

      if (@exists($data['parent_id'])) {
        $whereString .= ' AND `p`.`category_id` = :category_id';
      }

      if (@exists($data['vendor_id'])) {
        $whereString .= ' AND `p`.`vendor_id` = :vendor_id';
      }
    }

    $sql = 'SELECT COUNT(`p`.`id`) AS `total`
                FROM `products` AS `p` 
                LEFT JOIN `categories` AS `c1` ON `c1`.`id` = `p`.`category_id`  
                LEFT JOIN `categories` AS `c2` ON `c1`.`parent_id` = `c2`.`id` 
                LEFT JOIN `vendors` AS `v` ON `v`.`id` = `p`.`vendor_id`
                LEFT JOIN `languages` AS `l` ON `l`.`id` = `p`.`lang_id`';
    $sql .= $whereString;

    $stm = $this->dbh->prepare($sql);

    $stm->bindValue(':lang_id', (int)Trans::getLanguageId(), PDO::PARAM_INT);

    if (@exists($data)) {

      if (@exists($data['search'])) {
        $stm->bindValue(':search', (string)'%' . $data['search'] . '%', PDO::PARAM_STR);
      }

      if (@exists($data['parent_id'])) {
        $stm->bindValue(':category_id', (int)$data['parent_id'], PDO::PARAM_INT);
      }

      if (@exists($data['vendor_id'])) {
        $stm->bindValue(':vendor_id', (int)$data['vendor_id'], PDO::PARAM_INT);
      }
    }

    $stm->execute();

    $result = $stm->fetch(PDO::FETCH_OBJ);

    return $result->total;
  }


  public function getWithFilters($data) {

    $limit = @exists($data['items_per_page']) ? $data['items_per_page'] : Conf::get('items_per_page')['site_products'];
    $page = @exists($data['page']) ? $data['page'] : 1;
    $offset = @exists($page) ? ($page - 1) * $limit : 0;

    $whereString = ' WHERE `p`.`lang_id` = :lang_id';

    if (@exists($data)) {

      if (@exists($data['search'])) {
        $whereString .= ' AND (`p`.`title` like :search OR `p`.`subtitle` like :search OR `p`.`content` like :search OR `p`.`code` like :search)';
      }

      if (@exists($data['parent_id'])) {
        $whereString .= ' AND `p`.`category_id` = :category_id';
      }

      if (@exists($data['vendor_id'])) {
        $whereString .= ' AND `p`.`vendor_id` = :vendor_id';
      }
    }

    $sql = $this->selectQueryString;
    $sql .= $whereString;

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `p`.`published` = 1';
    }

    $sql .= ' LIMIT :offset, :limit';

    $stm = $this->dbh->prepare($sql);

    $stm->bindValue(':lang_id', (int)Trans::getLanguageId(), PDO::PARAM_INT);

    if (@exists($data)) {


      if (@exists($data['search'])) {
        $stm->bindValue(':search', (string)'%' . $data['search'] . '%', PDO::PARAM_STR);
      }

      if (@exists($data['parent_id'])) {
        $stm->bindValue(':category_id', (int)$data['parent_id'], PDO::PARAM_INT);
      }

      if (@exists($data['vendor_id'])) {
        $stm->bindValue(':vendor_id', (int)$data['vendor_id'], PDO::PARAM_INT);
      }
    }

    $stm->bindValue(':limit', (int)$limit, PDO::PARAM_INT);
    $stm->bindValue(':offset', (int)$offset, PDO::PARAM_INT);

    $stm->execute();

    return $stm->fetchAll(PDO::FETCH_OBJ);
  }


  /************************************ OTHER ************************************/


  private function setQueryString() {

    $this->selectQueryString = 'SELECT `p`.*, 
                                `c1`.`id` AS `parent_id`, `c1`.`name` AS `parent_name`, `c1`.`lang_group_id` AS `parent_lang_group_id`, `c1`.`lang_id` AS `parent_lang_id`,
                                `c2`.`id` AS `categories_parent_id`, `c2`.`name` AS `categories_parent_name`, `c2`.`parent_id` AS `categories_parent_parent_id`, `c2`.`lang_group_id` AS `categories_parent_lang_group_id`, `c2`.`lang_id` AS `categories_parent_lang_id`,
                                `v`.`name` AS `vendor_name`, `v`.`alias` AS `vendor_alias`, `v`.`code` AS `vendor_code`, `v`.`content` AS `vendor_content`, `v`.`link` AS `vendor_link`, `v`.`image` AS `vendor_image`, 
                                `l`.`name` AS `language_name`, `l`.`alias` AS `language_alias`,
                                `uc`.`username` AS `created_by_username`,
                                `uu`.`username` AS `update_by_username`
                                FROM `products` AS `p` 
                                LEFT JOIN `categories` AS `c1` ON `c1`.`id` = `p`.`category_id`  
                                LEFT JOIN `categories` AS `c2` ON `c1`.`parent_id` = `c2`.`id` 
                                LEFT JOIN `vendors` AS `v` ON `v`.`id` = `p`.`vendor_id`
                                LEFT JOIN `languages` AS `l` ON `l`.`id` = `p`.`lang_id`
                                LEFT JOIN `users` AS `uc` ON `uc`.`id` = `p`.`created_by`
                                LEFT JOIN `users` AS `uu` ON `uu`.`id` = `p`.`updated_by`';
  }


  private function getOrderByString($data) {

    if (@exists($data['order_by'])) {
      return $this->setOrderByString($data['order_by'], 'p');
    }
    return ' ORDER BY `p`.`rang`, `p`.`id` DESC';
  }

}

?>