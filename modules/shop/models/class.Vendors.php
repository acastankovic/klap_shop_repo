<?php


class Vendors extends Model {

  private $selectQueryString;

  public function __construct()  {
    parent::__construct();
    $this->setTable('vendors');
    $this->setQueryString();
  }


  /************************************ FETCH ************************************/


  public function getOne($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `v`.`id` = :id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `v`.`published` = 1';
    }

    return $this->exafe($sql, array('id' => $data['id']));
  }


  public function getAll($data = null) {

    $sql = $this->selectQueryString;

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' WHERE `v`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql);
  }


  public function getByAlias($data, $adminLogged) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `v`.`alias` = :alias';
    if (!$adminLogged) $sql .= ' AND `v`.`published` = 1';

    return $this->exafe($sql, array("alias" => $data["alias"]));
  }


  public function getAliases() {

    $sql = 'SELECT `id`, `alias` FROM `vendors`';
    return $this->exafeAll($sql);
  }


  public function getByLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `v`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `v`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_id' => $langId));
  }


  public function getByLanguageGroupId($data) {

    $sql = $this->selectQueryString;
    $sql .= ' WHERE `v`.`id` = :lang_group_id || `v`.`lang_group_id` = :lang_group_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `v`.`published` = 1';
    }

    $sql .= $this->getOrderByString($data);

    $sql .= $this->getLimitString($data);

    return $this->exafeAll($sql, array('lang_group_id' => $data['lang_group_id']));
  }


  public function getByLanguageGroupIdAndLanguageId($data) {

    $langId = $this->setLangId($data);

    $sql = $this->selectQueryString;
    $sql .= ' WHERE (`v`.`id` = :lang_group_id || `v`.`lang_group_id` = :lang_group_id) and `v`.`lang_id` = :lang_id';

    if (!$this->fetchWithUnpublished($data)) {
      $sql .= ' AND `v`.`published` = 1';
    }

    return $this->exafe($sql, array('lang_group_id' => $data['lang_group_id'], 'lang_id' => $langId));
  }


  /************************************ OTHER ************************************/


  private function setQueryString() {

    $this->selectQueryString = 'SELECT `v`.*,
                                `l`.`name` AS `language_name`, `l`.`alias` AS `language_alias`,
                                `uc`.`username` AS `created_by_username`,
                                `uu`.`username` AS `update_by_username`
                                FROM `vendors` AS `v`
                                LEFT JOIN `languages` AS `l` ON `l`.`id` = `v`.`lang_id`
                                LEFT JOIN `users` AS `uc` ON `uc`.`id` = `v`.`created_by`
                                LEFT JOIN `users` AS `uu` ON `uu`.`id` = `v`.`updated_by` ';
  }


  private function getOrderByString($data) {

    if (@exists($data['order_by'])) {
      return $this->setOrderByString($data['order_by'], 'v');
    }
    return ' ORDER BY `v`.`rang`, `v`.`id` DESC';
  }
}

?>