<?php


class ShopApplication extends Application {

  protected $name = 'shop';

  protected $resources = array('products', 'vendors');

  protected $routes = array(
        
    /************ PRODUCTS ************/

    //insert product
    array('route'      => 'products/insert',
          'method'     => 'post',
          'controller' => 'products',
          'action'     => 'insertProduct'),

    //update product
    array('route'      => 'products/update',
          'method'     => 'put',
          'controller' => 'products',
          'action'     => 'updateProduct'),


    /************* ORDERS *************/

    array('route'      => 'orders/get',
          'method'     => 'get',
          'controller' => 'orders',
          'action'     => 'fetchItems'),

    array('route'      => 'orders/add',
          'method'     => 'post',
          'controller' => 'orders',
          'action'     => 'addItem'),

    array('route'      => 'orders/increase-qty',
          'method'     => 'post',
          'controller' => 'orders',
          'action'     => 'increaseItemQuantity'),

    array('route'      => 'orders/decrease-qty',
          'method'     => 'post',
          'controller' => 'orders',
          'action'     => 'decreaseItemQuantity'),

    array('route'      => 'orders/change-qty',
          'method'     => 'post',
          'controller' => 'orders',
          'action'     => 'changeItemQuantity'),

    array('route'      => 'orders/remove',
          'method'     => 'post',
          'controller' => 'orders',
          'action'     => 'removeItem'),

    array('route'      => 'orders/empty',
          'method'     => 'post',
          'controller' => 'orders',
          'action'     => 'emptyOrder'),

    array('route'      => 'orders/place-order',
          'method'     => 'post',
          'controller' => 'orders',
          'action'     => 'placeOrder'),

    array('route'      => 'orders/delete/:id',
          'method'     => 'delete',
          'controller' => 'orders',
          'action'     => 'deleteOrder'),

    array('route'      => 'orders/change',
          'method'     => 'put',
          'controller' => 'orders',
          'action'     => 'changeOrder'),


    /************ VENDORS ************/

    // insert product
    array('route'      => 'vendors/insert',
          'method'     => 'post',
          'controller' => 'vendors',
          'action'     => 'insertVendor'),

    // update product
    array('route'      => 'vendors/update',
          'method'     => 'put',
          'controller' => 'vendors',
          'action'     => 'updateVendor'),

    // publish product
    array('route'      => 'vendors/publish',
          'method'     => 'put',
          'controller' => 'vendors',
          'action'     => 'publishVendor')
  );
}

?>