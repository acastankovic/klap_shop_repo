<?php


class OrdersController extends Controller {

  private $service;

  public function __construct() {
    parent::__construct();

    $service = OrdersServices::Instance();
    if ($service instanceof OrdersServices) {
      $this->service = $service;
    }

    sessionStart();
    Trans::initTranslations();
  }


  /************************************ FETCH ************************************/


  /*
   * @param   int  id
   *
   * @return  object
  */
  public function fetchOne() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->loadOne($params);

    $this->view->respond($data);
    return $data;
  }


  /*
   *
   * @return  array of objects
  */
  public function fetchAll() {

    $data = $this->service->loadAll();

    $this->view->respond($data);
    return $data;
  }


  public function fetchOrder() {

    $data = $this->service->loadOrder();

    $this->view->respond($data);
    return $data;
  }


  public function fetchItems() {

    $data = $this->service->loadItems();

    $this->view->respond($data);
    return $data;
  }


  /************************************ ACTIONS ************************************/


  // add product to cart
  public function addItem() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->addItem($params);

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  // increase product quantity by 1
  public function increaseItemQuantity() {

    $product_id = trim(Security::Instance()->purifier()->purify($this->params('product_id')));

    $data = $this->service->increaseItemQty($product_id);

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  // decrease product quantity by 1
  public function decreaseItemQuantity() {

    $product_id = trim(Security::Instance()->purifier()->purify($this->params('product_id')));

    $data = $this->service->decreaseItemQty($product_id);

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  // absolute amount entered in the field
  public function changeItemQuantity() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $data = $this->service->setItemAbsoluteQty($params['product_id'], $params['quantity']);

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  // remove one product from cart
  public function removeItem() {

    $product_id = trim(Security::Instance()->purifier()->purify($this->params('product_id')));

    $data = $this->service->removeItem($product_id);

    $this->view->respond($data, null, Request::JSON_REQUEST);
    return $data;
  }


  // remove all products from cart
  public function emptyOrder() {

    $data = $this->service->emptyOrder();

    $this->view->respond($data);
    return $data;
  }


  public function placeOrder() {

    $this->view = new MailsView();
    $this->view->initController($this);

    $params = $this->params();

    $order = $this->service->loadOrder();

    $fileHtml = $this->view->renderOrderTable($order);
    $fileName = 'Narudžbina broj: ' . $order->id;

    $domtopdf = new Domtopdf();
    $attachment = $domtopdf->createPdfAttachment($fileHtml, $fileName);

    $email = $this->view->emailOrderTemplate($params, $order, $attachment);

    if (@exists($params)) {
      $this->service->insertCustomerInfo($params);
    }

    $mailer = new Mailer();
    if ($mailer->sendMail($email)) {

      $this->service->changeOrderStatus(OrderStatus::PENDING, $order->id);

      $success = true;
      $message = 'Porudžbina je poslata!';
      $this->service->unsetOrderSession();

    } else {

      $success = false;
      $message = 'Porudžbina nije poslata, pokušajte ponovo!';
    }

    $response = new stdClass();
    $response->success = $success;
    $response->message = $message;

    $this->view->respond($response);
    return $response;
  }


  /********************************* ADMIN ACTIONS *********************************/

  public function deleteOrder() {

    $id = trim(Security::Instance()->purifier()->purify($this->params('id')));

    $this->service->deleteOrder($id);

    $this->view->respond(null);
    return null;
  }


  public function changeOrder() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $this->service->changeOrder($params);

    $data = new stdClass();
    $data->success = true;
    $data->message = Trans::get('Order updated');

    $this->view->respond($data);
    return $data;
  }

}

?>