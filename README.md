// TODO: more detailed explanation should be written


## 1. git clone
## 2. remove .git
## 3. composer install (if some packages defined in composer are missing in OS, they should be installed first)
## 4. on Linux permissions should be defined for:
    
* /log
* /log/log.txt
* /log/log_trace.txt
    * sudo find /path-to-project/log -type d -exec chmod 777 {} \;  
    * sudo find /path-to-project/log -type f -exec chmod 777 {} \; 
* /media
* /media/thumbs
    * sudo find /path-to-project/media -type d -exec chmod 777 {} \;  
    * sudo find /path-to-project/media/thumbs -type d -exec chmod 777 {} \; 
* /vendor/ezyang/htmlpurifier/library/HTMLPurifier/DefinitionCache/Serializer  
    * sudo find /path-to-project/vendor/ezyang/htmlpurifier/library/HTMLPurifier/DefinitionCache/Serializer -type d -exec chmod 777 {} \;
    * sudo find /path-to-project/vendor/ezyang/htmlpurifier/library/HTMLPurifier/DefinitionCache/Serializer.php -type f -exec chmod 777 {} \;  
   
   
   
  **example**: find /home/nikola/www/normacore/log -type d -exec chmod 777 {} \;  
   
   
  **NOTE**: 777 permission should be only given in dev  


## 5. /migration - import db
## 6. delete /migration
## 7. rewrite url in server config ()
  
###### apache
```
RewriteEngine On 
RewriteCond %{REQUEST_FILENAME} !-f 
RewriteCond %{REQUEST_FILENAME} !-d 
RewriteRule ^([^?]*)$ /project-name/index.php?path=$1 [NC,L,QSA] 
```

###### nginx
```
location /project-name { 
    if (!-e $request_filename){ 
        rewrite ^(.*)$ /project-name/index.php; 
    } 
} 
```

## 8. Set configuration in /config/params.php  


```php
Conf::set('base', '');              // localhost dir (doesn't exist on production)  
Conf::set('site_name', '');         // name of the new website  
Conf::set('session_prefix', '');    // used for login sessions (mandatory)  
Conf::set('db_hostname', '');       // db host  
Conf::set('db_username', '');       // db user  
Conf::set('db_password', '');       // db password  
Conf::set('db_name', '');           // db name  
```