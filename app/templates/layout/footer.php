<footer>
  <div class="container content">
    <div class="company">
      <a href="<?php echo Conf::get('url'); ?>"><img src="<?php echo Conf::get('css_img_url'); ?>/logo.png" alt="logo"/></a>
      <p>GENERALNI ZASTUPNIK</p>
      <p>ZA SRBIJU</p>
      <p>SEPHORA LEK DOO</p>
      <p>Hadži Đerina 27</p>
      <p>11000 Beograd</p>
    </div>
    <div class="contact">
      <h4>Kontakt:</h4>
      <p>Telefoni: <a href="telto:+381116303339">+381.11.630.3339</a></p>
      <p><a href="telto:+381112440143">+381.11.244.0143</a></p>
      <p><a href="telto:+381113441057">+381.11.344.1057</a></p>
      <p>Mail: <a href="mailto:office@sephoralek.rs">office@sephoralek.rs</a></p>
      <p><a href="mailto:sales@sephoralek.rs">sales@sephoralek.rs</a></p>
      <p><a href="mailto:marketing@sephoralek.rs">marketing@sephoralek.rs</a></p>
      <p><a href="mailto:direktor@sephoralek.rs">direktor@sephoralek.rs</a></p>
    </div>
    <div class="wt-sn">
      <h4>Radno Vreme:</h4>
      <p>Ponedeljak-Petak 9-17h</p>
      <h4>Pratite nas na:</h4>
      <div class="social">
          <a href="#"><img src="<?php echo Conf::get('css_img_url'); ?>/fb-icon.png" alt="fb"/></a>
          <a href="#"><img src="<?php echo Conf::get('css_img_url'); ?>/instagram-icon.png" alt="instagram"/></a>
          <a href="#"><img src="<?php echo Conf::get('css_img_url'); ?>/whatsapp-icon.png" alt="whatsapp"/></a>
          <a href="#"><img src="<?php echo Conf::get('css_img_url'); ?>/viber-icon.png" alt="viber"/></a>
          <a href="#"><img src="<?php echo Conf::get('css_img_url'); ?>/yt-icon.png" alt="yt"/></a>
      </div>
    </div>
    <div class="brands">
      <h4>Naši drugi brendovi:</h4>
      <a href="#"><img src="<?php echo Conf::get('css_img_url'); ?>/thalgo.png" alt="thalgo"/></a><br>
      <a href="#"><img src="<?php echo Conf::get('css_img_url'); ?>/ella-bache.png" alt="ella-bache"/></a>
    </div>
  </div>
  <div class="copyright">
    Copyright by Sephoralek Doo <?php echo date('Y') . ', ' . Trans::get('All rights reserved') . ', ' . Trans::get('Powered by'); ?> <a href="http://normasoft.net/" target="_blank">Normasoft</a>
  </div>
</footer>

<div id="overlay" class="page-overlay"></div>
<div id="loader">
  <div class="loader-dots">
    <span></span><span></span><span></span>
  </div>
</div>

<a id="backTop"><i></i></a>

<?php //$this->displaySearchForm(); ?>