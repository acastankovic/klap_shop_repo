<header>
	
	<div class="container clearfix">

    <div class="site-logo">
      <a href="<?php echo Conf::get('url'); ?>"><img src="<?php echo Conf::get('css_img_url'); ?>/logo.png" alt="logo"/></a>
    </div>

    <?php Dispatcher::instance()->dispatch('normacore', 'layout', 'mainMenu', null, Request::HTML_REQUEST); ?>
    <div class="header-right">
      <div class="nalog clearfix">
        <a href="#">
          <img src="<?= Conf::get('css_img_url'); ?>/nalog.png" alt="nalog"/><p>MOJ NALOG</p>
        </a>
      </div>
      <?php Dispatcher::instance()->dispatch("normacore", "layout", "cartWidget", null, Request::HTML_REQUEST); ?>
      <div class="search">
        <form method="post" action="<?= Conf::get('url') . '/' . Trans::get('search-results') ?>" id="searchForm">
          <input type="text" name="search" class="field" /><button type="submit"><img src="<?= Conf::get('css_img_url'); ?>/pretraga.png" alt="pretraga"/></button>
        </form>
      </div>
    </div>

	</div>

</header>

<?php // Dispatcher::instance()->dispatch('normacore', 'layout', 'languages', null, Request::HTML_REQUEST); ?>