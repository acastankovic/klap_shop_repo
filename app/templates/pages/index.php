<!DOCTYPE html>
<html lang="<?php $this->displayHtmlTagLangAttr(); ?>">
  <head>
    <?php $this->displayMetaTitle(); ?>
    <?php $this->displayTemplate('common/favicon.php', null); ?>
    <?php $this->displayTemplate('common/meta.php', null); ?>
    <?php $this->displayAdditionalMetaTags(); ?>
    <?php $this->displayTemplate('common/css.php', null); ?>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/plugins/slider/nc-slider.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/pages/shop/main.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/pages/home.css"/>
  </head>
  <body id="homePage">

    <?php $this->displayTemplate('layout/header.php', null); ?>

    <?php Dispatcher::instance()->dispatch('normacore', 'layout', 'homeSlider', null, Request::HTML_REQUEST); ?>

    <?php $this->displayPage(); ?>

    <?php $this->displayTemplate('layout/footer.php', null); ?>

    <?php $this->displayTemplate('common/scripts.php', null); ?>
    <?php $this->displayTemplate('common/constants.php', null); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.js"></script>
    <script src="<?php echo Conf::get('url'); ?>/plugins/slider/nc-slider.min.js"></script>
    <script>
      $('#slider').ncSlider();
    </script>

  </body>
</html>