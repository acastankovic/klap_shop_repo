<!DOCTYPE html>
<html lang="<?php $this->displayHtmlTagLangAttr(); ?>">
  <head>
    <?php $this->displayMetaTitle(); ?>
    <?php $this->displayTemplate('common/favicon.php', null); ?>
    <?php $this->displayTemplate('common/meta.php', null); ?>
    <?php $this->displayAdditionalMetaTags(); ?>
    <?php $this->displayTemplate('common/css.php', null); ?>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/pagination.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/pages/shop/main.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/products.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/pages/shop/category.css"/>
  </head>
  <body id="shopCategoryPage" class="category-<?php echo $this->category->id; ?>">

    <?php $this->displayTemplate('layout/header.php', null); ?>

    <?php $this->displayPage(); ?>

    <?php $this->displayTemplate('layout/footer.php', null); ?>

    <?php $this->displayTemplate('common/scripts.php', null); ?>
    <?php $this->displayTemplate('common/constants.php', null); ?>
    <script src="<?php echo Conf::get('url') . '/js/pagination.js'; ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.js"></script>

  </body>
</html>