<!DOCTYPE html>
<html lang="<?php $this->displayHtmlTagLangAttr(); ?>">
  <head>
    <?php echo $this->displayMetaTitle(); ?>
    <?php $this->displayTemplate('common/favicon.php', null); ?>
    <?php $this->displayTemplate('common/meta.php', null); ?>
    <?php $this->displayAdditionalMetaTags(); ?>
    <?php $this->displayTemplate('common/css.php', null); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/pages/category.css" />
  </head>
  <body id="categoryPage" class="category-<?php echo $this->category->id; ?>">

    <?php $this->displayTemplate('layout/header.php', null); ?>

    <?php $this->displayPage(); ?>

    <?php $this->displayTemplate('layout/footer.php', null); ?>

    <?php $this->displayTemplate('common/scripts.php', null); ?>
    <?php $this->displayTemplate('common/constants.php', null); ?>

  </body>
</html>