<!DOCTYPE html>
<html lang="<?php $this->displayHtmlTagLangAttr(); ?>">
  <head>
    <title>Test page | <?php echo Conf::get('site_name'); ?></title>
    <?php $this->displayTemplate('common/favicon.php', null); ?>
    <?php $this->displayTemplate('common/meta.php', null); ?>
    <?php $this->displayTemplate('common/css.php', null); ?>
  </head>
  <body id="testPage">

    <?php $this->displayTemplate('layout/header.php', null); ?>

    <div class="container">

      <?php $this->renderSimpleBreadcrumbs('Test page'); ?>

      <h1>Test page</h1>

    </div>

    <?php $this->displayTemplate('layout/footer.php', null); ?>

    <?php $this->displayTemplate('common/scripts.php', null); ?>
    <?php $this->displayTemplate('common/constants.php', null); ?>

  </body>
</html>