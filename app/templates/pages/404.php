<!DOCTYPE html>
<html lang="<?php $this->displayHtmlTagLangAttr(); ?>">
  <head>
    <?php $this->displayMetaTitle(); ?>
    <?php $this->displayTemplate('common/favicon.php', null); ?>
    <?php $this->displayTemplate('common/meta.php', null); ?>
    <?php $this->displayAdditionalMetaTags(); ?>
    <?php $this->displayTemplate('common/css.php', null); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo Conf::get('url'); ?>/css/pages/404.css"/>
  </head>
  <body id="errorPage">

    <?php $this->displayTemplate('layout/header.php', null); ?>

    <?php echo $this->displayPage(); ?>

    <?php $this->displayTemplate('layout/footer.php', null); ?>

    <?php $this->displayTemplate('common/scripts.php', null); ?>
    <?php $this->displayTemplate('common/constants.php', null); ?>

  </body>
</html>