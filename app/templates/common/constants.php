<script>
    var BASE_URL = "<?php echo Conf::get('url'); ?>";
    var MEDIA_URL = "<?php echo Conf::get('media_url'); ?>";
    var CSS_IMG_URL = "<?php echo Conf::get('css_img_url'); ?>";
    var SITE_LANG = "<?php echo Trans::getLanguageAlias(); ?>";

    var IE_BROWSER = false;
    <?php if(IEBrowser()) { ?>
    IE_BROWSER = true;
    <?php } ?>
</script>