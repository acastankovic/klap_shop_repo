<?php


class AsyncController extends MainController {

  public function __construct() {
    parent::__construct();
    $this->view = new MainView;
  }


  public function languagesSet() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $language = Dispatcher::instance()->dispatch('content', 'languages', 'fetchOne', array('id' => $params['lang_id']));

    $route = null;
    if (@exists($params['route'])) {

      $route = $params['route'];

      // check if anchor link
      if (strpos($params['route'], '#') != false) {

        $routeArray = explode('#', $params['route']);
        $route = $routeArray[0];
        $params['route'] = $routeArray[0];
      }
    }

    $params['langAlias'] = $language->alias;
    $params['page_type'] = $this->aliasDecoding($route)->pageType;

    Trans::setLanguage($language);

    $url = $this->setLanguageChangeUrl($params);

    if (!isset($url)) {
      $url = Conf::get('url') . '/404';
    }

    $this->view->respond($url, null);
  }


  public function sendEmail() {

    $this->view = new MailsView();
    $this->view->initController($this);

    $captchaCode = $this->params('captcha_code');

    if (!validateEmail($this->params('email'))) {

      $success = false;
      $message = Trans::get('Invalid E-mail address');

    } else if (isset($captchaCode) && !$this->secureImageCaptchaChecked()) {

      $success = false;
      $message = Trans::get('Enter the code from the image');

    } else {

      $params = Security::Instance()->purifyAll($this->params());

      $email = $this->view->contactEmailTemplate($params);

      $mailer = new Mailer();
      if ($mailer->sendMail($email)) {

        $success = true;
        $message = Trans::get('E-mail was successfully sent');

      } else {

        $success = false;
        $message = Trans::get('There was an error sending E-mail');
      }
    }

    $response = new stdClass();
    $response->success = $success;
    $response->message = $message;

    $this->view->respond($response, null);
  }


  public function addComment() {

    $this->view = new MailsView();
    $this->view->initController($this);

    $params = $this->params();

    $success = false;
    if (!@exists($params['type_id'])) {

      $message = 'Type missing';

    } else if (!validateEmail($params['email'])) {

      $message = Trans::get('Invalid E-mail address');
    } else {

      Dispatcher::instance()->dispatch('content', 'comments', 'insertComment', $params);
      $success = true;
      $message = Trans::get('Comment added and waiting for approval');

      $params['item'] = Dispatcher::instance()->dispatch('content', 'articles', 'fetchOne', array('id' => $params['target_id']));

//         $email = $this->view->commentEmailTemplate($params);
//         $mailer = new Mailer();
//         $mailer->sendMail($email);
    }

    $response = new stdClass();
    $response->success = $success;
    $response->message = $message;

    $this->view->respond($response, null);
    return $response;
  }


  public function newsletterSignup() {

    $params = trimFields(Security::Instance()->purifyAll($this->params()));

    $result = Dispatcher::instance()->dispatch('content', 'newsletter', 'signup', $params);

    if (!$result->success) {
      $this->view->respond($result);
      return;
    }

    $this->view = new MailsView();
    $this->view->initController($this);

    $emailToCustomer = $this->view->newsletterCustomerTemplate($params);

    $mailer = new Mailer();
    if ($mailer->sendMail($emailToCustomer)) {

      $emailToAdmin = $this->view->newsletterAdminTemplate($params);

      $mailer->sendMail($emailToAdmin);

      $success = true;
      $message = Trans::get('You have successfully subscribed to the newsletter, check your mail and spam folder.');

    } else {

      $success = false;
      $message = Trans::get('There was an error sending E-mail');
    }

    $this->view->respond((object)[
      'success' => $success,
      'message' => $message
    ]);
  }
}

?>