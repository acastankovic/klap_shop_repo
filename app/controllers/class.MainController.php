<?php


class MainController extends Controller {

  public function __construct() {
    parent::__construct();
  }


  protected function setLanguageChangeUrl($params) {

    $pageType = $this->setPageType($params);

    switch ($pageType) {
      case 'content-article':
        $data = Dispatcher::instance()->dispatch('content', 'articles', 'fetchByLanguageGroupIdAndLanguageId', array('lang_group_id' => $params['lang_group_id'], 'lang_id' => $params['lang_id']));
        return $data->url;
      case 'category':
        $data = Dispatcher::instance()->dispatch('content', 'categories', 'fetchByLanguageGroupIdAndLanguageId', array('lang_group_id' => $params['lang_group_id'], 'lang_id' => $params['lang_id']));
        return $data->url;
      case 'shop-product':
        $data = Dispatcher::instance()->dispatch('shop', 'products', 'fetchByLanguageGroupIdAndLanguageId', array('lang_group_id' => $params['lang_group_id'], 'lang_id' => $params['lang_id']));
        return $data->url;
      case '':
        $url = $this->setStaticRouteUrl($params);
        return $url;
      default:
        return Conf::get('url');
    }
  }


  protected function setPageType($params) {

    if (isset($params) && isset($params['page_type'])) {
      if ((string)$params['page_type'] === 'content-category' || (string)$params['page_type'] === 'shop-category' || (string)$params['page_type'] === 'shop-last-child-category') {
        return 'category';
      }
      return $params['page_type'];
    }
    return '';
  }


  protected function setStaticRouteUrl($params) {

    if (!@exists($params['route'])) return Conf::get('url');

    $matchedRoute = $this->findCurrentRoute($params);

    if (!@exists($matchedRoute)) return Conf::get('url');

    return $this->setStaticRouteUrlByLangAlias($matchedRoute, $params);
  }


  protected function findCurrentRoute($params) {

    $matchedRoute = array();
    foreach ($this->routes as $route) {
      if ($route['route'] === $params['route']) {
        $matchedRoute = $route;
      }
    }

    return $matchedRoute;
  }


  protected function setStaticRouteUrlByLangAlias($matchedRoute, $params) {

    $url = Conf::get('url') . '/';

    foreach ($this->routes as $route) {

      if ($route['action'] === $matchedRoute['action']) {

        if (@exists($route['langAlias']) && $route['langAlias'] === $params['langAlias']) {
          $url .= $route['route'];
        }
      }
    }

    return $url;
  }


  protected function secureImageCaptchaChecked() {
    $image = new Securimage();
    return $image->check($_POST['captcha_code']);
  }


  protected function googleRecaptchaChecked() {

    if (!isset($_POST['g-recaptcha-response']) || (string)$_POST['g-recaptcha-response'] === '' || !$_POST['g-recaptcha-response']) return false;

    $secretKey = Conf::get('recaptcha_secret_key');
    $remoteIp = $_SERVER['REMOTE_ADDR'];
    $captcha = $_POST['g-recaptcha-response'];

    $captchaUrl = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $secretKey . '&response=' . $captcha . '&remoteip=' . $remoteIp;

    $captchaResponse = file_get_contents($captchaUrl);

    $response = json_decode($captchaResponse);

    return $response->success;
  }
}

?>