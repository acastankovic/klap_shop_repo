<?php


class LayoutController extends MainController {

  public function __construct() {
    parent::__construct();
    $this->view = new MainView;
  }

  public function mainMenu() {

    $menuWithItems = Dispatcher::instance()->dispatch('content', 'menus', 'fetchOneWithItems', array('id' => Conf::get('main_menu_id')));

    $data = @exists($menuWithItems) ? formTree($menuWithItems->items, 0) : null;

    $this->view = new MenuView($data);
    $this->view->initController($this);
    $this->view->respond(null, 'layout/menu.php');
  }

  public function languages() {

    $data = Languages::getActive();

    $this->view->initController($this);
    $this->view->respond($data, 'layout/languages.php');
  }

  public function homeSlider() {

    $data = Dispatcher::instance()->dispatch('content', 'sliders', 'fetchOneWithItems', array('id' => Conf::get('home_slider_id')));

    $this->view = new BannerSliderView($data);
    $this->view->initController($this);
    $this->view->respond(null, 'layout/slider.php');
  }


  public function sidebarNavigation() {

    $rootId = Conf::get('shop_category_id');
    $langId = Trans::getLanguageId();

    $data = Dispatcher::instance()->dispatch('content', 'categories', 'fetchTree', array('root_id' => $rootId, 'lang_id' => $langId));

    $this->view = new SidebarView($data);
    $this->view->initController($this);
    $this->view->respond(null, 'layout/sidebar.php');
  }


  public function cartWidget() {

    $order = Dispatcher::instance()->dispatch('shop', 'orders', 'fetchOrder', null);

    $this->view = new CartWidgetView($order);
    $this->view->initController($this);
    $this->view->respond(null, 'layout/cart_widget.php');
  }
}

?>