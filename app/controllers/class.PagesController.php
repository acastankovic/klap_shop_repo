<?php


class PagesController extends MainController {

  public function __construct() {

    parent::__construct();
    $this->view = new MainView;

    Trans::initTranslations();
  }

    
  public function aliasDecoding($alias = null) {

    $data = parent::aliasDecoding($alias);

    switch ((string)$data->pageType) {
      case 'content-category':
        $this->contentCategoryPage($data);
        return $data;
      case 'content-article':
        $this->contentArticlePage($data);
        return $data;
      case 'shop-category':
        $this->shopCategoryPage($data);
        return $data;
      case 'shop-last-child-category':
        $this->shopCategoryPage($data);
        return $data;
      case 'shop-product':
        $this->shopProductPage($data);
        return $data;
      default:
        $this->errorPage();
        return $data;
    }
  }


  /******************************** CONTENT ********************************/


  public function contentCategoryPage($data) {

    Trans::setLanguageById($data->category->lang_id);

    $this->view = new CategoriesView($data);
    $this->view->initController($this);
    $this->view->respond($data, 'pages/category.php');
  }


  public function contentArticlePage($data) {

    Trans::setLanguageById($data->article->lang_id);

    $comments = Dispatcher::instance()->dispatch('content', 'comments', 'fetchByTypeIdAndTargetId', array('type_id' => Conf::get('comment_type_id')['article'], 'target_id' => $data->article->id));

    if (@exists($comments)) {
      $data->comments = $comments;
    }

    $this->view = new ArticlesView($data);
    $this->view->initController($this);
    $this->view->respond($data, 'pages/article.php');
  }


  /*************************** /END OF CONTENT  ***************************/


  /***************************** USER PAGES *****************************/


  public function profilePage() {

    if (!@exists($this->user)) {
      header('Location: ' . Conf::get('url'));
    }
    $this->setLanguageByAlias($this->langAlias());

    $data = new stdClass();
    $data->customer = $this->user;

    $this->view = new ProfileView($data);
    $this->view->initController($this);
    $this->view->respond(null, 'pages/profile.php');
  }


  public function resetPasswordPage() {

    $this->setLanguageByAlias($this->langAlias());

    $this->view = new ResetPasswordView($this->params('token'));
    $this->view->initController($this);
    $this->view->respond(null, 'pages/reset_password.php');
  }


  public function activateProfilePage() {

    $this->setLanguageByAlias($this->langAlias());

    $data = Dispatcher::instance()->dispatch('users', 'users', 'activate', array('token' => $this->params('token')));

    $this->view = new ActivateProfileView($data);
    $this->view->initController($this);
    $this->view->respond(null, 'pages/activate_profile.php');
  }


  /************************* /END OF USER PAGES *************************/


  /******************************** SHOP ********************************/


  public function shopCategoryPage($data) {

    Trans::setLanguageById($data->category->lang_id);

    $this->view = new ShopCategoryView($data, $this->params());
    $this->view->initController($this);
    $this->view->respond(null, 'pages/shop/category.php');
  }


  public function shopProductPage($data) {

    Trans::setLanguageById($data->product->lang_id);

    $comments = Dispatcher::instance()->dispatch('content', 'comments', 'fetchByTypeIdAndTargetId', array('type_id' => Conf::get('comment_type_id')['product'], 'target_id' => $data->product->id));

    if (@exists($comments)) {
      $data->comments = $comments;
    }

    $this->view = new ProductView($data);
    $this->view->initController($this);
    $this->view->respond(null, 'pages/shop/product.php');
  }


  public function cartPage() {

    $order = Dispatcher::instance()->dispatch('shop', 'orders', 'fetchOrder', null);

    $this->view = new CartView($order);
    $this->view->initController($this);
    $this->view->respond(null, 'pages/shop/cart.php');
  }


  public function orderPage() {

    $order = Dispatcher::instance()->dispatch('shop', 'orders', 'fetchOrder', null);

    $this->view = new OrderView($order);
    $this->view->initController($this);

    $items = json_decode($order->items);

    if (@exists($items)) {
      $this->view->respond(null, 'pages/shop/order.php');
    } else $this->errorPage();
  }


  public function shopSearchResultsPage() {

    $this->setLanguageByAlias($this->langAlias());

    $params = $this->params();

    $data = new stdClass();
    $data->searchTerm = $params['search'];

    $p = array();
    if (@exists($params['items_per_page'])) $p['items_per_page'] = $params['items_per_page'];
    if (@exists($params['page'])) $p['page'] = $params['page'];
    if (@exists($params['vendor_id'])) $p['vendor_id'] = $params['vendor_id'];
    if (@exists($params['search'])) $p['search'] = $params['search'];

    $results = Dispatcher::instance()->dispatch('shop', 'products', 'fetchWithFilters', $p);

    $data->productsCount = $results->total;
    $data->products = $results->items;

    $this->view = new ShopSearchResultsView($data, $this->params());
    $this->view->initController($this);
    $this->view->respond(null, 'pages/shop/search_results.php');
  }


  /**************************** /END OF SHOP ****************************/


  /**************************** STATIC PAGES  ****************************/


  public function indexPage() {

    $data = new stdClass();
    $data->categories = Dispatcher::instance()->dispatch('shop', 'categories', 'fetchByParentId', array('parent_id' => 0));

    $this->view = new HomeView($data);
    $this->view->initController($this);
    $this->view->respond(null, 'pages/index.php');
  }


  public function errorPage() {

    $this->view = new ErrorPageView();
    $this->view->initController($this);
    $this->view->respond(null, 'pages/404.php');
  }


  public function contactPage() {

    $this->setLanguageByAlias($this->langAlias());

    $this->view = new ContactView();
    $this->view->initController($this);
    $this->view->respond(null, 'pages/contact.php');
  }


  public function searchResultsPage() {

    $this->setLanguageByAlias($this->langAlias());

    $data = new stdClass();
    $data->searchTerm = $this->params('search');
    $data->categories = Dispatcher::instance()->dispatch('content', 'categories', 'fetchBySearch', array('search' => $this->params('search')));
    $data->articles = Dispatcher::instance()->dispatch('content', 'articles', 'fetchBySearch', array('search' => $this->params('search')));

    $this->view = new SearchResultsView($data);
    $this->view->initController($this);
    $this->view->respond(null, 'pages/search_results.php');
  }


  /************************ /END OF STATIC PAGES  ************************/


  public function testPage() {

    $data = new stdClass();
    $data->pageName = ucfirst('test');

    $this->view->respond($data, 'pages/test.php');
  }
}

?>