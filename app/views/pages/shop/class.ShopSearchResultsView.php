<?php


class ShopSearchResultsView extends MainView implements PagesViewInterface {

  public $pageName;
  private $searchTerm;
  private $products = array();
  private $productsCount = 0;
  private $params;

  public function __construct($data, $params) {
    parent::__construct();

    $this->pageName = ucfirst(Trans::get('Search results'));

    if (@exists($data)) {

      if (@exists($data->searchTerm)) {
        $this->searchTerm = $data->searchTerm;
      }

      if (@exists($data->searchTerm)) {
        $this->products = $data->products;
      }

      if (@exists($data->productsCount)) {
        $this->productsCount = $data->productsCount;
      }
    }

    if (@exists($params)) {

      $this->params = $params;

      if (!@exists($params['items_per_page'])) {
        $this->params['items_per_page'] = Conf::get('items_per_page')['site_products'];
      }

      if (@exists($data->category)) {
        $this->params['url'] = $data->category->url;
      }

      if (@exists($data->productsCount)) {
        $this->params['total'] = $data->productsCount;
      }
    }
  }


  private function resultsExist() {

    if (!@exists($this->products)) {
      return false;
    }
    return true;
  }


  public function displayBreadcrumbs() {
    $this->renderSimpleBreadcrumbs($this->pageName);
  }


  // meta title tag
  public function displayMetaTitle() {
    $title = $this->pageName . ' | ' . Conf::get('site_name');
    $this->renderMetaTitle($title);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayStaticAdditionalMetaTags(array('title' => $this->pageName));
  }

  public function displayPage() {

    //$this->dispalyHiddenFields();

    echo '<div class="container clearfix">';

      $this->renderSimpleBreadcrumbs($this->pageName);

      Dispatcher::instance()->dispatch('normacore', 'layout', 'sidebarNavigation', null, Request::HTML_REQUEST);

      echo '<div class="inner-wrapper">';

        echo '<h1>' . Trans::get('Search results for') . ': "' . $this->searchTerm . '"</h1>';

        if ($this->resultsExist()) {
          $this->renderProducts($this->products, $this->params);
        } else {
          $this->renderNoResultsMessage();
        }

      echo '</div>';

    echo '</div>';
  }


  private function renderNoResultsMessage() {

    echo '<h3>' . Trans::get('No results for term') . ': "' . $this->searchTerm . '"</h3>';
  }
}

?>