<?php

class CartView extends MainView implements PagesViewInterface {

  public $pageName;
  public $order = null;
  public $items = null;
  public $editableCart = true;

  public function __construct($order) {
    parent::__construct();

    $this->pageName = ucfirst(Trans::get('Cart'));

    if(@exists($order)) {
      $this->order = $order;
      $this->items = json_decode($this->order->items);
      if(@exists($order->items)) { 
        $this->items = json_decode($this->order->items);
      }
    }
  }


  protected function orderExists() {
    return @exists($this->order);
  }


  protected function itemsExists() {
    return @exists($this->items);
  }


  // meta title tag
  public function displayMetaTitle() {
    $title = $this->pageName . ' | ' . Conf::get('site_name');
    $this->renderMetaTitle($title);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayStaticAdditionalMetaTags(array('title' => $this->pageName));
  }


  public function displayPage() {

    echo '<div class="container clearfix">';

      $this->renderSimpleBreadcrumbs($this->pageName);

      Dispatcher::instance()->dispatch("normacore", "layout", "sidebarNavigation", null, Request::HTML_REQUEST);

      echo '<div class="inner-wrapper">';

        echo '<div id="cartWrapper">';

          if ($this->orderExists() && $this->itemsExists()) {
            $this->renderCartTable();
          }

          $this->renderEmptyCartMessage();

        echo '</div>';

      echo '</div>';

    echo '</div>';
  }


  public function renderEmptyCartMessage() {

    $activeClass = $this->orderExists() && $this->itemsExists() ? '' : ' active';

    echo '<h2 class="cart-empty-message' . $activeClass . '">' . Trans::get('Your shopping cart is empty') . '</h2>';
  }


  public function renderCartTable() {

    echo '<div id="cartTableWrapper">';

    echo '<table id="cartTable">';
      $this->renderCartTableHead();
      $this->renderCartTableBody();
      $this->renderCartTableFooter();
    echo '</table>';

    $this->renderCartTableFooterResponsive();

    if ($this->editableCart) {
      $this->renderCartActionButtons();
    }

    echo '</div>';
  }


  public function renderCartTableHead() {

    echo '<thead>';
      echo '<tr>';
        echo '<th class="align-center">' . Trans::get('Image') . '</th>';
        echo '<th class="align-left">' . Trans::get('Name') . '</th>';
        echo '<th class="align-right">' . Trans::get('Price') . '</th>';
        echo '<th class="align-center">' . Trans::get('Unit') . '</th>';
        echo '<th class="align-right">' . Trans::get('Total') . '</th>';
      echo '</tr>';
    echo '</thead>';
  }


  public function renderCartTableBody() {

    echo '<tbody>';

      foreach ($this->items as $item) {

        echo '<tr class="cart-row c-item-' . $item->id . '" data-id="' . $item->id . '">';

          echo '<td class="align-center">';
            echo '<span class="th-responsive">' . Trans::get('Image') . '</span>';
            echo '<a href="' . $item->url . '">';
              echo '<figure class="cart-item-image">';
                echo '<img src="' . $this->setMediaImageUrl(array('image' => $item->image, 'thumb' => true)) . '" alt="' . $item->image . '" />';
              echo '</figure>';
            echo '</a>';
          echo '</td>';

          echo '<td class="align-left">';
            echo '<span class="th-responsive">' . Trans::get('Name') . '</span>';
            echo '<div class="cart-item-title"><a href="' . $item->url . '">' . $item->title . '</a></div>';
            echo '<div class="cart-item-code">' . $item->code . '</div>';
          echo '</td>';

          echo '<td class="cart-prices align-right">';
            echo '<span class="th-responsive">' . Trans::get('Price') . '</span>';
            echo $this->renderItemPrice($item);
          echo '</td>';

          $this->renderQuantityCell($item);

          echo '<td class="cart-item-total-price align-right">';
            echo '<span class="th-responsive">' . Trans::get('Unit') . '</span>';
            echo formatPrice($item->quantity_total_price);
          echo '</td>';

        echo '</tr>';
      }

    echo '</tbody>';
  }


  public function renderCartTableFooter() {

    echo '<tfoot>';
    echo '<tr>';
    echo '<td class="align-right" colspan="4">' . Trans::get('Vat') . '</td>';
    echo '<td class="cart-total-vat align-right">' . formatPrice($this->order->order_vat) . '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td class="align-right" colspan="4">' . Trans::get('Savings') . '</td>';
    echo '<td class="cart-total-discount align-right">' . formatPrice($this->order->order_discount) . '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td class="align-right" colspan="4">' . Trans::get('Total') . '</td>';
    echo '<td class="cart-total-price align-right">' . formatPrice($this->order->order_price) . '</td>';
    echo '</tr>';
    echo '</tfoot>';
  }

  public function renderCartTableFooterResponsive() {

    echo '<div class="table-footer">';
      echo '<div class="row">';
        echo '<div class="left">' . Trans::get('Vat') . '</div>';
        echo '<div class="right">' . formatPrice($this->order->order_vat) . '</div>';
      echo '</div>';
      echo '<div class="row">';
        echo '<div class="left">' . Trans::get('Savings') . '</div>';
        echo '<div class="right">' . formatPrice($this->order->order_discount) . '</div>';
      echo '</div>';
      echo '<div class="row">';
        echo '<div class="left">' . Trans::get('Total') . '</div>';
        echo '<div class="right">' . formatPrice($this->order->order_price) . '</div>';
      echo '</div>';
    echo '</div>';
  }


  public function renderCartActionButtons() {

    $checkoutUrl = $this->setCheckoutUrl();

    echo '<div class="cart-action-buttons clearfix">';
    echo '<button type="button" class="btn cart-empty">' . Trans::get('Empty cart') . '</button>';
    echo '<a href="' . $checkoutUrl . '" class="btn cart-order">' . Trans::get('Checkout') . '</a>';
    echo '</div>';
  }


  public function renderItemPrice($item) {

    if (PricesCalculationService::discountExists($item)) {

      $html = '<p class="cart-item-price">' . formatPrice($item->total_price) . '</p>';
      $html .= '<p class="cart-item-price-without-discount">' . formatPrice($item->original_price) . '</p>';
      $html .= '<p class="cart-item-discount">(-' . PricesCalculationService::renderDiscountValue($item) . ')</p>';
    } else $html = '<p class="cart-item-price">' . formatPrice($item->total_price) . '</p>';

    return $html;
  }


  protected function renderQuantityCell($item) {

    if ($this->editableCart) {
      $this->renderEditableQuantityCell($item);
    } else {
      $this->renderFixedQuantityCell($item);
    }
  }


  protected function renderEditableQuantityCell($item) {

    echo '<td class="align-center">';
      echo '<span class="th-responsive">' . Trans::get('Unit') . '</span>';
      echo '<input type="number" value="' . $item->quantity . '" class="cart-item-change-qty" data-id="' . $item->id . '" />';
      echo '<div class="action-buttons">';
        echo '<button type="button" class="btn cart-item-increase-qty" data-id="' . $item->id . '"><i class="fa fa-plus"></i></button>';
        echo '<button type="button" class="btn cart-item-decrease-qty" data-id="' . $item->id . '"><i class="fa fa-minus"></i></button>';
        echo '<button type="button" class="btn cart-item-remove" data-id="' . $item->id . '"><i class="fa fa-times"></i></button>';
      echo '</div>';
    echo '</td>';
  }


  protected function renderFixedQuantityCell($item) {

    echo '<td class="align-center">';
      echo '<span class="th-responsive">' . Trans::get('Unit') . '</span>';
      echo '<div>' . $item->quantity . '</div>';
    echo '</td>';
  }


  public function renderOrderPrice() {
    return $this->orderExists() ? formatPrice($this->order->order_price) : formatPrice(0);
  }


  public function renderOrderQuantity() {
    return $this->orderExists() ? $this->order->quantity : 0;
  }
}

?>