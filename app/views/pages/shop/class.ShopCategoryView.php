<?php

class ShopCategoryView extends MainView implements PagesViewInterface {

  public $pageName;
  private $breadcrumbs;
  public $category;
  private $subCategories = array();
  private $products = array();
  private $productsCount = 0;
  private $params;

  public function __construct($data, $params) {
    parent::__construct();

    if (@exists($data)) {

      if (@exists($data->breadcrumbs)) {
        $this->breadcrumbs = $data->breadcrumbs;
      }

      if (@exists($data->category)) {
        $this->category = $data->category;
        $this->pageName = $this->category->name;
      }

      if (@exists($data->subCategoriesTree) && @exists($data->subCategoriesTree['children'])) {
        $this->subCategories = $data->subCategoriesTree['children'];
      }

      if (@exists($data->products)) {
        $this->products = $data->products;
      }

      if (@exists($data->productsCount)) {
        $this->productsCount = $data->productsCount;
      }
    }

    if (@exists($params)) {

      $this->params = $params;

      if (!@exists($params['items_per_page'])) {
        $this->params['items_per_page'] = Conf::get('items_per_page')['site_products'];
      }

      if (@exists($data->category)) {
        $this->params['url'] = $data->category->url;
      }

      if (@exists($data->productsCount)) {
        $this->params['total'] = $data->productsCount;
      }
    }
  }


  // meta title tag
  public function displayMetaTitle() {
    $this->renderMetaTitle($this->pageName, $this->category);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayGenericAdditionalMetaTags($this->category);
  }


  public function displayPage() {

    $this->displayHiddenFields();

    echo '<div class="container clearfix">';

      $this->renderBreadcrumbs($this->breadcrumbs);

      //Dispatcher::instance()->dispatch('normacore', 'layout', 'sidebarNavigation', null, Request::HTML_REQUEST);

      //echo '<div class="inner-wrapper">';

        $this->renderCategory();
        $this->renderCategories();
        $this->renderProducts($this->products, $this->params);

      //echo '</div>';

    echo '</div>';
  }


  private function displayHiddenFields() {

    $this->renderLangGroupIdHiddenField($this->category);
    echo '<input type="hidden" id="categoryId" value="' . $this->category->id . '" />';
  }


  private function renderCategory() {

    if(Conf::get('shop_category_id') != $this->category->id) {
      $this->renderPageTitle($this->category);
      $this->renderPageSubtitle($this->category);
    }


    // $withImage = $this->mediaImageExists($this->category->image);
    // $withImageClass = $withImage ? ' with-image' : '';

    // echo '<div class="category-desc' . $withImageClass . '">';

    //   if ($withImage) {
    //     $this->renderImage();
    //   }

    //   if (@exists($this->category->content)) {
    //     echo '<div class="page-content">' . $this->category->content . '</div>';
    //   }

    // echo '</div>';
    echo '<div class="page-content">' . $this->category->intro_text . '</div>';
  }


  private function renderImage() {

    $image = $this->setMediaImageUrl(array('image' => $this->category->image, 'thumb' => true));

    echo '<div class="image-wrapper">';
      echo '<img src="' . $image . '" alt="featured-image" id="itemImage" />';
    echo '</div>';
  }


  private function renderCategories() {

    if (@exists($this->subCategories)) {
      echo '<div class="categories">';

      foreach ($this->subCategories as $category) {

        if (!is_object($category)) $category = (object)$category;

        $image = $this->setMediaImageUrl(array('image' => $category->image, 'thumb' => true));
        echo '<div class="category">';
          echo '<img src="' . Conf::get('media_url') . '/' . $category->image . '" alt="category" />';
          echo '<p>' . $category->name . '</p>';
          echo '<a href="' . $category->url . '">Saznaj više</a>';
        echo '</div>';
      }

      echo '</div>';
    }
  }

  

  public function renderProducts() {
    if(@exists($this->products)) {
      echo '<div class="products">';
        foreach($this->products as $product) {
          echo '<div class="product">';
            echo '<img src="' . Conf::get('media_url') . '/' . $product->image . '" alt="product" />';
            echo '<p>' . $product->title . '</p>';
            echo '<a href="' . $product->url . '">Saznaj više</a>';
          echo '</div>';
        }
      echo '</div>';
    }
  }


  private function isCategory($id) {
    return (int)$this->category->id === (int)$id || (int)$this->category->lang_group_id === (int)$id;
  }


  private function isParent($id) {
    return (int)$this->category->parent_id === (int)$id || (int)$this->category->parent_lang_group_id === (int)$id;
  }
}

?>
