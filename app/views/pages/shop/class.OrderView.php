<?php

class OrderView extends CartView implements PagesViewInterface {

  public $pageName;
  public $editableCart = false;

  public function __construct($order) {
    parent::__construct($order);

    $this->pageName = ucfirst(Trans::get('Checkout'));

    if (@exists($order)) {
      $this->order = $order;
      $this->items = json_decode($this->order->items);
    }
  }


  public function displayBreadcrumbs() {

    $this->renderCustomBreadcrumbs(
      array(
        Trans::get('Cart') => Conf::get('url') . '/' . strtolower(Trans::get('Cart')),
        Trans::get('Checkout') => Conf::get('url') . '/' . strtolower(Trans::get('Checkout'))
      )
    );
  }


  // meta title tag
  public function displayMetaTitle() {
    $title = $this->pageName . ' | ' . Conf::get('site_name');
    $this->renderMetaTitle($title);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayStaticAdditionalMetaTags(array('title' => $this->pageName));
  }


  public function displayPage() {

    echo '<div class="container clearfix">';

      $this->displayBreadcrumbs();

      Dispatcher::instance()->dispatch('normacore', 'layout', 'sidebarNavigation', null, Request::HTML_REQUEST);

      echo '<div class="inner-wrapper">';

        echo '<div id="cartWrapper">';

          if ($this->orderExists() && $this->itemsExists()) {
            $this->renderCartTable();
            $this->renderOrderForm();
          }

          $this->renderEmptyCartMessage();

        echo '</div>';

      echo '</div>';

    echo '</div>';
  }


  public function renderOrderForm() {

    echo '<form id="placeOrder">';

      echo '<div class="form-wrapper">';

      echo '<div class="form-section">';
        echo '<input type="text" name="name" id="orderName" class="form-field required" placeholder="' . Trans::get('Name') . '" />';
      echo '</div>';

      echo '<div class="form-section">';
        echo '<input type="text" name="email" id="orderEmail" class="form-field required" placeholder="' . Trans::get('E-mail') . '" />';
      echo '</div>';

      echo '<div class="form-section">';
        echo '<input type="text" name="phone" id="orderPhone" class="form-field required" placeholder="' . Trans::get('Phone') . '" />';
      echo '</div>';

      echo '<div class="form-section">';
        echo '<input type="text" name="address" id="orderAddress" class="form-field required" placeholder="' . Trans::get('Address') . '" />';
      echo '</div>';

      echo '<div class="form-section big">';
        echo '<textarea name="message" id="orderMessage" class="form-field required" placeholder="' . Trans::get('Message') . '"></textarea>';
      echo '</div>';

      echo '<div class="form-section form-buttons big">';
        echo '<button type="button" class="form-btn-clear">' . Trans::get('Clear') . '</button>';
        echo '<button type="submit" class="form-btn-submit">' . Trans::get('Order') . '</button>';
      echo '</div>';

      echo '</div>';

    echo '</form>';
  }
}

?>