<?php

class ProductView extends MainView implements PagesViewInterface {

  public $pageName;
  private $breadcrumbs;
  public $product;
  private $comments;

  public function __construct($data) {
    parent::__construct();

    if (@exists($data)) {

      if (@exists($data->breadcrumbs)) {
        $this->breadcrumbs = $data->breadcrumbs;
      }

      if (@exists($data->product)) {
        $this->product = $data->product;
        $this->pageName = ucfirst($this->product->title);
      }

      if (@exists($data->comments)) {
        $this->comments = $data->comments;
      }
    }
  }


  // meta title tag
  public function displayMetaTitle() {
    $this->renderMetaTitle($this->pageName, $this->product);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayGenericAdditionalMetaTags($this->product);
  }


  public function displayPage() {

    $this->displayHiddenFields();

    echo '<div class="container clearfix">';

      $this->renderBreadcrumbs($this->breadcrumbs);

      //Dispatcher::instance()->dispatch('normacore', 'layout', 'sidebarNavigation', null, Request::HTML_REQUEST);

      //echo '<div class="inner-wrapper">';

        $this->renderProduct();

      //echo '</div>';

    echo '</div>';
  }


  public function displayHiddenFields() {
    $this->renderLangGroupIdHiddenField($this->product);
    echo '<input type="hidden" id="categoryId" value="' . $this->product->category_id . '" />';
  }


  private function renderProduct() {

    echo '<div class="product-wrapper">';

      echo '<div class="page-left">';
      $this->renderImage();
      $this->displayGallery($this->product->gallery);
      echo '</div>';

      echo '<div class="page-right">';
      $this->renderPageTitle($this->product);
      $this->renderPageSubtitle($this->product);
      $this->renderPageContent($this->product);
      echo '<div>' . $this->product->intro_text . '</div>';
      echo '<hr>';
      $this->renderCode();
      $this->renderVendor();
      $this->renderItemPrice();
      $this->renderActionButtons();
      echo '</div>';

    echo '</div>';

    $this->renderPageContent($this->product);
    $this->renderProductComments();
  }


  private function renderCode() {

    if (@exists($this->product->code)) {

      echo '<div class="info-wrapper code-wrapper">';
        //echo '<label>' . Trans::get('Code') . ':</label>';
        echo '<span class="info-value">' . $this->product->code . '</span>';
      echo '</div>';
    }
  }


  private function renderVendor() {

    if (@exists($this->product->vendor_id) && (int)$this->product->vendor_id !== 0) {

      $image = $this->setMediaImageUrl(array("image" => $this->product->vendor_image));
      $url = Conf::get('url') . '/' . strtolower(Trans::get('vendor')) . '/' . $this->product->vendor_alias;

      echo '<div class="info-wrapper vendor-wrapper">';
        echo '<div class="vendor-name">';
          echo '<label>' . Trans::get('Vendor') . ':</label>';
          echo '<a href="' . $url . '" class="btn btn-with-tooltip info-value">' . $this->product->vendor_name;
              echo '<div class="tooltip-container"><div class="tooltip">' . Trans::get('See this vendor products on our site') . '<i class="fa fa-caret-down"></i></div></div>';
          echo '</a>';
        echo '</div>';
        echo '<div class="vendor-image">';
          echo '<img src="' . $image . '" alt="' . $this->product->vendor_name . '" />';
        echo '</div>';
      echo '</div>';
    }
  }


  private function renderItemPrice() {

    echo '<div class="info-wrapper price-wrapper">';

    if (PricesCalculationService::discountExists($this->product)) {
      echo '<p class="price"><span class="info-value">' . formatPrice(PricesCalculationService::getProductPrice($this->product)) . ' RSD</span></p>';
      echo '<p class="price-without-discount"><label>' . Trans::get('Instead of') . ':</label><span class="info-value">' . formatPrice($this->product->price) . ' RSD</span></p>';
      //echo '<p class="discount"><label>' . Trans::get('Discount') . ':</label><span class="info-value">' . PricesCalculationService::renderDiscountValue($this->product) . '</span></p>';
      //echo '<p class="price"><label>' . Trans::get('Price') . ':</label><span class="info-value">' . formatPrice(PricesCalculationService::getProductPrice($this->product)) . '</span></p>';
    } else echo '<p class="price"><span class="info-value">' . formatPrice($this->product->price) . ' RSD</span></p>';

    echo '</div>';
  }


  private function renderImage() {

    $image = $this->setMediaImageUrl(array("image" => $this->product->image));

    echo '<div class="featured-image">';
      echo '<a href="' . $image . '" data-fancybox="gallery">';
        echo '<div class="image-wrapper">';
          echo '<img src="' . $image . '" alt="featured-image" id="itemImage" />';
        echo '</div>';
      echo '</a>';
    echo '</div>';
  }


  private function renderActionButtons() {

    echo '<div class="action-buttons-wrapper">';

      //echo '<input type="number" placeholder="0" class="quantity" id="itemQuantity" />';

      echo '<button type="button" class="site-btn cart-item-add" data-id="' . $this->product->id . '">';
        echo '<span>' . Trans::get('Add to cart') . '</span>';
      echo '</button>';

    echo '</div>';
  }


  public function renderProductComments() {

    if (@exists($this->product->allow_comments) && (int)$this->product->allow_comments === 1) {

      if (@exists($this->comments)) {

        $this->renderCommentsSection($this->comments, $this->product);
      }

      $this->renderCommentsForm($this->product->id, Conf::get('comment_type_id')['product']);
    }
  }


  private function isProduct($id) {
    return (int)$this->product->id === (int)$id || (int)$this->product->lang_group_id === (int)$id;
  }


  private function isParent($id) {
    return (int)$this->product->parent_id === (int)$id || (int)$this->product->parent_lang_group_id === (int)$id;
  }


  private function isCategoriesParent($id) {
    return (int)$this->product->categories_parent_id === (int)$id || (int)$this->product->categories_parent_lang_group_id === (int)$id;
  }
}

?>
