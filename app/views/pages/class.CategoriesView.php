<?php

class CategoriesView extends MainView implements PagesViewInterface {

  public $pageName;
  private $breadcrumbs;
  public $category;
  private $articles;
  private $subCategories;


  public function __construct($data) {
    parent::__construct();

    if (@exists($data)) {

      if (@exists($data->breadcrumbs)) {
        $this->breadcrumbs = $data->breadcrumbs;
      }

      if (@exists($data->category)) {
        $this->category = $data->category;
        $this->pageName = ucfirst($this->category->name);
      }

      if (@exists($data->articles)) {
        $this->articles = $data->articles;
      }

      if (@exists($data->subCategories)) {
        if (@exists($data->subCategories['children'])) {
          $this->subCategories = $data->subCategories['children'];
        }
      }
    }
  }


  // meta title tag
  public function displayMetaTitle() {
    $this->renderMetaTitle($this->pageName, $this->category);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayGenericAdditionalMetaTags($this->category);
  }


  public function displayBreadcrumbs() {
    $this->renderBreadcrumbs($this->breadcrumbs);
  }


  public function displayPage() {

    $this->renderLangGroupIdHiddenField($this->category);

    echo '<div class="container">';
      $this->renderBreadcrumbs($this->breadcrumbs);
      //$this->renderImage();
      //$this->renderPageTitle($this->category);
     // $this->renderPageSubtitle($this->category);
      //$this->renderPageContent($this->category);
      $this->renderSubCategories();
      $this->renderArticles();
    echo '</div>';
  }


  public function renderImage() {

    if (@exists($this->category->image)) {

      $image = $this->setMediaImageUrl(array('image' => $this->category->image));

      echo '<div class="article-image">';
        echo '<img src="' . $image . '" alt="' . $this->category->image . '" />';
      echo '</div>';
    }
  }


  public function renderSubCategories() {

    if (@exists($this->subCategories)) {

      echo '<div class="categories">';

      foreach ($this->subCategories as $category) {

        if (@exists($category)) {

          $category = (object)$category;

          $image = $this->setMediaImageUrl(array("image" => $category->intro_image));

          echo '<div class="category">';
            echo '<img src="' . Conf::get('media_url') . '/' . $category->image . '" alt="category" />';
            echo '<p>' . $category->name . '</p>';
            echo '<a href="' . $category->url . '">Saznaj više</a>';
          echo '</div>';
        }
      }

      echo '</div>';
    }
  }


  public function renderArticles() {

    if (@exists($this->articles)) {

      echo '<div class="articles">';

      foreach ($this->articles as $article) {

        $this->renderArticle($article);
      }

      echo '</div>';
    }
  }


  private function isCategory($id) {
    return (int)$this->category->id === (int)$id || (int)$this->category->lang_group_id === (int)$id;
  }


  private function isParent($id) {
    return (int)$this->category->parent_id === (int)$id || (int)$this->category->parent_lang_group_id === (int)$id;
  }
}

?>