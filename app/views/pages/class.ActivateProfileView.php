<?php


Class ActivateProfileView extends MainView implements PagesViewInterface {

  public $pageName;
  private $success;
  private $message;

  public function __construct($data) {
    parent::__construct();

    $this->pageName = ucfirst(Trans::get('Profile activation'));

    if(@exists($data)) {

      if(@exists($data->success)) {
        $this->success = $data->success;
      }

      if(@exists($data->message)) {
        $this->message = $data->message;
      }
    }
  }


  // meta title tag
  public function displayMetaTitle() {
    $title = $this->pageName . ' | ' . Conf::get('site_name');
    $this->renderMetaTitle($title);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayStaticAdditionalMetaTags(array('title' => $this->pageName));
  }


  public function displayPage() {

    echo '<div class="container clearfix">';

      $this->renderSimpleBreadcrumbs($this->pageName);

      echo '<div class="align-center">';
        echo '<h1>' . $this->pageName . '</h1>';
        echo '<h2>' . $this->message . '</h2>';
      echo '</div>';

    echo '</div>';
  }
}
?>