<?php


Class ProfileView extends MainView implements PagesViewInterface {

  public $pageName;
  private $breadcrumbs;
  private $customer;

  public function __construct($data) {
    parent::__construct();

    $this->pageName = Trans::get('Profile');

    if(@exists($data)) {

      if(@exists($data->breadcrumbs)) {
        $this->breadcrumbs = $data->breadcrumbs;
      }

      if(@exists($data->customer)) {
        $this->customer = $data->customer;
      }
    }
  }


  // meta title tag
  public function displayMetaTitle() {
    $title = $this->pageName . ' | ' . Conf::get('site_name');
    $this->renderMetaTitle($title);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayStaticAdditionalMetaTags(array('title' => $this->pageName));
  }


  public function displayPage() {

    echo '<div class="container">';
      $this->renderSimpleBreadcrumbs($this->pageName);
      $this->renderProfileDataForm();
      $this->renderChangePasswordForm();
    echo '</div>';
  }


  public function renderProfileDataForm() {

    $firstName = @exists($this->customer->first_name) ? $this->customer->first_name : '';
    $lastName = @exists($this->customer->last_name) ? $this->customer->last_name : '';
    $phone = @exists($this->customer->phone) ? $this->customer->phone : '';
    $address = @exists($this->customer->address) ? $this->customer->address : '';

    echo '<section>';

      echo '<h2>' . Trans::get('Change your personal info') . '</h2>';

      echo '<form id="profileDataForm">';

        echo '<div class="form-wrapper">';

          echo '<div class="form-section">';
            echo '<input type="text" name="first_name" class="form-field required" placeholder="' . Trans::get('First name') . '" value="' . $firstName . '">';
          echo '</div>';

          echo '<div class="form-section">';
            echo '<input type="text" name="last_name" class="form-field required" placeholder="' . Trans::get('Last name') . '" value="' . $lastName . '">';
          echo '</div>';

          echo '<div class="form-section">';
            echo '<input type="text" name="phone" class="form-field required" placeholder="' . Trans::get('Phone') . '" value="' . $phone . '">';
          echo '</div>';

          echo '<div class="form-section">';
            echo '<input type="text" name="address" class="form-field required" placeholder="' . Trans::get('Address') . '" value="' . $address . '">';
          echo '</div>';

          echo '<div class="form-section big align-center">';
            echo '<button class="site-btn submit"><i class="fa fa-spinner spinner-icon"></i>' . Trans::get('Change') . '</button>';
          echo '</div>';

          echo '<div class="error-message align-center"></div>';

        echo '</div>';

      echo '</form>';

    echo '</section>';
  }

  public function renderChangePasswordForm() {

    echo '<section>';

      echo '<h2>' . Trans::get('Change your password') . '</h2>';

      echo '<form id="changePasswordForm">';

        echo '<div class="form-wrapper">';

          echo '<div class="form-section">';
            echo '<input type="password" name="current_password" class="form-field required" placeholder="' . Trans::get('Current password') . '">';
          echo '</div>';

          echo '<div class="form-section">';
            echo '<input type="password" name="password" class="form-field required" placeholder="' . Trans::get('New password') . '">';
          echo '</div>';

          echo '<div class="form-section">';
            echo '<input type="password" name="repeated_password" class="form-field required" placeholder="' . Trans::get('Repeat new password') . '">';
          echo '</div>';

          echo '<div class="form-section big align-center">';
            echo '<button class="site-btn submit"><i class="fa fa-spinner spinner-icon"></i>' . Trans::get('Change') . '</button>';
          echo '</div>';

          echo '<div class="error-message align-center"></div>';

        echo '</div>';

      echo '</form>';

    echo '</section>';
  }
}
?>