<?php


Class ResetPasswordView extends MainView implements PagesViewInterface {

  public $pageName;
  private $token;

  public function __construct($token) {
    parent::__construct();

    $this->pageName = Trans::get('Reset password');

    if(@exists($token)) {
      $this->token = $token;
    }
  }


  // meta title tag
  public function displayMetaTitle() {
    $title = $this->pageName . ' | ' . Conf::get('site_name');
    $this->renderMetaTitle($title);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayStaticAdditionalMetaTags(array('title' => $this->pageName));
  }


  public function displayPage() {

    echo '<div class="container">';
      $this->renderSimpleBreadcrumbs($this->pageName);

      if(@exists($this->token)) {
        $this->renderForm();
      }
      else {
        echo '<h2>' . Trans::get('Token doesn\'t exist') . '</h2>';
      }

    echo '</div>';
  }

  public function renderForm() {

    echo '<section>';

      echo '<h2>' . Trans::get('Change your password') . '</h2>';

      echo '<form id="resetPasswordForm">';

        echo '<div class="form-wrapper">';

          echo '<input type="hidden" name="token" class="form-field required" value="' . $this->token . '">';

          echo '<div class="form-section">';
            echo '<input type="password" name="password" class="form-field required" placeholder="' . Trans::get('New password') . '">';
          echo '</div>';

          echo '<div class="form-section">';
              echo '<input type="password" name="repeated_password" class="form-field required" placeholder="' . Trans::get('Repeat new password') . '">';
          echo '</div>';

          echo '<div class="form-section big align-center">';
            echo '<button class="site-btn submit"><i class="fa fa-spinner spinner-icon"></i>Change</button>';
          echo '</div>';

          echo '<div class="error-message align-center"></div>';

        echo '</div>';

      echo '</form>';

    echo '</section>';
  }

}
?>