<?php

class ArticlesView extends MainView implements PagesViewInterface {

  public $pageName;
  private $breadcrumbs;
  public $article;
  private $comments;


  public function __construct($data) {
    parent::__construct();

    if (@exists($data)) {

      if (@exists($data->breadcrumbs)) {
        $this->breadcrumbs = $data->breadcrumbs;
      }

      if (@exists($data->article)) {
        $this->article = $data->article;
        $this->pageName = ucfirst($this->article->title);
      }

      if (@exists($data->comments)) {
        $this->comments = $data->comments;
      }
    }
  }


  // meta title tag
  public function displayMetaTitle() {
    $this->renderMetaTitle($this->pageName, $this->article);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayGenericAdditionalMetaTags($this->article);
  }


  public function displayPage() {

    $this->renderLangGroupIdHiddenField($this->article);

    echo '<div class="container">';
      $this->renderBreadcrumbs($this->breadcrumbs);
      $this->renderImage();
      $this->renderPageTitle($this->article);
      $this->renderPageSubtitle($this->article);
      $this->renderEventDate();
      $this->renderPageContent($this->article);
      $this->renderArticleGallery();
      $this->renderArticleComments();
    echo '</div>';
  }


  public function renderImage() {

    if (@exists($this->article->image)) {

      $image = $this->setMediaImageUrl(array('image' => $this->article->image));

      echo '<div class="article-image">';
        echo '<img src="' . $image . '" alt="' . $this->article->image . '" />';
      echo '</div>';
    }
  }


  public function renderEventDate() {

    $date = dateFormat($this->setDate());

    echo '<div class="article-date">' . Trans::get('Released') . ': <span class="date">' . $date . '</span></div>';
  }


  private function setDate() {

    return @exists($this->article->event_date) && $this->article->event_date != '0000-00-00 00:00:00' ? $this->article->event_date : $this->article->cdate;
  }


  public function renderArticleGallery() {

    echo $this->renderGallery($this->article->gallery);
  }


  public function renderArticleComments() {

    if (@exists($this->article->allow_comments) && (int)$this->article->allow_comments === 1) {

      if (@exists($this->comments)) {

        $this->renderCommentsSection($this->comments, $this->article);
      }

      $this->renderCommentsForm($this->article->id, Conf::get('comment_type_id')['article']);
    }
  }


  private function isArticle($id) {
    return (int)$this->article->id === (int)$id || (int)$this->article->lang_group_id === (int)$id;
  }


  private function isParent($id) {
    return (int)$this->article->parent_id === (int)$id || (int)$this->article->parent_lang_group_id === (int)$id;
  }


  private function isCategoriesParent($id) {
    return (int)$this->article->categories_parent_id === (int)$id || (int)$this->article->categories_parent_lang_group_id === (int)$id;
  }
}

?>