<?php


class ErrorPageView extends MainView implements PagesViewInterface {

  public $pageName;

  public function __construct() {
    parent::__construct();

    $this->pageName = ucfirst(Trans::get('404'));
  }


  // meta title tag
  public function displayMetaTitle() {
    $title = $this->pageName . ' | ' . Conf::get('site_name');
    $this->renderMetaTitle($title);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayStaticAdditionalMetaTags(array('title' => $this->pageName));
  }

  public function displayPage() {

    echo '<div class="container">';

      $this->renderSimpleBreadcrumbs($this->pageName);

      echo '<div class="error-wrapper">';
        echo '<h1>404</h1>';
        echo '<h2>' . Trans::get('Page not found') . '</h2>';
        echo '<p>' . Trans::get('The page you are looking for was moved, removed, renamed or might never existed') . '.</p>';
        echo '<div><a href="' . Conf::get('url') . '">' . Trans::get('Back to home') . '</a></div>';
      echo '</div>';

    echo '</div>';
  }
}

?>