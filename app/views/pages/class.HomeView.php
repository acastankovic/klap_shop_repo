<?php


class HomeView extends MainView implements PagesViewInterface {

  public $pageName;
  public $products;

  public function __construct($data) {
    parent::__construct();

    $this->pageName = Conf::get('site_name');

    if (@exists($data) && @exists($data->products)) {
      $this->products = $data->products;
    }
    
    if (@exists($data) && @exists($data->categories)) {
      $this->categories = $data->categories;
    }
  }

  // meta title tag
  public function displayMetaTitle() {
    $title = $this->pageName;
    $this->renderMetaTitle($title);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayStaticAdditionalMetaTags(array('title' => $this->pageName));
  }


  public function displayPage() {

    echo '<div class="container clearfix">';
      $this->displayCategories();
      $this->displayQuality();
      $this->displayAboutUs();
    echo '</div>';
  }

  public function displayCategories() {
    if(@exists($this->categories)) {
      echo '<div class="categories">';
        foreach($this->categories as $category) {
          echo '<div class="category">';
            echo '<img src="' . Conf::get('media_url') . '/' . $category->image . '" alt="category" />';
            echo '<p>' . $category->name . '</p>';
            echo '<a href="' . $category->url . '">Saznaj više</a>';
          echo '</div>';
        }
      echo '</div>';
    }
  }

  public function displayQuality() {
    echo '<div class="quality">';
      echo '<p>KLAPP Cosmetics is a pioneer in the world of beauty and stands for innovative<br/>cosmetic treatments. ahead of their time. For us “Made in Germany” is a promise of quality</p>';
      echo '<div class="logos">';
        echo '<img src="' . Conf::get('css_img_url') . '/iso.png" alt="iso" />';
        echo '<img src="' . Conf::get('css_img_url') . '/gerhard-klapp.png" alt="gerhard-klapp" />';
        echo '<img src="' . Conf::get('css_img_url') . '/beauty-forum.png" alt="beauty-forum" />';
      echo '</div>';
    echo '</div>';
  }

  public function displayAboutUs() {
    echo '<div class="about-us">';
      echo '<img src="' . Conf::get('css_img_url') . '/about-us.png" />';
      echo '<div>';
        echo '<h2>KOMPANIJA KLAPP</h2>';
        echo '<p>KLAPP Cosmetics is a pioneer in the world of beauty and stands for innovative cosmetic treatments ahead of their time. For us “Made in Germany” is a promise of quality. Over 40 years of experience and continuous research and development of new high-tech active ingredients and methods, always resulting in the highest treatment success.</p>';
        echo '<p>In Germany alone 230 employees, of which 50 work on-site in the customer service department, take care of your individual wishes and needs.</p>';
        echo '<p>We have earned the trust of over 35,000 cosmetic institutes and hotels in over 60 countries.</p>';
      echo '</div>';
      echo '<h3>OUR PHILOSOPHY</h3>';
      echo '<p>As one of the last big visionaries in the beauty branch, Gerhard Klapp has been setting trends since 1980. Gerhard Klapp does not spare any efforts to invest his wellgrounded expertise in the research and development of cosmetic treatments, special treatments and skin care products.</p>';
      echo '<p>That is why he has always been one of the precursors on the beauty market. His innovations include Retinol (vitamin A), O2 therapy, vitamin C und ASA Peel®.</p>';
      echo '<p>KLAPP Cosmetics is thus synonym for innovation, in the past, the present and also the future.</p>';
    echo '</div>';

  }
}

?>