<?php


class ContactView extends MainView implements PagesViewInterface {

  public $pageName;

  public function __construct() {
    parent::__construct();

    $this->pageName = ucfirst(Trans::get('Contact'));
  }


  // meta title tag
  public function displayMetaTitle() {
    $title = $this->pageName . ' | ' . Conf::get('site_name');
    $this->renderMetaTitle($title);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayStaticAdditionalMetaTags(array('title' => $this->pageName));
  }


  public function displayPage() {

    echo '<div class="container">';

      $this->renderSimpleBreadcrumbs($this->pageName);

      echo '<div class="map">';
        echo '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2830.306695965634!2d20.456470615535835!3d44.81531617909868!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a7ab262d773e9%3A0xec94a4d857dffae8!2sObili%C4%87ev+venac+19%2C+Beograd!5e0!3m2!1sen!2srs!4v1563538004948!5m2!1sen!2srs" frameborder="0" allowfullscreen></iframe>';
      echo '</div>';

      echo '<h2>' . Trans::get('Send us E-mail') . '</h2>';

      echo '<form id="contactForm">';

        echo '<div class="form-wrapper">';

          echo '<div class="form-section">';
            echo '<input type="text" name="name" id="contactName" class="form-field required" placeholder="' . Trans::get('Name') . '" />';
          echo '</div>';

          echo '<div class="form-section">';
            echo '<input type="text" name="email" id="contactEmail" class="form-field required" placeholder="' . Trans::get('E-mail') . '" />';
          echo '</div>';

          echo '<div class="form-section">';
            echo '<input type="text" name="phone" id="contactPhone" class="form-field required" placeholder="' . Trans::get('Phone') . '" />';
          echo '</div>';

          echo '<div class="form-section big">';
            echo '<textarea name="message" id="contactMessage" class="form-field required" placeholder="' . Trans::get('Message') . '"></textarea>';
          echo '</div>';

          echo '<div class="form-section form-buttons big">';
            echo '<button type="button" class="form-btn-clear">' . Trans::get('Clear') . '</button>';
            echo '<button type="submit" class="form-btn-submit">' . Trans::get('Send') . '</button>';
          echo '</div>';

        echo '</div>';

      echo '</form>';

    echo '</div>';
  }
}

?>