<?php


class SearchResultsView extends MainView implements PagesViewInterface {

  public $pageName;
  private $searchTerm;
  private $categories;
  private $articles;

  public function __construct($data) {
    parent::__construct();

    $this->pageName = ucfirst(Trans::get('Search results'));

    if (@exists($data)) {

      if (@exists($data->searchTerm)) {
        $this->searchTerm = $data->searchTerm;
      }

      if (@exists($data->categories)) {
        $this->categories = $data->categories;
      }

      if (@exists($data->searchTerm)) {
        $this->articles = $data->articles;
      }
    }
  }


  private function resultsExist() {

    if (!@exists($this->categories) && !@exists($this->articles)) {
      return false;
    }
    return true;
  }


  public function displayBreadcrumbs() {
    $this->renderSimpleBreadcrumbs($this->pageName);
  }


  // meta title tag
  public function displayMetaTitle() {
    $title = $this->pageName . ' | ' . Conf::get('site_name');
    $this->renderMetaTitle($title);
  }


  // meta description, keywords and og tags
  public function displayAdditionalMetaTags() {
    $this->displayStaticAdditionalMetaTags(array('title' => $this->pageName));
  }


  public function displayPage() {

    echo '<div class="container">';

    $this->renderSimpleBreadcrumbs($this->pageName);

    echo '<h1>' . Trans::get('Search results for') . ': "' . $this->searchTerm . '"</h1>';

    if ($this->resultsExist()) {
      $this->renderResults();
    }
    else {
      $this->renderNoResultsMessage();
    }

    echo '</div>';
  }


  public function renderResults() {

    echo '<div class="results">';

    if (@exists($this->categories)) {

      foreach ($this->categories as $category) {

        echo '<div class="result">';

        if (@exists($category->name)) {
          echo '<h3>' . $category->name . '</h3>';
        }


        if (@exists($category->content)) {
          echo '<div class="content">' . truncateString($category->content, 200) . '</div>';
        }

        echo '<div class="btn-wrapper"><a href="' . $category->url . '">' . Trans::get('Read more') . '</a></div>';

        echo '</div>';
      }
    }

    if (@exists($this->articles)) {

      foreach ($this->articles as $article) {

        echo '<div class="result">';

        if (@exists($article->title)) {
          echo '<h3>' . $article->title . '</h3>';
        }


        if (@exists($article->content)) {
          echo '<div class="content">' . truncateString($article->content, 200) . '</div>';
        }

        echo '<div class="btn-wrapper"><a href="' . $article->url . '">' . Trans::get('Read more') . '</a></div>';

        echo '</div>';
      }
    }

    echo '</div>';
  }


  private function renderNoResultsMessage() {
    echo '<h3>' . Trans::get('No results for term') . ': "' . $this->searchTerm . '"</h3>';
  }
}

?>