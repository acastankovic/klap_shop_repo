<?php


class MailsView extends MainView {

  public function __construct() {
    parent::__construct();
    Trans::initTranslations();
  }


  public function contactEmailTemplate($params) {

    $htmlBody = '';
    $altBody = '';

    $br = "\r\n";

    if (@exists($params['name'])) {
      $htmlBody .= '<p>' . Trans::get('Ime') . ': ' . $params['name'] . '</p>';
      $altBody .= Trans::get('Ime') . ': ' . $params['name'] . $br;
    }

    if (@exists($params['email'])) {
      $htmlBody .= '<p>' . Trans::get('Email') . ': ' . $params['email'] . '</p>';
      $altBody .= Trans::get('Email') . ': ' . $params['email'] . $br;
    }

    if (@exists($params['phone'])) {
      $htmlBody .= '<p>' . Trans::get('Phone') . ': ' . $params['phone'] . '</p>';
      $altBody .= Trans::get('Phone') . ': ' . $params['phone'] . $br;
    }

    if (@exists($params['message'])) {
      $htmlBody .= '<p>' . Trans::get('Message') . ': ' . $params['message'] . '</p>';
      $altBody .= Trans::get('Message') . ': ' . $params['message'] . $br;
    }

    $data = array();
    $data['form_address'] = Conf::get('mail_from_address');
    $data['form_name'] = Conf::get('mail_from_name');
    $data['to_address'] = Conf::get('mail_to_address');
    $data['subject'] = Trans::get('Contact Form');
    $data['html_body'] = $htmlBody;
    $data['alt_body'] = $altBody;

    return $data;
  }


  public function commentEmailTemplate($params) {

    $br = "\r\n";

    if ((int)$params['type_id'] === (int)Conf::get('comment_type_id')['article']) {
      $pageType = Trans::get('Article');
    }
    else if ((int)$params['type_id'] === (int)Conf::get('comment_type_id')['category']) {
      $pageType = Trans::get('Category');
    }

    $htmlBody  = '<h3>' . Trans::get('Comment added') . ':</h3>';
    $htmlBody .= '<p>' . Trans::get('Page type') . ': ' . $pageType . '</p>';
    $htmlBody .= '<p>' . Trans::get('Id') . ': ' . $params['item']->id . '</p>';
    $htmlBody .= '<p>' . Trans::get('Title') . ': ' . $params['item']->title . '</p>';
    $htmlBody .= '<p>' . Trans::get('Link') . ': ' . $params['item']->url . '</p>';

    $altBody  = Trans::get('Comment added') . ':' . $br;
    $altBody .= Trans::get('Page type') . ': ' . $pageType . $br;
    $altBody .= Trans::get('Id') . ': ' . $params['item']->id . $br;
    $altBody .= Trans::get('Title') . ': ' . $params['item']->title . $br;
    $altBody .= Trans::get('Link') . ': ' . $params['item']->url . $br;

    if (@exists($params['name'])) {
      $htmlBody .= '<p>' . Trans::get('Ime') . ': ' . $params['name'] . '</p>';
      $altBody .= Trans::get('Ime') . ': ' . $params['name'] . $br;
    }

    if (@exists($params['email'])) {
      $htmlBody .= '<p>' . Trans::get('Email') . ': ' . $params['email'] . '</p>';
      $altBody .= Trans::get('Email') . ': ' . $params['email'] . $br;
    }

    if (@exists($params['message'])) {
      $htmlBody .= '<p>' . Trans::get('Message') . ': ' . $params['message'] . '</p>';
      $altBody .= Trans::get('Message') . ': ' . $params['message'] . $br;
    }

    $data = array();
    $data['form_address'] = Conf::get('mail_from_address');
    $data['form_name'] = Conf::get('mail_from_name');
    $data['to_address'] = Conf::get('mail_to_address');
    $data['subject'] = Trans::get('Comment');
    $data['html_body'] = $htmlBody;
    $data['alt_body'] = $altBody;

    return $data;
  }


  public function emailOrderTemplate($params, $order, $attachment) {

    $br = "\r\n";

    $htmlBody = '';
    $altBody = '';

    if (@exists($params['name'])) {
      $htmlBody .= '<p>' . Trans::get('Ime') . ': ' . $params['name'] . '</p>';
      $altBody .= Trans::get('Ime') . ': ' . $params['name'] . $br;
    }

    if (@exists($params['email'])) {
      $htmlBody .= '<p>' . Trans::get('Email') . ': ' . $params['email'] . '</p>';
      $altBody .= Trans::get('Email') . ': ' . $params['email'] . $br;
    }

    if (@exists($params['phone'])) {
      $htmlBody .= '<p>' . Trans::get('Phone') . ': ' . $params['phone'] . '</p>';
      $altBody .= Trans::get('Phone') . ': ' . $params['phone'] . $br;
    }

    if (@exists($params['address'])) {
      $htmlBody .= '<p>' . Trans::get('Address') . ': ' . $params['address'] . '</p>';
      $altBody .= Trans::get('Address') . ': ' . $params['address'] . $br;
    }

    if (@exists($params['message'])) {
      $htmlBody .= '<p>' . Trans::get('Message') . ': ' . $params['message'] . '</p>';
      $altBody .= Trans::get('Message') . ': ' . $params['message'] . $br;
    }

    $data = array();
    $data['form_address'] = Conf::get('mail_from_address');
    $data['form_name'] = Conf::get('mail_from_name');
    $data['to_address'] = Conf::get('mail_to_address');
    $data['subject'] = Conf::get('site_name') . ' ' . Trans::get('Site') . ': ' . 'Narudžbina broj: ' . $order->id;;
    $data['html_body'] = $htmlBody;
    $data['alt_body'] = $altBody;
    $data['attachment'] = $attachment;

    return $data;
  }


  public function renderOrderTable($order) {

    $items = json_decode($order->items);

    $html = '<div>';

      $html .= '<style>body { font-family: DejaVu Sans, sans-serif; }.align-left {text-align:left;}.align-center {text-align:center;}.align-right {text-align:right;}</style>';

      $html .= '<table style="width:100%">';

      $html .= '<thead style="background-color: #f9f9f9;border-bottom: 2px solid #dddddd;">';
        $html .= '<tr>';
          $html .= '<th class="align-center">' . Trans::get('Image') . '</th>';
          $html .= '<th class="align-left">' . Trans::get('Name') . '</th>';
          $html .= '<th class="align-right">' . Trans::get('Price') . '</th>';
          $html .= '<th class="align-center">' . Trans::get('Unit') . '</th>';
          $html .= '<th class="align-right">' . Trans::get('Total') . '</th>';
        $html .= '</tr>';
      $html .= '</thead>';

      $html .= '<tbody>';

        foreach ($items as $item) {

          $html .= '<tr class="cart-row c-item-' . $item->id . '" data-id="' . $item->id . '">';

          $html .= '<td class="align-center">';
            $html .= '<figure style="display:block;width:70px;height:70px;text-align:center;position:relative;margin:auto;">';
              $html .= '<img src="' . $this->setMediaImageUrl(array('image' => $item->image, 'thumb' => true)) . '" style="position:absolute;max-width:70px;max-height:70px;top:0;bottom:0;left:0;right:0;z-index:1;margin: 0 auto;" />';
            $html .= '</figure>';
          $html .= '</td>';

          $html .= '<td class="align-left">';
            $html .= '<div class="cart-item-title">' . $item->title . '</div>';
            $html .= '<div class="cart-item-code">' . $item->code . '</div>';
          $html .= '</td>';

          $html .= '<td class="cart-prices align-right">';

          if (PricesCalculationService::discountExists($item)) {

            $html .= '<p style="text-decoration:line-through;margin:0;padding:0;">' . formatPrice($item->original_price) . '</p>';
            $html .= '<p style="margin:0;padding:0;">(-' . PricesCalculationService::renderDiscountValue($item) . ')</p>';
            $html .= '<p style="margin:0;padding:0;">' . formatPrice($item->total_price) . '</p>';
          } else $html .= '<p class="cart-item-price">' . formatPrice($item->total_price) . '</p>';

          $html .= '</td>';

          $html .= '<td class="align-center">';
            $html .= '<div>' . $item->quantity . '</div>';
          $html .= '</td>';

          $html .= '<td class="cart-item-total-price align-right">' . formatPrice($item->quantity_total_price) . '</td>';

          $html .= '</tr>';
        }

      $html .= '<tfoot style="background-color: #f9f9f9;border-top: 2px solid #dddddd;">';
        $html .= '<tr>';
          $html .= '<td class="align-right" colspan="4">' . Trans::get('Vat') . '</td>';
          $html .= '<td class="align-right">' . formatPrice($order->order_vat) . '</td>';
          $html .= '</tr>';
          $html .= '<tr>';
          $html .= '<td class="align-right" colspan="4">' . Trans::get('Savings') . '</td>';
          $html .= '<td class="align-right">' . formatPrice($order->order_discount) . '</td>';
          $html .= '</tr>';
          $html .= '<tr>';
          $html .= '<td class="align-right" colspan="4">' . Trans::get('Total') . '</td>';
          $html .= '<td class="align-right">' . formatPrice($order->order_price) . '</td>';
        $html .= '</tr>';
      $html .= '</tfoot>';

      $html .= '</table>';

    $html .= '</div>';

    return $html;
  }


  public function newsletterAdminTemplate($params) {

    $br = "\r\n";

    $htmlBody = '<p>' . Trans::get('New newsletter sign up.') . '</p>';
    $htmlBody .= '<p>' . Trans::get('Sign up info') . ':</p>';

    $altBody = Trans::get('New newsletter sign up.') . $br;
    $altBody .= Trans::get('Sign up info') . ': ' . $br;

    if (@exists($params['email'])) {
      $htmlBody .= '<p>' . Trans::get('Email') . ': ' . $params['email'] . '</p>';
      $altBody .= Trans::get('Email') . ': ' . $params['email'] . $br;
    }

    $data = array();
    $data['form_address'] = Conf::get('mail_from_address');
    $data['form_name'] = Conf::get('mail_from_name');
    $data['to_address'] = Conf::get('mail_to_address');
    $data['subject'] = Trans::get('Newsletter Signup');
    $data['html_body'] = $htmlBody;
    $data['alt_body'] = $altBody;

    return $data;
  }


  public function newsletterCustomerTemplate($params) {

    $br = "\r\n";

    $htmlBody = '<p>' . Trans::get('This e-mail is a confirmation of your subscription to the Newsletter.') . '</p>';
    $htmlBody .= '<p>' . Trans::get('Keep up with our promotions.') . '</p>';

    $altBody = Trans::get('This e-mail is a confirmation of your subscription to the Newsletter.') . $br;
    $altBody .= Trans::get('Keep up with our promotions.') . $br;

    $data = array();
    $data['form_address'] = Conf::get('mail_from_address');
    $data['form_name'] = Conf::get('mail_from_name');
    $data['to_address'] = $params['email'];
    $data['subject'] = Trans::get('Newsletter Signup');
    $data['html_body'] = $htmlBody;
    $data['alt_body'] = $altBody;

    return $data;
  }

  public function registrationTemplate($email, $token) {

    $activationUrl = Conf::get('url') . '/activate/' . $token;

    $htmlBody  = '<div style="width:100%;height:100%;background:#173042;color:#404040;font-family:Arial,serif;">';
      $htmlBody .= '<div style="width:80%;height:100%;margin: auto;background:#ffffff;text-align:center;">';
        $htmlBody .= '<h2>Aktivacija korisničkog naloga</h2>';
        $htmlBody .= '<p>Za aktivaciju Vašeg korisničkog naloga na e-mail adresi ' . $email . ' kliknite na link:</p>';
        $htmlBody .= '<div><a href="' . $activationUrl . '" style="display:inline-block;margin:6px 0;background:#dfafd1;color:#173042;padding:10px;text-decoration:none;">Aktivacija korisničkog naloga</a></div>';
        $htmlBody .= '<hr>';
        $htmlBody .= '<div>';
          $htmlBody .= '<p>Ne radi dugme? Prekopirajte sledeći link u vaš pretraživač:</p>';
          $htmlBody .= '<div><a href="' . $activationUrl . '">' . $activationUrl . '</a></div>';
        $htmlBody .= '</div>';
        $htmlBody .= '<p style="margin-top:20px">Srdačan pozdrav</p>';
      $htmlBody .= '</div>';
    $htmlBody .= '</div>';

    $altBody = 'Aktivacija korisničkog naloga ' . "\r\n";
    $altBody .= 'Za aktivaciju Vašeg korisničkog naloga na e-mail adresi ' . $email . ' kliknite ili prekopirajte link u vaš pretraživač: ' . "\r\n";
    $altBody .= $activationUrl . " \r\n";
    $altBody .= 'Srdačan pozdrav';

    $data = array();
    $data['form_address'] = Conf::get('mail_from_address');
    $data['form_name'] = Conf::get('mail_from_name');
    $data['to_address'] = $email;
    $data['subject'] = Trans::get('User account activation');
    $data['html_body'] = $htmlBody;
    $data['alt_body'] = $altBody;
    return $data;
  }


  public function resetPasswordTemplate($email, $token) {

    $resetPasswordUrl = Conf::get('url') . '/' . Trans::get('reset-password') . '/' . $token;

    $htmlBody  = '<div style="width:100%;height:100%;background:#173042;color:#404040;font-family:Arial,serif;">';
      $htmlBody .= '<div style="width:80%;height:100%;margin: auto;background:#ffffff;text-align:center;">';
        $htmlBody .= '<h2>Promena šifre</h2>';
        $htmlBody .= '<p>Za promenu šifre Vašeg korisničkog naloga na e-mail adresi ' . $email . ' kliknite na link:</p>';
        $htmlBody .= '<div><a href="' . $resetPasswordUrl . '" style="display:inline-block;margin:6px 0;background:#dfafd1;color:#173042;padding:10px;text-decoration:none;">Promena šifre</a></div>';
        $htmlBody .= '<hr>';
        $htmlBody .= '<div>';
          $htmlBody .= '<p>Ne radi dugme? Prekopirajte sledeći link u vaš pretraživač:</p>';
          $htmlBody .= '<div><a href="' . $resetPasswordUrl . '">' . $resetPasswordUrl . '</a></div>';
        $htmlBody .= '</div>';
        $htmlBody .= '<p style="margin-top:20px">Srdačan pozdrav</p>';
      $htmlBody .= '</div>';
    $htmlBody .= '</div>';

    $altBody = 'Promena šifre ' . "\r\n";
    $altBody .= 'Za promenu šifre Vašeg korisničkog naloga na e-mail adresi ' . $email . ' kliknite ili prekopirajte link u vaš pretraživač: ' . "\r\n";
    $altBody .= $resetPasswordUrl . " \r\n";
    $altBody .= 'Srdačan pozdrav';

    $data = array();
    $data['form_address'] = Conf::get('mail_from_address');
    $data['form_name'] = Conf::get('mail_from_name');
    $data['to_address'] = $email;
    $data['subject'] = Trans::get('Reset password');
    $data['html_body'] = $htmlBody;
    $data['alt_body'] = $altBody;

    return $data;
  }

}

?>