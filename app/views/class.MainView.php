<?php

interface PagesViewInterface {
  public function displayMetaTitle();

  public function displayAdditionalMetaTags();

  public function displayPage();
}

class MainView extends View {


  public function __construct() {
    parent::__construct();
  }

  /****************************************** BREADCRUMBS ******************************************/

  public function renderBreadcrumbs($breadcrumbs) {

    echo '<div class="breadcrumbs">';

      echo '<a href="' . Conf::get('url') . '">NASLOVNA</a>';

      $totalCrumbs = count($breadcrumbs);

      $counter = 1;
      foreach ($breadcrumbs as $breadcrumb) {

        foreach ($breadcrumb as $crumb) {

          if (@exists($crumb->title)) {
            $name = $crumb->title;
          }
          else if (@exists($crumb->name)) {
            $name = $crumb->name;
          }

          if ($counter < $totalCrumbs) {

            echo '<a href="' . $crumb->url . '">' . $name . '</a>';
          }
          else echo '<span class="crumb-current">' . $name . '</span>';

        }

        $counter++;
      }

    echo '</div>';
  }

  public function renderSimpleBreadcrumbs($pageName) {

    $pageName = ucfirst($pageName);

    echo '<div class="breadcrumbs">';
      echo '<a href="' . Conf::get('url') . '"><i class="fa fa-home"></i></a>';
      echo '<span class="crumb-current">' . $pageName . '</span>';
    echo '</div>';
  }

  public function renderCustomBreadcrumbs($data) {

    echo '<div class="breadcrumbs">';

    echo '<a href="' . Conf::get('url') . '"><i class="fa fa-home"></i></a>';

    $total = count($data);
    $i = 1;
    foreach ($data as $title => $url) {

      if ((int)$total !== $i) {
        echo '<a href="' . $url . '">' . $title . '</a>';
      }
      else {
        echo '<span class="crumb-current">' . $title . '</span>';
      }

      $i++;
    }
    echo '</div>';
  }


  /******************************************* META TAGS *******************************************/


  /**** SETTERS ****/

  public function setMetaTitle($pageName = null, $data = null) {

    $title = '';
    if (@exists($data) && @exists($data->meta_title)) {
      $title = $data->meta_title;
    }
    else {
      if (@exists($pageName)) {
        $title = $pageName;
      }
    }

    return @exists($title) ? $title : Conf::get('site_name');
  }

  public function setMetaKeywords($data) {
    return @exists($data) && @exists($data->meta_keywords) ? $data->meta_keywords : Conf::get('meta_tags')['keywords'];
  }

  public function setMetaDescription($data) {

    if (@exists($data) && @exists($data->meta_description)) {
      $description = $data->meta_description;
    } else {
      $description = '';
      if (@exists($data->title)) $description .= $data->title . ',';
      if (@exists($data->subtitle)) $description .= $data->subtitle . ',';
      if (@exists($data->name)) $description .= $data->name . ',';
      if (@exists($data->content)) $description .= strip_tags($data->content);
    }

    if ((string)$description === '') $description = Conf::get('meta_tags')['description'];

    $meta_description = truncateString($description, 160);

    return trim($meta_description, ',');
  }


  /**** COMPONENTS RENDERING ****/

  public function renderMetaTitle($data = null, $pageName = null) {
    $title = $this->setMetaTitle($data, $pageName);
    ?><title><?php echo $title; ?></title><?php
  }

  public function renderMetaKeywords($data) {
    $keywords = $this->setMetaKeywords($data);
    ?><meta name="keywords" content='<?php echo $keywords; ?>'><?php
  }

  public function renderMetaDescription($data) {
    $description = $this->setMetaDescription($data);
    ?><meta name="description" content='<?php echo $description; ?>'><?php
  }

  public function renderMetaOgTags($title, $description, $image, $url) {
    echo '<meta property="og:title" content="' . $title . '" />';
    echo '<meta property="og:type" content="article">';
    echo '<meta property="og:image" content="' . $image . '" />';
    echo '<meta property="og:image:width" content="' . Conf::get('meta_tags')['og']['width'] . '">';
    echo '<meta property="og:image:height" content="' . Conf::get('meta_tags')['og']['height'] . '">';
    echo '<meta property="og:image:alt" content="' . $title . '" />';
    echo '<meta property="og:url" content="' . $url . '" />';
    echo '<meta property="og:site_name" content="' . Conf::get('meta_tags')['og']['site_name'] . '">';
    ?><meta property="og:description" content='<?php echo $description; ?>' /><?php
  }

  /**** DISPLAY ****/

  // data => ('title', 'description', 'keywords', 'url', 'image')
  public function displayStaticAdditionalMetaTags($data) {

    $title = Conf::get('meta_tags')['title'];
    $description = Conf::get('meta_tags')['description'];
    $keywords = Conf::get('meta_tags')['keywords'];
    $ogUrl = Conf::get('meta_tags')['og']['url'];
    $ogImage = Conf::get('meta_tags')['og']['image'];

    if (@exists($data)) {
      if (@exists($data['title'])) $title = $data['title'];
      if (@exists($data['description'])) $description = $data['description'];
      if (@exists($data['keywords'])) $keywords = $data['keywords'];
      if (@exists($data['url'])) $ogUrl = $data['url'];
      if (@exists($data['image'])) $ogImage = $data['image'];
    }

    ?><meta name="description" content='<?php echo $description; ?>'><?php
    ?><meta name="keywords" content='<?php echo $keywords; ?>'><?php
    $this->renderMetaOgTags($title, $description, $ogImage, $ogUrl);
  }

  public function displayGenericAdditionalMetaTags($data) {

    $title = Conf::get('meta_tags')['title'];
    $ogImage = Conf::get('meta_tags')['og']['image'];
    $ogUrl = Conf::get('meta_tags')['og']['url'];
    $description = $this->setMetaDescription($data);

    if (@exists($data)) {

      if (@exists($data->title)) {
        $title = $data->title;
      } else if (@exists($data->name)) {
        $title = $data->name;
      }

      if ($this->mediaImageExists($data->image)) {
        $ogImage = Conf::get('media_url') . '/' . $data->image;
      }

      if (@exists($data->url)) {
        $ogUrl = $data->url;
      }
    }

    $this->renderMetaDescription($data);
    $this->renderMetaKeywords($data);
    $this->renderMetaOgTags($title, $description, $ogImage, $ogUrl);
  }

  /**************************************** MEDIA, GALLERIES ****************************************/

  protected function setMediaImageUrl($data) {

    $noImage = Conf::get('url') . '/modules/admin/css/img/no-image.png';

    if (!@exists($data) || !@exists($data['image'])) return $noImage;

    $image = $data['image'];
    $url = Conf::get('media_url');
    $root = Conf::get('media_root');

    if (@exists($data) && @exists($data['thumb']) && $data['thumb'] == true) {

      $url = Conf::get('media_thumbs_url');
      $root = Conf::get('media_thumbs_root');
    }

    $mediaImage = $url . '/' . $image;
    $mediaRoot = $root . '/' . $image;

    if (!file_exists($mediaRoot)) return $noImage;
    return $mediaImage;
  }

  protected function setYoutubeImageUrl($data) {

    if (!isset($data) || !isset($data['code'])) return '';
    return 'http://img.youtube.com/vi/' . $data['code'] . '/0.jpg';
  }

  protected function displayYoutubeImage($data) {

    if (!isset($data) || !isset($data['code'])) return '';
    $image = $this->setYoutubeImageUrl($data);
    echo '<img src="' . $image . '" alt="youtube-video" />';
  }

  protected function renderGalleryItemImage($item, $mediaExists = null) {

    if ((string)$item->type === 'media') {

      $image = Conf::get('css_img_url') . '/no-image.png';
      if (@exists($mediaExists) && $mediaExists !== false) {
        $image = $item->url;
      }

      $html = '<div class="image-wrapper"><img src="' . $image . '" alt="' . $image . '"></div>';
    } else if ((string)$item->type === 'youtube_code') {
      $html = '<div class="image-wrapper">';
        $html .= '<img src="' . $item->image_url . '" alt="' . $item->image_url . '">';
        $html .= '<img src="' . Conf::get('css_img_url') . '/icon-youtube.png" alt="icon-youtube" class="image-icon" />';
      $html .= '</div>';
    }

    return $html;
  }

  protected function setGalleryFancyboxUrl($item, $mediaExists = null) {

    if ((string)$item->type === 'media') {

      $url = Conf::get('css_img_url') . '/no-image.png';
      if (@exists($mediaExists) && $mediaExists !== false) {
        $url = $item->url;
      }

      return $url;
    } else if ((string)$item->type === 'youtube_code') {
      return $item->watch_url;
    }
  }

  public function renderGallery($gallery) {

    $html = '';

    if (@exists($gallery)) {

      $html .= '<div class="nc-gallery">';

        foreach ($gallery as $item) {

          $mediaExists = false;
          if (@exists($item->file_name)) {
            $mediaExists = $this->mediaImageExists($item->file_name);
          }

          $url = $this->setGalleryFancyboxUrl($item, $mediaExists);

          $description = isset($item->description) ? $item->description : '';

          $html .= '<div class="item">';
            $html .= '<a href="' . $url . '" data-fancybox="gallery" data-caption="' . $description . '">';
              $html .= $this->renderGalleryItemImage($item, $mediaExists);
            $html .= '</a>';
          $html .= '</div>';
        }

      $html .= '</div>';
    }

    return $html;
  }

  protected function displayGallery($gallery) {
    echo $this->renderGallery($gallery);
  }

  protected function renderFeaturedImage($data) {

    $file = $data['file'];
    $thumb = @exists($data['thumb']) ? $data['thumb'] : false;
    $wrapperClass = @exists($data['warpperClass']) ? $data['warpperClass'] : "featured-image image-wrapper";
    $imageClass = @exists($data['imageClass']) ? 'class="' . $data['imageClass'] . '"' : '';

    $image = $this->setMediaImageUrl(array('image' => $file, 'thumb' => $thumb));
    $fancyBoxImage = $this->setMediaImageUrl(array('image' => $file));

    echo '<div class="' . $wrapperClass . '">';
    echo '<a href="' . $fancyBoxImage . '" data-fancybox="gallery">';
      echo '<img src="' . $image . '" ' . $imageClass . ' alt="' . $file . '" />';
    echo '</a>';
    echo '</div>';
  }


  protected function mediaImageExists($image) {
    if (!@exists($image)) return false;
    if (!file_exists(Conf::get('media_root') . '/' . $image)) return false;
    return true;
  }


  public function displayHtmlTagLangAttr() {

    $lang = Trans::getLanguageAlias();

    if ((string)$lang === 'ср') {
      $lang = 'sr';
    }

    echo $lang;
  }


  /***************************************** PAGE ELEMENTS *****************************************/


  protected function renderPageTitle($data) {

    if (@exists($data->title)) {
      $title = $data->title;
    } else if (@exists($data->name)) {
      $title = $data->name;
    }

    echo '<h1 class="page-title">' . $title . '</h1>';
  }

  protected function renderPageSubtitle($data) {

    if (@exists($data->subtitle)) {

      echo '<h3 class="page-subtitle">' . $data->subtitle . '</h3>';
    }
  }


  protected function renderPageContent($data) {

    if (@exists($data->content)) {

      echo '<div class="page-content">' . $data->content . '</div>';
    }
  }


  protected function renderLangGroupIdHiddenField($data) {

    $langGroupId = @exists($data->lang_group_id) ? $data->lang_group_id : $data->id;

    echo '<input type="hidden" id="langGroupId" value="' . $langGroupId . '" />';
  }


  protected function renderButtonTooltip($title) {
    echo '<div class="tooltip-container"><div class="tooltip">' . $title . '<i class="fa fa-caret-down"></i></div></div>';
  }


  /******************************************* LANGUAGES *******************************************/

  public function displayLanguages($languages) {

    if (Conf::get('multilingual_enabled')) {

      if (@exists($languages) && count($languages) > 1) {

        echo '<div class="languages">';
        foreach ($languages as $lang) {

          $activeClass = (int)$lang->id === (int)Trans::getLanguageId() ? ' active' : '';

          echo '<a href="#" class="set-language' . $activeClass . '" data-id="' . $lang->id . '">' . $lang->aliasNameTranslated . '</a>';
        }
        echo '</div>';
      }
    }
  }


  /***************************************** INITIALS ICON *****************************************/

  public function setInitials($string, $initialsCount = null) {

    if (!@exists($string)) {
      return '';
    }

    if (!@exists($initialsCount)) {
      $initialsCount = 1;
    }

    $stringParts = explode(' ', $string);
    $partsCount = count($stringParts);

    if ((int)$initialsCount === 1) {
      return substr($stringParts[0], 0, 1);
    }

    if ((int)$initialsCount > 1) {

      if ((int)$partsCount === 1) {
        return substr($stringParts[0], 0, 1);
      } else if ((int)$partsCount > 1) {
        return substr($stringParts[0], 0, 1) . substr($stringParts[1], 0, 1);
      }
    }
  }


  public function renderInitialsIcon($string, $initialsCount = null) {

    $initials = $this->setInitials($string, $initialsCount);

    echo '<div class="initialsIcon">' . $initials . '</div>';
  }

  /******************************************* SEARCH ********************************************/


  public function displaySearchForm() {

    echo '<div class="page-overlay" id="searchOverlay">';

      echo '<button type="button" id="closeSearchForm"><i class="fa fa-times"></i></button>';

      echo '<div class="search-form-wrapper clearfix element-center">';

        echo '<h3>' . Trans::get('Search') . '</h3>';

        echo '<form method="post" action="' . Conf::get('url') . '/' . Trans::get('search-results') . '" id="searchForm">';

          echo '<input type="text" name="search" class="field" />';
          echo '<button type="submit"><i class="fa fa-search"></i></button>';

        echo '</form>';

      echo '</div>';

    echo '</div>';
  }

  /******************************************* COMMENTS ********************************************/

  public function renderCommentsSection($comments, $item) {

    if (@exists($comments)) {

      echo '<div class="comments" id="pageComments">';
        $this->renderComments($comments, $item);
      echo '</div>';
    }
  }


  public function renderComments($comments, $item, $child = null) {

    if (@exists($comments)) {

      foreach ($comments as $comment) {

        $childClass = @exists($child) ? ' child' : '';

        echo '<div class="comment clearfix' . $childClass . '">';

          echo '<div class="avatar"><i class="fa fa-user"></i></div>';
          echo '<div class="desc">';
            echo '<span>' . $comment["name"] . '</span>';
            echo '<span class="date">' . dateFormat($comment['cdate'], 'd.m.Y. | h:i') . '</span>';
            echo '<div class="message">' . $comment['message'] . '</div>';
          echo '</div>';
          echo '<div class="clearfix"></div>';
          echo '<div class="comment-reply-wrapper">';
            echo '<button type="button" class="comment-reply" data-target_id="' . $item->id . '" data-parent_id="' . $comment['id'] . '" data-type_id="' . Conf::get('comment_type_id')['article'] . '">' . Trans::get('Reply') . '</button>';
          echo '</div>';

          if (@exists($comment['children']) && count($comment['children']) > 0) {
            $this->renderComments($comment['children'], $item, true);
          }

        echo '</div>';
      }
    }
  }


  public function renderCommentsForm($id, $type) {

    echo '<div class="form-wrapper comments-form-wrapper" id="pageCommentForm">';

      echo '<form class="add-comment-form" data-type="comment">';

        echo '<input type="hidden" name="target_id" class="comment-target_id" value="' . $id . '" />';
        echo '<input type="hidden" name="parent_id" class="comment-parent_id" value="0" />';
        echo '<input type="hidden" name="type_id" class="comment-type_id" value="' . $type . '" />';

        echo '<h3>' . Trans::get('Leave a Reply') . ':</h3>';
        echo '<p>' . Trans::get('Your email address will not be published') . '</p>';

        echo '<label>' . Trans::get('Name') . ':</label>';
        echo '<input type="text" name="name" class="form-field required comment-name" required="required" />';

        echo '<label>' . Trans::get('E-mail') . ':</label>';
        echo '<input type="email" name="email" class="form-field required comment-email" required="required" />';

        echo '<label>' . Trans::get('Comment') . ':</label>';
        echo '<textarea name="message" class="form-field required comment-message" required="required"></textarea>';

        echo '<button type="submit">' . Trans::get('Post comment') . '</button>';

      echo '</form>';

    echo '</div>';
  }

  /********************************************** PAGINATION **********************************************/

  private function buildPaginationUrl($data) {

    if (@exists($data['alias'])) {
      unset($data['alias']);
    }

    $queryString = '';

    foreach ($data as $key => $value) {

      if ((string)$key !== 'page' && (string)$key !== 'total' && (string)$key !== 'items_per_page' && (string)$key !== 'url') {

        $queryString .= '&' . $key . '=' . $value;
      }
    }

    $url = $data['url'];

    if (@exists($data['items_per_page'])) {
      $url .= '?items_per_page=' . $data['items_per_page'];
    } else {
      $queryString = trim($queryString, '&');
      $url .= '?';
    }

    if (@exists($data['items_per_page']) && (string)$queryString === '') {
      $url .= '&';
    } else if ((string)$queryString !== '') {
      $url .= $queryString . '&';
    }

    $url .= 'page=';

    return $url;
  }


  public function renderPagination($data) {

    $total = $data['total'];
    $itemsPerPage = $data['items_per_page'];

    if ($total <= $itemsPerPage) return;

    $page = @exists($data['page']) ? $data['page'] : 1;

    $totalPagination = ceil((int)$total / (int)$itemsPerPage);

    $from = 1;
    if ((int)$page !== 0 || (int)$page !== 1) {
      $from = ((int)$page * (int)$itemsPerPage - (int)$itemsPerPage) + 1;
    }

    $to = (int)$page * (int)$itemsPerPage;
    if ($to > $total) $to = $total;

    $prevPage = 1;
    $prevPageDisabledClass = ' disabled';
    if ($page > 1) {
      $prevPage = (int)$page - 1;
      $prevPageDisabledClass = '';
    }

    $nextPage = $totalPagination;
    $nextPageDisabledClass = ' disabled';
    if ($page < $totalPagination) {
      $nextPage = (int)$page + 1;
      $nextPageDisabledClass = '';
    }


    $minButtonsForDisplay = 6;
    $url = $this->buildPaginationUrl($data);

    $prevPageUrl = (int)$page !== 1 ? $url . $prevPage : '#';
    $nextPageUrl = (int)$page !== (int)$totalPagination ? $url . $nextPage : '#';

    echo '<div class="nc-pagination-wrapper pagination-wrapper">';

      echo '<div class="nc-pagination">';
        echo '<a href="' . $prevPageUrl . '" class="paginate-btn previous' . $prevPageDisabledClass . '"><i class="fas fa-chevron-left"></i></a>';

        if ($totalPagination <= $minButtonsForDisplay) {

          for ($i = 1; $i <= $totalPagination; $i++) {

            $activeClass = (int)$i === (int)$page ? ' active' : '';
            echo '<a href="' . $url . $i . '" class="paginate-btn' . $activeClass . '">' . $i . '</a>';
          }
        } else {

          if ($page < $minButtonsForDisplay) {

            for ($i = 1; $i <= $minButtonsForDisplay; $i++) {

              $activeClass = (int)$i === (int)$page ? ' active' : '';
              echo '<a href="' . $url . $i . '" class="paginate-btn' . $activeClass . '">' . $i . '</a>';
            }
            echo '<a href="#" class="paginate-btn dots">...</a>';
            echo '<a href="' . $url . $totalPagination . '" class="paginate-btn">' . $totalPagination . '</a>';
          } else {

            if ((int)$page === (int)$totalPagination) {
              echo '<a href="' . $url . '1" class="paginate-btn">1</a>';
              echo '<a href="#" class="paginate-btn dots">...</a>';

              for ($i = ((int)$totalPagination - 2); $i <= $totalPagination; $i++) {

                $activeClass = (int)$i === (int)$page ? ' active' : '';
                echo '<a href="' . $url . $i . '" class="paginate-btn' . $activeClass . '">' . $i . '</a>';
              }
            } else if ((int)$page === ((int)$totalPagination - 1)) {
              echo '<a href="' . $url . '1" class="paginate-btn">1</a>';
              echo '<a href="#" class="paginate-btn dots">...</a>';
              echo '<a href="' . $url . $prevPage . '" class="paginate-btn">' . $prevPage . '</a>';
              echo '<a href="' . $url . $page . '" class="paginate-btn active">' . $page . '</a>';
              echo '<a href="' . $url . $nextPage . '" class="paginate-btn">' . $nextPage . '</a>';
            } else {
              echo '<a href="' . $url . '1" class="paginate-btn">1</a>';
              echo '<a href="#" class="paginate-btn dots">...</a>';
              echo '<a href="' . $url . $prevPage . '" class="paginate-btn">' . $prevPage . '</a>';
              echo '<a href="' . $url . $page . '" class="paginate-btn active">' . $page . '</a>';
              echo '<a href="' . $url . $nextPage . '" class="paginate-btn">' . $nextPage . '</a>';
              echo '<a href="#" class="paginate-btn dots">...</a>';
              echo '<a href="' . $url . $totalPagination . '" class="paginate-btn">' . $totalPagination . '</a>';
            }
          }
        }

        echo '<a href="' . $nextPageUrl . '" class="paginate-btn next' . $nextPageDisabledClass . '"><i class="fas fa-chevron-right"></i></a>';
      echo '</div>';

      echo '<div class="nc-pagination-info">' . Trans::get('Show') . ' ' . $from . ' ' . Trans::get('to') . ' ' . $to . ' ' . Trans::get('from') . ' ' . Trans::get('total') . ' ' . $total . ' ' . Trans::get('elements') . '</div>';

    echo '</div>';
  }


  // array expected as argument - array('10', '20', '40')
  public function renderItemsPerPageSelect($selectedValue, $data = null) {

    echo '<select class="nc-pagination-items-per-page">';

      if (!@exists($data)) {

        $selected1 = (int)$selectedValue === 12 ? 'selected="selected"' : '';
        $selected2 = (int)$selectedValue === 24 ? 'selected="selected"' : '';
        $selected3 = (int)$selectedValue === 48 ? 'selected="selected"' : '';

        echo '<option value="12"' . $selected1 . '>12</option>';
        echo '<option value="24"' . $selected2 . '>24</option>';
        echo '<option value="48"' . $selected3 . '>48</option>';
      } else {

        if (!is_array($data)) $data = array();

        if (!empty($data)) {
          foreach ($data as $option) {
            $selected = (int)$option == (int)$selectedValue ? 'selected="selected"' : "";
            echo '<option value="' . $option . '"' . $selected . '>' . $option . '</option>';
          }
        }
      }

    echo '</select>';
  }


  /********************************************** SHOP **********************************************/


  protected function renderProductInList($product) {

    if (@exists($product)) {

      $image = $this->setMediaImageUrl(array('image' => $product->image));

      echo '<div class="product">';
        echo '<a href="' . $product->url . '" class="more">';
          echo '<div class="image-wrapper">';
            echo '<img src="' . $image . '" alt="' . $product->image . '">';
          echo '</div>';
        echo '</a>';
        echo '<div class="caption">';
          if (@exists($product->title)) echo '<div class="product-title">' . $product->title . '</div>';
          if (@exists($product->code)) echo '<div class="product-code">' . $product->code . '</div>';
          if (@exists($product->vendor_name)) echo '<div class="product-vendor">' . $product->vendor_name . '</div>';
          $this->renderProductPrice($product);
        echo '</div>';
        echo '<div class="action-buttons">';
          $this->renderAddToCartButton($product->id);
          $this->renderLinkButton($product->url);
          $this->renderQuickViewButton($product->id);
        echo '</div>';
        $this->renderQuickViewPopup($product);
      echo '</div>';
    }
  }


  protected function renderQuickViewPopup($product) {

    if (@exists($product)) {

      echo '<div class="product-quick-view-popup" id="productQuickViewPopup-' . $product->id . '" style="display:none;">';

        echo '<div class="product-quick-view-popup-inner">';

          $image = $this->setMediaImageUrl(array("image" => $product->image));

          echo '<div class="image-wrapper product-image">';
            echo '<img src="' . $image . '" alt="product-image" />';
          echo '</div>';

          echo '<div class="caption">';
            if (@exists($product->parent_name)) echo '<div class="product-parent">' . $product->parent_name . '</div>';
            if (@exists($product->title)) echo '<div class="product-title">' . $product->title . '</div>';
            if (@exists($product->code)) echo '<div class="product-code">' . Trans::get('Code') . ': ' . $product->code . '</div>';
            if (@exists($product->vendor_name)) echo '<div class="product-vendor">' . Trans::get('Vendor') . ': ' . $product->vendor_name . '</div>';
            echo '<p class="price product-price">' . formatPrice(PricesCalculationService::getProductPrice($product)) . '</p>';
            echo '<div class="action-buttons">';
              $this->renderAddToCartButton($product->id);
              $this->renderLinkButton($product->url);
            echo '</div>';
          echo '</div>';
          $this->renderPageContent($product);
        echo '</div>';


      echo '</div>';
    }
  }


  protected function renderProductPrice($product) {

    if (@exists($product)) {

      $productPrice = PricesCalculationService::getProductPrice($product);

      if (PricesCalculationService::discountExists($product)) {

        $priceWithoutDiscount = PricesCalculationService::getPriceWithoutDiscount($product);

        echo '<span class="price product-price-without-discount">' . formatPrice($priceWithoutDiscount) . '</span>';
        echo '<span class="price product-discount">-' . PricesCalculationService::renderDiscountValue($product) . '</span>';
        echo '<span class="price product-price">' . formatPrice($productPrice) . '</span>';
      } else echo '<p class="price product-price">' . formatPrice($productPrice) . '</p>';
    }
  }


  protected function renderAddToCartButton($productId) {

    echo '<button type="button" class="btn btn-with-tooltip cart-item-add" data-id="' . $productId . '">';
      echo '<i class="fa fa-shopping-cart cart-icon"></i>';
      echo '<i class="fa fa-spinner spinner-icon"></i>';
      $this->renderButtonTooltip(Trans::get('Add to cart'));
    echo '</button>';
  }


  protected function renderLinkButton($productUrl) {

    if (!@exists($productUrl)) {
      $productUrl = '#';
    }

    echo '<a href="' . $productUrl . '" class="btn btn-with-tooltip product-link">';
      echo '<i class="fa fa-link"></i>';
      $this->renderButtonTooltip(Trans::get('See more'));
    echo '</a>';
  }


  protected function renderQuickViewButton($productId) {

    echo '<a type="button" class="btn btn-with-tooltip product-quick-view" data-id="' . $productId . '" href="#productQuickViewPopup-' . $productId . '" data-fancybox="product-quick-view">';
      echo '<i class="fa fa-eye"></i>';
      $this->renderButtonTooltip(Trans::get('Quick view'));
    echo '</a>';
  }


  public function setCheckoutUrl() {

    $url = Conf::get('url');
    if ((string)Trans::getLanguageAlias() === 'sr') {
      $url .= '/poruci';
    } else if ((string)Trans::getLanguageAlias() === 'en') {
      $url .= '/checkout';
    }

    return $url;
  }


  public function renderProducts($products, $paginationParams) {

    if (@exists($products)) {

      echo '<div class="nc-pagination-top-filters">';
        echo '<div class="nc-pagination-items-per-page-wrapper">';
          echo '<label>' . Trans::get('Products per page') . ':</label>';
          $this->renderItemsPerPageSelect($paginationParams['items_per_page']);
        echo '</div>';
        $this->renderPagination($paginationParams);
      echo '</div>';

      echo '<div class="products-list">';

        foreach ($products as $product) {

          $this->renderProductInList($product);
        }

      echo '</div>';

      $this->renderPagination($paginationParams);
    }
  }


  /******************************************* ARTICLES ********************************************/

  public function renderArticle($article) {

    if (@exists($article)) {

      $image = $this->setMediaImageUrl(array('image' => $article->image));

      echo '<div class="article">';

        echo '<a href="' . $article->url . '" class="more">';
          echo '<div class="image-wrapper">';
            echo '<img src="' . $image . '" alt="' . $article->image . '">';
          echo '</div>';
        echo '</a>';

        echo '<div class="content">';
          if (@exists($article->title)) echo '<h4 class="title">' . $article->title . '</h4>';
          if (@exists($article->intro_text)) echo '<p class="intro-text">' . $article->intro_text . '</p>';
          echo '<a href="' . $article->url . '" class="more">' . Trans::get('Read more') . '</a>';
        echo '</div>';

      echo '</div>';
    }
  }
}

?>