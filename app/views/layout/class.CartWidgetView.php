<?php

class CartWidgetView extends MainView {

  public $order = array();
  public $items = null;

  public function __construct($order) {
    parent::__construct();

    if (@exists($order)) {
      $this->order = $order;
      if (@exists($this->order->items)) {
        $this->items = json_decode($this->order->items);
      }
    }
  }


  public function displayCartWidget() {

    echo '<div id="cartWidget">';
      $this->renderCartWidgetToggle();
      echo '<div id="cartWidgetDropdown">';
        $this->renderCartWidgetDropdown();
      echo '</div>';
    echo '</div>';
  }


  public function renderEmptyCartMessage() {

    $activeClass = $this->setEmptyCartMessageActiveClass();

    echo '<div class="cart-empty-message' . $activeClass . '">' . Trans::get('Your shopping cart is empty') . '!</div>';
  }


  private function renderCartWidgetDropdown() {

    $this->renderCartWidgetItems();
    $this->renderCartWidgetPrices();
    $this->renderCartWidgetActionButtons();
    $this->renderEmptyCartMessage();
  }


  private function renderCartWidgetToggle() {
    echo '<button type="button" id="cartWidgetToggle">';
      echo '<img src="' . Conf::get('css_img_url') . '/korpa.png" alt="korpa"/>';
      //echo '<span class="price cart-widget-total-price">' . $this->renderOrderPrice() . '</span>';
      echo '<span class="quantity">(<span class="cart-widget-quantity">' . $this->renderOrderQuantity() . '</span>)</span>';
    echo '</button>';
  }


  private function renderCartWidgetItems() {

    $activeClass = $this->setDropdownElementsActiveClass();

    echo '<div class="cart-widget-dropdown-element' . $activeClass . '" id="cartWidgetItems">';

      if (@exists($this->order) && @exists($this->items)) {

        foreach ($this->items as $item) {

          echo '<div class="cart-widget-item clearfix cw-item-' . $item->id . '" data-id="' . $item->id . '">';

            echo '<button type="button" class="cart-item-remove" data-id="' . $item->id . '"><i class="fa fa-times"></i></button>';

            echo '<div class="image-wrapper">';
              echo '<img src="' . $this->setMediaImageUrl(array('image' => $item->image, 'thumb' => true)) . '" alt="' . $item->image . '" />';
            echo '</div>';

            echo '<div class="caption">';
              echo '<p class="title">' . $item->title . '</p>';
              echo '<p class="code">' . $item->code . '</p>';
              echo '<p class="price">';
              echo '<i class="fa fa-times"></i>';
              echo '<span class="quantity">' . $item->quantity . '</span>';
              echo formatPrice($item->total_price);
              echo '</p>';
            echo '</div>';

          echo '</div>';
        }
      }

    echo '</div>';
  }


  private function renderCartWidgetPrices() {

    $activeClass = $this->setDropdownElementsActiveClass();

    echo '<div class="cart-widget-prices cart-widget-dropdown-element' . $activeClass . '">';
      echo '<p class="vat"><span class="title">' . Trans::get('Vat') . ':</span><span class="cart-widget-total-vat">' . $this->renderOrderVatPrice() . '</span></p>';
      echo '<p class="discount"><span class="title">' . Trans::get('Savings') . ':</span><span class="cart-widget-total-discount">' . $this->renderOrderDiscountPrice() . '</span></p>';
      echo '<p class="price"><span class="title">' . Trans::get('Total') . ':</span><span class="cart-widget-total-price">' . $this->renderOrderPrice() . '</span></p>';
    echo '</div>';
  }


  private function renderCartWidgetActionButtons() {

    $checkoutUrl = $this->setCheckoutUrl();

    $activeClass = $this->setDropdownElementsActiveClass();

    echo '<div class="cart-widget-action-buttons clearfix cart-widget-dropdown-element' . $activeClass . '">';
      echo '<a href="' . Conf::get('url') . '/' . strtolower(Trans::get('Cart')) . '">' . Trans::get('Cart') . '</a>';
      echo '<a href="' . $checkoutUrl . '" class="btn cart-order">' . Trans::get('Checkout') . '</a>';
    echo '</div>';
  }


  private function renderOrderPrice() {
    return @exists($this->order) && @exists($this->order->order_price) ? formatPrice($this->order->order_price) : formatPrice(0);
  }


  private function renderOrderDiscountPrice() {
    return @exists($this->order) && @exists($this->order->order_discount) ? formatPrice($this->order->order_discount) : formatPrice(0);
  }


  private function renderOrderVatPrice() {
    return @exists($this->order) && @exists($this->order->order_vat) ? formatPrice($this->order->order_vat) : formatPrice(0);
  }


  private function renderOrderQuantity() {
    return @exists($this->order) && $this->order != false ? $this->order->quantity : 0;
  }


  private function setDropdownElementsActiveClass() {
    return @exists($this->order) && @exists($this->items) ? ' active' : '';
  }


  private function setEmptyCartMessageActiveClass() {
    return @exists($this->order) && @exists($this->items) ? '' : ' active';
  }
}

?>