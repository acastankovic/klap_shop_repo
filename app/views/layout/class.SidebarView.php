<?php

class SidebarView extends MainView {

  private $data;

  public function __construct($data) {
    parent::__construct();

    if (@exists($data)) {
      $this->data = $data;
    }
  }


  public function displaySidebar() {

    $catalog = $this->data[0]['children'];

    $this->renderSidebarOpener();

    echo '<div class="sidebar-navigation" id="sidebarNavigation">';
      $this->renderSidebarCloser();
      Dispatcher::instance()->dispatch('normacore', 'layout', 'mainMenu', null, Request::HTML_REQUEST);
      echo '<h2 class="title">' . Trans::get('Catalog') . '</h2>';
      echo '<div class="sidebar-shop" id="sidebarShop">';
        $this->displayTree($catalog);
      echo '</div>';
    echo '</div>';
  }


  public function renderSidebarOpener() {
    echo '<div class="sidebar-opener-wrapper">';
      echo '<button type="button" id="sidebarOpener">';
        echo '<span class="bar"></span>';
        echo '<span class="bar"></span>';
        echo '<span class="bar"></span>';
      echo '</button>';
    echo '</div>';
  }


  public function renderSidebarCloser() {
    echo '<div class="sidebar-closer-wrapper">';
      echo '<button type="button" id="sidebarCloser">';
        echo '<span class="bar"></span>';
        echo '<span class="bar"></span>';
        echo '<span class="bar"></span>';
      echo '</button>';
    echo '</div>';
  }


  public function displayTree($items, $child = false) {

    if ($child) echo '<ul class="sub-category clearfix">';
    else echo '<ul class="category-list clearfix">';

    if (@exists($items)) {

      foreach ($items as $item) {

        if (@exists($item['children'])) {
          $class = 'category-item';
          $categoryElement = '<a href="' . $item['url'] . '" data-id="' . $item['id'] . '" class="category-link link">' . $item['name'] . '</a>';
          $categoryElement .= '<a href="#" class="opener">';
          $categoryElement .= '<i class="fas fa-chevron-down element-center"></i>';
          $categoryElement .= '</a>';
        } else {
          $class = 'last-category-item';
          $categoryElement = '<a href="' . $item['url'] . '" data-id="' . $item['id'] . '" class="product-link link"><span>' . $item['name'] . '</span></a>';
        }

        if (@exists($item['published']) && $item['published'] == 1) {
          echo '<li class="' . $class . ' clearfix">';
          echo $categoryElement;
          if (@exists($item['children'])) $this->displayTree($item['children'], true);
          echo '</li>';
        }
      }
    }

    echo '</ul>';
  }

}

?>