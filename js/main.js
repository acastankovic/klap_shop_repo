$(document).ready(
  function () {
    setPageWrapper();
  }
);

/******************************************* BACK TO TOP *******************************************/

$(window).scroll(
  function () {
    if ($(this).scrollTop() > 200) {
      $('#backTop').fadeIn();
    }
    else {
      $('#backTop').fadeOut();
    }
  }
);

$(document).on('click', '#backTop',
  function () {
    $('html, body').animate({scrollTop: 0}, 800);
    return false;
  }
);


/*************************************** /end of back to top ***************************************/


/********************************************** LOADER *********************************************/

$(window).on('load',
  function () {
    $('#loader').fadeOut();
    $('#overlay').fadeOut();
  }
);

/****************************************** /end of loader *****************************************/


/******************************************* PAGE WRAPPER ******************************************/

function setPageWrapper() {

  appendWrapperStart();
  wrapPage();
  setPageWrapperHeight();
};


function appendWrapperStart() {

  $('body').prepend('<div class="page-wrapper-start"></div>');

  if ($('.page-wrapper-start').length == 0) {

    setTimeout(function () {
      appendWrapperStart();
    }, 200);
  }
};


function wrapPage() {

  if ($('.page-wrapper-start').length != 0) $('.page-wrapper-start').nextUntil('footer').wrapAll('<div id="pageWrapper"></div>');

  if ($('#pageWrapper').length == 0) {

    setTimeout(function () {
      wrapPage();
    }, 200);
  }
};


function setPageWrapperHeight() {

  if ($('footer').length != 0) {

    var height = $('footer').height();
    var marginTop = parseFloat($('footer').css('margin-top'));
    var marginBottom = parseFloat($('footer').css('margin-top'));
    var paddingTop = parseFloat($('footer').css('padding-top'));
    var paddingBottom = parseFloat($('footer').css('padding-bottom'));

    var footerHeight = height + marginBottom + paddingTop + paddingBottom;

    $('#pageWrapper').attr('style', 'min-height: calc(100vh - ' + footerHeight + 'px)');
  }
};


/*************************************** /end of page wrapper **************************************/


/********************************************* LANGUAGES ********************************************/

$('.set-language').on('click',
  function (event) {
    eventPreventDefault(event);

    if ($(this).hasClass('active')) return;

    var langId = $(this).attr('data-id');
    var route = getUrlRoute();
    var dataString = 'lang_id=' + langId;

    if ($('#langGroupId').length != 0) {
      var langGroupId = $('#langGroupId').val();
      dataString += '&lang_group_id=' + langGroupId;
    }

    if (exists(route)) {
      dataString += '&route=' + route;
    }

    ajaxCall(
      '/languages-set/',
      'POST',
      dataString,
      function (response) {

        if (ajaxSuccess(response)) {

          // console.log(response.data);

          var url = response.data;

          location.href = url;
        } else defaultErrorHandler(response);
      }
    );
  }
);

/**************************************** /end of languages ***************************************/


/*********************************************** SEARCH **********************************************/

$('#openSearchForm').on('click',
  function (event) {
    eventPreventDefault(event);

    $('#searchOverlay').fadeIn();
  }
);


$('#closeSearchForm').on('click',
  function (event) {
    eventPreventDefault(event);

    $('#searchOverlay').fadeOut();
  }
);

/****************************************** /end of search *****************************************/


/******************************************** NEWSLETTER *******************************************/

$('#newsletterForm').on('submit',
  function (event) {
    eventPreventDefault(event);

    var validated = validateRequiredFieldsWithWarningMessage('#newsletterForm');

    if (!validated) return;

    var self = this;

    ajaxCall(
      '/newsletter-signup',
      'post',
      $(self).serialize(),
      function (response) {
        if (ajaxSuccess(response)) {

          console.log(response);

          var success = response.data.success;
          var message = response.data.message;

          alert(message);

          if (success) {
            location.reload();
          }

        } else defaultErrorHandler(response);
      }
    );
  }
);


/**************************************** /end of newsletter ***************************************/


/********************************************** OTHER *********************************************/

(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));

/****************************************** /end of other *****************************************/