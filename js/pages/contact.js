$(document).ready(
  function () {
    setFormFieldsPlaceholderAttribute('#contactForm');
  }
);

$('#contactForm').on('submit',
  function (event) {
    eventPreventDefault(event);

    var validated = validateRequiredFieldsWithWarningMessage('#contactForm');

    if (!validated) return;

    var self = this;

    ajaxCall(
      '/send-email',
      'post',
      $(self).serialize(),
      function (response) {
        if (ajaxSuccess(response)) {

          var success = response.data.success;
          var message = response.data.message;

          alert(message);

          if (success) {
            location.reload();
          }
        }
        else defaultErrorHandler(response);
      }
    );
  }
);

$('.form-btn-clear').on('click',
  function (event) {
    eventPreventDefault(event);

    clearFormFields(this);
  }
);