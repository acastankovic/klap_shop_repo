/******************************* DOM ELEMENTS *******************************/

var $cartEmptyMessage = $('.cart-empty-message');


/*** CART TABLE ***/

var $cartTableWrapper = $('#cartTableWrapper');
var $cartTable = $('#cartTable');
var $cartTotalDiscount = $('.cart-total-discount');
var $cartTotalVat = $('.cart-total-vat');
var $cartTotalPrice = $('.cart-total-price');


/*** CART WIDGET ***/

var $cartWidgetToggle = $('#cartWidgetToggle');
var $cartWidgetDropdown = $('#cartWidgetDropdown');
var $cartWidgetItems = $('#cartWidgetItems');
var $cartWidgetTotalDiscount = $('.cart-widget-total-discount');
var $cartWidgetTotalVat = $('.cart-widget-total-vat');
var $cartWidgetTotalPrice = $('.cart-widget-total-price');
var $cartWidgetQuantity = $('.cart-widget-quantity');
var $cartWidgetDropdownElement = $('.cart-widget-dropdown-element');


/*** PRODUCT PAGE ***/

var $itemImage = $('#itemImage');
var $itemQuantity = $('#itemQuantity');


/*** CALL TO ACTION BUTTONS ***/

var removeItemBtnClass = '.cart-item-remove';
var $addItemToCartBtn = $('.cart-item-add');
var $increaseItemQtyBtn = $('.cart-item-increase-qty');
var $decreaseItemQtyBtn = $('.cart-item-decrease-qty');
var $changeItemQtyBtn = $('.cart-item-change-qty');
var $emptyCartBtn = $('.cart-empty');
var $placeOrderBtn = $('#placeOrder');


/************************ GET DOM ELEMENTS BY ITEM ID ************************/

function cartItemElement(id) {
  return $cartTable.find('.c-item-' + id);
};


function cartWidgetItemElement(id) {
  return $cartWidgetItems.find('.cw-item-' + id);
};


function cartItemQtyElement(id) {
  var $item = cartItemElement(id);
  return $item.find('.cart-item-change-qty');
};


function cartItemTotalPriceElement(id) {
  var $item = cartItemElement(id);
  return $item.find('.cart-item-total-price');
};


function cartWidgetItemQtyElement(id) {
  var $item = cartWidgetItemElement(id);
  return $item.find('.quantity');
};


/******************************* EVENT HANDLERS *******************************/


$addItemToCartBtn.on('click', function (event) {
  eventPreventDefault(event);
  addItemToCart(this);
});


$increaseItemQtyBtn.on('click', function () {
  increaseItemQty(this);
});


$decreaseItemQtyBtn.on('click', function (event) {
  eventPreventDefault(event);
  decreaseItemQty(this);
});


$changeItemQtyBtn.on('change', function (event) {
  eventPreventDefault(event);
  changeItemQty(this);
});


$emptyCartBtn.on('click', function (event) {
  eventPreventDefault(event);
  emptyCart(this);
});


$placeOrderBtn.on('submit', function (event) {
  eventPreventDefault(event);
  placeOrder(this);
});


$cartWidgetToggle.on('click', function (event) {
  eventPreventDefault(event);
  event.stopPropagation();

  $cartWidgetDropdown.fadeIn();
});


// remove item - cart widget
$cartWidgetDropdown.on('click', function (event) {
  event.stopPropagation();

  if ($(event.target).is(removeItemBtnClass)) {
    removeItemFromCart(event.target);
  }

  if ($(event.target).is('i')) {
    var $btn = $(event.target).parent('.cart-item-remove');
    removeItemFromCart($btn);
  }
});


// remove item - cart page
$(document).on('click', removeItemBtnClass, function () {
  eventPreventDefault(event);
  removeItemFromCart(this);
});


$(document).on('click', function () {
  $cartWidgetDropdown.fadeOut();
});


$(document).ready(function() {

  $itemQuantity.inputFilter(function(value) {
    return validatePositiveNumber(value);
  });

  $changeItemQtyBtn.inputFilter(function(value) {
    return validatePositiveNumber(value);
  });
});

/******************************* AJAX ACTIONS *******************************/


function addItemToCart(elem) {

  if ($(elem).hasClass('disabled')) return;

  disableAddToCart(elem);

  var productId = $(elem).data('id');

  var $image;
  var dataString = 'product_id=' + productId;
  if (productPage()) {
    $image = $itemImage;
    var quantity = $itemQuantity.val();
    dataString += '&quantity=' + quantity;
  } else {
    $image = $(elem).parents('.product').find('.product-image');
  }

  var self = elem;

  ajaxCall(
    '/shop/orders/add',
    'POST',
    dataString,
    function (response) {

      animateItemAdding(self, $image, $cartWidgetToggle);

      handleCartChanges(response.data);

      enableAddToCart(self);
    }
  );
};


function increaseItemQty(elem) {

  var productId = $(elem).attr('data-id');

  ajaxCall(
    '/shop/orders/increase-qty',
    'POST',
    'product_id=' + productId,
    function (response) {

      handleCartChanges(response.data);
    }
  );
};


function decreaseItemQty(elem) {

  var productId = $(elem).attr('data-id');

  ajaxCall(
    '/shop/orders/decrease-qty',
    'POST',
    'product_id=' + productId,
    function (response) {

      handleCartChanges(response.data);
    }
  );
};


function changeItemQty(elem) {

  var productId = $(elem).attr('data-id');
  var quantity = $(elem).val();

  ajaxCall(
    '/shop/orders/change-qty',
    'POST',
    'product_id=' + productId + '&quantity=' + quantity,
    function (response) {

      handleCartChanges(response.data);
    }
  );
};


function removeItemFromCart(elem) {

  var productId = $(elem).attr('data-id');

  ajaxCall(
    '/shop/orders/remove',
    'POST',
    'product_id=' + productId,
    function (response) {

      handleCartItemRemoval(response.data);
    }
  );
};


function emptyCart() {

  ajaxCall(
    '/shop/orders/empty',
    'POST',
    null,
    function (response) {

      handleEmptyCartChanges(response.data);
    }
  );
};


function placeOrder() {

  var validated = validateRequiredFieldsWithWarningMessage('#placeOrder');

  if (!validated) return;

  ajaxCall(
    '/shop/orders/place-order',
    'POST',
    $('#placeOrder').serialize(),
    function (response) {

      var success = response.data.success;
      var message = response.data.message;

      alert(message);
      if (success) {
        backToHome(300);
      }
    }
  );
};


/********************************** OTHER **********************************/


function handleCartChanges(order) {

  var productId = order.product_id;
  var items = order.items;

  for (var key in items) {

    var item = items[key];

    if (item.id == productId) {

      var $cwItem = cartWidgetItemElement(item.id);

      if ($cwItem.length == 0) {
        addCartItem(item);
      } else {
        displayCartItemChanges(item);
      }
    }
  }

  displayCartChanges(order);
};


function handleCartItemRemoval(order) {

  var productId = order.product_id;
  var items = order.items;

  for (var key in items) {

    var item = items[key];

    if (item.id != productId) {
      removeCartItem(productId);
    }
  }

  if (items.length == 0) {

    order = new Object();
    order.items = [];
    order.order_discount = 0;
    order.order_price = 0;
    order.order_vat = 0;
    order.product_id = 0;
    order.quantity = 0;

    handleEmptyCartChanges(order);
  }

  displayCartChanges(order);
};

function addCartItem(item) {

  var html = renderCartWidgetItem(item);
  $cartWidgetItems.append(html);

  hideEmptyCartMessage();
  showCartWidgetDropdownElements();
};


function removeCartItem(productId) {

  var $cItem = cartItemElement(productId);
  var $cwItem = cartWidgetItemElement(productId);

  $cItem.remove();
  $cwItem.remove();
};


function handleEmptyCartChanges(order) {

  // table
  $cartTableWrapper.html('');

  // widget
  hideCartWidgetDropdownElements();
  $cartWidgetItems.html('');

  showEmptyCartMessage();
  displayCartChanges(order);
};


function displayCartChanges(order) {

  displayCartTotalPrices(order);
  displayCartWidgetTotalQuantity(order);
};


function displayCartItemChanges(item) {

  displayCartItemPrice(item);
  displayCartItemQuantity(item);
};


function displayCartTotalPrices(order) {

  // table
  $cartTotalDiscount.text(formatPrice(order.order_discount));
  $cartTotalVat.text(formatPrice(order.order_vat));
  $cartTotalPrice.text(formatPrice(order.order_price));

  // widget
  $cartWidgetTotalDiscount.text(formatPrice(order.order_discount));
  $cartWidgetTotalVat.text(formatPrice(order.order_vat));
  $cartWidgetTotalPrice.text(formatPrice(order.order_price));
};


function displayCartWidgetTotalQuantity(order) {
  $cartWidgetQuantity.text(order.quantity);
};

function displayCartItemPrice(item) {

  // table
  var $cartItemTotalPriceElemen = cartItemTotalPriceElement(item.id);
  $cartItemTotalPriceElemen.text(formatPrice(item.quantity_total_price));
};


function displayCartItemQuantity(item) {

  // table
  var $cartItemQtyElement = cartItemQtyElement(item.id);
  $cartItemQtyElement.val(item.quantity);

  // widget
  var $widgetItemQtyElement = cartWidgetItemQtyElement(item.id);
  $widgetItemQtyElement.text(item.quantity);
};


function showCartWidgetDropdownElements() {
  $cartWidgetDropdownElement.addClass('active');
};


function hideCartWidgetDropdownElements() {
  $cartWidgetDropdownElement.removeClass('active');
};


function showEmptyCartMessage() {
  $cartEmptyMessage.addClass('active');
};


function hideEmptyCartMessage() {
  $cartEmptyMessage.removeClass('active');
};


function animateItemAdding(elem, image, animateTo) {

  var $image = image;
  var $animateTo = animateTo;

  var animationItem = $image.clone();
  var animationTopStart = $(elem).offset().top - $image.height() / 2;
  var animationLeftStart = $(elem).offset().left;
  var animationTopEnd = $animateTo.offset().top + 10;
  var animationLeftEnd = $animateTo.offset().left + $animateTo.width() / 2;


  $(animationItem).attr('id', 'animated_item');
  $(animationItem).css('position', 'absolute');
  $(animationItem).css('z-index', '999999');
  $(animationItem).css('top', animationTopStart);
  $(animationItem).css('left', animationLeftStart);
  $('body').append(animationItem);

  $(animationItem).animate({
      left: animationLeftEnd,
      top: animationTopEnd,
      width: 40,
      opacity: 0.5
    }, 1000, function () {
      $(animationItem).remove();
    }
  );
};


function disableAddToCart(element) {
  $(element).addClass('disabled');
};


function enableAddToCart(element) {
  $(element).removeClass('disabled');
};


/********************************** RENDER **********************************/

function renderCartWidgetItem(item) {

  var html = '';

    html += '<div class="cart-widget-item clearfix cw-item-' + item.id + '" data-id="' + item.id + '">';

      html += '<button type="button" class="cart-item-remove" data-id="' + item.id + '"><i class="fa fa-times"></i></button>';

      html += '<div class="image-wrapper">';
        html += '<img src="' + displayMediaImage(item.image) + '" alt="' + item.image + '" />';
      html += '</div>';

      html += '<div class="caption">';
        html += '<p class="title">' + item.title + '</p>';
        html += '<p class="code">' + item.code + '</p>';
        html += '<p class="price"><i class="fa fa-times"></i><span class="quantity">' + item.quantity + '</span>' + formatPrice(item.total_price) + '</p>';
      html += '</div>';

    html += '</div>';

  return html;
};


/********************************** OTHER **********************************/

function productPage() {
  return $('#productPage').length > 0;
};