$('.pagination-items-per-page').on('change',
  function (event) {
    eventPreventDefault(event);

    var value = $(this).val();

    var variables = setUrlVariables();

    if (typeof variables.items_per_page == 'undefined' || variables.items_per_page == null || variables.items_per_page == '') {
      variables.items_per_page = value;
    }

    if (typeof variables.page == 'undefined' || variables.page == null || variables.page == '') {
      variables.page = 1;
    }

    var pageBaseUrl = getUrlWithoutVariables();

    var urlString = pageBaseUrl + '?';

    var counter = 0;
    for (var key in variables) {

      if (key == 'page') {
        variables[key] = 1;
      }

      if (key == 'items_per_page') {
        variables[key] = value;
      }

      if (counter != 0) {

        urlString += '&';
      }

      urlString += key + '=' + variables[key];

      counter++;
    }

    location.href = urlString;
  }
);