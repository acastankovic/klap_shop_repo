var $sidebar = $('#sidebarNavigation');
var $sidebarShop = $('#sidebarShop');
var $opener = $('#sidebarOpener');
var $closer = $('#sidebarCloser');
var $overlay = $('#overlay');

$(document).ready(
  function () {
    setSidebarActiveNodes();
  }
);


$opener.on('click',
  function (event) {
    eventPreventDefault(event);
    showSidebar();
  }
);


$closer.on('click',
  function (event) {
    eventPreventDefault(event);
    hideSidebar();
  }
);


$sidebar.find('.opener').on('click',
  function (event) {
    eventPreventDefault(event);

    $(this).toggleClass('active');
    $(this).parent('.category-item').toggleClass('active');
    $(this).prev('.category-link').toggleClass('active');

    if ($(this).hasClass('active')) {

      $(this).next('ul').addClass('active');
      $(this).next('ul').slideDown();
    } else {

      $(this).next('ul').slideUp(
        function () {
          $(this).next('ul').removeClass('active');
        }
      );
    }
  }
);


function showSidebar() {

  $('body').addClass('no-scroll');

  $opener.addClass('active');
  $closer.addClass('active');

  $overlay.show();
  $sidebar.animate({left: 0});
};


function hideSidebar() {

  $('body').removeClass('no-scroll');

  $opener.removeClass('active');
  $closer.removeClass('active');

  $sidebar.animate({left: -400});
  $overlay.hide();
};


function setSidebarActiveNodes() {

  if (domElemExists('#categoryId')) {

    var categoryId = $('#categoryId').val();

    $sidebarShop.find('a').each(
      function () {

        var nodeCatId = $(this).data('id');

        if (categoryId == nodeCatId) {

          $(this).parents('li').addClass('active');

          // var elemPosition = $(this).position();
          //
          // $('#sidebarShop').animate({scrollTop : elemPosition.top});
          // console.log(elemPosition.top);
        }
      }
    );


    $sidebarShop.find('li.active').each(
      function () {

        $(this).find('.link').eq(0).addClass('active');
        $(this).find('.opener').eq(0).addClass('active');

        $(this).find('ul').eq(0).addClass('active');
        $(this).find('ul').eq(0).css('display', 'block');
      }
    );
  }
};